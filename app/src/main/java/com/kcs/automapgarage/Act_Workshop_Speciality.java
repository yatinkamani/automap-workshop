    package com.kcs.automapgarage;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Car_brand_model;
import com.kcs.automapgarage.Model.Fuel_Model;
import com.kcs.automapgarage.Model.Garage_Brand_Model;
import com.kcs.automapgarage.Model.Manufacture_Country_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Workshop_Speciality extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    SharedPreferences preferences;
    //    SharedPreferences.Editor editor;
    String garage_id = "", manufacture_country_id = "", service_id = "";
    LinearLayout lin_country, lin_brand, lin_fuel, lin_service;
    GridLayout lin_dynamic_country, lin_dynamic_brand, lin_dynamic_fuel, grid_dynamic_service;
    List<Garage_Brand_Model> garage_brand_models = new ArrayList<>();
    List<Car_brand_model> car_brand_models = new ArrayList<>();
    List<Fuel_Model> fuel_models = new ArrayList<>();
    List<Fuel_Model> provider_fuel = new ArrayList<>();
    List<String> brand;
    List<String> m_Country;
    Button btn_service_retry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__workshop__speciality);

        img_back = findViewById(R.id.img_back);
        lin_country = findViewById(R.id.lin_country);
        lin_brand = findViewById(R.id.lin_brand);
        lin_fuel = findViewById(R.id.lin_fuel);
        lin_service = findViewById(R.id.lin_service);
        lin_dynamic_country = findViewById(R.id.grid_dynamic_country);
        lin_dynamic_brand = findViewById(R.id.grid_dynamic_brand);
        lin_dynamic_fuel = findViewById(R.id.grid_dynamic_fule);
        grid_dynamic_service = findViewById(R.id.grid_dynamic_service);
        btn_service_retry = findViewById(R.id.btn_service_retry);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");
        manufacture_country_id = preferences.getString(getString(R.string.pref_manufacturing_country_id), "");
        service_id = preferences.getString(getString(R.string.pref_service_id), "");
        img_back.setOnClickListener(this);
        btn_service_retry.setOnClickListener(this);

        if (manufacture_country_id.equals("")) {
            lin_country.setVisibility(View.GONE);
        }

        GetGarageCarBrandModelData();
        get_garage_service();

    }

    // get selected car brand, model and fuel data and list view
    private void GetGarageCarBrandModelData() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_garage_brand_model", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("1")) {
                        JSONObject result_object = obj.getJSONObject("result");

                        if (result_object.has("brand_model")) {

                            Object o = result_object.get("brand_model");
                            if (o instanceof JSONArray) {
                                brand = new ArrayList<>();
                                garage_brand_models = new ArrayList<>();
                                m_Country = new ArrayList<>();
                                JSONArray jsonArray = result_object.getJSONArray("brand_model");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String garage_brand_model_id = object.getString("garage_brand_model_id");
                                    String garage_id = object.getString("garage_id");
                                    String country_name = check(object,"country_name");
                                    String model_id = object.getString("model_id");
                                    String brand_id = object.getString("brand_id");
                                    String created_at = object.getString("created_at");
                                    String updated_at = object.getString("updated_at");

                                    if (!brand.contains(brand_id)) {
                                        brand.add(brand_id);
                                        garage_brand_models.add(new Garage_Brand_Model(garage_brand_model_id, garage_id, model_id, brand_id, created_at, updated_at));
                                    }

                                    if (!m_Country.contains(country_name)) {
                                        m_Country.add(country_name);
                                    }
                                }
                                lin_country.setVisibility(View.VISIBLE);
                                CarCountryDynamicView(m_Country);
                                Log.e("Tag Fuel Size", garage_brand_models.size() + "");
                                Log.e("Tag M C Size", m_Country.size() + "");
                            } else {
                                lin_brand.setVisibility(View.GONE);
                            }
                        }

                        if (result_object.has("garage_fule")) {

                            Object o = result_object.get("garage_fule");

                            if (o instanceof JSONArray) {

                                JSONArray fule_array = result_object.getJSONArray("garage_fule");
                                provider_fuel = new ArrayList<>();
                                List<String> list = new ArrayList<>();
                                for (int i = 0; i < fule_array.length(); i++) {
                                    JSONObject object = fule_array.getJSONObject(i);
                                    Fuel_Model fuel_model = new Fuel_Model();
                                    if (!list.contains(object.getString("fule_id"))) {
                                        fuel_model.setFuel_id(object.getString("fule_id"));
                                        provider_fuel.add(fuel_model);
                                        list.add(object.getString("fule_id"));
                                    }
                                }
                                Log.e("Tag Provider Size", provider_fuel.size() + "");
                            } else {
                                lin_fuel.setVisibility(View.GONE);
                            }
                        }
                        retrieveJSON();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag Error", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("params", " " + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    // get selected service view
    private void get_garage_service() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_service_provider_facility", new Response.Listener<String>() {
            @SuppressLint({"Long_Log_Tag", "LongLogTag"})
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("garage_facility", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("status").equals("true")) {

                        JSONArray jsonArray = obj.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String ser_id = object.getString("service_id");
                            String service_provider = object.getString("service_provider");
                            String service_name = check(object,"service_name");
                            String service_image = object.getString("service_image");
                            String created_at = object.getString("created_at");
                            String updated_at = object.getString("updated_at");

                            if (ser_id.equals(service_id)) {
                                btn_service_retry.setVisibility(View.GONE);
                                ServiceView(service_name);
                                break;
                            }

                        }

                    } else {
                        btn_service_retry.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    btn_service_retry.setVisibility(View.VISIBLE);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(Act_Garage_Car_Service.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.e("My error", "" + error);
                progressDialog.dismiss();
                btn_service_retry.setVisibility(View.VISIBLE);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    // get all brand, model, fuel and manufacture country data and display value
    private void retrieveJSON() {
        final ProgressDialog dialog = new ProgressDialog(Act_Workshop_Speciality.this);
        dialog.setMessage(getString(R.string.loading));
        if (!isFinishing()){
            dialog.show();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "car_detail", new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        car_brand_models = new ArrayList<>();
                        JSONObject object_result = obj.getJSONObject("result");

                        JSONArray carBrand = object_result.getJSONArray("car_brand");
                        for (int i = 0; i < carBrand.length(); i++) {

                            Car_brand_model car_brand_model = new Car_brand_model();
                            JSONObject c_brand = carBrand.getJSONObject(i);
                            car_brand_model.setBrand_id(c_brand.getString("brand_id"));
                            car_brand_model.setBrand_nm(check(c_brand,"brand_name"));
                            car_brand_model.setManufacture_country_id(c_brand.getString("manufacture_country_id"));
                            car_brand_model.setCreated_at(c_brand.getString("created_at"));
                            car_brand_model.setUpdated_at(c_brand.getString("updated_at"));
                            car_brand_model.setIsChecked("0");

                            for (int j = 0; j < garage_brand_models.size(); j++) {
                                Log.e("Tag Provider Brand", garage_brand_models.get(j).getBrand_id() + "  Default " + car_brand_model.getBrand_id());
                                if (garage_brand_models.get(j).getBrand_id().equals(car_brand_model.getBrand_id())) {
                                    car_brand_models.add(car_brand_model);
                                }
                            }
                        }

                        if (car_brand_models != null && car_brand_models.size() > 0) {
                            CarBrandDynamicView(car_brand_models);
                            lin_brand.setVisibility(View.VISIBLE);
                        } else {
                            lin_brand.setVisibility(View.GONE);
                        }

                        JSONArray fuel_data = object_result.getJSONArray("car_fule");
                        for (int i = 0; i < fuel_data.length(); i++) {

                            JSONObject c_model = fuel_data.getJSONObject(i);
                            Fuel_Model fuel_model = new Fuel_Model();
                            fuel_model.setFuel_id(c_model.getString("fule_id"));
                            fuel_model.setFuel_type(check(c_model,"fule_type"));
                            fuel_model.setIsChecked("0");

                            for (int j = 0; j < provider_fuel.size(); j++) {
                                Log.e("Tag Provider Fuel", provider_fuel.get(j).getFuel_id() + "  Default " + fuel_model.getFuel_id());
                                if (provider_fuel.get(j).getFuel_id().equals(fuel_model.getFuel_id())) {
                                    fuel_models.add(fuel_model);
                                }
                            }
                        }

                        if (fuel_models != null && fuel_models.size() > 0) {
                            CarFuelDynamicView(fuel_models);
                            lin_fuel.setVisibility(View.VISIBLE);
                        } else {
                            lin_fuel.setVisibility(View.GONE);
                        }

                        JSONArray m_country = object_result.getJSONArray("manufacture_country");
                        for (int i = 0; i < m_country.length(); i++) {
                            JSONObject object = m_country.getJSONObject(i);
                            Manufacture_Country_Model model = new Manufacture_Country_Model();
                            model.setMc_id(object.getString("manufacture_country_id"));
                            model.setMc_name(check(object,"country_name"));
                            model.setIsChecked("0");

                            /*if (m_Country != null && m_Country.size() > 0) {
                                lin_country.setVisibility(View.VISIBLE);
                                CarCountryDynamicView(m_Country);
                            } else {

                                if (manufacture_country_id.equals(object.getString("manufacture_country_id"))) {
                                    lin_country.setVisibility(View.VISIBLE);
                                    m_Country = new ArrayList<>();
                                    m_Country.add(object.getString("country_name"));
                                    CarCountryDynamicView(m_Country);
                                }
                            }*/
//                            manufacture_country_models.add(model);
                        }
                    }

                    if (!isFinishing()){
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!isFinishing()){
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Tag Error", error.toString());
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                return map;

            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    // listing car brand value
    public void CarBrandDynamicView(List<Car_brand_model> models) {

        lin_dynamic_brand.removeAllViews();

        int row = 1;
        if (models != null && models.size() > 3 && (models.size() / 3) > 0) {
            row = Math.round(models.size() / 3);
            Log.e("Tag Result", "" + row);
            Log.e("Tag Result", "" + models.size() / 3);
        }
        Log.e("Tag Result", "" + models.size());
        lin_dynamic_brand.setRowCount(row);
        lin_dynamic_brand.setColumnCount(3);

        if (models.size() > 0) {

            lin_brand.setVisibility(View.VISIBLE);
            for (int i = 0; i < models.size(); i++) {

                Log.e("Tag Brand", models.get(i).getBrand_nm());
                TextView textView = new TextView(this);
                textView.setTextColor(Color.WHITE);

                StringBuilder s = new StringBuilder();
                s.append(i+1);
                s.append(" )");
                s.append(models.get(i).getBrand_nm().toString().trim().trim());
//                textView.setText(Html.fromHtml(s.toString()));
                textView.setText(" " + (i + 1) + ") " + models.get(i).getBrand_nm());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.leftMargin = 20;
                textView.setPadding(5, 5, 5, 5);
                textView.setLayoutParams(params);
                textView.setSelected(true);
                textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                textView.setMarqueeRepeatLimit(2);
                textView.setSingleLine(true);
//
                GridLayout.LayoutParams param = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                        GridLayout.spec(GridLayout.UNDEFINED, 1f));
                param.width = 0;

                lin_dynamic_brand.addView(textView, param);
            }
        } else {
            lin_brand.setVisibility(View.GONE);
        }
    }

    // display name of selected country
    public void CarCountryDynamicView(List<String> country) {

        lin_dynamic_country.removeAllViews();

        int row = 1;
        if (country != null && country.size() > 3 && (country.size() / 3) > 0) {
            row = Math.round(country.size() / 3);
            Log.e("Tag Result", "" + row);
            Log.e("Tag Result", "" + country.size() / 3);
        }
        Log.e("Tag Result", "" + country.size());
        lin_dynamic_country.setRowCount(row);
        lin_dynamic_country.setColumnCount(3);

        if (country.size() > 0) {
            for (int i = 0; i < country.size(); i++) {
                lin_fuel.setVisibility(View.VISIBLE);

                TextView textView = new TextView(this);
                textView.setTextColor(Color.WHITE);
                textView.setText((i+1)+" ) "+ country.get(i));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.leftMargin = 20;
                textView.setPadding(5, 5, 5, 5);
                textView.setLayoutParams(params);

                GridLayout.LayoutParams param = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                        GridLayout.spec(GridLayout.UNDEFINED, 1f));
                param.width = 0;

                lin_dynamic_country.addView(textView, param);
            }
        } else {
            lin_fuel.setVisibility(View.GONE);
        }
    }

    // select service name viwe
    public void ServiceView(String service) {

        grid_dynamic_service.removeAllViews();

        lin_service.setVisibility(View.VISIBLE);

        TextView textView = new TextView(this);
        textView.setTextColor(Color.WHITE);
        textView.setText("" + service);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 20;
        textView.setPadding(5, 5, 5, 5);
        textView.setLayoutParams(params);

        grid_dynamic_service.addView(textView);
    }

    // selected fuel list view
    public void CarFuelDynamicView(List<Fuel_Model> models) {

        lin_dynamic_fuel.removeAllViews();

        int row = 1;
        if (models != null && models.size() > 3 && (models.size() / 3) > 0) {
            row = Math.round(models.size() / 3);
            Log.e("Tag Result", "" + row);
            Log.e("Tag Result", "" + models.size() / 3);
        }
        Log.e("Tag Result", "" + models.size());
        lin_dynamic_fuel.setRowCount(row);
        lin_dynamic_fuel.setColumnCount(3);

        if (models.size() > 0) {

            lin_fuel.setVisibility(View.VISIBLE);
            for (int i = 0; i < models.size(); i++) {

                Log.e("Tag Fuel Type", models.get(i).getFuel_type());
                TextView textView = new TextView(this);
                textView.setTextColor(Color.WHITE);
                textView.setText(" " + (i + 1) + ") " + models.get(i).getFuel_type());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.leftMargin = 20;
                textView.setPadding(5, 5, 5, 5);
                textView.setLayoutParams(params);

                GridLayout.LayoutParams param = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                        GridLayout.spec(GridLayout.UNDEFINED, 1f));
                param.width = 0;
                lin_dynamic_fuel.addView(textView, param);
            }
        } else {
            lin_fuel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (img_back == view) {
            onBackPressed();
        }else if (btn_service_retry == view){
            get_garage_service();
        }
    }

}
