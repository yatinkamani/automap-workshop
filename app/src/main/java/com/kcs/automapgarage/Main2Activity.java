package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import static com.kcs.automapgarage.Constant.Constant.API_KEY;
import static com.kcs.automapgarage.Constant.Constant.OUT_JSON;
import static com.kcs.automapgarage.Constant.Constant.PLACES_API_BASE;
import static com.kcs.automapgarage.Constant.Constant.TYPE_AUTOCOMPLETE;


public class Main2Activity extends AppCompatActivity {

    private AutoCompleteTextView acGroundSearch;
    private ArrayList<HashMap<String, String>> resultList;
    double lat, lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        acGroundSearch = (AutoCompleteTextView)findViewById(R.id.ac_ground_search);
        acGroundSearch.setThreshold(1);
        acGroundSearch.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.adapter_ac_ground_search));
        acGroundSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String placeID = resultList.get(position).get("place_id").toString();
                placeDetail(placeID);
            }
        });
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_ac_ground_search,null);
            TextView tvLoc = (TextView)convertView.findViewById(R.id.tv_loc);
            TextView tvTitle = (TextView)convertView.findViewById(R.id.tv_title);
            String totalAddress = getItem(position);
            String title = "";
            String splitAdd[] = totalAddress.split(",");
            if(splitAdd.length > 0) {
                title = splitAdd[0];
            }
            else {
                title = totalAddress;
            }
            tvTitle.setText(title);
            tvLoc.setText(totalAddress);
            return convertView;
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index).get("description");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public void placeDetail(final String input) {

        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, ArrayList<Double>> async = new AsyncTask<Void, Void, ArrayList<Double>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
            @Override
            protected ArrayList<Double> doInBackground(Void... params) {

                ArrayList<Double> latLonList = new ArrayList<Double>();
                HttpURLConnection conn = null;
                String data="";
                String TYPE_DETAIL = "/details";
                StringBuilder jsonResults = new StringBuilder();
                try {
                    StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAIL + OUT_JSON);
                    sb.append("?placeid=" + URLEncoder.encode(input, "utf8"));
                    sb.append("&key=" + API_KEY);
                    URL url = new URL(sb.toString());
                    Log.e("czddszc"," "+sb);

                    System.out.println("URL: " + url);
                    System.out.println(" ******************************* connexion au server ***************************************** ");
                    conn = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);
                    }
                    System.out.println("le json result" + jsonResults.toString());
                    data = jsonResults.toString();
                } catch (IOException ignored) {
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                    System.out.println(" ******************************* fin de la connexion************************************************* ");
                }
                try {
                    System.out.println("fabrication du Json Objet");
                    JSONObject jsonObj = new JSONObject(jsonResults.toString());
                    JSONObject result = jsonObj.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
                    System.out.println("la chain Json " + result);
                    Double longitude = result.getDouble("lng");
                    Double latitude = result.getDouble("lat");
                    System.out.println("longitude et latitude " + longitude + latitude);
                    latLonList.add(latitude);
                    latLonList.add(longitude);

                } catch (JSONException e) {
                }
                return latLonList;
            }

            @Override
            protected void onPostExecute(ArrayList<Double> result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);

                if(result!= null && result.size() !=0) {
                    lat = result.get(0);
                    lon = result.get(1);
                }
                //    moveMap(lat,lon);
                Intent intentback = new Intent();

                String lat1 = String.valueOf(lat);
                String lon1 = String.valueOf(lon);
                String address = acGroundSearch.getText().toString();

                intentback.putExtra("LAT",lat1);
                intentback.putExtra("LON",lon1);
                intentback.putExtra("ADDRESS",address);
                setResult(RESULT_OK, intentback);
                finish();
            }
        };
        async.execute();
    }

    public static ArrayList<HashMap<String, String>> autocomplete(String input) {
        ArrayList<HashMap<String, String>> resultList = null;
        HashMap<String, String> Hahs_list;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);

            sb.append("&types=geocode");
            //sb.append("&components=country:za");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("Log", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("Log", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            Log.e("response result", "" + jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<HashMap<String, String>>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                Hahs_list = new HashMap<String, String>();
                Hahs_list.put("description", predsJsonArray.getJSONObject(i).getString("description"));
                Hahs_list.put("place_id", predsJsonArray.getJSONObject(i).getString("place_id"));
                resultList.add(Hahs_list);
            }
        } catch (JSONException e) {
            Log.e("Log", "Cannot process JSON results", e);
        }
        return resultList;
    }
}