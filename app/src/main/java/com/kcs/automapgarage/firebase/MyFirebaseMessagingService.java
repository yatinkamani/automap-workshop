package com.kcs.automapgarage.firebase;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kcs.automapgarage.Act_Admin_Message;
import com.kcs.automapgarage.Act_Customer_Request;
import com.kcs.automapgarage.Act_Dealer_Request;
import com.kcs.automapgarage.Act_Make_Request;
import com.kcs.automapgarage.Act_Mobile_Request;
import com.kcs.automapgarage.R;

import org.json.JSONObject;

import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final int REQUEST_CODE = 1;
    int notification_id = 0;
    private NotificationUtils notificationUtils;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int badge_count_garage = 0;
    int badge_count_mobile_request = 0;
    int badge_count_make_request = 0;
    int badge_count_dealer_request = 0;
    int badge_count_admin_msg = 0;

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("receive_notification", "aaa");

        preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        editor = preferences.edit();

        badge_count_garage = preferences.getInt(getString(R.string.pref_badge_count), 0);
        badge_count_mobile_request = preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0);
        badge_count_make_request = preferences.getInt(getString(R.string.pref_badge_count_make_request), 0);
        badge_count_dealer_request = preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0);
        badge_count_admin_msg = preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0);

       /* Intent i = new Intent(this, Act_Edit_Profile_Detail.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE,
                i, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                .setContentText(remoteMessage.getNotification().getBody())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());*/

        JSONObject obj = new JSONObject(remoteMessage.getData());
        String jsonMessage = obj.toString();
        Log.e("response", "JSON Object" + obj.toString());

        //Calling method to generate notification
        sendNotification(jsonMessage);
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    private void sendNotification(String messageBody) {
        try {
            JSONObject jsonMain = new JSONObject(messageBody);
            String messageFor = "";
            String message = "";
            Intent intent = null;

            Log.e("Tag Badge Mobile Count", "" + badge_count_mobile_request);
            Log.e("Tag Badge Garage Count", "" + badge_count_garage);
            Log.e("Tag Badge admin msg", "" + badge_count_admin_msg);
            Intent intents = new Intent("send_data");

            // notification different badge count brodcast Receiver
            if (jsonMain.getString("success").equals("1")) {
                messageFor = jsonMain.getString("message_for");
                message = jsonMain.optString("msg", "");
                //messageFor = jsonMain.optString("message_for", "track");
                if (messageFor.equals("mobile_request")) {

                    notification_id = 100;
                    intent = new Intent(this, Act_Mobile_Request.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent);
                    badge_count_mobile_request = badge_count_mobile_request + 1;
                    editor.putInt(getString(R.string.pref_badge_count_mobile_request), badge_count_mobile_request);
                    intents.putExtra(getString(R.string.pref_badge_count_mobile_request), badge_count_mobile_request);
                    intents.putExtra(getString(R.string.pref_badge_count), badge_count_garage);
                    intents.putExtra(getString(R.string.pref_badge_count_make_request), badge_count_make_request);
                    intents.putExtra(getString(R.string.pref_badge_count_dealer_request), badge_count_dealer_request);
                    intents.putExtra(getString(R.string.pref_badge_count_admin_msg), badge_count_admin_msg);
                } else if (messageFor.equalsIgnoreCase("garage")) {
                    notification_id = 200;
                    intent = new Intent(this, Act_Customer_Request.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent);
                    badge_count_garage = badge_count_garage + 1;
                    editor.putInt(getString(R.string.pref_badge_count), badge_count_garage);
                    intents.putExtra(getString(R.string.pref_badge_count_mobile_request), badge_count_mobile_request);
                    intents.putExtra(getString(R.string.pref_badge_count), badge_count_garage);
                    intents.putExtra(getString(R.string.pref_badge_count_make_request), badge_count_make_request);
                    intents.putExtra(getString(R.string.pref_badge_count_dealer_request), badge_count_dealer_request);
                    intents.putExtra(getString(R.string.pref_badge_count_admin_msg), badge_count_admin_msg);
                } else if (messageFor.equalsIgnoreCase("dealer_request")) {
                    notification_id = 300;
                    intent = new Intent(this, Act_Make_Request.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent);
                    badge_count_make_request = badge_count_make_request + 1;
                    editor.putInt(getString(R.string.pref_badge_count_make_request), badge_count_make_request);
                    intents.putExtra(getString(R.string.pref_badge_count_mobile_request), badge_count_mobile_request);
                    intents.putExtra(getString(R.string.pref_badge_count), badge_count_garage);
                    intents.putExtra(getString(R.string.pref_badge_count_make_request), badge_count_make_request);
                    intents.putExtra(getString(R.string.pref_badge_count_dealer_request), badge_count_dealer_request);
                    intents.putExtra(getString(R.string.pref_badge_count_admin_msg), badge_count_admin_msg);

                    /*String id = jsonMain.optString("dealer_request_id", "");
                    if (!id.equals("")){
                        int old = preferences.getInt(id,0);
                        editor.putInt(id,(old + 1));
                    }*/
                } else if (messageFor.equalsIgnoreCase("make_request")) {
                    notification_id = 400;
                    intent = new Intent(this, Act_Dealer_Request.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent);
                    badge_count_dealer_request = badge_count_dealer_request + 1;
                    editor.putInt(getString(R.string.pref_badge_count_dealer_request), badge_count_dealer_request);
                    intents.putExtra(getString(R.string.pref_badge_count_mobile_request), badge_count_mobile_request);
                    intents.putExtra(getString(R.string.pref_badge_count), badge_count_garage);
                    intents.putExtra(getString(R.string.pref_badge_count_make_request), badge_count_make_request);
                    intents.putExtra(getString(R.string.pref_badge_count_dealer_request), badge_count_dealer_request);
                    intents.putExtra(getString(R.string.pref_badge_count_admin_msg), badge_count_admin_msg);
                } else if (messageFor.equalsIgnoreCase("garage_message")) {
                    notification_id = new Random().nextInt();
                    intent = new Intent(this, Act_Admin_Message.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent);
                    badge_count_admin_msg = badge_count_admin_msg + 1;
                    editor.putInt(getString(R.string.pref_badge_count_admin_msg), badge_count_admin_msg);
                    intents.putExtra(getString(R.string.pref_badge_count_admin_msg), badge_count_admin_msg);
                    intents.putExtra(getString(R.string.pref_badge_count_mobile_request), badge_count_mobile_request);
                    intents.putExtra(getString(R.string.pref_badge_count), badge_count_garage);
                    intents.putExtra(getString(R.string.pref_badge_count_make_request), badge_count_make_request);
                    intents.putExtra(getString(R.string.pref_badge_count_dealer_request), badge_count_dealer_request);
                }

//                if (messageFor.equals("garage_message") || messageFor.equalsIgnoreCase("make_request") ||
//                        messageFor.equalsIgnoreCase("dealer_request")){
                    editor.apply();
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intents);
//                }
            }
            ShortcutBadger.applyCount(getApplicationContext(), (badge_count_garage + badge_count_mobile_request + badge_count_dealer_request + badge_count_make_request + badge_count_admin_msg));

//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, @NonNull Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, notification_id, message, timeStamp, intent);
    }
}


