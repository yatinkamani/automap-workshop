package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Admin_Msg_Model;
import com.kcs.automapgarage.extra.RecyclerItemClickListener;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;
import com.potyvideo.library.AndExoPlayerView;
import com.potyvideo.library.globalEnums.EnumAspectRatio;
import com.potyvideo.library.globalEnums.EnumResizeMode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Admin_Message extends AppCompatActivity implements View.OnClickListener {

    ImageView back, img_refresh;
    TextView txt_title;
    RecyclerView rcv_view;
    View lin_error;
    Button btnRetry;
    TextView txtMsg;
    List<Admin_Msg_Model> admin_msg_models = new ArrayList<>();
    List<Admin_Msg_Model> selected_admin_msg_models = new ArrayList<>();
    AdapterAdminMsg adapterAdminMsg;
    SharedPreferences preferences;
//    SharedPreferences.Editor editor;

    String garage_id;

    RelativeLayout rel_header_selected, rel_header;
    TextView txt_title_selected;
    ImageView img_delete;
    private boolean isMultiSelect = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__admin__message);

        back = findViewById(R.id.back);
        img_refresh = findViewById(R.id.img_refresh);
        txt_title = findViewById(R.id.txt_title);
        lin_error = findViewById(R.id.lin_error);
        rcv_view = findViewById(R.id.rcy_view);
        txtMsg = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);

        rel_header = findViewById(R.id.rel_header);
        rel_header_selected = findViewById(R.id.rel_header_selected);
        txt_title_selected = findViewById(R.id.txt_title_selected);
        img_delete = findViewById(R.id.img_delete);
        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);

        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");
        back.setOnClickListener(this);
        btnRetry.setOnClickListener(this);
        img_refresh.setOnClickListener(this);
        img_delete.setOnClickListener(this);

        AdminMsg();
        InitializeBroadcast();
        ClearNotification();

        /*admin_msg_models = new ArrayList<>();
        admin_msg_models.add(new Admin_Msg_Model("1", "sds", "s", "2020-06-16 12:45:36"));
        admin_msg_models.add(new Admin_Msg_Model("2", "sds fdk ffs djs asd skd ksk kdk sad eww iew fdsf", "s", "2020-06-16 01:45:36"));
        admin_msg_models.add(new Admin_Msg_Model("3", "fie lsa djs ad sa dj ajd sas js jas asd ksa d", "", "2020-06-16 09:45:36"));
        admin_msg_models.add(new Admin_Msg_Model("4", "des ksd kf sdk dsf d", "s", "2020-03-16 03:11:36"));
        subscriptionOffer = new AdapterAdminMsg(admin_msg_models, getApplicationContext());
        rcv_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        rcv_view.setAdapter(subscriptionOffer);*/

        rcv_view.addOnItemTouchListener(new RecyclerItemClickListener(this, rcv_view, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (isMultiSelect){
                    multiSelect(position);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {
                if (!isMultiSelect){
                    selected_admin_msg_models = new ArrayList<>();
                    isMultiSelect = true;

                    rel_header_selected.setVisibility(View.VISIBLE);
                    rel_header.setVisibility(View.INVISIBLE);
                }
                multiSelect(position);
            }
        }));

    }

    // clear preference message badge count
    public void ClearNotification() {
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.pref_badge_count_admin_msg), 0);
        editor.apply();
        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0));
        if (tot == 0) {
            ShortcutBadger.removeCount(getApplicationContext());
        } else {
            ShortcutBadger.applyCount(getApplicationContext(), tot);
        }
    }

    private void AdminMsg() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            API api = new API(Act_Admin_Message.this, new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Log.e("Tag response", response);
                    List<Admin_Msg_Model> admin_msg_models_in = new ArrayList<>();

                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("status").equals("true")) {

                            JSONArray array = object.getJSONArray("result");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                String id = object1.getString("message_id");
                                String message = check(object1,"message");
                                String img = object1.getString("message_image");
                                String created_at = object1.getString("created_at");

                                String video = "";
                                if (object1.has("message_video")) {
                                    video = object1.getString("message_video");
                                }

                                admin_msg_models_in.add(new Admin_Msg_Model(id, message, img, video, created_at, false));
                            }
                            admin_msg_models = new ArrayList<>();
                            for (Admin_Msg_Model msgModel : admin_msg_models_in) {
                                boolean isFound = false;
                                for (Admin_Msg_Model model : admin_msg_models) {
                                    if (model.getId().equals(msgModel.getId())) {
                                        isFound = true;
                                        break;
                                    }
                                }
                                if (!isFound) {
                                    admin_msg_models.add(msgModel);
                                }
                            }

                            Collections.reverse(admin_msg_models);
                            if (admin_msg_models != null && admin_msg_models.size() > 0) {

                                rcv_view.setVisibility(View.VISIBLE);
                                lin_error.setVisibility(View.GONE);

                                adapterAdminMsg = new AdapterAdminMsg(admin_msg_models, selected_admin_msg_models, Act_Admin_Message.this);
                                rcv_view.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                rcv_view.setAdapter(adapterAdminMsg);
                                rcv_view.setItemAnimator(new DefaultItemAnimator());

                                /*SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt(getString(R.string.pref_badge_count_admin_msg), 0);
                                editor.apply();
                                int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0));
                                if (tot == 0) {
                                    ShortcutBadger.removeCount(getApplicationContext());
                                } else {
                                    ShortcutBadger.applyCount(getApplicationContext(), tot);
                                }*/

                            } else {
                                rcv_view.setVisibility(View.GONE);
                                lin_error.setVisibility(View.VISIBLE);
                                txtMsg.setText(getString(R.string.no_data_found));
                                btnRetry.setVisibility(View.GONE);
                            }

                        } else {
                            rcv_view.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txtMsg.setText(getString(R.string.no_data_available));
                            btnRetry.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        rcv_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txtMsg.setText(getString(R.string.something_wrong_please));
                        btnRetry.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Log.e("Tag Error", error);
                    rcv_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txtMsg.setText(error);
                    btnRetry.setVisibility(View.VISIBLE);
                }
            });

            Map<String, String> map = new HashMap<>();
            map.put("garage_id", garage_id);
            map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
            Log.e("Tag Params", map.toString());

            api.execute(100, Constant.URL + "get_admin_messages", map, true);
            /*StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_admin_messages", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("garage_id", garage_id);
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);*/

        } else {
            rcv_view.setVisibility(View.GONE);
            lin_error.setVisibility(View.VISIBLE);
            txtMsg.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

        if (view == back) {
            onBackPressed();
        } else if (view == btnRetry) {
            AdminMsg();
        } else if (view == img_refresh) {
            img_refresh.setRotation(0);
            img_refresh.animate().rotation(360).setDuration(1000).start();
            AdminMsg();
        } else if (view == img_delete){
            deleteDialog();
        }
    }

    @SuppressLint("SetTextI18n")
    public void multiSelect(int position) {
        if (rel_header_selected.getVisibility() == View.VISIBLE){

            if (selected_admin_msg_models.contains(admin_msg_models.get(position))){
                selected_admin_msg_models.remove(admin_msg_models.get(position));
                admin_msg_models.get(position).setSelected(false);
            } else {
                selected_admin_msg_models.add(admin_msg_models.get(position));
                admin_msg_models.get(position).setSelected(true);
            }

            if (selected_admin_msg_models.size()>0){
                txt_title_selected.setText("  "+ selected_admin_msg_models.size());
            } else {
                txt_title_selected.setText("");
                rel_header_selected.setVisibility(View.INVISIBLE);
                rel_header.setVisibility(View.VISIBLE);
                isMultiSelect = false;
            }

            refreshAdapter();
        }
    }

    public void refreshAdapter() {
        adapterAdminMsg.selected_msg_models = selected_admin_msg_models;
        adapterAdminMsg.msg_models = admin_msg_models;
        adapterAdminMsg.notifyDataSetChanged();
    }

    public void deleteDialog() {

        SweetAlertDialog dialog = new SweetAlertDialog(Act_Admin_Message.this, SweetAlertDialog.WARNING_TYPE);
        dialog.setContentText(getString(R.string.are_you_sure_remove_message));
        dialog.setCancelText(getString(R.string.cancel));
        dialog.setConfirmText(getString(R.string.ok));
        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                    JSONArray jsonArray = new JSONArray();
                    if (selected_admin_msg_models.size()>0){

                        for(int i=0;i<selected_admin_msg_models.size();i++){
                            jsonArray.put(selected_admin_msg_models.get(i).getId());
//                            admin_msg_models.remove(selected_admin_msg_models.get(i));
                        }

//                        adapterAdminMsg.notifyDataSetChanged();

//                        rel_header_selected.setVisibility(View.INVISIBLE);
//                        rel_header.setVisibility(View.VISIBLE);
//                        isMultiSelect = false;
                        callDeleteApi(jsonArray.toString(),sweetAlertDialog);
                    }
                    Log.e("Tag Garage ids ", jsonArray.toString());
//                sweetAlertDialog.dismissWithAnimation();

            }
        });
        dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    public void callDeleteApi(String garage_id_json, SweetAlertDialog sweetAlertDialog){

        if (sweetAlertDialog != null){
            sweetAlertDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
        }
        API api = new API(getApplicationContext(), new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("Tag Response",response);
                if (isSuccess){

                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equals("true")){
                            if (sweetAlertDialog != null){
                                sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                            if (selected_admin_msg_models.size()>0){

                                for(int i=0;i<selected_admin_msg_models.size();i++)
                                    admin_msg_models.remove(selected_admin_msg_models.get(i));

                                adapterAdminMsg.notifyDataSetChanged();

                                rel_header_selected.setVisibility(View.INVISIBLE);
                                rel_header.setVisibility(View.VISIBLE);
                                isMultiSelect = false;
                            }
                        } else {
                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),getString(R.string.try_again_after_some_time),Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(),getString(R.string.no_data_found),Toast.LENGTH_LONG).show();
                }
                if (sweetAlertDialog != null){
                    sweetAlertDialog.dismissWithAnimation();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error,Toast.LENGTH_LONG).show();
                if (sweetAlertDialog != null){
                    sweetAlertDialog.dismissWithAnimation();
                }
            }
        });

        Map<String,String> map = new HashMap<>();
        map.put("garage_id", garage_id);
        map.put("message_id_json",garage_id_json);
        map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
        api.execute(100, Constant.URL+"deleteMessage", map,false);
    }

    class AdapterAdminMsg extends RecyclerView.Adapter<AdapterAdminMsg.ViewHolder> {

        List<Admin_Msg_Model> msg_models;
        List<Admin_Msg_Model> selected_msg_models;
        Context context;

        private AdapterAdminMsg(List<Admin_Msg_Model> msg_models, List<Admin_Msg_Model> selected_msg_models, Context context) {
            this.msg_models = msg_models;
            this.selected_msg_models = selected_msg_models;
            this.context = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.adapter_admin_msg, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (msg_models.get(position).getMsg().equals("")) {
                holder.txt_msg.setVisibility(View.GONE);
            } else {
                holder.txt_msg.setVisibility(View.VISIBLE);
                holder.txt_msg.setText(Html.fromHtml(msg_models.get(position).getMsg()));
            }

            if (msg_models.get(position).getImg().equals("")) {
                holder.img_msg.setVisibility(View.GONE);
            } else {
                holder.img_msg.setVisibility(View.VISIBLE);
                Glide.with(context).asBitmap().load(msg_models.get(position).getImg()).thumbnail(0.1f)
                        .placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.img_msg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.img_msg.setImageBitmap(resource);
                    }
                });
            }

            if (!msg_models.get(position).getVideo().equals("")) {
                holder.lin_player.setVisibility(View.VISIBLE);
                holder.lin_player.removeAllViews();
                holder.lin_player.addView(VideoPlayer(getApplicationContext(), msg_models.get(position).getVideo()));
            } else {
                holder.lin_player.setVisibility(View.GONE);
                holder.lin_player.removeAllViews();
            }

            /* if (!msg_models.get(position).getVideo().equals("")){
                holder.video_play.setVisibility(View.VISIBLE);
                holder.video_play.setSource(msg_models.get(position).getVideo());
            }else {
                holder.video_play.setVisibility(View.GONE);
            }*/

            /*if (position>0){
                if (AppUtils.ConvertLocalTime(msg_models.get(position).getCreated_at()).equals(AppUtils.ConvertLocalTime(msg_models.get(position-1).getCreated_at()))){
                    holder.txt_time.setText(AppUtils.ConvertLocalTime(msg_models.get(position).getCreated_at()));
                }else {
                    holder.txt_time.setText(AppUtils.ConvertLocalTime(msg_models.get(position).getCreated_at())+" Last");
                }
            }*/

            holder.txt_time.setText(AppUtils.ConvertLocalTime(msg_models.get(position).getCreated_at()));

            Log.e("Tag Selected", selected_admin_msg_models.toString());
            Log.e("Tag admin", ""+msg_models.get(position));
            Log.e("Tag is Selected", ""+msg_models.get(position).isSelected());
            boolean is = msg_models.get(position).isSelected();
//            if(selected_msg_models.contains(msg_models.get(position)))

            holder.itemView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), is ? R.color.trans_whit : R.color.transparent));

            if (is){
                holder.card.setScaleX(0.95f);
            }else {
                holder.card.setScaleX(1f);
            }
//            else
//                holder.card.setCardBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.trans_whit));

            holder.card.setClickable(true);
        }

        @Override
        public int getItemCount() {
            return msg_models.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView txt_time, txt_msg;
            RoundedImage img_msg;
            //  AndExoPlayerView video_play;
            LinearLayout lin_player;
            CardView card;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                card = itemView.findViewById(R.id.card);
                txt_msg = itemView.findViewById(R.id.txt_msg);
                img_msg = itemView.findViewById(R.id.img_msg);
                txt_time = itemView.findViewById(R.id.txt_time);
//                video_play = itemView.findViewById(R.id.video_play);
                lin_player = itemView.findViewById(R.id.lin_player);

            }
        }
    }

    public View VideoPlayer(Context context, String url) {

        AndExoPlayerView playerView = new AndExoPlayerView(context);
        playerView.setAspectRatio(EnumAspectRatio.ASPECT_16_9);
        playerView.setResizeMode(EnumResizeMode.FIT);
        playerView.setShowController(true);
        playerView.setPlayWhenReady(false);
        playerView.setSource(url);

        return playerView;
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Admin_Message.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // new message arrive to redress api and clear notification
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
//            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
//            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
//            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (admin != 0 && !Act_Admin_Message.this.isFinishing()) {
                AdminMsg();
                ClearNotification();
            }
        }
    };
}
