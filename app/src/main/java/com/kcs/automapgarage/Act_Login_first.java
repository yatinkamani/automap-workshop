package com.kcs.automapgarage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import static com.kcs.automapgarage.Constant.Constant.LOGIN_PREF;

public class Act_Login_first extends AppCompatActivity {

    CardView btn_login;
    TextView btn_reg;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), " ");
            if (lng.equals(" ")){
                Locale locale = Locale.getDefault();
                locale.getLanguage();
                lng = getResources().getConfiguration().locale.getLanguage();
            }
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
            overrideConfiguration.setLayoutDirection(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__login_first);

        btn_login = findViewById(R.id.BT_LOGIN);
        btn_reg = findViewById(R.id.BT_SIGN_UP);

        Intent intent = getIntent();
        setDefault();

        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Act_Login_first.this, Act_Registration.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Act_Login_first.this,Act_Login.class);
                startActivity(intent);
            }
        });
    }

    public void setDefault() {
        Locale locale = Locale.getDefault();
        locale.getLanguage();
        Log.e("Tag Language", getResources().getConfiguration().locale.getLanguage());
        SharedPreferences preferences = getSharedPreferences(LOGIN_PREF, MODE_PRIVATE);
        if (preferences.getString(getString(R.string.pref_language), "").equals("")){
            String lng = getResources().getConfiguration().locale.getLanguage();
            /*if (lng.equals("ar")){
                preferences.edit().putString(getString(R.string.pref_language), lng).apply();
            }else {
                preferences.edit().putString(getString(R.string.pref_language), "en").apply();
            }*/
            preferences.edit().putString(getString(R.string.pref_language), "ar").apply();
        }
    }

}
