package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Sales_Person_Model;
import com.kcs.automapgarage.Model.Subscription_Offers_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Subscription_Offers extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    RecyclerView rcv_view;
    LinearLayout linError;
    Button btn_add, btnRetry;
    TextView txtError;
    TextView txt_dealer, txt_provider;

    AdapterSubscriptionOffer subscriptionOffer;
    AdapterDialogSubscriptionOffer dialogSubscriptionOffer;
    List<Subscription_Offers_Model> subscription_offers_models = new ArrayList<>();

    BottomSheetDialog sheetDialog;
    String garage_id = "", offer_id = "", service_id = "";
    SharedPreferences preferences;
    boolean isActive = true;

    List<Sales_Person_Model> sales_models;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__subscripton__offers);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");
//        garage_id = "10";
//        sales_id = preferences.getString(getString(R.string.pref_sales_id), "");
        service_id = preferences.getString(getString(R.string.pref_service_id), "");

        img_back = findViewById(R.id.img_back);
        rcv_view = findViewById(R.id.rcv_view);
        txt_dealer = findViewById(R.id.txt_dealer);
        txt_provider = findViewById(R.id.txt_provider);
        linError = findViewById(R.id.linError);
        btn_add = findViewById(R.id.btn_add);
        btnRetry = findViewById(R.id.btnRetry);
        txtError = findViewById(R.id.txtError);

        img_back.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btnRetry.setOnClickListener(this);

//        retrieveJSON2();
        getActiveOffers();
    }

    @Override
    protected void onResume() {
        if (Act_Active_Offers.resume.equals("1")){
            if (sheetDialog != null && sheetDialog.isShowing()){
                sheetDialog.dismiss();
            }
            getActiveOffers();
        }
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        if (view == img_back) {
            onBackPressed();
        } else if (view == btnRetry) {
            getActiveOffers();
        } else if (view == btn_add) {
            getAllOffers();
        }
    }

    // get active subscriptio offer get and detail
    private void getActiveOffers() {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_active_subscription", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("Tag Error", response);

                    try {
                        JSONObject object = new JSONObject(response);
                        subscription_offers_models = new ArrayList<>();
                        if (object.getString("status").equals("1")) {

                            JSONArray array = object.getJSONArray("result");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                Subscription_Offers_Model model = new Subscription_Offers_Model();
                                model.setId(object1.getString("offer_id"));
                                model.setName(check(object1,"offer_name"));
                                model.setPrice(object1.getString("offer_price"));
                                model.setValidity(object1.getString("subscription_validity"));
                                model.setService_provide_number(object1.getString("daily_participate_service_to_user"));
                                model.setDealer_number(object1.getString("daily_participate_dealer_to_owner"));
                                model.setCreated_at(object1.getString("created_at"));

                                if (!object1.getString("left_daily_participate_service_to_user").equals("")) {
                                    txt_provider.setVisibility(View.VISIBLE);
                                    txt_provider.setText(getString(R.string.daily_participate_service_to_user)+" "+ object1.getString("left_daily_participate_service_to_user")+" "+ getString(R.string.left));
                                }

                                if (!object1.getString("left_daily_participate_dealer_to_owner").equals("")) {
                                    txt_dealer.setVisibility(View.VISIBLE);
                                    txt_dealer.setText(getString(R.string.daily_participate_dealer_to_owner)+" " + object1.getString("left_daily_participate_dealer_to_owner")+" "+ getString(R.string.left));
                                }

                                subscription_offers_models.add(model);

                            }

                            if (subscription_offers_models != null && subscription_offers_models.size() > 0) {

                                rcv_view.setVisibility(View.VISIBLE);
                                linError.setVisibility(View.GONE);

                                subscriptionOffer = new AdapterSubscriptionOffer(subscription_offers_models, getApplicationContext());
                                rcv_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                                rcv_view.setAdapter(subscriptionOffer);

                                /*SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt(getString(R.string.pref_badge_count_admin_msg), 0);
                                editor.apply();
                                int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0));
                                if (tot == 0) {
                                    ShortcutBadger.removeCount(getApplicationContext());
                                } else {
                                    ShortcutBadger.applyCount(getApplicationContext(), tot);
                                }*/

                            } else {
                                rcv_view.setVisibility(View.GONE);
                                linError.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.no_any_subscription));
                                btnRetry.setVisibility(View.GONE);
                            }

                        } else {
                            rcv_view.setVisibility(View.GONE);
                            linError.setVisibility(View.VISIBLE);
                            txtError.setText(getString(R.string.no_any_subscription));
                            btnRetry.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        rcv_view.setVisibility(View.GONE);
                        linError.setVisibility(View.VISIBLE);
                        txtError.setText(getString(R.string.something_wrong_please));
                        btnRetry.setVisibility(View.VISIBLE);
                    }
                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    rcv_view.setVisibility(View.GONE);
                    linError.setVisibility(View.VISIBLE);
                    txtError.setText(AppUtils.serverError(getApplicationContext(), error));
                    btnRetry.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("garage_id", garage_id);
                    map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            rcv_view.setVisibility(View.GONE);
            linError.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            dialog.dismiss();

        }
    }

    private void AddSubscription(BottomSheetDialog bottomSheetDialog) {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "add_subscription", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("Tag Error", response);

                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("status").equals("1")) {
                            bottomSheetDialog.dismiss();
                            offer_id = "";
                            getActiveOffers();
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
//                    Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("garage_id", garage_id);
                    map.put("offer_id", offer_id);
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            rcv_view.setVisibility(View.GONE);
            linError.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            dialog.dismiss();

        }
    }

    // adaper active subscription offer
    private class AdapterSubscriptionOffer extends RecyclerView.Adapter<AdapterSubscriptionOffer.MyViewHolder> {

        List<Subscription_Offers_Model> models;
        Context context;

        private AdapterSubscriptionOffer(List<Subscription_Offers_Model> models, Context context) {
            this.models = models;
            this.context = context;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_active_offers, parent, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            Subscription_Offers_Model model = models.get(position);

            holder.txt_offers_name.setText(model.getName());
            holder.txt_user_limit.setText(model.getService_provide_number());
            holder.txt_dealer_limit.setText(model.getDealer_number());
            holder.txt_price.setText(Html.fromHtml(model.getPrice()+ "JD"));
            holder.txt_validity.setText(model.getValidity()+" "+ getString(R.string.month));
            holder.txt_date.setText(AppUtils.getddmmmyyyy(AppUtils.ConvertLocalTime(model.getCreated_at())));

            int day = Integer.parseInt(model.getValidity()) * 30;
            Date date = new Date(AppUtils.ConvertLocalTime(model.getCreated_at()));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + day);
            Log.e("Tag date", calendar.get(Calendar.DAY_OF_MONTH) + " " + calendar.get(Calendar.MONTH));

            String start = "0", end = "";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            start = format.format(new Date());
            end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(calendar.getTime());

            Log.e("Tag Data", start + "  " + end);
            if (Integer.parseInt(AppUtils.getDaysDifference(start, end)) <= 5 && Integer.parseInt(AppUtils.getDaysDifference(start, end)) >= 1) {
                holder.txt_left_days.setTextColor(Color.RED);
                holder.txt_left_days.setText(AppUtils.getDaysDifference(start, end)+" "+ getString(R.string.days_left));
                isActive = false;
                btn_add.setVisibility(View.GONE);
            } else if (Integer.parseInt(AppUtils.getDaysDifference(start, end)) <= 0) {
                holder.txt_left_days.setTextColor(Color.RED);
                holder.txt_left_days.setText(getString(R.string.expire_your_subscription));
                btn_add.setVisibility(View.VISIBLE);
            } else {
                holder.txt_left_days.setTextColor(Color.BLACK);
                holder.txt_left_days.setText(AppUtils.getDaysDifference(start, end)+" "+ getString(R.string.days_left));
                isActive = false;
                btn_add.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return models.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView txt_offers_name, txt_user_limit, txt_dealer_limit, txt_price, txt_validity, txt_date, txt_left_days;

            private MyViewHolder(@NonNull View itemView) {
                super(itemView);

                txt_offers_name = itemView.findViewById(R.id.txt_offers_name);
                txt_user_limit = itemView.findViewById(R.id.txt_user_limit);
                txt_dealer_limit = itemView.findViewById(R.id.txt_dealer_limit);
                txt_price = itemView.findViewById(R.id.txt_price);
                txt_validity = itemView.findViewById(R.id.txt_validity);
                txt_date = itemView.findViewById(R.id.txt_date);
                txt_left_days = itemView.findViewById(R.id.txt_left_days);
                TextView txt_user_limit1  = itemView.findViewById(R.id.txt_user_limit1);
                txt_user_limit1.setText(getString(R.string.user_request_participate));
                TextView txt_dealer_limit1  = itemView.findViewById(R.id.txt_dealer_limit1);
                txt_dealer_limit1.setText(getString(R.string.dealer_request_participate));
                TextView txt_price1  = itemView.findViewById(R.id.txt_price1);
                txt_price1.setText(getString(R.string.paid_amount));
                TextView txt_validity1  = itemView.findViewById(R.id.txt_validity1);
                txt_validity1.setText(getString(R.string.subscription_validity));
                TextView txt_date1  = itemView.findViewById(R.id.txt_date1);
                txt_date1.setText(getString(R.string.subscription_date));

            }
        }
    }

    int count = 0;

    // bottomsheet plan offer list dialog
    private void DetailDialog(List<Subscription_Offers_Model> offers_models) {

        sheetDialog = new BottomSheetDialog(Act_Subscription_Offers.this, R.style.SheetDialog);

        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_subscription_offers, null, false);

        sheetDialog.setContentView(view);
        sheetDialog.setCanceledOnTouchOutside(false);
        sheetDialog.setOnDismissListener(dialogInterface -> offer_id = "");
//        Button btn_add = view.findViewById(R.id.btn_add);
        RecyclerView dialog_rcv_view = view.findViewById(R.id.dialog_rcv_view);
//        EditText ed_sales_code = view.findViewById(R.id.ed_sales_code);

        if (offers_models != null && offers_models.size() > 0) {
            dialogSubscriptionOffer = new AdapterDialogSubscriptionOffer(offers_models, Act_Subscription_Offers.this);
            dialog_rcv_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
            dialog_rcv_view.setAdapter(dialogSubscriptionOffer);
            sheetDialog.show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.no_data_found, Toast.LENGTH_LONG).show();
        }

        /*btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (offer_id.equals("")) {
                    Toast.makeText(getApplicationContext(), "select offers", Toast.LENGTH_LONG).show();
                } else {
                    if (!isActive) {
                        Toast.makeText(getApplicationContext(), "your Subscription is already Active", Toast.LENGTH_LONG).show();
                    } */
        /*else if (sales_id.equals("") || sales_id.equals("0")) {
                        Toast.makeText(getApplicationContext(), "no any sales person entered for registration", Toast.LENGTH_LONG).show();
                    } else if (ed_sales_code.getVisibility() == View.GONE) {
                        ed_sales_code.setVisibility(View.VISIBLE);
                        ed_sales_code.requestFocus();
                    } else if (ed_sales_code.getText().toString().equals("") || count == 3) {
                        freeRegistrationDialog();
                        if (count == 3)
                            count = 2;
                    } else if (!ed_sales_code.getText().toString().equals(sales_code)) {
                        count++;
                        ed_sales_code.setError("Enter valid sales code");
                    }*/
        /* else {
                        AddSubscription(sheetDialog);
                    }
                }
            }
        });*/
    }

    // get all particular service plan offers and set bottom sheet dialog
    private void getAllOffers() {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_offers", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("Tag Response", response);

                    try {
                        JSONObject object = new JSONObject(response);

                        List<Subscription_Offers_Model> offersModels = new ArrayList<>();
                        if (object.getString("status").equals("1")) {

                            JSONArray array = object.getJSONArray("result");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                Subscription_Offers_Model model = new Subscription_Offers_Model();
                                model.setId(object1.getString("offer_id"));
                                model.setName(check(object1,"offer_name"));
                                model.setPrice(object1.getString("offer_price"));
                                model.setValidity(object1.getString("subscription_validity"));
                                model.setService_provide_number(object1.getString("daily_participate_service_to_user"));
                                model.setDealer_number(object1.getString("daily_participate_dealer_to_owner"));
                                model.setCreated_at(object1.getString("created_at"));

                                if (!model.getPrice().equals("0")) {
                                    offersModels.add(model);
                                }
                            }

                            if (offersModels != null && offersModels.size() > 0) {
                                DetailDialog(offersModels);
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.no_any_offers_for_this_type_service, Toast.LENGTH_LONG).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("service_id", service_id);
                    map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.d("Tag params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            rcv_view.setVisibility(View.GONE);
            linError.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            dialog.dismiss();
        }
    }

    List<LinearLayout> views = new ArrayList<>();

    // bottom sheet list all plan offer
    private class AdapterDialogSubscriptionOffer extends RecyclerView.Adapter<AdapterDialogSubscriptionOffer.MyViewHolder> {

        List<Subscription_Offers_Model> models;
        Context context;

        AdapterDialogSubscriptionOffer(List<Subscription_Offers_Model> models, Context context) {
            this.models = models;
            this.context = context;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_subscription_offer, parent, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            Subscription_Offers_Model model = models.get(position);

            holder.txt_offers_title.setText(model.getName().toString());
            holder.txt_user_limit.setText(model.getService_provide_number().toString());
            holder.txt_dealer_limit.setText(model.getDealer_number().toString());
            holder.txt_price.setText(Html.fromHtml(getString(R.string.price)+" "+ model.getPrice()));
            holder.txt_validity.setText(getString(R.string.validity)+" " + model.getValidity()+" "+ getString(R.string.month));

            if (!views.contains(holder.lin_main)) {
                views.add(holder.lin_main);
            }

            holder.lin_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    holder.lin_main.setEnabled(false);
                    for (LinearLayout view1 : views) {
                        view1.setBackgroundTintList(ColorStateList.valueOf(Color.BLACK));
                    }
                    if (!offer_id.equals(model.getId())) {
                        offer_id = model.getId();
                        holder.lin_main.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    } else {
                        offer_id = "";
                    }

                    Intent intent = new Intent(getApplicationContext(), Act_Active_Offers.class);
                    intent.putExtra("offer_id", model.getId());
                    intent.putExtra("offer_name", model.getName());
                    startActivity(intent);
                    if (sheetDialog != null){
                        sheetDialog.dismiss();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return models.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView txt_offers_title, txt_user_limit, txt_dealer_limit, txt_price, txt_validity;
            LinearLayout lin_main;

            private MyViewHolder(@NonNull View itemView) {
                super(itemView);

                lin_main = itemView.findViewById(R.id.lin_main);
                txt_offers_title = itemView.findViewById(R.id.txt_offers_title);
                txt_user_limit = itemView.findViewById(R.id.txt_user_limit);
                txt_dealer_limit = itemView.findViewById(R.id.txt_dealer_limit);
                txt_price = itemView.findViewById(R.id.txt_price);
                txt_validity = itemView.findViewById(R.id.txt_validity);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void freeRegistrationDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Act_Subscription_Offers.this);

        View view = View.inflate(Act_Subscription_Offers.this, R.layout.free_registration_dialog, null);
        Button btn_contact_us = view.findViewById(R.id.btn_contact_us);
        TextView txt_message = view.findViewById(R.id.txt_message);
        alertDialog.setView(view);

        AlertDialog dialog = alertDialog.create();

        txt_message.setText(getString(R.string.for_subscription));
        btn_contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(Act_Subscription_Offers.this, Act_Contact_Us.class);
                startActivity(intent);
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    /*private void retrieveJSON2() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Tag Country City", response);
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.optString("status").equals("true")) {

                                sales_models = new ArrayList<>();
                                Sales_Person_Model models = new Sales_Person_Model();
                                JSONObject object = obj.getJSONObject("result");
                                if (object.has("salesman")) {
                                    JSONArray sales_team = object.getJSONArray("salesman");
                                    for (int i = 0; i < sales_team.length(); i++) {
                                        JSONObject object1 = sales_team.getJSONObject(i);
                                        Sales_Person_Model model = new Sales_Person_Model();
                                        model.setId(object1.getString("salesman_id"));
                                        model.setPerson_name(object1.getString("name"));
                                        model.setPerson_code(object1.getString("code"));
                                        model.setPerson_email(object1.getString("email"));
                                        model.setPerson_mobile(object1.getString("contact_no"));
                                        sales_models.add(model);
                                    }
                                }

                                if (sales_models != null && sales_models.size() > 0) {

                                    for (int i = 0; i < sales_models.size(); i++) {
                                        if (sales_id.equals(sales_models.get(i).getId())) {
                                            sales_code = sales_models.get(i).getPerson_code();
                                        }
                                    }
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }*/

}
