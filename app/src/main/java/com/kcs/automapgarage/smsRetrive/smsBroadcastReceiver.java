package com.kcs.automapgarage.smsRetrive;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.kcs.automapgarage.R;

// not use
public class smsBroadcastReceiver extends BroadcastReceiver {

    private static SMSListener smsListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        /*if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = null;
            if (extras != null) {
                status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            }

            if (status != null) {
                switch (status.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get SMS message contents
                        String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                        // Extract one-time code from the message and complete verification
                        // by sending the code back to your server.
                        if (smsListener != null)
                            smsListener.onSuccess(message);
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Waiting for SMS timed out (5 minutes)
                        // Handle the error ...
                        if (smsListener != null)
                            smsListener.onError("Failed to extract from Broadcast Receiver");
                        break;
                }
            }
        }*/

        // Get Bundle object contained in the SMS intent passed in
        Bundle bundle = intent.getExtras();
        SmsMessage[] smsMsg = null;
        String smsStr ="";
        if (bundle != null) {
            // Get the SMS message
            Object[] pdus = (Object[]) bundle.get("pdus");
            smsMsg = new SmsMessage[pdus.length];
            for (int i=0; i<smsMsg.length; i++){
                smsMsg[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                smsStr = smsMsg[i].getMessageBody().toString();

                if (smsStr.contains(context.getString(R.string.app_name))){
//                    Log.e("Tag contain", "App Name Found");
                    String Sender = smsMsg[i].getOriginatingAddress();
                    //Check here sender is yours
                    Intent smsIntent = new Intent("otp");
                    smsIntent.putExtra("message",smsStr);
                    smsIntent.putExtra("Sender",Sender);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(smsIntent);
                }

            }
        }
    }

    public static void initSMSListener(SMSListener listener) {
        smsListener = listener;
    }
}
