package com.kcs.automapgarage.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kcs.automapgarage.Act_User_Request_Detail;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Request_Data_Model;
import com.kcs.automapgarage.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

// appointment request adapter
public class Adapter_Customer_Request extends RecyclerView.Adapter<Adapter_Customer_Request.MyViewHolder> {

    private Activity activity;
    private List<Request_Data_Model> garage_List;

    public Adapter_Customer_Request(Activity activity, List<Request_Data_Model> garage_List) {
        this.activity = activity;
        this.garage_List = garage_List;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_customer_request, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final Request_Data_Model requestData = garage_List.get(i);

//        holder.user_city.setText("City :       " + requestData.getMobile_no());

        Log.e("TAG", requestData.getName());
        holder.user_name.setText(Html.fromHtml("<b>"+ activity.getString(R.string.car_brand) +":       </b>" + requestData.getBrand()));
        holder.txt_cart_fule.setText(Html.fromHtml("<b>"+ activity.getString(R.string.fuel_of_car) + ":       </b>" + requestData.getFuel_name()));
        holder.txt_cart_model.setText(Html.fromHtml("<b>"+ activity.getString(R.string.car_model) +":       </b>" + requestData.getModel_name() + " " + requestData.getModel_year()));
        /*if (requestData.getUser_image() != null){
            Glide.with(activity).asBitmap().load(requestData.getUser_image()).apply(new RequestOptions().placeholder(R.drawable.users)).into(holder.user_image);
        }else {
            holder.user_image.setImageDrawable(activity.getDrawable(R.drawable.users));
        }*/

        if (requestData.getStatus().equals("0") && requestData.getTimeout_status().equals("0")) {
            holder.user_status.setText(activity.getString(R.string.status) +":    "+  activity.getString(R.string.neww) );
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.red_low_opacity));
        } else if (requestData.getStatus().equals("0") && requestData.getTimeout_status().equals("1")) {
            holder.user_status.setText(activity.getString(R.string.status) +":   " + activity.getString(R.string.request_de_active));
            holder.llDetail.setEnabled(false);
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.yellow_low_opacity));
        } else if (requestData.getStatus().equals("1")) {
            holder.user_status.setText(activity.getString(R.string.status) +":   " + activity.getString(R.string.participated));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.yellow_low_opacity));
        } else if (requestData.getStatus().equals("2")) {
            holder.user_status.setText(activity.getString(R.string.status) +":   " + activity.getString(R.string.you_have_ignore));
            holder.llDetail.setEnabled(false);
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.blue_low_opacity));
        } else if (requestData.getStatus().equals("3")) {
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.confirm));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.green_low_opacity));
        } else if (requestData.getStatus().equals("4")) {
            holder.llDetail.setEnabled(false);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.sorry_you_have_not_choose));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.light_gray));
        } else if (requestData.getStatus().equals("5")) {
            holder.llDetail.setEnabled(false);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.sorry_the_user_canceled_the_order));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.blue_low_opacity));
        } else if (requestData.getStatus().equals("6")) {
            holder.llDetail.setEnabled(true);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.user_agree_with_your_workshop));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.green_low_opacity));
        } else if (requestData.getStatus().equals("7")) {
            holder.llDetail.setEnabled(true);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.user_not_agree_with_your_workshop));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.blue_low_opacity));
        } else if (requestData.getStatus().equals("8")) {
            holder.llDetail.setEnabled(true);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.you_agree_with_user));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.green_low_opacity));
        } else if (requestData.getStatus().equals("9")) {
            holder.llDetail.setEnabled(false);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.you_are_not_agree_this_service));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.blue_low_opacity));
        } else if (requestData.getStatus().equals("10")) {
            holder.llDetail.setEnabled(true);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.work_is_progress_user_confirm));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.green_low_opacity));
        } else if (requestData.getStatus().equals("11")) {
            holder.llDetail.setEnabled(true);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.please_rate_the_service));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.green_low_opacity));
        } else if (requestData.getStatus().equals("12")) {
//            holder.llDetail.setEnabled(false);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.order_is_complete));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.default_low_opacity));
        } else if (requestData.getStatus().equals("13")) {
            holder.llDetail.setEnabled(false);
            holder.user_status.setText(activity.getString(R.string.status) +" :   " + activity.getString(R.string.the_user_complete_to_you));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.default_low_opacity));
        } else if (requestData.getStatus().equals("14")) {
            holder.llDetail.setEnabled(false);
            holder.user_status.setText(activity.getString(R.string.status) +" :  " + activity.getString(R.string.you_complained_to_user));
            holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.default_low_opacity));
        } else {
            holder.user_status.setVisibility(View.GONE);
        }

        holder.txt_city.setText(activity.getString(R.string.city) +" :  " + requestData.getCity());
        holder.txt_city.setVisibility(View.GONE);
        if (requestData.getProblem() != null && !requestData.getProblem().equals("")) {
            holder.txt_problem.setVisibility(View.VISIBLE);
            holder.txt_problem.setText(activity.getString(R.string.problem) +" :  " + requestData.getProblem());
        } else {
            holder.txt_problem.setVisibility(View.GONE);
        }

        if (requestData.getAppointment_type().size() > 0) {
            holder.appointment_type.setVisibility(View.VISIBLE);
            Log.e("Tag Type", requestData.getAppointment_type().toString());
            holder.appointment_type.setText(Html.fromHtml(activity.getString(R.string.requestments) + requestData.getAppointment_type().toString().replaceAll("\\[|\\]", "")));
        } else {
            holder.appointment_type.setVisibility(View.GONE);
        }

        if (!requestData.getOil_brand().trim().equals("") || !requestData.getOil_brand().isEmpty()) {
            holder.appointment_oil_brand.setVisibility(View.VISIBLE);
            holder.appointment_oil_brand.setText(Html.fromHtml("<b>"+ activity.getString(R.string.oil_brand) +" </b> : " + requestData.getOil_brand()));
        } else {
            holder.appointment_oil_brand.setVisibility(View.GONE);
        }

        if (!requestData.getBattery_brand().trim().equals("") || !requestData.getBattery_brand().isEmpty()) {
            holder.appointment_battery_brand.setVisibility(View.VISIBLE);
            holder.appointment_battery_brand.setText(Html.fromHtml("<b>"+ activity.getString(R.string.battery_brand) +" </b> : " + requestData.getBattery_brand()));
        } else {
            holder.appointment_battery_brand.setVisibility(View.GONE);
        }

        if (!requestData.getTire_brand().trim().equals("") || !requestData.getTire_brand().isEmpty()) {
            holder.appointment_tire_brand.setVisibility(View.VISIBLE);
            holder.appointment_tire_brand.setText(Html.fromHtml("<b>"+ activity.getString(R.string.tire_brand) +" </b> : " + requestData.getTire_brand()));
        } else {
            holder.appointment_tire_brand.setVisibility(View.GONE);
        }

        if (!requestData.getTire_size_no().trim().equals("") || !requestData.getTire_size_no().isEmpty()) {
            holder.appointment_tire_size_no.setVisibility(View.VISIBLE);
            holder.appointment_tire_size_no.setText(Html.fromHtml("<b>"+ activity.getString(R.string.tire_size_no) +" </b> : " + requestData.getTire_size_no()));
        } else {
            holder.appointment_tire_size_no.setVisibility(View.GONE);
        }

        if (!requestData.getRims_brand().trim().equals("") || !requestData.getRims_brand().isEmpty()) {
            holder.appointment_rims_brand.setVisibility(View.VISIBLE);
            holder.appointment_rims_brand.setText(Html.fromHtml("<b>"+ activity.getString(R.string.rims_brand) +" </b> : " + requestData.getRims_brand()));
        } else {
            holder.appointment_rims_brand.setVisibility(View.GONE);
        }

        if (!requestData.getRims_size().trim().equals("") || !requestData.getRims_size().isEmpty()) {
            holder.appointment_rims_size.setVisibility(View.VISIBLE);
            holder.appointment_rims_size.setText(Html.fromHtml("<b>"+ activity.getString(R.string.rims_size) +" </b> : " + requestData.getRims_size()));
        } else {
            holder.appointment_rims_brand.setVisibility(View.GONE);
        }

        if (!requestData.getOil_sae().trim().equals("") || !requestData.getOil_sae().isEmpty()) {
            holder.appointment_oil_sae.setVisibility(View.VISIBLE);
            holder.appointment_oil_sae.setText(Html.fromHtml("<b>"+ activity.getString(R.string.oil_sae) +"</b> : " + requestData.getOil_sae()));
        } else {
            holder.appointment_oil_sae.setVisibility(View.GONE);
        }

        holder.llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, Act_User_Request_Detail.class);
                Constant.request_data = requestData;
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return garage_List.size();
    }

    @Override
    public int getItemViewType(int position) {
        return garage_List.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView user_name, txt_cart_model, txt_city, txt_problem, user_status, appointment_type, txt_cart_fule,
                appointment_oil_brand, appointment_tire_brand, appointment_oil_sae, appointment_tire_size_no,
                appointment_battery_brand, appointment_rims_brand, appointment_rims_size;
        CircleImageView user_image;
        LinearLayout llDetail;
        CardView car_detail;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            user_name = itemView.findViewById(R.id.name);
            txt_cart_model = itemView.findViewById(R.id.txt_cart_model);
            txt_city = itemView.findViewById(R.id.txt_city);
            txt_problem = itemView.findViewById(R.id.txt_problem);
            user_status = itemView.findViewById(R.id.user_status);
            txt_cart_fule = itemView.findViewById(R.id.txt_cart_fule);
            llDetail = itemView.findViewById(R.id.llDetail);
            car_detail = itemView.findViewById(R.id.car_detail);
            user_image = itemView.findViewById(R.id.user_image);
            appointment_type = itemView.findViewById(R.id.appointment_type);
            appointment_oil_brand = itemView.findViewById(R.id.appointment_oil_brand);
            appointment_tire_brand = itemView.findViewById(R.id.appointment_tire_brand);
            appointment_battery_brand = itemView.findViewById(R.id.appointment_battery_brand);
            appointment_oil_sae = itemView.findViewById(R.id.appointment_oil_sae);
            appointment_tire_size_no = itemView.findViewById(R.id.appointment_tire_size_no);
            appointment_rims_brand = itemView.findViewById(R.id.appointment_rims_brand);
            appointment_rims_size = itemView.findViewById(R.id.appointment_rims_size);
        }
    }

    private String getCapsSentences(String tagName) {
        String[] splits = tagName.toLowerCase().split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < splits.length; i++) {
            String eachWord = splits[i];
            if (i > 0 && eachWord.length() > 0) {
                sb.append(" ");
            }
            String cap = eachWord.substring(0, 1).toUpperCase()
                    + eachWord.substring(1);
            sb.append(cap);
        }
        return sb.toString();
    }
}
