package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Car_Model_model;
import com.kcs.automapgarage.Model.Car_brand_model;
import com.kcs.automapgarage.Model.Fuel_Model;
import com.kcs.automapgarage.Model.Garage_Brand_Model;
import com.kcs.automapgarage.Model.Manufacture_Country_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.CompoundButtonCompat;

import static com.kcs.automapgarage.Act_Login.LOGIN_PREF;
import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Edit_Car_Model extends AppCompatActivity implements View.OnClickListener {

    ImageView ed_pro_back;
    TextView car_model_title;
    LinearLayout main_lin_car_brand, lin_brand_all;
    List<Car_brand_model> car_brand_modelList = new ArrayList<>();
    List<Car_Model_model> car_model_modelList = new ArrayList<>();
    CardView edit_E_card;
    ArrayList<String> brand = new ArrayList<>();
    ArrayList<String> model = new ArrayList<>();
    //    List<Car_Model_model> result_list = new ArrayList<>();
    List<Car_Model_model> result_list_default = new ArrayList<>();
    List<Car_brand_model> car_brand_models = new ArrayList<>();
    LinearLayout main_lin_type_car;

    TextView txt_brand_title;
    GridLayout lin_dynamic_car_type, lin_dynamic_country, lin_dynamic_brand_check;

    LinearLayout main_lin_choose, rg_dynamic_choose;
    LinearLayout main_lin_choose_part, rg_dynamic_choose_part;
    LinearLayout main_lin_country;
    TextView txt_choose, txt_choose_part;
    List<Garage_Brand_Model> garage_brand_models = new ArrayList<>();

    ArrayList<String> part_type = new ArrayList<>();
    ArrayList<String> part_type_value = new ArrayList<>();
    ArrayList<String> provider_type = new ArrayList<>();
    ArrayList<String> provider_type_value = new ArrayList<>();

    ArrayList<Fuel_Model> fuel_models = new ArrayList<>();
    ArrayList<Fuel_Model> provider_fule = new ArrayList<>();

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String garage_id = "", choose = "", part_type_choose = "", provider_type_position = "", part_type_position = "";
    private boolean isEdit = false;
    int brand_select_limit = 0;

    List<Manufacture_Country_Model> manufacture_country_models;

    String service_type = "", service_id = "", manufacture_country_id = "", provider_service = "";

    ScrollView scroll_view;
    View lin_error;
    TextView txt_msg;
    Button btn_retry;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @SuppressLint({"CommitPrefEdits", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__edit__car__model);

        scroll_view = findViewById(R.id.scroll_view);
        lin_error = findViewById(R.id.lin_error);
        txt_msg = findViewById(R.id.txtError);
        btn_retry = findViewById(R.id.btnRetry);

        ed_pro_back = findViewById(R.id.ed_pro_back);
        car_model_title = findViewById(R.id.car_model_title);

        lin_dynamic_car_type = findViewById(R.id.grid_dynamic_car_type);
        main_lin_type_car = findViewById(R.id.main_lin_type_car);

        main_lin_car_brand = findViewById(R.id.main_lin_car_brand);
        lin_brand_all = findViewById(R.id.lin_brand_all);
        lin_dynamic_brand_check = findViewById(R.id.grid_dynamic_brand_check);

        main_lin_choose = findViewById(R.id.main_lin_choose);
        rg_dynamic_choose = findViewById(R.id.rg_dynamic_choose);
        txt_choose = findViewById(R.id.txt_choose);

        main_lin_country = findViewById(R.id.main_lin_country);
        lin_dynamic_country = findViewById(R.id.grid_dynamic_country);
        txt_brand_title = findViewById(R.id.txt_brand_title);

        main_lin_choose_part = findViewById(R.id.main_lin_choose_part);
        rg_dynamic_choose_part = findViewById(R.id.rg_dynamic_choose_part);
        txt_choose_part = findViewById(R.id.txt_choose_part);

        edit_E_card = findViewById(R.id.edit_E_card);

        ed_pro_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        sharedpreferences = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        garage_id = sharedpreferences.getString(getString(R.string.pref_garage_id), "");
//        garage_id = "4";
        service_type = sharedpreferences.getString(getString(R.string.pref_special_service_name), "");
//        service_type = "Mechanic shop";
        service_id = sharedpreferences.getString(getString(R.string.pref_service_id), "");
//        service_id = "18";
        manufacture_country_id = sharedpreferences.getString(getString(R.string.pref_manufacturing_country_id), "");
//        manufacture_country_id = "1";
        provider_service = sharedpreferences.getString(getString(R.string.pref_provider_service), "");
        Log.e("TAG garage_id", garage_id);
        Log.e("TAG Service Type", service_type);
        init();
        if (getIntent().getBooleanExtra("SPLASH", false)) {
//            ed_pro_back.setVisibility(View.GONE);
        }

        btn_retry.setOnClickListener(this);
        car_model_title.setText(getString(R.string.workshop_speciality_setting));

        // service part shop
        if (service_id.equals("4")) {
            /*if (isEdit) {
                car_model_title.setText(getString(R.string.Edit_car_model));
            } else {
                car_model_title.setText(getString(R.string.add_car_model));
            }*/
            main_lin_type_car.setVisibility(View.VISIBLE);
            main_lin_choose_part.setVisibility(View.VISIBLE);
            main_lin_car_brand.setVisibility(View.VISIBLE);
            main_lin_country.setVisibility(View.VISIBLE);
            main_lin_choose.setVisibility(View.GONE);

            GetGarageCarBrandModelData();
            getEnumType();
            brand_select_limit = 5;

        }
        // service workshop
        else if (service_id.equals("18")) {

            /*if (isEdit) {
                car_model_title.setText(getString(R.string.Edit_car_model));
            } else {
                car_model_title.setText(getString(R.string.add_car_model));
            }*/
            main_lin_type_car.setVisibility(View.VISIBLE);
            main_lin_choose_part.setVisibility(View.GONE);
            main_lin_car_brand.setVisibility(View.VISIBLE);
            main_lin_choose.setVisibility(View.GONE);
            main_lin_choose.setVisibility(View.VISIBLE);
            main_lin_country.setVisibility(View.VISIBLE);
            GetGarageCarBrandModelData();
            getEnumType();
            brand_select_limit = 3;
        }
        // service other service
        else if ( service_id.equals("8") || service_id.equals("10") || service_id.equals("12") || service_id.equals("13")
                || provider_service.equalsIgnoreCase("true")) {
            /*if (isEdit) {
                car_model_title.setText(getString(R.string.choos_provider_type));
            } else {
                car_model_title.setText(getString(R.string.choos_provider_type));
            }
            car_model_title.setVisibility(View.GONE);

            main_lin_country.setVisibility(View.GONE);
            main_lin_type_car.setVisibility(View.GONE);
            main_lin_car_brand.setVisibility(View.GONE);
            main_lin_choose_part.setVisibility(View.GONE);
            main_lin_choose.setVisibility(View.VISIBLE);*/

//            getEnumType();
            Intent intent = new Intent(getApplicationContext(), Act_Edit_Profile_Detail.class);
            intent.putExtra(getString(R.string.pref_garage_id), garage_id);
            startActivity(intent);
            finish();
//            ChooseType();

        } else {
            Intent intent = new Intent(getApplicationContext(), Act_Edit_Profile_Detail.class);
            intent.putExtra(getString(R.string.pref_garage_id), garage_id);
            startActivity(intent);
            finish();
        }

        edit_E_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean selected = false;
                /*if (car_brand_models != null) {
                    for (Car_brand_model model : car_brand_models) {
                        Log.e("Tag Result", model.getBrand_id() + "  " + model.getIsChecked());
                        if (model.getIsChecked().equals("1")) {
                            Log.e("TAG", "" + model.getBrand_nm());
                            selected = true;
                            break;
                        }
                    }
                }*/

                // car brand validation
                if (car_brand_modelList != null) {
                    for (Car_brand_model model : car_brand_modelList) {
                        Log.e("Tag Result", model.getBrand_id() + "  " + model.getIsChecked());
                        if (model.getIsChecked().equals("1")) {
                            Log.e("TAG", "" + model.getBrand_nm());
                            selected = true;
                            break;
                        }
                    }
                }

                // fuel type validation
                boolean selected_type = false;
                if (fuel_models != null && main_lin_type_car.getVisibility() == View.VISIBLE) {
                    for (Fuel_Model fuel_model : fuel_models) {
                        Log.e("Tag Result", fuel_model.getFuel_type() + "  " + fuel_model.getIsChecked());
                        if (fuel_model.getIsChecked().equals("1")) {
                            selected_type = true;
                            break;
                        }
                    }
                }

                if (!selected && main_lin_car_brand.getVisibility() == View.VISIBLE) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_select_car_brand_model), Toast.LENGTH_LONG).show();
                } else if (!selected_type && (main_lin_type_car.getVisibility() == View.VISIBLE || main_lin_country.getVisibility() == View.VISIBLE)) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_select_provider_fuel_type_car_service), Toast.LENGTH_LONG).show();
                } else {
                    AddEditCarModel();

                }
            }
        });
    }

    public void init() {

        Intent intent = getIntent();
        if (intent != null) {
            isEdit = intent.getBooleanExtra("EDIT", false);
        }
    }

    int count = 0;

    // get manufacture country , model, brand and car fuel data and set view
    private void retrieveJSON() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        if (!isFinishing() ){
            dialog.show();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "car_detail", new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                if (!isFinishing()){
                    dialog.dismiss();
                }
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        count = 0;
                        car_brand_modelList = new ArrayList<>();
                        car_model_modelList = new ArrayList<>();

//                        result_list = new ArrayList<>();
                        JSONObject object_result = obj.getJSONObject("result");

                        JSONArray carBrand = object_result.getJSONArray("car_brand");
                        for (int i = 0; i < carBrand.length(); i++) {

                            Car_brand_model car_brand_model = new Car_brand_model();
                            JSONObject c_brand = carBrand.getJSONObject(i);
                            car_brand_model.setBrand_id(c_brand.getString("brand_id"));
                            car_brand_model.setBrand_nm(check(c_brand,"brand_name"));
                            car_brand_model.setManufacture_country_id(c_brand.getString("manufacture_country_id"));
                            car_brand_model.setCreated_at(c_brand.getString("created_at"));
                            car_brand_model.setUpdated_at(c_brand.getString("updated_at"));
                            car_brand_model.setIsChecked("0");
                            car_brand_modelList.add(car_brand_model);
                        }

                        JSONArray car_model = object_result.getJSONArray("car_model");
                        for (int i = 0; i < car_model.length(); i++) {

                            Car_Model_model car_model_model = new Car_Model_model();
                            JSONObject c_model = car_model.getJSONObject(i);
                            car_model_model.setModel_id(c_model.getString("model_id"));
                            car_model_model.setBrand_id(c_model.getString("brand_id"));
                            car_model_model.setManufacture_country_id(c_model.getString("manufacture_country_id"));
                            car_model_model.setModel_nm(check(c_model,"model_name"));
                            car_model_model.setModel_year(c_model.getString("model_year"));
                            car_model_model.setCreated_at(c_model.getString("created_at"));
                            car_model_model.setUpdated_at(c_model.getString("updated_at"));
                            car_model_modelList.add(car_model_model);
                        }

                        fuel_models = new ArrayList<>();
                        JSONArray fuel_data = object_result.getJSONArray("car_fule");
                        for (int i = 0; i < fuel_data.length(); i++) {

                            JSONObject c_model = fuel_data.getJSONObject(i);
                            Fuel_Model fuel_model = new Fuel_Model();
                            fuel_model.setFuel_id(c_model.getString("fule_id"));
                            fuel_model.setFuel_type(check(c_model,"fule_type"));
                            fuel_model.setIsChecked("0");
                            fuel_models.add(fuel_model);
                        }
                        TypeCar(fuel_models);

                        manufacture_country_models = new ArrayList<>();
                        JSONArray m_country = object_result.getJSONArray("manufacture_country");
                        for (int i = 0; i < m_country.length(); i++) {
                            JSONObject object = m_country.getJSONObject(i);
                            Manufacture_Country_Model model = new Manufacture_Country_Model();
                            model.setMc_id(object.getString("manufacture_country_id"));
                            model.setMc_name(check(object,"country_name"));
                            model.setIsChecked("0");
                            manufacture_country_models.add(model);
                        }
                        DynamicCountryAdd(manufacture_country_models);

                    }
//                    GetGarageCartBrandModelData();
                    Log.e("Tag 1", "" + car_brand_modelList.size());
                    Log.e("Tag 2", "" + car_model_modelList.size());
//                    DynamicBrandAdd(car_brand_modelList);
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (!isFinishing()){
                        dialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (!isFinishing()){
                    dialog.dismiss();
                }
                Log.e("Tag String", error.toString());
//                error_dialog();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    // get select car brand model and set default selected
    private void GetGarageCarBrandModelData() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (!isFinishing()){
            dialog.show();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_garage_brand_model", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                if (!isFinishing()){
                    dialog.dismiss();
                }

                try {
                    brand = new ArrayList<>();
                    model = new ArrayList<>();
//                    result_list = new ArrayList<>();
                    JSONObject obj = new JSONObject(response);
                    garage_brand_models = new ArrayList<>();
                    result_list_default = new ArrayList<>();
                    if (obj.optString("status").equals("1")) {
                        JSONObject result_object = obj.getJSONObject("result");

                        // default brand selected
                        if (result_object.has("brand_model")) {

                            Object o = result_object.get("brand_model");
                            if (o instanceof JSONArray) {
                                JSONArray jsonArray = result_object.getJSONArray("brand_model");

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                    }
                                });
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String garage_brand_model_id = object.getString("garage_brand_model_id");
                                    String garage_id = object.getString("garage_id");
                                    String manufacture_country_id = object.getString("manufacture_country_id");
                                    String model_id = object.getString("model_id");
                                    String brand_id = object.getString("brand_id");
                                    String created_at = object.getString("created_at");
                                    String updated_at = object.getString("updated_at");

                                    garage_brand_models.add(new Garage_Brand_Model(garage_brand_model_id, garage_id, model_id, brand_id, created_at, updated_at));

                                    if (!brand.contains(brand_id)) {
                                        brand.add(brand_id);
                                        result_list_default.add(new Car_Model_model(model_id, brand_id, manufacture_country_id, "", "", "", "", "0"));
                                    }

                                    if (!model.contains(model_id)) {
                                        model.add(model_id);
                                    }
                                    editor.putBoolean("car_model_brand", true);
                                }
                            }
                        }

                        // default fule selected
                        if (result_object.has("garage_fule")) {

                            Object o = result_object.get("garage_fule");

                            if (o instanceof JSONArray) {

                                JSONArray fule_array = result_object.getJSONArray("garage_fule");

                                for (int i = 0; i < fule_array.length(); i++) {
                                    JSONObject object = fule_array.getJSONObject(i);
                                    Fuel_Model fuel_model = new Fuel_Model();
                                    fuel_model.setFuel_id(object.getString("fule_id"));
//                                  provider_fuel.get(i).setFuel_id(object.getString("fuel_id"));
                                    provider_fule.add(fuel_model);
//                                provider_fuel.get(i).setFuel_type(object.getString("fuel_type"));
                                }
                            }
                        }
                    }
                    retrieveJSON();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (!isFinishing()){
                        dialog.dismiss();
                    }
                    scroll_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.something_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // displaying the error in toast if occurs
                // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Tag Error", error.toString());
                if (!isFinishing()){
                    dialog.dismiss();
                }
                scroll_view.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_msg.setText(AppUtils.serverError(getApplicationContext(), error));
//                txt_msg.setText(getString(R.string.something_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                map.put(language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("params", " " + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    // add selected card details
    private void AddEditCarModel() {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        if (!isFinishing()){
            dialog.show();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "add_edit_garage_brand_model", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);

                try {
                    response = Html.fromHtml(response).toString();
                    response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {
//                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), getString(R.string.model), Toast.LENGTH_LONG).show();
                        editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_provider_type), provider_type_position);
                        editor.putString(getString(R.string.pref_part_type_choose), part_type_position);
                        editor.putString(getString(R.string.pref_manufacturing_country_id), manufacture_country_id);
                        editor.apply();
                        if (isEdit) {
                            onBackPressed();
                        } else {
                            if (service_id.equals("61")){
                                Intent intent = new Intent(getApplicationContext(), Navigation_Activity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), Act_Edit_Profile_Detail.class);
                                intent.putExtra(getString(R.string.pref_garage_id), garage_id);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    if (!isFinishing()){
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (!isFinishing()){
                        dialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                if (!isFinishing()){
                    dialog.dismiss();
                }
                System.out.println("Tag_Exception"+ error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                map.put(language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));

                if (!provider_type_position.equals("") && main_lin_choose.getVisibility() == View.VISIBLE) {
                    map.put("provider_type", choose);
                    editor.putString(getString(R.string.pref_provider_type), provider_type_position).apply();
                }

                if (!part_type_position.equals("") && main_lin_choose_part.getVisibility() == View.VISIBLE) {
                    map.put("part_type", part_type_choose);
                    editor.putString(getString(R.string.pref_part_type), part_type_position).apply();
                    editor.putString(getString(R.string.pref_part_type_choose), part_type_choose).apply();
                }

                if (main_lin_type_car.getVisibility() == View.VISIBLE) {

                    JSONObject object1 = new JSONObject();
                    JSONArray array1 = new JSONArray();
                    for (int k = 0; k < fuel_models.size(); k++) {
                        if (fuel_models.get(k).getIsChecked().equals("1")) {
                            JSONObject obj = new JSONObject();
                            try {
                                obj.put("fule_id", fuel_models.get(k).getFuel_id());
//                                obj.put("fuel_type", fuel_models.get(k).getFuel_type());
                                array1.put(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    try {
                        object1.put("fule_type_data", array1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    map.put("fule_json", "" + object1);
                }

                JSONObject object = new JSONObject();
                JSONArray array = new JSONArray();
//                for (int i = 0; i < car_brand_models.size(); i++) {
//                    Log.e("Tag Brand Id", car_brand_models.get(i).getBrand_nm());
//                    if (car_brand_models.get(i).getIsChecked().equals("1") && !car_brand_models.get(i).getBrand_id().equals("")) {

                Log.e("Tag brand Size", ""+car_brand_modelList.size());
                Log.e("Tag Model Size", ""+car_model_modelList.size());
                List<String> manu_country = new ArrayList<>();
                if (car_brand_modelList.size()>0){

                    for (int i=0; i<car_brand_modelList.size(); i++){

                        if (car_brand_modelList.get(i).getIsChecked().equalsIgnoreCase("1")){

                            for (int j = 0; j < car_model_modelList.size(); j++) {

                                if (car_brand_modelList.get(i).getBrand_id().equals(car_model_modelList.get(j).getBrand_id()) /*||
                        car_brand_models.get(i).getBrand_nm().equals(car_model_modelList.get(j).getBrand_nm())*/) {

                                    JSONObject obj = new JSONObject();
                                    try {
//                                        Log.e("Tag brand name", car_model_modelList.get(j).getBrand_nm());
//                                        Log.e("Tag brand name", car_model_modelList.get(j).getBrand_id());
                                        obj.put("brand_id", car_model_modelList.get(j).getBrand_id());
                                        obj.put("model_id", car_model_modelList.get(j).getModel_id());
                                        array.put(obj);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (!manu_country.contains(car_model_modelList.get(j).getManufacture_country_id())){
                                        manu_country.add(car_model_modelList.get(j).getManufacture_country_id());
                                    }
                                }
                            }
                        }
                    }

                } else {
                    /*JSONObject obj = new JSONObject();
                    try {

                        Log.e("Tag brand name", car_brand_models.get(i).getBrand_nm());
                        Log.e("Tag brand name", car_brand_models.get(i).getBrand_id());
                        obj.put("brand_id", car_brand_models.get(i).getBrand_id());
                        obj.put("model_id", "");
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }
//                    }
//                }

                try {
                    object.put("brand_model_data", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSONObject jsonObject = new JSONObject();
                JSONArray m_array = new JSONArray();
                if (manu_country != null && manu_country.size()>0){

                    for (int m=0; m<manu_country.size(); m++){
                        JSONObject object1 = new JSONObject();
                        try {
                            object1.put("manufacture_country_id", manu_country.get(m));
                            m_array.put(object1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    try {
                        jsonObject.put("manufacture_country_data", m_array);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (main_lin_country.getVisibility() == View.VISIBLE) {
                        map.put("manufacture_country_id", manu_country.get(0));
                        if (object != null) {
                            map.put("manufacture_country_json", "" + jsonObject);
                        }
                    }
                }

                if (object != null) {
                    map.put("brand_json", "" + object);
                }

//                Log.e("params", " " + map);
                final int chunkSize = 2048;
                for (int i = 0; i < map.toString().length(); i += chunkSize) {
                    Log.d("params", map.toString().substring(i, Math.min(map.toString().length(), i + chunkSize)));
                }
                return map;
//                return new HashMap<>();
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    // get the part type and provider type data and default selection
    private void getEnumType() {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        if (!isFinishing()){
            dialog.dismiss();
        }
        StringRequest request = new StringRequest(Request.Method.GET, Constant.URL + "get_enum", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag response", response.toString());

                try {
                    JSONObject object = new JSONObject(response);

                    JSONArray array = object.getJSONArray("result");

                    provider_type = new ArrayList<>();
                    provider_type_value = new ArrayList<>();
                    part_type = new ArrayList<>();
                    part_type_value = new ArrayList<>();
                    ArrayList<String> part = new ArrayList<>();
                    ArrayList<String> provider = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object1 = array.getJSONObject(i);

                        if (object1.getString("type").equalsIgnoreCase("Part Type")) {
//                            part.add(object1.getString("value"));
                            part_type.add(object1.getString("enum_id"));
                            String p_id = sharedpreferences.getString(getString(R.string.pref_part_type), "");
                            if (object1.getString("enum_id").equals("31")){
                                part_type_value.add(check(object1,"value"));
                                part.add(getString(R.string.new_part));
                            } else if (object1.getString("enum_id").equals("32")){
                                part_type_value.add(check(object1,"value"));
                                part.add(getString(R.string.used_part));
                            } else if (object1.getString("enum_id").equals("33")){
                                part_type_value.add(check(object1,"value"));
                                part.add(getString(R.string.new_and_used_part));
                            }

                            if (p_id.equals("31")){
                                editor.putString(getString(R.string.pref_part_type_choose), getString(R.string.new_part)).apply();
                            } else if (p_id.equals("32")){
                                editor.putString(getString(R.string.pref_part_type_choose), getString(R.string.used_part)).apply();
                            } else if (p_id.equals("33")){
                                editor.putString(getString(R.string.pref_part_type_choose), getString(R.string.new_and_used_part)).apply();
                            }
                        }

                        if (object1.getString("type").equalsIgnoreCase("Provider Type")) {
//                            provider.add(object1.getString("value"));
                            provider_type.add(object1.getString("enum_id"));

                            if (object1.getString("enum_id").equals("35")){
                                provider_type_value.add(check(object1,"value"));
                                provider.add(getString(R.string.i_provide_the_service_in_my_workshop_only));
                            }else if (object1.getString("enum_id").equals("34")){
                                provider_type_value.add(check(object1,"value"));
                                provider.add(getString(R.string.i_provide_the_service_in_my_workshop_and_in_the_customers_site));
                            }
                        }
                    }
                    ChoosePartType(part);
                    ChooseType(provider);

                    scroll_view.setVisibility(View.VISIBLE);
                    lin_error.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    scroll_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.something_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                }
                if (!isFinishing()){
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                scroll_view.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_msg.setText(AppUtils.serverError(getApplicationContext(), error));
//                txt_msg.setText(getString(R.string.something_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
                if (!isFinishing()){
                    dialog.dismiss();
                }
                Log.e("Tag Error", error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    // listing view or radio button country
    @SuppressLint("ResourceType")
    public void DynamicCountryAdd(List<Manufacture_Country_Model> models) {

        lin_dynamic_country.removeAllViews();
        int row = 1;
        if (models != null && models.size() > 3 && (models.size() / 3) > 0) {
            row = Math.round(models.size() / 3);
            Log.e("Tag Result", "" + row);
            Log.e("Tag Result", "" + models.size() / 3);
        }
        Log.e("Tag Result", "" + models.size());
        lin_dynamic_country.setRowCount(row);
        lin_dynamic_country.setColumnCount(3);

        for (int i = 0; i < models.size(); i++) {

            final CheckBox checkBox = new CheckBox(Act_Edit_Car_Model.this);
            checkBox.setText(models.get(i).getMc_name());
            checkBox.setId(i);
            checkBox.setTextColor(Color.BLACK);
            checkBox.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            checkBox.setButtonDrawable(Color.TRANSPARENT);
            checkBox.setElevation(5f);
            checkBox.setBackground(getResources().getDrawable(R.drawable.background_shadow));
            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            checkBox.setLayoutParams(par);

            if (manufacture_country_id != null && !manufacture_country_id.equals("") && !manufacture_country_id.equals("0")) {

                Log.e("Tag Manufacture id", models.get(i).getMc_id() + " " + manufacture_country_id);

                if (manufacture_country_id.equals(models.get(i).getMc_id())) {
                    models.get(i).setIsChecked("1");
                    checkBox.setChecked(true);
                    txt_brand_title.setText(checkBox.getText().toString());
                    checkBox.setTextColor(getResources().getColor(R.color.navy));
                    checkBox.setTypeface(null, Typeface.BOLD);
                }

                car_brand_models = new ArrayList<>();
                car_brand_models.add(new Car_brand_model("", getString(R.string.select_all), "", "", "", ""));
                for (int c = 0; c < models.size(); c++) {
                    if (models.get(c).getIsChecked().equals("1")) {
                        for (int b = 0; b < car_brand_modelList.size(); b++) {
                            if (models.get(c).getMc_id().equals(car_brand_modelList.get(b).getManufacture_country_id())) {
                                car_brand_models.add(car_brand_modelList.get(b));
                            }
                        }
                    }
                }

                DynamicBrandAdd(car_brand_models);
            } else if (i == 0) {
                manufacture_country_id = models.get(i).getMc_id();
                models.get(i).setIsChecked("1");
                checkBox.setChecked(true);
                txt_brand_title.setText(checkBox.getText().toString());
//                checkBox.setBackground(getResources().getDrawable(R.drawable.round_corner_border));
                checkBox.setTextColor(getResources().getColor(R.color.navy));
                checkBox.setTypeface(null, Typeface.BOLD);
                car_brand_models = new ArrayList<>();
                car_brand_models.add(new Car_brand_model("", getString(R.string.select_all), "", "", "", ""));
                for (int c = 0; c < models.size(); c++) {
                    if (models.get(c).getIsChecked().equals("1")) {
                        for (int b = 0; b < car_brand_modelList.size(); b++) {
                            if (models.get(c).getMc_id().equals(car_brand_modelList.get(b).getManufacture_country_id())) {
                                car_brand_models.add(car_brand_modelList.get(b));
                            }
                        }
                    }
                }

                DynamicBrandAdd(car_brand_models);
            }
            Typeface pt_sans_caption = ResourcesCompat.getFont(getApplicationContext(),R.font.pt_sans_caption);
            final int finalI = i;
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.e("Tag Checked", "" + isChecked);

                    for (int c = 0; c < lin_dynamic_country.getChildCount(); c++) {
                        if (isChecked) {
                            View view = lin_dynamic_country.getChildAt(c);
                            ((CheckBox) view).setTextColor(getResources().getColor(R.color.black));
                            try {
                                ((CheckBox) view).setTypeface(pt_sans_caption);
                            }catch (Exception e){
                                ((CheckBox) view).setTypeface(null,Typeface.NORMAL);
                            }
                            ((CheckBox) view).setElevation(3);
                            ((CheckBox) view).setChecked(false);
                            models.get(c).setIsChecked("0");
                        }
                    }

                    if (isChecked) {
                        manufacture_country_id = models.get(finalI).getMc_id();
                        checkBox.setChecked(true);
                        models.get(finalI).setIsChecked("1");
                        checkBox.setTextColor(getResources().getColor(R.color.navy));
                        checkBox.setTypeface(null, Typeface.BOLD);
                        txt_brand_title.setText(buttonView.getText().toString());
                        Log.e("Tag size Add After", "" + models.get(finalI).getMc_name());
                    }

                    if (isChecked) {
                        car_brand_models = new ArrayList<>();
                        car_brand_models.add(new Car_brand_model("", getString(R.string.select_all), "", "", "", ""));
                        for (int c = 0; c < models.size(); c++) {
                            if (models.get(c).getIsChecked().equals("1")) {
                                for (int b = 0; b < car_brand_modelList.size(); b++) {
                                    Log.e("Check Tag Set ", "" + car_brand_modelList.get(c).getIsChecked());
                                    if (models.get(c).getMc_id().equals(car_brand_modelList.get(b).getManufacture_country_id())) {
                                        car_brand_models.add(car_brand_modelList.get(b));
                                    }
                                }
                            }
                        }
                        DynamicBrandAdd(car_brand_models);
                    }
                }
            });

            GridLayout.LayoutParams params = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                    GridLayout.spec(GridLayout.UNDEFINED, 1f));
            params.width = 0;
            lin_dynamic_country.addView(checkBox, params);
        }
    }

    // listing check box of brand data and default selected
    public void DynamicBrandAdd(List<Car_brand_model> car_brand_List) {

        lin_dynamic_brand_check.removeAllViews();
        lin_brand_all.removeAllViews();

        int row = 1;
        if (car_brand_List != null && car_brand_List.size() > 3 && (car_brand_List.size() / 3) > 0) {
            row = Math.round(car_brand_List.size() / 3);
            Log.e("Tag Result", "" + row);
            Log.e("Tag Result", "" + car_brand_List.size() / 3);
        }
        Log.e("Tag Result", "" + car_brand_List.size());
        lin_dynamic_brand_check.setRowCount(row);
        lin_dynamic_brand_check.setColumnCount(3);

        boolean isAll = true;
        int default_size = 0;
        for (int i = 0; i < car_brand_List.size(); i++) {

            final CheckBox checkBox = new CheckBox(Act_Edit_Car_Model.this);
            checkBox.setText(car_brand_List.get(i).getBrand_nm());
            checkBox.setTextColor(Color.WHITE);
            checkBox.setSelected(true);
            checkBox.setSingleLine();
            checkBox.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            checkBox.setMarqueeRepeatLimit(3);
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[] = {Color.WHITE, Color.WHITE};
            CompoundButtonCompat.setButtonTintList(checkBox, new ColorStateList(states, colors));
//            checkBox.setLayoutParams(params);

            if (result_list_default != null && result_list_default.size()>0){
                int r = 0;

                for (Car_Model_model model1 : result_list_default){
                    for (Car_brand_model  model2 : car_brand_modelList) {
                        if (model1.getBrand_id().equalsIgnoreCase(model2.getBrand_id())) {
                            model2.setIsChecked("1");
                        }
                    }
                    if (r == (result_list_default.size()-1)){
                        result_list_default = null;
                        break;
                    }
                    r++;
                }
            }

            List<Car_Model_model> selected_list_default = new ArrayList<>();
            for (Car_brand_model c_model : car_brand_modelList) {

                if (c_model.getIsChecked().equalsIgnoreCase("1")){
                    if (c_model.getBrand_id().equalsIgnoreCase(car_brand_List.get(i).getBrand_id())){
//                        car_brand_List.get(i).setIsChecked("1");
//                        checkBox.setChecked(true);
                        Car_Model_model model = new Car_Model_model();
                        model.setBrand_nm(c_model.getBrand_nm());
                        model.setBrand_id(c_model.getBrand_id());
                        model.setModel_nm("");
                        model.setModel_id("");
                        model.setManufacture_country_id(c_model.getManufacture_country_id());
                        model.setIsChecked(c_model.getIsChecked());

                        selected_list_default.add(model);
                    }
                    Log.e("Tag Brand Name", "" + c_model.getBrand_nm());
                }
            }

            if (selected_list_default != null && selected_list_default.size() > 0) {

                for (Car_Model_model news : selected_list_default) {
                    Log.e("Tag size Remove Before", news.getBrand_id() + " " + car_brand_List.get(i).getBrand_id());

                    if (!news.getModel_nm().equals(getString(R.string.select_all)) && news.getBrand_id().equals(car_brand_List.get(i).getBrand_id())) {
                        car_brand_List.get(i).setIsChecked("1");
                        checkBox.setChecked(true);
                        default_size++;
                    }
                }

                Log.e("Tag Size", car_brand_List.size() + "  " + (i + 1));
                if (car_brand_List.size() == (i + 1)) {
                    for (int m = 0; m < car_brand_List.size(); m++) {
                        Log.e("Tag Is Checked", car_brand_List.get(m).getIsChecked());

                        if (!car_brand_List.get(m).getBrand_nm().equals(getString(R.string.select_all)) && !car_brand_List.get(m).getIsChecked().equals("1")) {
                            isAll = false;
                            break;
                        }
                    }
                    if (isAll) {
                        for (int l = 0; l < lin_dynamic_brand_check.getChildCount(); l++) {
                            View view = lin_dynamic_brand_check.getChildAt(l);
                            ((CheckBox) view).setChecked(true);
                            ((CheckBox)lin_brand_all.getChildAt(0)).setChecked(true);
                        }
                    }
                }
            }

            final int finalI = i;
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.e("Tag size Add After", "" + buttonView.getText());
                    /*if (count == brand_select_limit && isChecked) {
                        buttonView.setChecked(false);
                        Toast.makeText(getApplicationContext(), "Max" + brand_select_limit + "Selected", Toast.LENGTH_SHORT).show();
                    } else*/

                    if (isChecked && buttonView.getText().toString().equals(getString(R.string.select_all))) {
                        for (int i = 0; i < lin_dynamic_brand_check.getChildCount(); i++) {
                            if (lin_dynamic_brand_check.getChildCount()+1 != i){
                                View view = lin_dynamic_brand_check.getChildAt(i);
                                ((CheckBox) view).setChecked(true);
                            }
                            ((CheckBox)lin_brand_all.getChildAt(0)).setChecked(true);
                            car_brand_List.get(i).setIsChecked("1");

                            for (int j=0; j<car_brand_modelList.size(); j++){
//                                Log.e("c_b Tag Brand Name New", "" + car_brand_List.get(i).getBrand_id());
//                                Log.e("c_b Brand Name", "" + car_brand_modelList.get(j).getBrand_id());
                                if (car_brand_modelList.get(j).getBrand_id().equalsIgnoreCase(car_brand_List.get(i).getBrand_id())){
                                    car_brand_modelList.get(j).setIsChecked("1");
                                }
                            }
                        }

                    } else if (!isChecked && buttonView.getText().toString().equals(getString(R.string.select_all))) {
                        for (int i = 0; i < lin_dynamic_brand_check.getChildCount(); i++) {
                            if (lin_dynamic_brand_check.getChildCount()+1 != i){
                                View view = lin_dynamic_brand_check.getChildAt(i);
                                ((CheckBox) view).setChecked(false);
                            }
                            ((CheckBox)lin_brand_all.getChildAt(0)).setChecked(false);
                            car_brand_List.get(i).setIsChecked("0");
                            for (int j=0; j<new Act_Edit_Car_Model().car_brand_modelList.size(); j++){
//                                Log.e("c_b Tag Brand Name New", "" + car_brand_List.get(i).getBrand_id());
//                                Log.e("c_b Brand Name", "" + car_brand_modelList.get(j).getBrand_id());
                                if (car_brand_modelList.get(j).getBrand_id().equalsIgnoreCase(car_brand_List.get(i).getBrand_id())){
                                    car_brand_modelList.get(j).setIsChecked("0");
                                }
                            }
                        }
                    } else if (isChecked) {
                        checkBox.setEnabled(true);
                        car_brand_List.get(finalI).setIsChecked("1");
                        for (int j=0; j<new Act_Edit_Car_Model().car_brand_modelList.size(); j++){
//                            Log.e("c_b Tag Brand Name New", "" + car_brand_List.get(finalI).getBrand_id());
//                            Log.e("c_b Brand Name", "" + car_brand_modelList.get(j).getBrand_id());
                            if (car_brand_modelList.get(j).getBrand_id().equalsIgnoreCase(car_brand_List.get(finalI).getBrand_id())){
                                car_brand_modelList.get(j).setIsChecked("1");
                            }
                        }
                        Log.e("Tag size Add After", "" + car_brand_List.get(finalI).getBrand_id());
                        count++;
                    } else if (!isChecked) {

                        car_brand_List.get(finalI).setIsChecked("0");
                        for (int j=0; j<new Act_Edit_Car_Model().car_brand_modelList.size(); j++){
//                            Log.e("c_b Tag Brand Name New", "" + car_brand_List.get(finalI).getBrand_id());
//                            Log.e("c_b Brand Name", "" + car_brand_modelList.get(j).getBrand_id());
                            if (car_brand_modelList.get(j).getBrand_id().equalsIgnoreCase(car_brand_List.get(finalI).getBrand_id())){
                                car_brand_modelList.get(j).setIsChecked("0");
                            }
                        }
                        count--;
                    }

                    boolean allCheck = true;
                    for (int i = 0; i < lin_dynamic_brand_check.getChildCount(); i++) {
//                        if (i != 0) {
                            View view = lin_dynamic_brand_check.getChildAt(i);
                            if (((CheckBox) view).isChecked()) {
                                allCheck = true;
                            } else {
                                allCheck = false;
                                break;
                            }
//                        }
                    }
                    for (int i = 0; i < lin_dynamic_brand_check.getChildCount(); i++) {
                        View view = lin_dynamic_brand_check.getChildAt(i);
                        if (buttonView.getText().equals(getString(R.string.select_all))) {
                            if (allCheck) {
                                ((CheckBox) view).setChecked(true);
                                ((CheckBox)lin_brand_all.getChildAt(0)).setChecked(true);
                            } else {
                                ((CheckBox) view).setOnCheckedChangeListener(null);
                                ((CheckBox) view).setChecked(false);
                                ((CheckBox) view).setOnCheckedChangeListener(this);
                            }
                        } else if (allCheck){
                            ((CheckBox)lin_brand_all.getChildAt(0)).setChecked(true);
                        } else if (!allCheck){
                            ((CheckBox)lin_brand_all.getChildAt(0)).setOnCheckedChangeListener(null);
                            ((CheckBox)lin_brand_all.getChildAt(0)).setChecked(false);
                            ((CheckBox)lin_brand_all.getChildAt(0)).setOnCheckedChangeListener(this);
                        }
                        if (((CheckBox) view).isChecked()) {
                            ((CheckBox) view).setChecked(true);
                        }
                    }

                    for (int p=0; p<car_brand_List.size(); p++){
                        if (car_brand_List.get(p).getIsChecked().equals("1")){
                            Log.e("Tag_car Check Brand", "" + car_brand_List.get(p).getBrand_nm());
                        }
                    }

                    for (int j=0; j<car_brand_modelList.size(); j++){
                        if (car_brand_modelList.get(j).getIsChecked().equalsIgnoreCase("1")){
                            Log.e("Tag Selected Brand Name", "" + car_brand_modelList.get(j).getBrand_nm());
                        }
                    }

                    Log.e("Tag Result Default Size", "" + selected_list_default.size());
                }
            });

            GridLayout.LayoutParams par = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                    GridLayout.spec(GridLayout.UNDEFINED, 1f));
            par.width = 0;
            par.topMargin = 20;
            par.bottomMargin = 20;

            if (i == 0){
                lin_brand_all.addView(checkBox);
            }else {
                lin_dynamic_brand_check.addView(checkBox, par);
            }
        }
    }

    public void AddRemove(Car_Model_model model, boolean isAdd) {

        if (isAdd) {
            boolean Add = false;
            for (int i = 0; i < result_list_default.size(); i++) {
                if (model.getBrand_id().equals(result_list_default.get(i).getBrand_id())) {
                    Add = true;
                    break;
                }
            }
            if (!Add) {
                result_list_default.add(model);
            }

        } else {
            for (int i = 0; i < result_list_default.size(); i++) {
                if (model.getBrand_id().equals(result_list_default.get(i).getBrand_id())) {
                    result_list_default.remove(i);
                }
            }
        }
    }

    // list all type of car fule and default selected
    public void TypeCar(ArrayList<Fuel_Model> strings) {

        /*ArrayList<String> strings = new ArrayList<>();
        strings.add("Hybrid");
        strings.add("Electric");
        strings.add("Gasoline");
        strings.add("Diesel");*/

        lin_dynamic_car_type.removeAllViews();
        int row = 1;
        if (strings != null && strings.size() > 3 && (strings.size() / 3) > 0) {
            row = Math.round(strings.size() / 3);
            Log.e("Tag Result", "" + row);
            Log.e("Tag Result", "" + strings.size() / 3);
        }
        Log.e("Tag Result", "" + strings.size());
        lin_dynamic_car_type.setRowCount(row);
        lin_dynamic_car_type.setColumnCount(3);
        for (int i = 0; i < strings.size(); i++) {
            AppCompatCheckBox checkBox1 = new AppCompatCheckBox(Act_Edit_Car_Model.this);
            checkBox1.setText(strings.get(i).getFuel_type());
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[] = {Color.WHITE, Color.WHITE};
            CompoundButtonCompat.setButtonTintList(checkBox1, new ColorStateList(states, colors));
            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            par.weight = 1;
            checkBox1.setLayoutParams(par);
            checkBox1.setTextColor(Color.WHITE);
//            checkBox1.setChecked(true);

            for (Fuel_Model fuelModel : provider_fule) {
                if (fuelModel.getFuel_id().equals(fuel_models.get(i).getFuel_id())) {
                    checkBox1.setChecked(true);
                    fuel_models.get(i).setIsChecked("1");
                }
            }

            int position = i;
            checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    if (b) {
                        fuel_models.get(position).setIsChecked("1");
                    } else {
                        fuel_models.get(position).setIsChecked("0");
                    }
                }
            });
            GridLayout.LayoutParams params = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                    GridLayout.spec(GridLayout.UNDEFINED, 1f));
            params.width = 0;
            lin_dynamic_car_type.addView(checkBox1, params);
        }
    }

    // list out all provider type radio button and default selected button
    @SuppressLint("SetTextI18n")
    public void ChooseType(@NotNull ArrayList<String> strings) {

        txt_choose.setText(R.string.choos_provider_type);

        rg_dynamic_choose.removeAllViews();
        boolean isChecked = false;
        for (int i = 0; i < strings.size(); i++) {
            RadioButton radioButton = new RadioButton(Act_Edit_Car_Model.this);
            radioButton.setText(strings.get(i));
            radioButton.setId(View.generateViewId());
            int[][] states = {{android.R.attr.state_checked}, {}};
            int[] colors = {Color.WHITE, Color.WHITE};
            CompoundButtonCompat.setButtonTintList(radioButton, new ColorStateList(states, colors));
            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            radioButton.setLayoutParams(par);
            radioButton.setTextColor(Color.WHITE);
            Log.e("Tag", sharedpreferences.getString("choose", ""));
            Log.e("Tag", strings.get(i));

            if (sharedpreferences.getString(getString(R.string.pref_provider_type), "").equals(provider_type.get(i))) {
                choose = provider_type_value.get(i);
                provider_type_position = provider_type.get(i);
                radioButton.setChecked(true);
                isChecked = true;
            } else if (strings.get(i).equals(sharedpreferences.getString(getString(R.string.pref_provider_type), ""))) {
                choose = provider_type_value.get(i);
                provider_type_position = provider_type.get(i);
                Log.e("Tag inside", strings.get(i));
                radioButton.setChecked(true);
                isChecked = true;
            } else if (!isChecked) {
                radioButton.setChecked(true);
                provider_type_position = provider_type.get(i);
                choose = provider_type_value.get(i);
            }

            final int finalI = i;
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        choose = provider_type_value.get(finalI);
                        provider_type_position = provider_type.get(finalI);
                        editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_provider_type), provider_type.get(finalI));
                        editor.apply();
                        Log.e("TAG Checked", strings.get(finalI));
                    } else {
                        Log.e("TAG UnChecked", strings.get(finalI));
                    }

                    if (main_lin_choose.getVisibility() == View.GONE) {
                        choose = "";
                        provider_type_position = "";
                    }
                }
            });
            rg_dynamic_choose.addView(radioButton);
        }
    }

    // list out all part type radio button and default selected button
    public void ChoosePartType(@NotNull ArrayList<String> strings) {

        // id 0 - New parts, id 1 - Used parts, 2 - New and Used parts
        txt_choose.setText(R.string.choose_auto_parts_that_you_sell);

        boolean isChecked = false;
        rg_dynamic_choose_part.removeAllViews();
        for (int i = 0; i < strings.size(); i++) {
            RadioButton radioButton = new RadioButton(Act_Edit_Car_Model.this);
            radioButton.setText(strings.get(i));
            radioButton.setId(View.generateViewId());
            int[][] states = {{android.R.attr.state_checked}, {}};
            int[] colors = {Color.WHITE, Color.WHITE};
            CompoundButtonCompat.setButtonTintList(radioButton, new ColorStateList(states, colors));
            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            radioButton.setLayoutParams(par);
            radioButton.setTextColor(Color.WHITE);
            Log.e("Tag", sharedpreferences.getString(getString(R.string.pref_part_type_choose), ""));
            Log.e("Tag", strings.get(i));
            if (strings.get(i).equals(sharedpreferences.getString(getString(R.string.pref_part_type_choose), ""))) {
                part_type_choose = part_type_value.get(i);
                part_type_position = part_type.get(i);
                radioButton.setChecked(true);
                isChecked = true;
                Log.e("Tag inside", strings.get(i));
            } else if (part_type.get(i).equals(sharedpreferences.getString(getString(R.string.pref_part_type), ""))) {
                part_type_choose = part_type_value.get(i);
                part_type_position = part_type.get(i);
                radioButton.setChecked(true);
                isChecked = true;
            } else if (!isChecked) {
                radioButton.setChecked(true);
            }

            final int finalI = i;
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        part_type_choose = part_type_value.get(finalI);
                        part_type_position = part_type.get(finalI);
                        /*editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_part_type_choose), part_type.get(finalI));
                        editor.putString(getString(R.string.pref_part_type), part_type.get(finalI));
                        editor.apply();*/
                        Log.e("TAG Checked", strings.get(finalI));
                    } else {
                        Log.e("TAG UnChecked", strings.get(finalI));
                    }

                    if (main_lin_choose_part.getVisibility() == View.GONE) {
                        part_type_choose = "";
                        part_type_position = "";
                    }
                }
            });
            rg_dynamic_choose_part.addView(radioButton);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_retry) {

            if (main_lin_car_brand.getVisibility() == View.VISIBLE) {
                GetGarageCarBrandModelData();
                getEnumType();
            } else if ( main_lin_choose_part.getVisibility() == View.VISIBLE) {
                getEnumType();
            } else if (main_lin_choose.getVisibility() == View.VISIBLE){
                getEnumType();
            }
        }
    }

}
