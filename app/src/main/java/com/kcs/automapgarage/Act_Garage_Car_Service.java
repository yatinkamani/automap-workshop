package com.kcs.automapgarage;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.core.widget.CompoundButtonCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Garage_Facility_Model;
import com.kcs.automapgarage.Model.Garage_Services;
import com.kcs.automapgarage.Model.Services_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.kcs.automapgarage.Act_Login.LOGIN_PREF;
import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Garage_Car_Service extends AppCompatActivity implements View.OnClickListener {

    ImageView ed_pro_back;
    CardView edit_E_card;
    TextView car_service_title;
    RadioGroup rg_dynamic_service;
    LinearLayout lin_dynamic_service;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    RelativeLayout rel_main;
    LinearLayout lin_header_title;
    ImageView service_img;
    TextView txt_service_name, txt_workshop_header, txt_user_header;

    List<Garage_Facility_Model> garage_facility_models = new ArrayList<>();
    List<Services_Model> Special_services_models = new ArrayList<>();
    List<Services_Model> services_models = new ArrayList<>();
    List<Garage_Services> result_service = new ArrayList<>();
    List<Garage_Services> result_service_default = new ArrayList<>();
    Services_Model car_owner_data = new Services_Model();

    ListView service_list;

    String garage_id = "";
    String special_facility_id = ""; // same get server
    String service_id = ""; // same get server
    String service_name = ""; // same get server
    String service_provider = ""; // same get server
    String special_services_id = ""; // same get shared preference
    String special_services_name = "";

    String service_image = "";

    LinearLayout lin_error;
    TextView txt_msg;
    Button btn_retry;
    ScrollView scrollView;
    AdapterService adapterService;

    RelativeLayout rel_workshop_owner;
    LinearLayout lin_car_owner;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__garage__car__service);

        preferences = getSharedPreferences(LOGIN_PREF, MODE_PRIVATE);
        editor = preferences.edit();
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");

        edit_E_card = findViewById(R.id.edit_E_card);
        car_service_title = findViewById(R.id.car_service_title);
        car_service_title.setVisibility(View.GONE);
        rg_dynamic_service = findViewById(R.id.rg_dynamic_service);
        lin_dynamic_service = findViewById(R.id.lin_dynamic_service);
        service_list = findViewById(R.id.service_list);
        btn_retry = findViewById(R.id.btnRetry);
        lin_error = findViewById(R.id.linError);
        txt_msg = findViewById(R.id.txtError);
        scrollView = findViewById(R.id.scroll_view);
        ed_pro_back = findViewById(R.id.ed_pro_back);
        rel_main = findViewById(R.id.rel_main);
        lin_header_title = findViewById(R.id.lin_header_title);
        service_img = findViewById(R.id.service_img);
        txt_service_name = findViewById(R.id.txt_service_name);
        txt_workshop_header = findViewById(R.id.txt_workshop_header);
        txt_user_header = findViewById(R.id.txt_user_header);
        manageBlinkEffect(txt_workshop_header);
        manageBlinkEffect(txt_user_header);

        rel_workshop_owner = findViewById(R.id.rel_workshop_owner);
        rel_workshop_owner.setVisibility(View.GONE);
        lin_header_title.setVisibility(View.GONE);
        lin_car_owner = findViewById(R.id.lin_car_owner);
        lin_car_owner.setVisibility(View.GONE);

        // show dialog when come this screen
        dialogView();

        ed_pro_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // back arrow visibility
        if (getIntent().getBooleanExtra("SPLASH", false)) {
            ed_pro_back.setVisibility(View.INVISIBLE);
        } else {
            ed_pro_back.setVisibility(View.VISIBLE);
        }

        edit_E_card.setOnClickListener(this);
        btn_retry.setOnClickListener(this);

        init();
        /*final int[] prevSelection = {-1}; //nothing selected

        service_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (prevSelection[0] == -1) //nothing selected
                {
//                    services_models.get(position).setIsSelected(true);
                    view.setBackgroundColor(getResources().getColor(R.color.transparent));
                    prevSelection[0] = position;
                } else if (prevSelection[0] == position) //deselect previously selected
                {
//                    musicRealmResults.get(position).setIsSelected(false);
                    view.setBackgroundColor(getResources().getColor(R.color.transparent));
                    adapterService.notifyDataSetChanged();
                    prevSelection[0] = -1;
                } else // Some other selection
                {
                    view.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    musicRealmResults.get(prevSelection[0]).setIsSelected(false);
//                    adapterService.get(position).setIsSelected(true);
                    adapterService.notifyDataSetChanged();
                    prevSelection[0] = position;
                }
            }
        });*/


        // service olny customer view
        rel_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                rel_main.animate().scaleX(1.0f).scaleY(1.0f).start();
                rel_main.setBackgroundColor(getResources().getColor(R.color.grey));
                result_service = new ArrayList<>();
                service_id = car_owner_data.getService_id();
                service_name = car_owner_data.getService_name();
                service_provider = car_owner_data.getService_provider();
                service_image = car_owner_data.getService_image();
                result_service.add(new Garage_Services("" + car_owner_data.getService_id(),
                        "" + car_owner_data.getService_name(),
                        "",
                        "" + car_owner_data.getCreated_at(),
                        "" + car_owner_data.getUpdated_at()));

                adapterService = new AdapterService(services_models, getApplicationContext());
                service_list.setAdapter(adapterService);
            }
        });

        // service only workshop owner
        service_list.setSelected(true);
        service_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                result_service = new ArrayList<>();

                service_id = services_models.get(i).getService_id();
                service_name = services_models.get(i).getService_name();
                service_provider = services_models.get(i).getService_provider();
                service_image = services_models.get(i).getService_image();
                result_service.add(new Garage_Services("" + services_models.get(i).getService_id(),
                        "" + services_models.get(i).getService_name(),
                        "",
                        "" + services_models.get(i).getCreated_at(),
                        "" + services_models.get(i).getUpdated_at()));

                Log.e("Tag jsk", "" + i);
                for (int j = 0; j < service_list.getChildCount(); j++) {
//                    service_list.getChildAt(j).animate().scaleX(0.8f).scaleY(0.8f).start();
                }
                rel_main.setBackgroundColor(getResources().getColor(R.color.transparent));
//                rel_main.animate().scaleX(0.8f).scaleY(0.1f).start();
//                view.animate().scaleX(1.0f).scaleY(1.0f).start();

            }
        });

        //        Log.e("Tag Select", ""+services_models.get(service_list.getSelectedItemPosition()).getService_name());
        //        GetGarageServicesData();
        get_garage_service();
    }

    // header item color animation
    public void manageBlinkEffect(TextView txt) {
        ObjectAnimator anim = ObjectAnimator.ofInt(txt, "textColor", Color.RED, Color.RED,
                Color.YELLOW);
        anim.setDuration(1500);
        anim.setEvaluator(new ArgbEvaluator());
        anim.setRepeatMode(ValueAnimator.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }

    // workshop owner and custome dialog
    public void dialogView(){

        Dialog dialog = new Dialog(Act_Garage_Car_Service.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_select_provider);

        LinearLayout lin_car_owner = dialog.findViewById(R.id.lin_car_owner);
        LinearLayout lin_workshop_owner = dialog.findViewById(R.id.lin_workshop_owner);

        lin_car_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rel_main.setVisibility(View.VISIBLE);
                lin_car_owner.setBackgroundColor(getResources().getColor(R.color.grey));
                lin_header_title.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });

        lin_workshop_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rel_workshop_owner.setVisibility(View.VISIBLE);
                lin_workshop_owner.setBackgroundColor(getResources().getColor(R.color.grey));
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void init() {

        Intent intent = getIntent();
        if (intent != null) {
            boolean isEdit = intent.getBooleanExtra("EDIT", false);
            if (isEdit) {
                car_service_title.setText(getString(R.string.edit_car_service));
            } else {
                car_service_title.setText(getString(R.string.add_car_service));
            }
        }
    }

    private void get_garage_facility() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_service_provider_facility", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("garage_facility", " " + response);
                try {

//                    garage_services = new ArrayList<>();
                    Special_services_models = new ArrayList<>();
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("status").equals("true")) {

                        JSONArray jsonArray = obj.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String service_id = object.getString("service_id");
                            String service_provider = object.getString("service_provider");
                            String service_type = object.optString("service_type","");
                            String service_name = check(object, "service_name");
                            String service_image = object.getString("service_image");
                            String created_at = object.getString("created_at");
                            String updated_at = object.getString("updated_at");

                            Special_services_models.add(new Services_Model(service_id, service_provider, service_type, service_name, service_image, created_at, updated_at));
                        }

                        rg_dynamic_service.removeAllViews();
                        for (int i = 0; i < Special_services_models.size(); i++) {

                            final ImageView imageView = new ImageView(Act_Garage_Car_Service.this);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            params.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            params.height = 200;
                            params.gravity = Gravity.CENTER;
                            params.setMargins(0, 10, 0, 0);
                            params.topMargin = 20;
                            imageView.setPadding(0, 20, 0, 0);
                            imageView.setLayoutParams(params);
                            imageView.setAdjustViewBounds(true);
                            Glide.with(getApplicationContext()).load(Special_services_models.get(i).getService_image()).apply(new RequestOptions().placeholder(R.drawable.b_break)).into(imageView);

                            final RadioButton radioButton = new RadioButton(Act_Garage_Car_Service.this);
                            radioButton.setText(Html.fromHtml(Special_services_models.get(i).getService_name()));
                            int[][] states = {{android.R.attr.state_checked}, {}};
                            int[] colors = {Color.WHITE, Color.RED};
                            CompoundButtonCompat.setButtonTintList(radioButton, new ColorStateList(states, colors));
                            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            par.gravity = Gravity.CENTER;
                            radioButton.setLayoutParams(par);
                            radioButton.setTextColor(Color.WHITE);
                            radioButton.setButtonDrawable(null);
                            radioButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            Log.e("Tag pref Service", preferences.getString("special_service_id", ""));

                            if (special_facility_id == null) {
                                special_facility_id = "";
                            }

                            if (special_facility_id.equals("")) {
                                if (Special_services_models.get(i).getService_id().equalsIgnoreCase(preferences.getString("special_service_id", ""))) {
                                    imageView.setBackgroundColor(getResources().getColor(R.color.sky));
                                    radioButton.setBackgroundColor(getResources().getColor(R.color.sky));
                                    Log.e("Tag Services SS Data", Special_services_models.get(i).getService_id());
                                }
                            } else {
                                if (Special_services_models.get(i).getService_id().equalsIgnoreCase(special_facility_id)) {
                                    imageView.setBackgroundColor(getResources().getColor(R.color.sky));
                                    radioButton.setBackgroundColor(getResources().getColor(R.color.sky));
                                    Log.e("Tag Services SS", Special_services_models.get(i).getService_id());
                                }
                            }

                            final int finalI1 = i;
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    for (int c = 0; c < rg_dynamic_service.getChildCount(); c++) {
                                        View view = rg_dynamic_service.getChildAt(c);
                                        if (view instanceof ImageView) {
                                            ImageView imageView1 = (ImageView) view;
                                            imageView1.setBackgroundColor(getResources().getColor(R.color.transparent));
                                        }
                                        if (view instanceof RadioButton) {
                                            RadioButton radioButton1 = (RadioButton) view;
                                            radioButton1.setBackgroundColor(getResources().getColor(R.color.transparent));
                                        }
                                    }

                                    /*for (int i=0; i<garage_services.size(); i++){
                                        imageView.setBackgroundColor(getResources().getColor(R.color.transparent));
                                        radioButton.setBackgroundColor(getResources().getColor(R.color.transparent));
                                    }*/

                                    imageView.setBackgroundColor(getResources().getColor(R.color.sky));
                                    radioButton.setBackgroundColor(getResources().getColor(R.color.sky));

//                                    radioButton.setChecked(true);
                                    special_services_id = Special_services_models.get(finalI1).getService_id();
                                    special_services_name = Special_services_models.get(finalI1).getService_name();

                                    Log.e("Tag", special_services_id);
                                    Log.e("Tag", special_services_name);
                                }
                            });

                            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                    if (isChecked) {
                                        imageView.setBackgroundColor(getResources().getColor(R.color.sky));
                                        radioButton.setBackgroundColor(getResources().getColor(R.color.sky));
                                    } else {
                                        imageView.setBackgroundColor(getResources().getColor(R.color.transparent));
                                        radioButton.setBackgroundColor(getResources().getColor(R.color.transparent));
                                    }
                                }
                            });
                            rg_dynamic_service.addView(imageView);
                            rg_dynamic_service.addView(radioButton);
                        }

                    } else {
//                        error_dialog("No Data Found");
                        Toast.makeText(getApplicationContext(), "" + obj.getString("status").equals("1"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("My error", "" + error);
                progressDialog.dismiss();
                error_dialog(AppUtils.serverError(getApplicationContext(),error));
//                error_dialog(getString(R.string.something_wrong_please));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("service_type", "Special Service");
                return map;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void GetGarageServicesData() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_garage_service", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        JSONObject jsonArray = obj.getJSONObject("result");
//                        for (int i=0; i<jsonArray.length(); i++){
//                            JSONObject object = jsonArray.getJSONObject(i);
                        JSONArray special_service = jsonArray.getJSONArray("special_service");
                        for (int i = 0; i < special_service.length(); i++) {

                            JSONObject object = special_service.getJSONObject(i);
                            String garage_facility_id = object.getString("garage_service_id");
                            String garage_id = object.getString("garage_id");
                            String facility_id = object.getString("special_service_id");
                            String facility_name = check(object,"service_name");

                            editor.putString(getString(R.string.pref_service_id), facility_id);
                            editor.putString(getString(R.string.pref_special_service_name), facility_name);
                            editor.putBoolean("car_service", true);
                            editor.apply();
                        }

                        JSONArray array = jsonArray.getJSONArray("general_service");
                        for (int j = 0; j < array.length(); j++) {
                            JSONObject object = array.getJSONObject(j);
                            String garage_facility_id = object.getString("garage_general_service_id");
                            String garage_id = object.getString("garage_id");
                            String facility_id = object.getString("general_service_id");
                            String facility_name = check(object,"service_name");
                            garage_facility_models.add(new Garage_Facility_Model(garage_facility_id, garage_id, facility_id, facility_name));
                            result_service_default.add(new Garage_Services(facility_id, "", "", "", ""));
                        }

                            /*  editor.putBoolean("garage_service",true);
                                editor.apply();     */
//                        }
                    }
//                    get_garage_facility();
//                    get_garage_service();
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                error_dialog(AppUtils.serverError(getApplicationContext(),error));
                error_dialog(AppUtils.serverError(getApplicationContext(),error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("params", " " + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    private void AddEditCarModel() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "add_edit_garage_brand_model", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //displaying the error in toast if occurs
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
//                error_dialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                map.put("provider_type", "I provide the service in my workshop and in the customer's site");

                Log.e("params", " " + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    // get service list car or workshop owner
    private void get_garage_service() {

        final ProgressDialog progressDialog = new ProgressDialog(Act_Garage_Car_Service.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_service_provider_facility", new Response.Listener<String>() {
            @SuppressLint({"Long_Log_Tag", "LongLogTag"})
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("garage_facility", " " + response);
                try {

                    services_models = new ArrayList<>();
                    List<Services_Model> normal_service = new ArrayList<>();
                    List<Services_Model> Part_machenic = new ArrayList<>();
                    List<Services_Model> standard_service = new ArrayList<>();
                     JSONObject obj = new JSONObject(response);
                    if (obj.getString("status").equals("true")) {

                        JSONArray jsonArray = obj.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String service_id = object.getString("service_id");
                            String service_provider = object.getString("service_provider");
                            //  String service_type = object.getString("service_type");
                            String service_name = check(object,"service_name");
                            String service_image = object.getString("service_image");
                            String created_at = object.getString("created_at");
                            String updated_at = object.getString("updated_at");

                            // 61 means customer services
                            if (service_id.equals("61")){
//                                rel_main.setVisibility(View.VISIBLE);
//                                lin_header_title.setVisibility(View.VISIBLE);
                                Glide.with(getApplicationContext()).load(service_image).into(service_img);
                                txt_service_name.setText(service_name);
                                car_owner_data = new Services_Model(service_id, service_provider, "", service_name, service_image, created_at, updated_at);
//                                standard_service.add(new Services_Model("", "", "", getString(R.string.user_registration_selection), "", "", ""));
//                                standard_service.add(new Services_Model(service_id, service_provider, "", service_name, service_image, created_at, updated_at));
                            // 4 part shop and 18 workshop services
                            } else if (service_id.equals("4") || service_id.equals("18")) {
//                                if (Part_machenic.size() == 0)
//                                    Part_machenic.add(new Services_Model("", "", "", getString(R.string.workshop_owner_registration_department), "", "", ""));
                                Part_machenic.add(new Services_Model(service_id, service_provider, "", service_name, service_image, created_at, updated_at));
                            }
                            // other services
                            else {
                                normal_service.add(new Services_Model(service_id, service_provider, "", service_name, service_image, created_at, updated_at));
                            }

                        }
//                        services_models.addAll(standard_service);
                        services_models.addAll(Part_machenic);
                        services_models.addAll(normal_service);

                        if (services_models != null && services_models.size() > 0) {

                            adapterService = new AdapterService(services_models, getApplicationContext());
                            service_list.setAdapter(adapterService);
                            scrollView.setVisibility(View.VISIBLE);
                            lin_error.setVisibility(View.GONE);
                        } else {
                            scrollView.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.no_data_found));
                            btn_retry.setVisibility(View.GONE);
                        }
                        /*lin_dynamic_service.removeAllViews();
                        for (int i = 0; i < services_models.size(); i++) {

                            CheckBox checkBox = new CheckBox(Act_Garage_Car_Service.this);
                            checkBox.setText(Html.fromHtml(services_models.get(i).getService_name()));
                            int[][] states = {{android.R.attr.state_checked}, {}};
                            int[] colors = {Color.WHITE, Color.RED};
                            CompoundButtonCompat.setButtonTintList(checkBox, new ColorStateList(states, colors));
                            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            checkBox.setLayoutParams(par);
                            checkBox.setTextColor(Color.WHITE);

                            for (int l = 0; l < result_service_default.size(); l++) {
                                if (result_service_default.get(l).getFacility_id().equalsIgnoreCase(services_models.get(i).getService_id())) {
                                    checkBox.setChecked(true);
                                    result_service.add(new Garage_Services(services_models.get(i).getService_id(), services_models.get(i).getService_name(), "", "", ""));
                                }
                            }

                            final int finalI1 = i;
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    Log.e("Tag Size()", "" + result_service.size());
                                    if (isChecked) {
                                        result_service.add(new Garage_Services(services_models.get(finalI1).getService_id(), services_models.get(finalI1).getService_name(), "", "", ""));
                                    } else {
                                        List<Garage_Services> newsList = result_service;
                                        Garage_Services newsToRemove = null;
                                        for (Garage_Services news : newsList) {
                                            Log.e("Tag Name", news.getFacility_id() + " " + services_models.get(finalI1).getService_id());
                                            if (news.getFacility_id() == services_models.get(finalI1).getService_id()) {
                                                newsToRemove = news;
                                                break;
                                            }
                                        }
                                        if (newsToRemove != null) {
                                            newsList.remove(newsToRemove);
                                        }
                                    }
                                }
                            });

                            lin_dynamic_service.addView(checkBox);
                        }*/
                    } else {
//                        error_dialog("No Data Found");
                        scrollView.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.no_data_available));
                        btn_retry.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    scrollView.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.something_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                }
                if (progressDialog != null && !isFinishing())
                    progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(Act_Garage_Car_Service.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.e("My error", "" + error);
                progressDialog.dismiss();
                scrollView.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_msg.setText(AppUtils.serverError(getApplicationContext(), error));
//                txt_msg.setText(getString(R.string.something_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    // select and add service api call
    private void AddEditCarService() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "add_edit_garage_service", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {
                        Toast.makeText(getApplicationContext(), getString(R.string.add_car_service), Toast.LENGTH_LONG).show();
//                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();

                        // check service is customer or not current not work
                        boolean isService = false;
                        if (service_name.equalsIgnoreCase("Mechanic shop") || service_id.equals("4") ||
                                service_name.equalsIgnoreCase("Auto parts shop") || service_id.equals("9") ||
                                service_name.equalsIgnoreCase("Car wash & oil change") || service_id.equals("10") ||
                                service_name.equalsIgnoreCase("Dry clean, detailing, polish, nano-ceramic") || service_id.equals("12") ||
                                service_name.equalsIgnoreCase("Tires and Auto wheels repair") || service_id.equals("13") ||
                                service_name.equalsIgnoreCase("Locksmiths, Keys Auto Service") || service_id.equals("18")
                        ) {
                            editor.putString(getString(R.string.pref_service_id), service_id);
                            editor.putString(getString(R.string.pref_special_service_name), service_name);
                            editor.putBoolean("car_service", true);
                            editor.apply();
                            isService = true;
                        }

                        // customer car service
                        if (service_id.equals("61")){
                            editor.putString(getString(R.string.pref_service_id), service_id);
                            editor.putString(getString(R.string.pref_special_service_name), service_name);
                            editor.putBoolean("car_service", true);
                            editor.putString(getString(R.string.pref_workshop_image), service_image);
                            editor.putString(getString(R.string.pref_user_image), service_image);
                            editor.apply();
                        }

                        /*if (service_name.equalsIgnoreCase("Request Mobile Maintenance") || service_id.equalsIgnoreCase("19") ||
                                service_name.equalsIgnoreCase("Request Mobile Dry-clean") || service_id.equalsIgnoreCase("20") ||
                                service_name.equalsIgnoreCase("Request Key Specialist (to open Vehicle)") || service_id.equalsIgnoreCase("21") ||
                                service_name.equalsIgnoreCase("Request mobile oil service") || service_id.equalsIgnoreCase("24") ||
                                service_name.equalsIgnoreCase("Request Mobile Gas supply") || service_id.equalsIgnoreCase("25") ||
                                service_name.equalsIgnoreCase("Request Mobile wheels service") || service_id.equalsIgnoreCase("28")
                        ) {
                            AddEditCarModel();
                            editor.putString(getString(R.string.pref_provider_type), "34");
                            editor.apply();
                        }*/

                        Log.e("Tag service provider",service_provider);
                        if (service_provider.matches(".*Request mobile Auto service*.")){
                            editor.putString(getString(R.string.pref_service_id), service_id);
                            editor.putString(getString(R.string.pref_special_service_name), service_name);
                            editor.putBoolean("car_service", true);
                            editor.apply();
                            isService = true;
                            Log.e("Tag service  matches",service_provider);
                        }

                        if (service_provider.toLowerCase().contains("request mobile auto service")){
                            Log.e("Tag service  contains",service_provider);
                            editor.putString(getString(R.string.pref_service_id), service_id);
                            editor.putString(getString(R.string.pref_special_service_name), service_name);
                            editor.putBoolean("car_service", true);
                            editor.apply();
                            isService = true;
                        }

                        for (String s : service_provider.split(",")){

                            if (s.equalsIgnoreCase("GET QUOTES and booking") ||
                                    s.equalsIgnoreCase("Request mobile Auto service")){
                                Log.e("Tag service  contains",service_provider);
                                editor.putString(getString(R.string.pref_service_id), service_id);
                                editor.putString(getString(R.string.pref_special_service_name), service_name);
                                editor.putString(getString(R.string.pref_provider_service),"true");
                                editor.putBoolean("car_service", true);
                                editor.apply();
                                isService = true;
                            }
                        }

                        if (service_id.equals("61")){
                            editor.putString(getString(R.string.pref_service_id), service_id);
                            editor.putString(getString(R.string.pref_special_service_name), service_name);
                            editor.putBoolean("car_service", true);
                            editor.apply();
                            isService = false;
                        }

                        if (isService) {
                            editor = preferences.edit();
                            editor.putBoolean(getString(R.string.pref_isModel), true);
                            editor.apply();
                            Intent intent = new Intent(getApplicationContext(), Act_Edit_Car_Model.class);
                            intent.putExtra("SPLASH", true);
                            startActivity(intent);
//                            finish();
                        } else {
                            editor = preferences.edit();
                            editor.putBoolean(getString(R.string.pref_isModel), false);
                            editor.putString(getString(R.string.pref_service_id), service_id);
                            editor.putString(getString(R.string.pref_special_service_name), service_name);
                            editor.apply();
                            if (!service_id.equals("61")){
                                Intent intent = new Intent(getApplicationContext(), Act_Edit_Profile_Detail.class);
                                intent.putExtra(getString(R.string.pref_garage_id), garage_id);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(getApplicationContext(), Navigation_Activity.class);
                                startActivity(intent);
                                finish();
                            }
//                            finish();
                        }
//                    }
                    } else {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (dialog != null && dialog.isShowing() && !isFinishing())
                    dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                error_dialog(AppUtils.serverError(getApplicationContext(),error));
//                error_dialog(getString(R.string.something_wrong_please));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                map.put("service_id", "" + service_id);
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                /*JSONArray array = new JSONArray();
                for (int i = 0; i < result_service.size(); i++) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("service_id", service_id);
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                map.put("service_json", "" + array);*/

                Log.e("params", " " + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    public void error_dialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(msg);
        builder.setMessage("Click \"Ok\" retry ");
        builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                GetGarageServicesData();
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onClick(View view) {

        if (view == edit_E_card) {
            if (!service_id.equalsIgnoreCase("")) {
                AddEditCarService();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.select_service), Toast.LENGTH_LONG).show();
            }
        }
    }

    // service list adapter class
    class AdapterService extends BaseAdapter {

        List<Services_Model> services_models;
        Context context;

        public static final int TYPE_ODD = 0;
        public static final int TYPE_EVEN = 1;

        AdapterService(List<Services_Model> services_models, Context context) {
            this.services_models = services_models;
            this.context = context;
        }

        @Override
        public int getCount() {
            return services_models.size();
        }

        @Override
        public Object getItem(int i) {
            return services_models.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getItemViewType(int position) {
            /*if (services_models.get(position).getService_id().isEmpty()){
                return TYPE_ODD;
            }else {
                return TYPE_EVEN;
            }*/
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return services_models.size();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            ViewHolder viewHolder;

            if (view == null) {
//                if (TYPE_EVEN == getItemViewType(i)){
                    view = LayoutInflater.from(context).inflate(R.layout.adapter_service_list, viewGroup, false);
                    viewHolder = new ViewHolder();
                    viewHolder.service_img = view.findViewById(R.id.service_img);
                    viewHolder.service_name = view.findViewById(R.id.service_name);
                    viewHolder.lin_main = view.findViewById(R.id.lin_main);
                    view.setTag(viewHolder);
                /*}else {
                    view = LayoutInflater.from(context).inflate(R.layout.adapter_service_header, viewGroup, false);
                    viewHolder = new ViewHolder();
                    viewHolder.txt_service_header = view.findViewById(R.id.txt_service_header);
                    view.setTag(viewHolder);
                    view.setFocusable(false);
                }*/

            }else {
                viewHolder = (ViewHolder) view.getTag();
            }


            final Services_Model services_model = services_models.get(i);

            Log.e("Tag Images", services_models.get(i).getService_image());

//            if (TYPE_EVEN == getItemViewType(i)){

                Glide.with(getApplicationContext()).asBitmap().load(services_models.get(i).getService_image())
                        .placeholder(R.drawable.b_break)
                        .thumbnail(0.01f)
                        .into(new BitmapImageViewTarget(viewHolder.service_img) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                viewHolder.service_img.setImageBitmap(resource);
                            }
                        });

                viewHolder.service_name.setText(Html.fromHtml(services_models.get(i).getService_name()));
            /*}else {
                viewHolder.txt_service_header.setText(Html.fromHtml(services_models.get(i).getService_name()));
            }*/

            /*lin_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result_service = new ArrayList<>();
                    result_service.add(new Garage_Services(services_model.getService_id(),services_model.getService_name(),services_model.getCreated_at(),
                            services_model.getService_image(),services_model.getService_image()));
                    if (lin)
                    lin_main.setBackgroundColor(getResources().getColor(R.color.quantum_yellow100));
                    notifyDataSetChanged();
                }
            });*/
            return view;
        }

        public class ViewHolder{
            TextView service_name, txt_service_header;
            ImageView service_img;
            RelativeLayout lin_main;
        }
    }
}