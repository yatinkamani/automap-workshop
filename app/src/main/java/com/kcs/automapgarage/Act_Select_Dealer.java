package com.kcs.automapgarage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Dealer_Data_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Select_Dealer extends AppCompatActivity implements View.OnClickListener {

    ImageView dealer_back;
    TextView dealer_title;
    RadioGroup rg_dynamic_service;
    CardView card_save;

    LinearLayout linError;
    TextView txtError;
    Button btnRetry;
    String dealer_service_id = "", garare_id = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    List<Dealer_Data_Model> dataModels = new ArrayList<>();

    String isEdit = "0";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__select__dealer);

        if (getIntent() != null){
            isEdit = getIntent().getStringExtra(getString(R.string.pref_dealer_service)) == null ? "" : getIntent().getStringExtra(getString(R.string.pref_dealer_service)) ;
        }

        BindView();

    }

    private void BindView() {

        dealer_back = findViewById(R.id.dealer_back);
        dealer_title = findViewById(R.id.dealer_title);
        rg_dynamic_service = findViewById(R.id.rg_dynamic_service);
        card_save = findViewById(R.id.card_save);

        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        editor = preferences.edit();
        garare_id = preferences.getString(getString(R.string.pref_garage_id),"");
        dealer_service_id = preferences.getString(getString(R.string.pref_dealer_service),"");
        dealer_back.setOnClickListener(this);
        card_save.setOnClickListener(this);
        btnRetry.setOnClickListener(this);

        /*dataModels = new ArrayList<>();
        Dealer_Data_Model model = new Dealer_Data_Model();
        model.setId("1");
        model.setDealer_name("First Data");
        dataModels.add(model);

        Dealer_Data_Model model2 = new Dealer_Data_Model();
        model2.setId("2");
        model2.setDealer_name("First Data");
        dataModels.add(model2);

        Dealer_Data_Model model1 = new Dealer_Data_Model();
        model1.setId("4");
        model1.setDealer_name("First Data");
        dataModels.add(model1);

        Dealer_Data_Model model4 = new Dealer_Data_Model();
        model4.setId("3");
        model4.setDealer_name("First Data");
        dataModels.add(model4);*/

//        MakeRadioGroup(dataModels);

        getDealer();

    }

    public void getDealer() {

        ProgressDialog dialog = new ProgressDialog(Act_Select_Dealer.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        card_save.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_service_provider_facility", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag Response", response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("true")) {

                        JSONArray array = object.optJSONArray("result");
                        dataModels = new ArrayList<>();

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            Dealer_Data_Model model = new Dealer_Data_Model();
                            model.setId(jsonObject.getString("service_id"));
                            model.setDealer_name(check(jsonObject,"service_name"));
                            dataModels.add(model);
                        }
                        MakeRadioGroup(dataModels);

                    } else {
                        rg_dynamic_service.setVisibility(View.GONE);
                        linError.setVisibility(View.VISIBLE);
                        card_save.setVisibility(View.GONE);
                        txtError.setText(getString(R.string.no_data_found));
                        btnRetry.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    rg_dynamic_service.setVisibility(View.GONE);
                    linError.setVisibility(View.VISIBLE);
                    txtError.setText(getString(R.string.something_wrong_please));
                    btnRetry.setVisibility(View.VISIBLE);
                }
                dialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                rg_dynamic_service.setVisibility(View.GONE);
                linError.setVisibility(View.VISIBLE);
                txtError.setText(getString(R.string.something_wrong_please));
                btnRetry.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("service_provider", getString(R.string.dealer));
                map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag params", map.toString());
                return map;

            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void MakeRadioGroup(List<Dealer_Data_Model> model) {

        Log.e("Tag Models", "" + model);
        rg_dynamic_service.removeAllViews();
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{new int[]{-android.R.attr.state_checked}, new int[]{android.R.attr.state_checked}
                }, new int[]{Color.DKGRAY, Color.rgb (242,81,112),});

        for (int i = 0; i < model.size(); i++) {

            RadioButton button = new RadioButton(this);

            button.setTextColor(Color.WHITE);
            button.setId(Integer.parseInt(model.get(i).getId()));
            button.setText(model.get(i).getDealer_name());
            button.setPadding(5, 20, 0, 20);
            button.setGravity(Gravity.CENTER);
//            button.setButtonTintList(colorStateList);
            button.setBackground(getResources().getDrawable(R.drawable.round_background));
            button.setBackgroundTintList(colorStateList);
            button.setButtonDrawable(null);
            button.setTextSize(20f);
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT,RadioGroup.LayoutParams.WRAP_CONTENT);
            params.bottomMargin = 10;
            params.topMargin = 10;
            params.leftMargin = 10;
            params.rightMargin = 10;
            params.gravity = Gravity.CENTER;
            button.setLayoutParams(params);
            if (!dealer_service_id.equals("")){
                if (model.get(i).getId().equals(dealer_service_id)){
                    button.setChecked(true);
                }
            }else if (i == 0) {
                button.setChecked(true);
            }
            rg_dynamic_service.addView(button);
        }

        if (rg_dynamic_service.getChildCount() > 1) {
            rg_dynamic_service.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {

                    dealer_service_id = "" + radioGroup.getCheckedRadioButtonId();

                    for (int g = 0; g< radioGroup.getChildCount(); g++){
                        RadioButton button = (RadioButton) radioGroup.getChildAt(g);
                        button.setBackgroundTintList(colorStateList);
                    }
                }
            });
        }
    }

    public void addDealer() {
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "addGarageDealer", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag Response", response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("true")) {
                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                        editor.putString(getString(R.string.pref_dealer_service), dealer_service_id);
                        editor.apply();
                        if (isEdit.equals("0")){
                            Intent i = new Intent(Act_Select_Dealer.this, Act_Garage_Car_Service.class);
                            i.putExtra("SPLASH", true);
                            startActivity(i);
                        }else {
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("service_id", dealer_service_id);
                map.put("garage_id", garare_id);
                map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public void onClick(View view) {

        if (view == btnRetry){
            getDealer();
        } else if (view == dealer_back){
            onBackPressed();
        } else if (view == card_save){
            if (dataModels != null && dataModels.size() == 0){
                if (isEdit.equals("0")){
                    Intent i = new Intent(Act_Select_Dealer.this, Act_Garage_Car_Service.class);
                    i.putExtra("SPLASH", true);
                    startActivity(i);
                }else {
                    finish();
                }
            } else if (dataModels == null){
                if (isEdit.equals("0")){
                    Intent i = new Intent(Act_Select_Dealer.this, Act_Garage_Car_Service.class);
                    i.putExtra("SPLASH", true);
                    startActivity(i);
                }else {
                    finish();
                }
            }else {
                addDealer();
            }
        }
    }
}
