package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Mobile_Request_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Mobile_Request extends AppCompatActivity implements View.OnClickListener {

    ImageView back;
    RecyclerView rv_customer_request;
    LinearLayout linError;
    TextView txtError;
    Button btnRetry;
    List<Mobile_Request_Model> mobile_request_models = new ArrayList<>();
    Adapter_Mobile_Request adapter_mobile_request;
    SharedPreferences sharedpreferences;
    String garage_id = "", latitude = "", longitude = "";
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__mobile__request);

        back = findViewById(R.id.back);
        rv_customer_request = findViewById(R.id.rv_customer_request);
        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);

        sharedpreferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = sharedpreferences.getString(getString(R.string.pref_garage_id), "");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnRetry.setOnClickListener(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        btnRetry.setOnClickListener(this);
        getMobileRequest();
        InitializeBroadcast();
        ClearNotification();
    }

    // clear preference message badge count
    public void ClearNotification(){
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.pref_badge_count_mobile_request), 0);
        editor.apply();
        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
        if (tot == 0) {
            ShortcutBadger.removeCount(getApplicationContext());
        } else {
            ShortcutBadger.applyCount(getApplicationContext(), tot);
        }
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (AppUtils.checkGPSPermissions(this)) {
            if (AppUtils.isLocationEnabled(getApplicationContext())) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    latitude = String.valueOf(location.getLatitude());
                                    longitude = String.valueOf(location.getLatitude());
//                                    UpdateLocation();
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            AppUtils.requestGPSPermissions(this);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
//            UpdateLocation(ad b);
        }
    };

    @Override
    public void onClick(View view) {
        if (view == back) {
            onBackPressed();
        } else if (view == btnRetry) {
            getMobileRequest();
        }
    }

    // get get mobile request and position change status wise
    private void getMobileRequest() {
        final ProgressDialog dialog = new ProgressDialog(Act_Mobile_Request.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "mobile_request_list",
                    new Response.Listener<String>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(String response) {
                            response = Html.fromHtml(response).toString();
                            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);

                            Log.e("Appointment List", " " + response);
                            Log.e("Appointment Url", Constant.URL + "garage_appointment_list");
                            dialog.dismiss();
                            mobile_request_models = new ArrayList<>();
                            try {
                                Log.e("notification_list", " " + response);
                                JSONObject mainJsonObject = new JSONObject(response);
                                JSONArray result = mainJsonObject.getJSONArray("result");
                                if (result.length() > 0) {

                                    List<Mobile_Request_Model> s0 = new ArrayList<>();
                                    List<Mobile_Request_Model> s1 = new ArrayList<>();
                                    List<Mobile_Request_Model> s2 = new ArrayList<>();
                                    List<Mobile_Request_Model> s3 = new ArrayList<>();
                                    List<Mobile_Request_Model> s4 = new ArrayList<>();
                                    List<Mobile_Request_Model> s5 = new ArrayList<>();
                                    List<Mobile_Request_Model> s6 = new ArrayList<>();
                                    List<Mobile_Request_Model> s7 = new ArrayList<>();
                                    mobile_request_models = new ArrayList<>();
                                    for (int i = 0; i < result.length(); i++) {

                                        JSONObject jsonObject = result.getJSONObject(i);
                                        String request_id = jsonObject.getString("mobile_request_id");
                                        String request_detail_id = jsonObject.getString("mobile_request_garage_id");
                                        String user_id = jsonObject.getString("user_id");
                                        String garage_id = jsonObject.getString("garage_id");
                                        String name = check(jsonObject,"name");
                                        String status = jsonObject.getString("status");
                                        String model_name = check(jsonObject,"model_name");
                                        String model_year = jsonObject.getString("model_year");
                                        String car_year = jsonObject.getString("car_year");
                                        String fuel_type = check(jsonObject,"fule_type");
                                        String brand_name = check(jsonObject,"brand_name");
                                        String request_date = jsonObject.getString("request_date");
                                        String request_time = jsonObject.getString("request_time");
                                        String date = jsonObject.getString("created_at");
                                        String update_at = jsonObject.getString("updated_at");
                                        String request_type = check(jsonObject,"request_type");
                                        String oil_brand = check(jsonObject,"oil_brand");
                                        String oil_sae = check(jsonObject,"oil_sae");
                                        String user_lat = jsonObject.getString("user_latitude");
                                        String user_lng = jsonObject.getString("user_longitude");
                                        String amount = check(jsonObject,"amount");
                                        String location_address = jsonObject.getString("location_address");
                                        String leather_seats = check(jsonObject,"leather_seat");
                                        String change_filter = check(jsonObject,"change_filter");
                                        String timeout_status = "";
                                        String mobile_request_time = jsonObject.getString("mobile_request_time");
                                        if (jsonObject.has("timeout_status")) {
                                            timeout_status = jsonObject.getString("timeout_status");
                                        }
                                        if (mobile_request_time.equals("")) {
                                            mobile_request_time = "15 minutes";
                                        }

                                        timeout_status = "0";
                                        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        Log.e("Tag Timezone Local", dateFormatGmt.format(new Date()));
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        Date appointmentDate = null;
                                        Date currentDate = null;
                                        long days = 0;
                                        try {

                                            appointmentDate = format.parse("" + date);
                                            currentDate = format.parse(dateFormatGmt.format(new Date()));
                                            Log.e("TAG TIME", "" + format.format(currentDate));
                                            Log.e("TAG TIME", "" + format.format(appointmentDate));

                                            Log.e("TAG TIME", "" + request_date + " " + request_time);
                                            if (currentDate.before(appointmentDate)) {
                                                long diff = appointmentDate.getTime() - currentDate.getTime();
                                                Log.e("Tag days", "" + diff);
                                            } else {
                                                long diff = currentDate.getTime() - appointmentDate.getTime();
                                                Log.e("days", "" + diff);
                                                if (diff > 0) {
                                                    long second = diff / 1000;
                                                    long minute = second / 60;
                                                    long hour = minute / 60;
                                                    days = (hour / 24) + 1;
                                                    Log.e("Tag hour", "" + hour);
                                                    Log.e("Tag day", "" + days);

                                                    if (!mobile_request_time.equals("null") && !mobile_request_time.equals("")) {
                                                        if (mobile_request_time.matches(".*minute*.") ||
                                                                mobile_request_time.contains("minute") ||
                                                                mobile_request_time.matches(".*minutes*.") ||
                                                                mobile_request_time.contains("minutes")) {
//                                                          Integer.parseInt(mobile_request_time.replaceAll("\\D",""));
                                                            if (minute >= Integer.parseInt(mobile_request_time.replaceAll("\\D", ""))) {
                                                                timeout_status = "1";
                                                            } else {
                                                                timeout_status = "0";
                                                            }
                                                        } else {
                                                            if (hour >= Integer.parseInt(mobile_request_time.replaceAll("\\D", ""))) {
                                                                timeout_status = "1";
                                                            } else {
                                                                timeout_status = "0";
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            Log.e("TAG TIME", e.toString());
                                        }

                                        /*mobile_request_models.add(new Mobile_Request_Model(request_id, request_detail_id, user_id,
                                                status,name, "", "", "", location_address,  amount, oil_brand, oil_sae,
                                                request_date,request_time,brand_name, model_name, model_year,"","",
                                                leather_seats, change_filter, user_lat, user_lng,request_type, timeout_status, date,update_at));*/

                                        Mobile_Request_Model m = new Mobile_Request_Model(request_id, request_detail_id, user_id,
                                                status, name, "", "", "", location_address, amount, oil_brand, oil_sae,
                                                request_date, request_time, brand_name, model_name, car_year, "", fuel_type,
                                                leather_seats, change_filter, user_lat, user_lng, request_type, timeout_status, date, update_at);

                                        if (days > 1 && status.equals("0") || status.equals("2")) {

                                        } else {

                                            if (status.equals("0") && timeout_status.equals("0")) {
                                                s0.add(m);
                                            } else if (status.equals("0") && timeout_status.equals("1")) {
                                                s2.add(m);
                                            } else if (status.equals("1")) {
                                                s1.add(m);
                                            } else if (status.equals("2")) {
                                                s2.add(m);
                                            } else if (status.equals("3")) {
                                                s3.add(m);
                                            } else if (status.equals("4")) {
                                                s4.add(m);
                                            } else if (status.equals("5")) {
                                                s5.add(m);
                                            } else if (status.equals("6")) {
                                                s6.add(m);
                                            } else {
                                                s7.add(m);
                                            }
                                        }
                                    }

                                    mobile_request_models = new ArrayList<>();
                                    mobile_request_models.addAll(s0);
                                    mobile_request_models.addAll(s1);
                                    mobile_request_models.addAll(s3);
                                    mobile_request_models.addAll(s5);
                                    mobile_request_models.addAll(s6);
                                    mobile_request_models.addAll(s4);
                                    mobile_request_models.addAll(s2);
                                    mobile_request_models.addAll(s7);
                                    Log.e("Tag Arrary", "" + mobile_request_models.size());
                                    if (mobile_request_models != null && mobile_request_models.size() > 0) {
                                        adapter_mobile_request = new Adapter_Mobile_Request(Act_Mobile_Request.this, mobile_request_models);
                                        rv_customer_request.setLayoutManager(new GridLayoutManager(Act_Mobile_Request.this, 1));
                                        rv_customer_request.setAdapter(adapter_mobile_request);
                                        linError.setVisibility(View.GONE);
                                        rv_customer_request.setVisibility(View.VISIBLE);
                                    } else {
                                        linError.setVisibility(View.VISIBLE);
                                        txtError.setText("No Request Data Found");
                                        btnRetry.setVisibility(View.GONE);
                                        rv_customer_request.setVisibility(View.GONE);
                                    }
                                    /*SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt(getString(R.string.pref_badge_count_mobile_request), 0);
                                    editor.apply();
                                    int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                            preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                            preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                            preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                                            preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
                                    if (tot == 0) {
                                        ShortcutBadger.removeCount(getApplicationContext());
                                    } else {
                                        ShortcutBadger.applyCount(getApplicationContext(), tot);
                                    }*/

                                } else {
                                    linError.setVisibility(View.VISIBLE);
                                    txtError.setText(getString(R.string.no_data_found));
                                    btnRetry.setVisibility(View.GONE);
                                    rv_customer_request.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("size == > Error", e.toString());
                                linError.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.something_wrong_please));
                                btnRetry.setVisibility(View.VISIBLE);
                                rv_customer_request.setVisibility(View.GONE);
                                dialog.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //displaying the error in toast if occurs
                            Log.e("size ==> Error", error.toString());
//                          Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            linError.setVisibility(View.VISIBLE);
                            txtError.setText(AppUtils.serverError(getApplicationContext(),error));
//                            txtError.setText(getString(R.string.something_wrong_please));
                            btnRetry.setVisibility(View.VISIBLE);
                            rv_customer_request.setVisibility(View.GONE);
                            dialog.dismiss();
//                            error_dialog();
                        }
                    }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("garage_id", garage_id);
                    params.put(AppUtils.language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            linError.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            rv_customer_request.setVisibility(View.GONE);
            dialog.dismiss();
        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Mobile_Request.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // when come new request refress api
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
//            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
//            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
//            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (mobile != 0 && !Act_Mobile_Request.this.isFinishing()){
                getMobileRequest();
                ClearNotification();
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == AppUtils.PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                Toast.makeText(getApplicationContext(), "Please Grant Permission Use Features", Toast.LENGTH_LONG).show();
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUtils.checkGPSPermissions(this)) {
            getLastLocation();
        }
        getMobileRequest();
        ClearNotification();
    }

    // list mobile request apdater
    class Adapter_Mobile_Request extends RecyclerView.Adapter<Adapter_Mobile_Request.MyViewHolder> {

        private Activity activity;
        private List<Mobile_Request_Model> requestModels;

        public Adapter_Mobile_Request(Activity activity, List<Mobile_Request_Model> requestModels) {
            this.activity = activity;
            this.requestModels = requestModels;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_mobile_request, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
            final Mobile_Request_Model requestData = requestModels.get(i);

            Log.e("TAG", requestData.getName());
            holder.user_name.setText(getCapsSentences(requestData.getName()));

            if (requestData.getStatus().equals("0")) {
                holder.llDetail.setEnabled(true);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.red_low_opacity));
                holder.user_status.setText(getString(R.string.neww));
            } else if (requestData.getStatus().equals("1")) {
                holder.llDetail.setEnabled(true);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.red_low_opacity));
                holder.user_status.setText(R.string.offered_tis_request);
            } else if (requestData.getStatus().equals("2")) {
                holder.user_status.setText(R.string.you_have_ignore);
                holder.llDetail.setEnabled(false);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.default_low_opacity));
            } else if (requestData.getStatus().equals("3")) {
                holder.llDetail.setEnabled(true);
                holder.user_status.setText(R.string.confirm_the_request);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.green_low_opacity));
            } else if (requestData.getStatus().equals("4")) {
                holder.llDetail.setEnabled(false);
                holder.user_status.setText(R.string.sorry_you_have_not_choose);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.light_gray));
            } else if (requestData.getStatus().equals("5")) {
                holder.llDetail.setEnabled(true);
                holder.user_status.setText(R.string.user_rated);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.default_low_opacity));
            } else if (requestData.getStatus().equals("6")) {
                holder.llDetail.setEnabled(false);
                holder.user_status.setText(R.string.complete_the_request_service);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.white));
            } else if (requestData.getStatus().equals("7")) {
                holder.llDetail.setEnabled(false);
                holder.user_status.setText(R.string.the_user_has_complained_to_you);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.white));
            } else if (requestData.getStatus().equals("8")) {
                holder.llDetail.setEnabled(false);
                holder.user_status.setText(R.string.you_complained_to_the_user);
                holder.car_detail.setCardBackgroundColor(activity.getResources().getColor(R.color.white));
            } else {
                holder.user_status.setVisibility(View.GONE);
            }

            if (requestData.getStatus().equals("0") && requestData.getTimeout_status().equals("1")) {
                holder.user_status.setVisibility(View.VISIBLE);
                holder.user_status.setText(getString(R.string.request_de_active));
                holder.llDetail.setEnabled(false);
            }

            //  holder.user_address.setText("Address :       " + requestData.getAddress());
            //  holder.user_contact_no.setText("Contact No.: " + requestData.getContact_no());
            holder.txt_car_year.setText(requestData.getModel_year());
            holder.txt_car_brand.setText(requestData.getBrand_name());
            holder.txt_car_fuel.setText(requestData.getFuel_type());
            holder.txt_car_model.setText(Html.fromHtml(requestData.getModel_name()));

            if (requestData.getRequest_type().length() > 0) {
                holder.lin_services.setVisibility(View.VISIBLE);
                holder.txt_services.setText(Html.fromHtml(requestData.getRequest_type().toString().replaceAll("\\[|\\]", "")));
            } else {
                holder.lin_services.setVisibility(View.GONE);
            }

            if (!requestData.getOil_brand().trim().equals("") || !requestData.getOil_brand().isEmpty()) {
                holder.lin_oil_brand.setVisibility(View.VISIBLE);
                holder.txt_oil_brand.setText(Html.fromHtml(requestData.getOil_brand()));
            } else {
                holder.lin_oil_brand.setVisibility(View.GONE);
            }

            if (!requestData.getLocation_address().equals("")) {
                holder.lin_address.setVisibility(View.VISIBLE);
                holder.txt_address.setText(requestData.getLocation_address());
            } else {
                holder.lin_address.setVisibility(View.GONE);
            }

            if (!requestData.getMobile_no().equals("")) {
                holder.lin_contact.setVisibility(View.VISIBLE);
                holder.txt_contact.setText(requestData.getMobile_no());
            } else {
                holder.lin_contact.setVisibility(View.GONE);
            }

            if (!requestData.getAmount().trim().equals("") || !requestData.getAmount().isEmpty()) {
                holder.lin_amount.setVisibility(View.VISIBLE);
                holder.txt_amount.setText(Html.fromHtml(requestData.getAmount()) + " " + getString(R.string.jd));
            } else {
                holder.lin_amount.setVisibility(View.GONE);
            }

            if (!requestData.getChange_filter().trim().equals("") &&
                    !requestData.getChange_filter().isEmpty() &&
                    !requestData.getChange_filter().toLowerCase().equals("false")) {
                holder.lin_filter.setVisibility(View.VISIBLE);
                holder.txt_filter.setText(Html.fromHtml(requestData.getChange_filter()));
            } else {
                holder.lin_filter.setVisibility(View.GONE);
            }

            if (!requestData.getLeather_seats().trim().equals("") &&
                    !requestData.getLeather_seats().isEmpty() &&
                    !requestData.getLeather_seats().equals("0") && !requestData.getLeather_seats().equals("null")) {
                holder.lin_leather_seat.setVisibility(View.VISIBLE);
                holder.txt_leather_seat.setText(Html.fromHtml(requestData.getLeather_seats()));
            } else {
                holder.lin_leather_seat.setVisibility(View.GONE);
            }

            if (!requestData.getOil_sae().trim().equals("") || !requestData.getOil_sae().isEmpty()) {
                holder.lin_oil_sae.setVisibility(View.VISIBLE);
                holder.txt_oil_sae.setText(Html.fromHtml(requestData.getOil_sae()));
            } else {
                holder.lin_oil_sae.setVisibility(View.GONE);
            }

            holder.llDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (requestData.getStatus().equals("2") || requestData.getStatus().equals("4")) {
                        Toast.makeText(getApplicationContext(), getString(R.string.you_are_not_interrest_for_this_request), Toast.LENGTH_LONG).show();
                    } else {

                        Intent intent = new Intent(activity, Act_Mobile_Request_Detail.class);
                        intent.putExtra(getString(R.string.pref_request_id), requestData.getRequest_id());
                        intent.putExtra(getString(R.string.pref_user_id), requestData.getUser_id());
                        intent.putExtra("model_year", requestData.getModel_year());
                        activity.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return requestModels.size();
        }

        @Override
        public int getItemViewType(int position) {
            return requestModels.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView user_name, txt_car_model, txt_car_brand, txt_car_fuel, txt_car_year, user_status, txt_services, txt_amount,
                    txt_oil_brand, txt_oil_sae, txt_address, txt_contact, txt_filter, txt_leather_seat;

            LinearLayout lin_address, lin_car_model, lin_car_brand,lin_car_fuel,lin_car_year, lin_filter, lin_leather_seat, lin_oil_brand,
                    lin_oil_sae, lin_services, lin_amount, lin_status, lin_contact;

            CircleImageView user_image;
            LinearLayout llDetail;
            CardView car_detail;

            MyViewHolder(@NonNull View itemView) {
                super(itemView);
                user_name = itemView.findViewById(R.id.name);
                txt_car_model = itemView.findViewById(R.id.txt_car_model);
                txt_car_brand = itemView.findViewById(R.id.txt_car_brand);
                txt_car_fuel = itemView.findViewById(R.id.txt_car_fule);
                txt_car_year = itemView.findViewById(R.id.txt_car_year);
                lin_car_model = itemView.findViewById(R.id.lin_car_model);
                lin_car_brand = itemView.findViewById(R.id.lin_car_brand);
                lin_car_fuel = itemView.findViewById(R.id.lin_car_fule);
                lin_car_year = itemView.findViewById(R.id.lin_car_year);
                user_status = itemView.findViewById(R.id.txt_status);
                lin_status = itemView.findViewById(R.id.lin_status);
                llDetail = itemView.findViewById(R.id.llDetail);
                user_image = itemView.findViewById(R.id.user_image);
                txt_services = itemView.findViewById(R.id.txt_services);
                lin_services = itemView.findViewById(R.id.lin_services);
                txt_oil_brand = itemView.findViewById(R.id.txt_oil_brand);
                lin_oil_brand = itemView.findViewById(R.id.lin_oil_brand);
                txt_oil_sae = itemView.findViewById(R.id.txt_oil_sae);
                lin_oil_sae = itemView.findViewById(R.id.lin_oil_sae);
                txt_address = itemView.findViewById(R.id.txt_address);
                lin_address = itemView.findViewById(R.id.lin_address);
                txt_amount = itemView.findViewById(R.id.txt_amount);
                lin_amount = itemView.findViewById(R.id.lin_amount);
                lin_leather_seat = itemView.findViewById(R.id.lin_leather_seat);
                txt_leather_seat = itemView.findViewById(R.id.txt_leather_seat);
                txt_filter = itemView.findViewById(R.id.txt_filter);
                lin_filter = itemView.findViewById(R.id.lin_filter);
                car_detail = itemView.findViewById(R.id.car_detail);
                txt_contact = itemView.findViewById(R.id.txt_contact);
                lin_contact = itemView.findViewById(R.id.lin_contact);

            }
        }

        private String getCapsSentences(String tagName) {
            String[] splits = tagName.toLowerCase().split(" ");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < splits.length; i++) {
                String eachWord = splits[i];
                if (i > 0 && eachWord.length() > 0) {
                    sb.append(" ");
                }
                String cap = eachWord.substring(0, 1).toUpperCase()
                        + eachWord.substring(1);
                sb.append(cap);
            }
            return sb.toString();
        }

    }
}
