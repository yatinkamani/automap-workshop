package com.kcs.automapgarage;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.smsRetrive.SMSListener;
import com.kcs.automapgarage.smsRetrive.smsBroadcastReceiver;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;
import com.mukesh.OtpView;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

public class Act_forgot_password extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack;
    TextView textTitle;
    SharedPreferences sharedpreferences;
    EditText edContactNo, edPwd, edCPwd;
    AutoCompleteTextView edCountryCode;
    CardView card_forgot;
    String[] codes = null;

    LinearLayout lin_send_view;

    View verify_view;
    TextView txt_message, txt_timer;
    OtpView otpView;
    AVLoadingIndicatorView av_loading;
    LinearLayout linBtn;
    Button btn_verify;

    FirebaseAuth auth;
    PhoneAuthProvider.ForceResendingToken token;
    String verificationId;
    private boolean isVerified = false;

    PhoneAuthCredential authCredential;
    boolean isInstant = false;

    String countryCode = "";

    BroadcastReceiver receiver;
    private final int SMS_CONSENT_REQUEST = 1002;
    public boolean isCheck = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_forgot_password);

        lin_send_view = findViewById(R.id.lin_send_view);
        verify_view = findViewById(R.id.verify_view);
        textTitle = findViewById(R.id.textTitle);
        imgBack = findViewById(R.id.imgBack);

        auth = FirebaseAuth.getInstance();
        imgBack.setOnClickListener(this);

        sharedpreferences = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);

        countryCode = getDeviceCountryCode(this);

        bindVerifyView();
        bindSendView();
        getCountryCode();
//        smsRetrieveClient();

        if (getIntent() != null) {
            if (getIntent().getStringExtra(getString(R.string.pref_contact_no)) != null && !getIntent().getStringExtra(getString(R.string.pref_contact_no)).equals("")) {
                edContactNo.setText(getIntent().getStringExtra(getString(R.string.pref_contact_no)));
            }
        }
    }

    // chate password enter view and otp enter view
    public void changeView() {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (lin_send_view.getVisibility() == View.VISIBLE) {
            verify_view.setVisibility(View.VISIBLE);
            lin_send_view.setVisibility(View.GONE);
            textTitle.setText(getString(R.string.veryfy_contact));
            OtpViewShow();
        } else {
            verify_view.setVisibility(View.GONE);
            lin_send_view.setVisibility(View.VISIBLE);
            textTitle.setText(getString(R.string.forgot_password));
        }
    }

    // otp view display
    public void OtpViewShow() {
        otpView.setOtpCompletionListener(otp -> {
            AppUtils.hideKeyboard(Act_forgot_password.this);
        });
        txt_message.setText(getString(R.string.please_type_verification_code) + " \n" + edCountryCode.getText().toString() + " " + edContactNo.getText().toString());
        otpView.setTextColor(Color.BLACK);
        startCountDown(txt_timer);

        if (isInstant && authCredential != null) {
            auth.signInWithCredential(authCredential)
                    .addOnCompleteListener(Act_forgot_password.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                isInstant = true;
                                PasswordChangeApi(edContactNo.getText().toString(), edPwd.getText().toString());
                                Toast.makeText(Act_forgot_password.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                            } else {
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    Toast.makeText(Act_forgot_password.this, getString(R.string.verification_failed_invalid_credentials), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(Act_forgot_password.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                                }
                            }
                            linBtn.setVisibility(View.VISIBLE);
                            av_loading.setVisibility(View.GONE);
                        }
                    });
        }

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isVerified) {
                    PasswordChangeApi(edContactNo.getText().toString(), edPwd.getText().toString());
                } else {
                    if (otpView.getText().toString().length() < 6) {
                        Toast.makeText(Act_forgot_password.this, getString(R.string.enter_otp), Toast.LENGTH_SHORT).show();
                    } else {
                        linBtn.setVisibility(View.VISIBLE);
                        av_loading.setVisibility(View.GONE);
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
                        auth.signInWithCredential(credential)
                                .addOnCompleteListener(Act_forgot_password.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            isVerified = true;
                                            PasswordChangeApi(edContactNo.getText().toString(), edPwd.getText().toString());
                                            Toast.makeText(Act_forgot_password.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                                        } else {
                                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                                Toast.makeText(Act_forgot_password.this, getString(R.string.verification_failed_invalid_credentials), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(Act_forgot_password.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        linBtn.setVisibility(View.VISIBLE);
                                        av_loading.setVisibility(View.GONE);
                                    }
                                });
                    }
                }
            }
        });
    }

    // otp send timer to send data
    CountDownTimer countDownTimer;
    public void startCountDown(TextView textView) {

        //60_000=60 sec or 1 min and another is interval of count down is 1 sec

        countDownTimer = new CountDownTimer(120_000, 1000) {
            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                String min = String.valueOf(remainedSecs / 60);
                String sec = String.valueOf(remainedSecs % 60);
                if (min.length() == 1) {
                    min = "0" + min;
                }

                if (sec.length() == 1) {
                    sec = "0" + sec;
                }

                textView.setText(getString(R.string.enter_code_within) + " " + (min) + ":" + (sec));
                textView.setTextColor(Color.BLACK);
                textView.setBackground(null);
            }

            public void onFinish() {
                textView.setText(getString(R.string.resend_otp));
                textView.setBackground(getResources().getDrawable(R.drawable.btm_line_shape));
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        PhoneAuthProvider.verifyPhoneNumber(PhoneAuthOptions.newBuilder()
                                .setPhoneNumber(edCountryCode.getText().toString() + edContactNo.getText().toString())
                                .setForceResendingToken(token)
                                .setActivity(Act_forgot_password.this)
                                .setTimeout(2L, TimeUnit.MINUTES)
                                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                })
                                .build());
                        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(edCountryCode.getText().toString() + edContactNo.getText().toString()
                                , 2
                                , TimeUnit.MINUTES
                                , Act_forgot_password.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                },
                                token);*/
                    }
                });
            }
        }.start();
    }

    public void bindVerifyView() {
        txt_message = findViewById(R.id.txt_message);
        txt_timer = findViewById(R.id.txt_timer);
        otpView = findViewById(R.id.otpView);
        av_loading = findViewById(R.id.av_loading);
        btn_verify = findViewById(R.id.btn_verify);
        linBtn = findViewById(R.id.linBtn);
    }

    // otp send view view
    public void bindSendView() {
        edCountryCode = findViewById(R.id.edCountryCode);
        edContactNo = findViewById(R.id.edContact);
        edPwd = findViewById(R.id.edPwd);
        edCPwd = findViewById(R.id.edCPwd);
        card_forgot = findViewById(R.id.card_forgot);

        edCountryCode.setVisibility(View.GONE);

        edCountryCode.setOnClickListener(view -> {
            if (codes == null) {
                getCountryCode();
            }
        });

        card_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edContactNo.getText().toString())) {
                    edContactNo.setError(getString(R.string.enter_contatct));
                } else if (TextUtils.isEmpty(edCountryCode.getText().toString())) {
                    edCountryCode.setError(getString(R.string.enter_country_code));
                } else if (TextUtils.isEmpty(edPwd.getText().toString())) {
                    edContactNo.setError(getString(R.string.enter_password));
                } else if (edPwd.getText().toString().length() < 6) {
                    edContactNo.setError(getString(R.string.please_enter_6digit_password));
                } else if (!edPwd.getText().toString().matches(edCPwd.getText().toString())) {
                    edCPwd.setError(getString(R.string.password_does_not_match));
                } else if (edContactNo.getText().toString().length() < 6 || edContactNo.getText().toString().length() > 13) {
                    edContactNo.setError(getString(R.string.contact_is_not_valid));
                } else {
                    if (!edCountryCode.getText().toString().contains("+")) {
                        edCountryCode.setText("+" + edCountryCode.getText().toString());
                    }
                    edCountryCode.setTextDirection(View.TEXT_DIRECTION_LTR);
//                    getCode(edContactNo.getText().toString());
//                    if (checkAndRequestPermissions()){
//                    }
//                    smsRetrieveClient();

//                    if (checkAndRequestPermissions() || isCheck){
                        checkNumber();
//                    }
                }
            }
        });
    }

    // password change after verify otp
    private void PasswordChangeApi(String number, String psw) {

        API api = new API(Act_forgot_password.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(Act_forgot_password.this, getString(R.string.change_password), Toast.LENGTH_SHORT).show();
                    sharedpreferences.edit().clear().apply();
                    Intent intent = new Intent(Act_forgot_password.this, Act_Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finishAffinity();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(Act_forgot_password.this, error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        });
        Map<String, String> map = new HashMap<String, String>();
        map.put("contact_no", number);
        map.put("password", psw);
        map.put(AppUtils.language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));
        if (!isFinishing()){
            api.execute(100, Constant.URL + "garage_forgotpassword", map, true);
        }else {
            api.execute(100, Constant.URL + "garage_forgotpassword", map, false);
        }
    }

    // get country code
    private void getCountryCode() {
        if (AppUtils.isNetworkAvailable(this)) {

            /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            response = Html.fromHtml(response).toString();
                            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                            Log.e("Country Response", response);
                            try {
                                JSONObject obj = new JSONObject(response);
                                if (obj.optString("status").equals("true")) {
                                    JSONObject object = obj.getJSONObject("result");
                                    JSONArray dataArray = object.getJSONArray("country");
                                    codes = new String[dataArray.length()];
                                    for (int i = 0; i < dataArray.length(); i++) {

                                        JSONObject data_obj = dataArray.getJSONObject(i);
                                        codes[i] = data_obj.getString("phone_code");
                                        edCountryCode.setText(codes[0]);
                                    }
                                    ArrayAdapter<String> codeAdapter = new ArrayAdapter<String>(Act_forgot_password.this, android.R.layout.simple_spinner_dropdown_item, codes);
                                    edCountryCode.setAdapter(codeAdapter);
                                    edCountryCode.setThreshold(1);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //  displaying the error in toast if occurs
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            // request queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            requestQueue.add(stringRequest);*/

            API api = new API(getApplicationContext(), new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Log.e("Country Response", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.optString("status").equals("true")) {
                            JSONObject object = obj.getJSONObject("result");
                            JSONArray dataArray = object.getJSONArray("country");
                            codes = new String[dataArray.length()];
                            edCountryCode.setVisibility(View.VISIBLE);
                            edCountryCode.setText("+" + GetCountryZipCode());
                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject data_obj = dataArray.getJSONObject(i);
                                codes[i] = AppUtils.check(data_obj, "phone_code");

                                if (AppUtils.check(data_obj, "country_code").toLowerCase().equals(countryCode.toLowerCase())) {
                                    edCountryCode.setText(AppUtils.check(data_obj, "phone_code"));
                                    edCountryCode.setVisibility(View.GONE);
                                }
                            }
                            ArrayAdapter<String> codeAdapter = new ArrayAdapter<String>(Act_forgot_password.this, android.R.layout.simple_spinner_dropdown_item, codes);
                            edCountryCode.setAdapter(codeAdapter);
                            edCountryCode.setThreshold(1);
                        } else {
                            edCountryCode.setText("+" + GetCountryZipCode());
                            edCountryCode.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        edCountryCode.setText("+" + GetCountryZipCode());
                        edCountryCode.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    edCountryCode.setVisibility(View.VISIBLE);
                    edCountryCode.setText("+" + GetCountryZipCode());
                }
            });
            Map<String, String> map = new HashMap<>();
            map.put(AppUtils.language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));

            api.execute(200, Constant.URL + "get_country", map, false);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
            edCountryCode.setVisibility(View.VISIBLE);
            edCountryCode.setText("+" + GetCountryZipCode());
        }
    }

    // check enter number is available or not
    ProgressDialog progress;
    private void checkNumber() {
        ProgressDialog dialog = new ProgressDialog(Act_forgot_password.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        API api = new API(Act_forgot_password.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("Tag Response", response);

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    if (object.getString("status").equals("false")) {
                        progress = new ProgressDialog(Act_forgot_password.this);
                        progress.setMessage(getString(R.string.loading));
                        progress.setCanceledOnTouchOutside(false);
                        progress.setCancelable(false);
                        progress.show();
                        PhoneAuthProvider.verifyPhoneNumber(PhoneAuthOptions.newBuilder()
                                .setPhoneNumber(edCountryCode.getText().toString() + edContactNo.getText().toString())
                                .setTimeout(2L, TimeUnit.MINUTES)
                                .setActivity(Act_forgot_password.this)
                                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        if (progress != null && progress.isShowing())
                                            progress.dismiss();
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                        dialog.dismiss();
                                        authCredential = phoneAuthCredential;
                                        isInstant = true;
                                        changeView();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        if (progress != null && progress.isShowing())
                                            progress.dismiss();
                                        if (e instanceof FirebaseTooManyRequestsException) {
                                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                                        }
                                        edCountryCode.setVisibility(View.VISIBLE);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        token = forceResendingToken;
                                        verificationId = s;
                                        dialog.dismiss();
                                        if (progress != null && progress.isShowing())
                                            progress.dismiss();
                                        changeView();
                                    }
                                })
                                .build());
                        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(edCountryCode.getText().toString() + edContactNo.getText().toString()
                                , 2
                                , TimeUnit.MINUTES
                                , Act_forgot_password.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                        dialog.dismiss();
                                        authCredential = phoneAuthCredential;
                                        isInstant = true;
                                        changeView();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        if (e instanceof FirebaseTooManyRequestsException){
                                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                        }else {
                                            Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                                        }
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        token = forceResendingToken;
                                        verificationId = s;
                                        dialog.dismiss();
                                        changeView();
                                    }
                                });*/
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.enter_registered_contact), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("contact_no", edContactNo.getText().toString());
        map.put("email", "email");
        map.put(AppUtils.language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));

        api.execute(100, Constant.URL + "checkGarareEmailAndContact", map, true);
    }

    // not use
    public void smsRetrieveClient(){
        /*SmsRetrieverClient client = SmsRetriever.getClient(this);

        Task<Void> task = client.startSmsUserConsent(null);
        smsBroadcastReceiver.initSMSListener(null);
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsVerificationReceiver, intentFilter);
        SmsRetriever.getClient(this).startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("Tag Client Success", "SuccessFull");
                *//*smsBroadcastReceiver.initSMSListener(new SMSListener() {
                    @Override
                    public void onSuccess(String message) {
                        System.out.println("Tag Message  "+ message);
                    }

                    @Override
                    public void onError(String message) {
                        Log.e("Tag Message", message);
                    }
                });*//*
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                Log.e("Tag Client Failed", e.getMessage());
            }
        });*/

//        checkAndRequestPermissions();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    final String sender = intent.getStringExtra("Sender");

                    Toast.makeText(getBaseContext(), AppUtils.getOTPForMessage(message), Toast.LENGTH_LONG).show();
                    Log.e("OTP MESSAGE", message);
                    changeView();
                    otpView.setText(AppUtils.getOTPForMessage(message));
                    btn_verify.performClick();
                }
            }
        };

    }

    private final BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get consent intent
                        Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                        try {
                            // Start activity to show consent dialog to user, activity must be started in
                            // 5 minutes, otherwise you'll receive another TIMEOUT intent
                            startActivityForResult(consentIntent, SMS_CONSENT_REQUEST);
                        } catch (ActivityNotFoundException e) {
                            // Handle the exception ...
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Time out occurred, handle the error.
                        break;
                }
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    private boolean checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            int receiveSMS = ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS);
            int readSMS = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS);
            List<String> listPermissionsNeeded;
            listPermissionsNeeded = new ArrayList<>();
            if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
            }

            if (readSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_SMS);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
                return false;
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)){
                return true;
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)){
                return true;
            }

            return true;
        }
        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        checkNumber();
        isCheck = true;
        Map<String, Integer> perms = new HashMap<>();

        perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);

        // Fill with actual results from user
        if (grantResults.length > 0) {
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for both permissions
            if (perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
            ) {
                Log.d("TAG", "camera & location services permission granted");
                checkNumber();
            } else {
                /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getApplicationContext(), "permission Denied", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "you not use auto read otp in this app", Toast.LENGTH_LONG).show();
                }*/
                checkNumber();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Tag String Message", "Code get It "+resultCode);
        if (requestCode == SMS_CONSENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get SMS message content
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                // Extract one-time code from the message and complete verification
                // `sms` contains the entire text of the SMS message, so you will need
                // to parse the string.
                if (message != null) {
                    // get the message here
                    Log.e("Tag Otp", message);
                    if (message != null) {
                        otpView.setText(parseOneTimeCode(message));
                        Log.e("Tag Otp", parseOneTimeCode(message));
                    }
                }
                // send one time code to the server
            } else {
                // Consent canceled, handle the error ...
            }
        }

    }

    private String parseOneTimeCode(String otp) {
        return otp.substring(0, 6);
    }

    @Override
    public void onClick(View view) {
        if (view == imgBack) {
            onBackPressed();
        }
    }

    // get country zip code
    public String GetCountryZipCode() {
        String CountryID = "";
        String CountryZipCode = "962";

        CountryID = getDeviceCountryCode(this);
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    @Override
    public void onBackPressed() {
        if (verify_view.getVisibility() == View.VISIBLE) {
            changeView();
        } else {
            super.onBackPressed();
        }
    }

    // get country code on based on device
    private static @NotNull String getDeviceCountryCode(@NotNull Context context) {
        String countryCode;

        // Try to get country code from TelephonyManager service
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            // Query first getSimCountryIso()
            countryCode = tm.getSimCountryIso();
            if (countryCode != null && countryCode.length() == 2)
                return countryCode;

            if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                // Special case for CDMA Devices
                String network = tm.getNetworkCountryIso();
                if (network != null && network.length() == 2) {
                    countryCode = tm.getNetworkCountryIso();
                }
            } else {
                // For 3G devices (with SIM) query getNetworkCountryIso()
                countryCode = tm.getNetworkCountryIso();
            }

            if (countryCode != null && countryCode.length() == 2)
                return countryCode;
        }

        // If network country not available (tablets maybe), get country code from Locale class
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            countryCode = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            countryCode = context.getResources().getConfiguration().locale.getCountry();
        }

        if (countryCode != null && countryCode.length() == 2)
            return countryCode;

        // General fallback to "us"
        return countryCode;
    }

    public void getCode(String mobileNo) {

        PhoneNumberUtil utils = PhoneNumberUtil.createInstance(this);
        try {
            for (String region : utils.getSupportedRegions()) {
                // Check whether it's a valid number.
                boolean isValid = utils.isPossibleNumber(mobileNo, region);
                if (isValid) {
                    Phonenumber.PhoneNumber number = utils.parse(mobileNo, region);
                    // Check whether it's a valid number for the given region.
                    isValid = utils.isValidNumberForRegion(number, region);
                    if (isValid) {
                        Log.d("Region:", region); // IN
                        Log.d("Phone Code", "" + number.getCountryCode()); // 91
                        Log.d("Phone No.", "" + number.getNationalNumber()); // 99xxxxxxxxxx
                    }
                }
            }
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
    }

}
