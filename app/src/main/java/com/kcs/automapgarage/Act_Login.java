package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Active_user_model;
import com.kcs.automapgarage.Model.Check_user_Model;
import com.kcs.automapgarage.Model.Login_Model;
import com.kcs.automapgarage.Model.Services_Model;
import com.kcs.automapgarage.newPackage.Helper;
import com.kcs.automapgarage.utils.AppUtils;


import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Login extends AppCompatActivity {

    public static final String LOGIN_PREF = "Login_preference";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    public static String NAME;
    public static String PASSWORD = "phoneKey";
    public static String USER_ID = "emailKey";
    public static String EMAIL = "emailKey";

    //     Button btn_login, btn_forgot, btn_fb, btn_google;

    TextView btn_new_user;

    TextView btn_login, forgot;
    EditText ed_contact, ed_pass;

    String st_contact, st_pass;
    FirebaseAnalytics mFireBaseAnalytics;

    //    String checkGarage = "http://webmobdemo.xyz/automap/api/check_garage_status";
    //    String activeUserApi = "http://webmobdemo.xyz/automap/api/active_account";
    //    private static final String URL_FOR_LOGIN = "http://webmobdemo.xyz/automap/api/garage_login";

    // server key :- AAAAI4D9SwY:APA91bFxG1dyZ0CiM1qh-FJXZEcgN0UQyPgYfF38rsAlPaZpdjPUvfVt4aTNeL6UvqbU6z1_jDoNxfTUekBAJ2MeO9tNThl71KxMOHLkJSsyZ7z7r7lyxjPUkSebsj0qkVoXb_ZnJKNU

    private ArrayList<Login_Model> loginModel = new ArrayList<>();
    private ArrayList<Active_user_model> Active_user_model;
    private Active_user_model active_user_model = new Active_user_model();
    private ArrayList<Check_user_Model> check_model;

    Check_user_Model playerModel = new Check_user_Model();
    ProgressDialog progressDialog;

    Login_Model login_model = new Login_Model();
    ImageView login_back;
    String DEVICE_TOKEN;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            /*if (lng.equals("ar")){
                preferences.edit().putString(getString(R.string.pref_language), lng).apply();
            }else {
                preferences.edit().putString(getString(R.string.pref_language), "ar").apply();
            }*/
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
            overrideConfiguration.setLayoutDirection(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__login);

        // firebase initialize and get token
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().setFcmAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    return;
                }
                DEVICE_TOKEN = task.getResult().getToken();
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
                //  "fihfQBbZf7M:APA91bEDndPUJIwb-naFmnlkF0EhTd8iFH3cehat2Dwo67j0UZ7NyarRT-RFcoH2ifnA5EDjAtImN6rPvmULC9dHq_kzmDMG8L0SSSK4g3vuTxKGRlluEKpMNZGt0lVtMIK7xJYYzqgY"
            }
        });
        InitializeToken();
        mFireBaseAnalytics = FirebaseAnalytics.getInstance(this);
        login_back = (ImageView) findViewById(R.id.login_back);
        login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sharedpreferences = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        Intent intent = getIntent();
        if (sharedpreferences.getString(getString(R.string.pref_language), "").equals("")){
            setDefault();
        }
        onNewIntent(intent);

        btn_login = findViewById(R.id.btn_Login);
        btn_new_user = findViewById(R.id.btn_new_user);
        forgot = findViewById(R.id.TV_forgot);
        ed_contact = findViewById(R.id.Ed_L_uid);
        ed_pass = findViewById(R.id.Ed_L_pwd);

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_Login.this, Act_forgot_password.class);
                intent.putExtra(getString(R.string.pref_contact_no), ed_contact.getText().toString());
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(Act_Login.this);
                st_contact = ed_contact.getText().toString().trim();
                st_pass = ed_pass.getText().toString();

                if (TextUtils.isEmpty(st_contact)) {
                    ed_contact.setError(getString(R.string.enter_email));
                } else if (!isMobile(st_contact)) {
                    ed_contact.setError(getString(R.string.contact_is_not_valid));
                } else if (TextUtils.isEmpty(st_pass)) {
                    ed_pass.setError(getString(R.string.enter_password));
                } else if (!Helper.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), R.string.internet_not_connect, Toast.LENGTH_LONG).show();
                } else {
                    RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
                    progressDialog.setMessage(getString(R.string.loading));
                    if (!isFinishing() && progressDialog!=null){
                        progressDialog.show();
                    }
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "garage_login", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            response = Html.fromHtml(response).toString();
                            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                            Log.e("response", " " + response);

                            if (!isFinishing() && progressDialog!=null){
                                progressDialog.dismiss();
                            }
                            try {
                                JSONObject obj = new JSONObject(response);
                                loginModel = new ArrayList<>();
                                boolean isModelSelect = false;
                                String provider_type = "", part_type = "", owner_name = "", area_id = "", sales_id = "", garage_banner = "",
                                        manufacture_country_id = "", dealer_service_id = "", user_type = "", language = "ar";
                                if (obj.optString("status").equals("true")) {

                                    login_model.setStatus_up(obj.getString("status"));
                                    login_model.setMessage(obj.getString("message"));

                                    JSONArray dataArray = obj.getJSONArray("result");

                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject data_obj = dataArray.getJSONObject(i);
                                        login_model.setUser_id(data_obj.getString("garage_id"));
                                        login_model.setName(check(data_obj,"garage_name").equals("null") ? check(data_obj,"owner_name") : check(data_obj,"garage_name"));
                                        login_model.setEmail(check(data_obj,"email"));
                                        login_model.setContact_no(data_obj.getString("contact_no"));
                                        login_model.setCountry_id(data_obj.getString("country_id"));
//                                        login_model.setState(data_obj.getString("state"));
//                                        login_model.setCity_id("41");
                                        login_model.setCity_id(data_obj.optString("city_id", "null"));
                                        login_model.setAddress(check(data_obj,"address"));
                                        login_model.setLati(data_obj.getString("latitude"));
                                        login_model.setLongi(data_obj.getString("longitude"));
                                        login_model.setService_id(data_obj.getString("service_id"));
                                        login_model.setStatus(data_obj.getString("status"));
                                        login_model.setUpdate_date(data_obj.getString("updated_at"));
                                        area_id = data_obj.has("area_id") ? data_obj.getString("area_id") : "";
                                        owner_name = check(data_obj,"owner_name");
//                                        area_id = "87";
                                        sales_id = data_obj.has("salesman_id") ? data_obj.getString("salesman_id") : "";
                                        garage_banner = data_obj.has("garage_banner") ? data_obj.getString("garage_banner") : "";
                                        user_type = data_obj.has("user_type") ? data_obj.getString("user_type") : "0";
//                                        language = data_obj.has("language") ? data_obj.getString("language") : "ar";
                                        login_model.setService_id(data_obj.getString("service_id"));
                                        if (data_obj.has("provider_type_id")) {
                                            provider_type = data_obj.getString("provider_type_id");
                                        }
                                        if (data_obj.has("part_type_id")) {
                                            part_type = data_obj.getString("part_type_id");
                                        }
                                        if (data_obj.has("manufacture_country_id")) {
                                            manufacture_country_id = data_obj.getString("manufacture_country_id");
                                        }

                                        dealer_service_id = data_obj.getString("dealer_service_id");

                                        if (provider_type.equals("0") && (login_model.getService_id().equalsIgnoreCase("18") ||
                                                login_model.getService_id().equalsIgnoreCase("4") /*||
                                                login_model.getService_id().equalsIgnoreCase("9") ||
                                                login_model.getService_id().equalsIgnoreCase("10") ||
                                                login_model.getService_id().equalsIgnoreCase("12") ||
                                                login_model.getService_id().equalsIgnoreCase("13")*/)) {
                                            isModelSelect = true;
                                        }

                                        loginModel.add(login_model);
                                    }
//                                    Log.e("garage_id", login_model.getUser_id());
//                                    Log.e("name", login_model.getName());
//                                    Log.e("email_id", login_model.getEmail());

                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString(getString(R.string.pref_garage_id), login_model.getUser_id());
                                    editor.putString(getString(R.string.pref_garage_name), login_model.getName());
                                    editor.putString(getString(R.string.pref_owner_name), owner_name);
                                    editor.putString(getString(R.string.pref_garage_pwd), ed_pass.getText().toString());
                                    editor.putString(getString(R.string.pref_email), login_model.getEmail());
                                    editor.putString(getString(R.string.pref_workshop_image), garage_banner);
                                    editor.putString(getString(R.string.pref_user_image), garage_banner);
                                    editor.putString(getString(R.string.pref_contact_no), login_model.getContact_no());
                                    editor.putString(getString(R.string.pref_country_id), login_model.getCountry_id());
                                    editor.putString(getString(R.string.pref_provider_type), provider_type);
                                    editor.putString(getString(R.string.pref_area_id), area_id);
                                    editor.putString(getString(R.string.pref_city_id), login_model.getCity_id());
                                    editor.putString(getString(R.string.pref_sales_id), sales_id);
                                    editor.putString(getString(R.string.pref_service_id), login_model.getService_id());
                                    editor.putString(getString(R.string.pref_part_type_choose), part_type);
                                    editor.putString(getString(R.string.pref_part_type), part_type);
                                    editor.putString(getString(R.string.pref_manufacturing_country_id), manufacture_country_id);
                                    editor.putString(getString(R.string.pref_dealer_service), dealer_service_id);
                                    editor.putString(getString(R.string.pref_user_type), user_type);
//                                    editor.putString(getString(R.string.pref_language), language);
                                    editor.apply();
                                    Bundle bundle = new Bundle();
                                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Login Garage Id " + login_model.getUser_id());
                                    mFireBaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                                    // login first time to service screen redirect
                                    if (login_model.getService_id().equals("0")) {

                                        Intent i = new Intent(Act_Login.this, Act_Garage_Car_Service.class);
                                        i.putExtra("SPLASH", true);
                                        startActivity(i);
                                        finish();
                                    }
                                    // user provider not select to check part type api
                                    // 18 means workshop and 4 means part shop service type
                                    else {
                                        if (provider_type.equals("0") && (login_model.getService_id().equalsIgnoreCase("18") ||
                                                login_model.getService_id().equalsIgnoreCase("4") /*||
                                                login_model.getService_id().equalsIgnoreCase("9") ||
                                                login_model.getService_id().equalsIgnoreCase("10") ||
                                                login_model.getService_id().equalsIgnoreCase("12") ||
                                                login_model.getService_id().equalsIgnoreCase("13")*/)) {
                                            editor = sharedpreferences.edit();
                                            editor.putBoolean(getString(R.string.pref_isModel), true);
                                            editor.apply();
                                            GetGarageCarBrandModelData(login_model.getUser_id());
                                        } else if (login_model.getService_id().equals("61") && (!login_model.getCity_id().equals("null") || !area_id.equals("0"))) {
                                            Intent intent = new Intent(Act_Login.this, Navigation_Activity.class);
                                            startActivity(intent);
                                            finish();
                                        } else if (login_model.getService_id().equals("61")) {
                                            Intent intent = new Intent(Act_Login.this, Navigation_Activity.class);
                                            startActivity(intent);
                                            finish();
                                        } else if (isModelSelect) {
                                            editor = sharedpreferences.edit();
                                            editor.putBoolean(getString(R.string.pref_isModel), true);
                                            editor.apply();
                                            GetGarageCarBrandModelData(login_model.getUser_id());
                                        } /*else if (provider_type.equals("0")){
                                            get_garage_service();
                                        }*/ else {
                                            editor = sharedpreferences.edit();
                                            editor.putBoolean(getString(R.string.pref_isModel), false);
                                            editor.apply();
                                            retrieveJSON(login_model.getUser_id());
                                        }
                                    }

                                    Toast.makeText(Act_Login.this, login_model.getMessage(), Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(Act_Login.this, getString(R.string.login_successfully), Toast.LENGTH_SHORT).show();
                                } else {
                                    if (!isFinishing() && progressDialog!=null){
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(Act_Login.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (!isFinishing() && progressDialog!=null){
                                    progressDialog.dismiss();
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (!isFinishing() && progressDialog!=null){
                                progressDialog.hide();
                            }
                            //  Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
//                            Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                            Log.i("My error", "" + error);
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> map = new HashMap<String, String>();
                            map.put("email", st_contact);
                            map.put("contact_no", st_contact);
                            map.put("password", st_pass);
                            map.put("device_token", DEVICE_TOKEN);
                            String language = "";
                            if (sharedpreferences.getString(getString(R.string.pref_language),"ar").equals("ar")){
                                language = "Arabic";
                            } else {
                                language = "English";
                            }
                            map.put(AppUtils.language,language);
                            Log.e("params", " " + map);
                            return map;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queue.add(request);
//                    LoginApi();
                }
            }
        });

        btn_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_new = new Intent(Act_Login.this, Act_Registration.class);
                startActivity(i_new);
            }
        });
    }

    // token generate from firebase
    public void InitializeToken() {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
                // Used to get firebase token until its null so it will save you from null pointer exeption
                while(DEVICE_TOKEN == null) {
                    DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
                }
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
            }
        }.execute();
    }

    public void setDefault() {
        Locale locale = Locale.getDefault();

        locale.getLanguage();
        Log.e("Tag Language", getResources().getConfiguration().locale.getLanguage());
        SharedPreferences.Editor editor = sharedpreferences.edit();
        String lng = getResources().getConfiguration().locale.getLanguage();
        /*if (lng.equals("ar")){
                sharedpreferences.edit().putString(getString(R.string.pref_language), lng).apply();
        }else {
                sharedpreferences.edit().putString(getString(R.string.pref_language), "en").apply();
        }*/

        editor.putString(getString(R.string.pref_language), "ar").apply();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    // account active or not but current not use
    private void active(final String id, final String type) {
        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "active_account", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);

                try {
                    JSONObject obj = new JSONObject(response);
                    obj.getString("message");
                    String msg = obj.getString("message");
                    active_user_model.setMessage(obj.getString("message"));
                    Toast.makeText(Act_Login.this, msg, Toast.LENGTH_SHORT).show();
                    Active_user_model = new ArrayList<>();
                    Active_user_model.add(active_user_model);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                map.put("type", type);
                Log.e("test2", " " + map.toString());

                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    // check user garage is active or not and check user status
    private void retrieveJSON(final String id) {

        if (progressDialog != null){
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
        }

        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "check_garage_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);

                try {
                    Log.e("check_garage_response", " " + response);
                    JSONObject obj = new JSONObject(response);
                    obj.getString("request");
                    if (obj.optString("status").equals("true")) {
                        check_model = new ArrayList<>();
                        JSONArray dataArray = obj.getJSONArray("result");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            playerModel.setAddress(data_obj.getString("garage_id"));
                            playerModel.setName(check(data_obj,"garage_name").equals("null") ? check(data_obj,"owner_name") : check(data_obj,"garage_name"));
                            playerModel.setEmail(check(data_obj,"email"));
                            playerModel.setContect_no(data_obj.getString("contact_no"));
                            playerModel.setContry_id(data_obj.getString("country_id"));
                            playerModel.setCity(data_obj.optString("city_id", ""));
                            playerModel.setAddress(check(data_obj,"address"));
                            playerModel.setLatitude(data_obj.getString("latitude"));
                            playerModel.setLongitude(data_obj.getString("longitude"));
                            playerModel.setService_id(data_obj.getString("service_id"));
                            playerModel.setStatus(data_obj.getString("status"));
                            playerModel.setUpdated_at(data_obj.getString("updated_at"));
                            playerModel.setUser_image(data_obj.getString("owner_profile_img"));
                            playerModel.setBanner_image(data_obj.getString("garage_banner"));

                            try {
                                playerModel.setProvide_role(data_obj.getString("provider_type_id"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            check_model.add(playerModel);
                        }
                        editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_user_image), playerModel.getUser_image());
                        editor.putString(getString(R.string.pref_workshop_image), playerModel.getBanner_image());
                        editor.putString(getString(R.string.pref_provider_role), "");
                        editor.putString(getString(R.string.pref_service_id), playerModel.getService_id());
                        editor.putString(getString(R.string.pref_provider_type), playerModel.getProvide_role());
                        editor.apply();

                        if (obj.getString("request").equals("profile")) {
                            if (!isFinishing() && progressDialog != null)
                                progressDialog.dismiss();

                            if (playerModel.getService_id().equals("61")){
                                Intent intent = new Intent(Act_Login.this, Navigation_Activity.class);
                                startActivity(intent);
                                finish();
                            } else {

                                Intent intent = new Intent(Act_Login.this, Act_Edit_Profile_Detail.class);
                                intent.putExtra(getString(R.string.pref_garage_id), login_model.getUser_id());
                                intent.putExtra(getString(R.string.pref_garage_name), login_model.getName());
                                intent.putExtra(getString(R.string.pref_email), login_model.getEmail());
                                intent.putExtra(getString(R.string.pref_country_id), login_model.getCountry_id());
                                intent.putExtra(getString(R.string.pref_contact_no), login_model.getContact_no());
                                intent.putExtra("SPLASH", true);
                                startActivity(intent);
                                finish();
                            }
                        } else if (obj.getString("request").equals("dashboard")) {
                            if (!isFinishing() && progressDialog != null)
                                progressDialog.dismiss();
                            Intent intent = new Intent(Act_Login.this, Navigation_Activity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (!isFinishing() && progressDialog != null)
                        progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (!isFinishing() && progressDialog != null)
                    progressDialog.dismiss();
//                Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", id);
                map.put(AppUtils.language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));

                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // check workshop select car brand model or not
    private void GetGarageCarBrandModelData(final String garage_id) {

        if (progressDialog != null ){
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_garage_brand_model", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("response", " " + response);

                if (progressDialog != null)
                    progressDialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("1")) {
//                            getGarageDataApi(id);
                        JSONObject object = obj.getJSONObject("result");

                        if (object.has("brand_model")) {

                            Object o = object.get("brand_model");
                            if (o instanceof JSONArray) {

                                JSONArray array = object.getJSONArray("brand_model");
                                if (array.length() > 0) {
                                    retrieveJSON(garage_id);
                                } else {
                                    Intent i = new Intent(Act_Login.this, Act_Edit_Car_Model.class);
                                    i.putExtra("SPLASH", true);
                                    startActivity(i);
                                    finish();
                                }
                            } else {
                                Intent i = new Intent(Act_Login.this, Act_Edit_Car_Model.class);
                                i.putExtra("SPLASH", true);
                                startActivity(i);
                                finish();
                            }
                        } else {
                            Intent i = new Intent(Act_Login.this, Act_Edit_Car_Model.class);
                            i.putExtra("SPLASH", true);
                            startActivity(i);
                            finish();
                        }

                    } else {
                        Intent i = new Intent(Act_Login.this, Act_Edit_Car_Model.class);
                        i.putExtra("SPLASH", true);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (progressDialog != null)
                        progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                Log.e("Tag", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("garage_id", garage_id);
                Log.e("params", " " + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void get_garage_service() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_service_provider_facility", new Response.Listener<String>() {
            @SuppressLint({"Long_Log_Tag", "LongLogTag"})
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("garage_facility", " " + response);
                try {

                    Services_Model normal_service = null;

                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("status").equals("true")) {

                        boolean isProvider = false;
                        JSONArray jsonArray = obj.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String service_id = object.getString("service_id");
                            String service_provider = object.getString("service_provider");
                            //  String service_type = object.getString("service_type");
                            String service_name = object.getString("service_name");
                            String service_image = object.getString("service_image");
                            String created_at = object.getString("created_at");
                            String updated_at = object.getString("updated_at");

                            if (service_id.equals(login_model.getService_id())){
                                normal_service = new Services_Model(service_id, service_provider, "", service_name, service_image, created_at, updated_at);

                                for (String s : normal_service.getService_provider().split(",")){

                                    if (s.equalsIgnoreCase("GET QUOTES and booking") ||
                                            s.equalsIgnoreCase("Request mobile Auto service")){
                                        isProvider = true;
                                        Log.e("Tag service  contains",service_provider);

                                        break;
                                    }
                                }
                            }
                        }

                        if (isProvider){
                            editor = sharedpreferences.edit();
                            editor.putString(getString(R.string.pref_service_id), normal_service.getService_id());
                            editor.putString(getString(R.string.pref_special_service_name), normal_service.getService_name());
                            editor.putString(getString(R.string.pref_provider_service),"true");
                            editor.putBoolean("car_service", true);
                            editor.apply();
                            Intent intent = new Intent(Act_Login.this, Act_Edit_Car_Model.class);
                            intent.putExtra("SPLASH", true);
                            startActivity(intent);
                            finish();
                        } else {
                            editor = sharedpreferences.edit();
                            editor.putBoolean(getString(R.string.pref_isModel), false);
                            editor.apply();
                            retrieveJSON(login_model.getUser_id());
                        }

                    } else {

                        Toast.makeText(getApplicationContext(), "" + obj.getString("status").equals("1"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(Act_Garage_Car_Service.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.e("My error", "" + error);
                progressDialog.dismiss();

            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null && intent.getData() != null) {
            String s0 = intent.getData().toString();
            Log.e("Tag Intent Data", s0);
            String s1 = s0.substring(0, 54);
            Log.e("first", " " + s1);
            String s2 = s0.substring(54, s0.length());
            Log.e("second", s2);

            String[] separated = s0.split("/");
            String str1 = separated[0];
            String str2 = separated[1];
            String str3 = separated[2]; // = stackOverflow.com
            String str4 = separated[3]; // = questions
            String str5 = separated[4];
            String str6 = separated[5];
            String str7 = separated[6];
            String str8 = separated[7];

            /*byte[] decodeValue = Base64.decode(s2.trim(), Base64.DEFAULT);
            Log.e("decode", "" + new String(decodeValue));
            String decode = new String();
            decode = new String(decodeValue);*/

            active(str8, str7);
        }
    }

    public boolean isMobile(@NotNull String data) {

        if (data.matches("[0-9]+")) {
            return true;
        } else {
            return false;
        }
    }
}
