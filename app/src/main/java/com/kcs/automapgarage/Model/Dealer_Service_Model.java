package com.kcs.automapgarage.Model;

public class Dealer_Service_Model {

    private String dealer_service_request_id, service_id, dealer_service_id, dealer_service_name, service_name, created_at;

    public Dealer_Service_Model(String dealer_service_request_id, String service_id, String dealer_service_id, String dealer_service_name, String service_name, String created_at) {
        this.dealer_service_request_id = dealer_service_request_id;
        this.service_id = service_id;
        this.dealer_service_id = dealer_service_id;
        this.dealer_service_name = dealer_service_name;
        this.service_name = service_name;
        this.created_at = created_at;
    }

    public String getDealer_service_request_id() {
        return dealer_service_request_id;
    }

    public void setDealer_service_request_id(String dealer_service_request_id) {
        this.dealer_service_request_id = dealer_service_request_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getDealer_service_id() {
        return dealer_service_id;
    }

    public void setDealer_service_id(String dealer_service_id) {
        this.dealer_service_id = dealer_service_id;
    }

    public String getDealer_service_name() {
        return dealer_service_name;
    }

    public void setDealer_service_name(String dealer_service_name) {
        this.dealer_service_name = dealer_service_name;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
