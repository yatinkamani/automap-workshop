package com.kcs.automapgarage.Model;

import java.util.ArrayList;
import java.util.List;

public class Request_Data_Model {

    private String garage_appointment_id;
    private String garage_appointment_detail_id;
    private String user_id;
    private String garage_id;
    private String name;
    private String email;
    private String contact_no;
    private String state;
    private String city;
    private String address;
    private String pincode;
    private String user_image;
    private String latitude;
    private String longitude;
    private String status;
    private String date;
    private String model_name;
    private String brand;
    private String model_year;
    private String variant_name;
    private String fuel_name;
    private String problem;
    private String appointment_date;
    private String appointments_time;
    private List<Garage_Facility> garageFacilityList;
    private ArrayList<String> image;
    private String oil_brand;
    private String tire_brand;
    private String battery_brand;
    private String tire_size_no;
    private String rims_brand;
    private String rims_size;
    private String oil_sae;
    private String timeout_status;
    private ArrayList<String> appointment_type;

    public Request_Data_Model(String garage_appointment_id, String garage_appointment_detail_id, String user_id,
                              String garage_id, String name, String email, String contact_no, String state, String city,
                              String address, String pincode, String user_image, String latitude, String longitude,
                              String status, String date, String model_name, String brand, String model_year,
                              String variant_name, String fuel_name, String problem, String appointment_date,
                              String appointments_time, List<Garage_Facility> garageFacilityList, ArrayList<String> image,
                              String oil_brand, String tire_brand, String battery_brand, String tire_size_no, String oil_sae,
                              String rims_brand, String rims_size, ArrayList<String> appointment_type, String timeout_status) {
        this.garage_appointment_id = garage_appointment_id;
        this.garage_appointment_detail_id = garage_appointment_detail_id;
        this.user_id = user_id;
        this.garage_id = garage_id;
        this.name = name;
        this.email = email;
        this.contact_no = contact_no;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pincode = pincode;
        this.user_image = user_image;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.date = date;
        this.model_name = model_name;
        this.brand = brand;
        this.model_year = model_year;
        this.variant_name = variant_name;
        this.fuel_name = fuel_name;
        this.problem = problem;
        this.appointment_date = appointment_date;
        this.appointments_time = appointments_time;
        this.garageFacilityList = garageFacilityList;
        this.image = image;
        this.oil_brand = oil_brand;
        this.tire_brand = tire_brand;
        this.battery_brand = battery_brand;
        this.tire_size_no = tire_size_no;
        this.oil_sae = oil_sae;
        this.rims_brand = rims_brand;
        this.rims_size = rims_size;
        this.appointment_type = appointment_type;
        this.timeout_status = timeout_status;
    }

    public String getTimeout_status() {
        return timeout_status;
    }

    public void setTimeout_status(String timeout_status) {
        this.timeout_status = timeout_status;
    }

    public String getGarage_appointment_id() {
        return garage_appointment_id;
    }

    public void setGarage_appointment_id(String garage_appointment_id) {
        this.garage_appointment_id = garage_appointment_id;
    }

    public String getGarage_appointment_detail_id() {
        return garage_appointment_detail_id;
    }

    public void setGarage_appointment_detail_id(String garage_appointment_detail_id) {
        this.garage_appointment_detail_id = garage_appointment_detail_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGarage_id() {
        return garage_id;
    }

    public void setGarage_id(String garage_id) {
        this.garage_id = garage_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel_year() {
        return model_year;
    }

    public void setModel_year(String model_year) {
        this.model_year = model_year;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getFuel_name() {
        return fuel_name;
    }

    public void setFuel_name(String fuel_name) {
        this.fuel_name = fuel_name;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointments_time() {
        return appointments_time;
    }

    public void setAppointments_time(String appointments_time) {
        this.appointments_time = appointments_time;
    }

    public List<Garage_Facility> getGarageFacilityList() {
        return garageFacilityList;
    }

    public void setGarageFacilityList(List<Garage_Facility> garageFacilityList) {
        this.garageFacilityList = garageFacilityList;
    }

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    public String getOil_brand() {
        return oil_brand;
    }

    public void setOil_brand(String oil_brand) {
        this.oil_brand = oil_brand;
    }

    public String getTire_brand() {
        return tire_brand;
    }

    public void setTire_brand(String tire_brand) {
        this.tire_brand = tire_brand;
    }

    public String getBattery_brand() {
        return battery_brand;
    }

    public void setBattery_brand(String battery_brand) {
        this.battery_brand = battery_brand;
    }

    public String getTire_size_no() {
        return tire_size_no;
    }

    public void setTire_size_no(String tire_size_no) {
        this.tire_size_no = tire_size_no;
    }

    public String getOil_sae() {
        return oil_sae;
    }

    public void setOil_sae(String oil_sae) {
        this.oil_sae = oil_sae;
    }

    public String getRims_brand() {
        return rims_brand;
    }

    public void setRims_brand(String rims_brand) {
        this.rims_brand = rims_brand;
    }

    public String getRims_size() {
        return rims_size;
    }

    public void setRims_size(String rims_size) {
        this.rims_size = rims_size;
    }

    public ArrayList<String> getAppointment_type() {
        return appointment_type;
    }

    public void setAppointment_type(ArrayList<String> appointment_type) {
        this.appointment_type = appointment_type;
    }
}
