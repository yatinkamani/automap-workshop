package com.kcs.automapgarage.Model;

public class Dealer_Request_Model {

    private String request_id, deal_receiver_id, sender_garage_id, name, city, area, contact, banner_image, car_info, timeout_status,
                    price_request, item_name, item_quantity, item_type, return_type, item_image, price, status, available_quantity,
                    request_time, create_at, maintenance_name, desc_type, done_time;

    public Dealer_Request_Model(String request_id, String deal_request_id, String sender_garage_id, String name, String city,
                                String area, String contact, String banner_image, String car_info, String price_request, String price,
                                String item_name, String item_quantity, String available_quantity, String item_type, String return_type, String item_image,
                                String status, String timeout_status) {
        this.request_id = request_id;
        this.deal_receiver_id = deal_request_id;
        this.sender_garage_id = sender_garage_id;
        this.name = name;
        this.city = city;
        this.area = area;
        this.contact = contact;
        this.banner_image = banner_image;
        this.car_info = car_info;
        this.price_request = price_request;
        this.price = price;
        this.item_name = item_name;
        this.item_quantity = item_quantity;
        this.available_quantity = available_quantity;
        this.item_type = item_type;
        this.return_type = return_type;
        this.item_image = item_image;
        this.status = status;
        this.timeout_status = timeout_status;

    }

    public Dealer_Request_Model(String request_id, String deal_request_id, String sender_garage_id, String name, String city,
                                String area, String contact, String banner_image, String car_info, String price_request, String price,
                                String item_name, String item_quantity, String available_quantity, String item_type, String return_type,
                                String item_image, String status, String timeout_status, String request_time, String create_at,
                                String maintenance_name, String desc_type) {
        this.request_id = request_id;
        this.deal_receiver_id = deal_request_id;
        this.sender_garage_id = sender_garage_id;
        this.name = name;
        this.city = city;
        this.area = area;
        this.contact = contact;
        this.banner_image = banner_image;
        this.car_info = car_info;
        this.price_request = price_request;
        this.price = price;
        this.item_name = item_name;
        this.item_quantity = item_quantity;
        this.available_quantity = available_quantity;
        this.item_type = item_type;
        this.return_type = return_type;
        this.item_image = item_image;
        this.status = status;
        this.timeout_status = timeout_status;
        this.request_time = request_time;
        this.create_at = create_at;
        this.maintenance_name = maintenance_name;
        this.desc_type = desc_type;
    }

    public Dealer_Request_Model(String request_id, String deal_request_id, String sender_garage_id, String name, String city,
                                String area, String contact, String banner_image, String car_info, String price_request, String price,
                                String item_name, String item_quantity, String available_quantity, String item_type, String return_type,
                                String item_image, String status, String timeout_status, String request_time, String create_at,
                                String maintenance_name, String desc_type, String done_time) {
        this.request_id = request_id;
        this.deal_receiver_id = deal_request_id;
        this.sender_garage_id = sender_garage_id;
        this.name = name;
        this.city = city;
        this.area = area;
        this.contact = contact;
        this.banner_image = banner_image;
        this.car_info = car_info;
        this.price_request = price_request;
        this.price = price;
        this.item_name = item_name;
        this.item_quantity = item_quantity;
        this.available_quantity = available_quantity;
        this.item_type = item_type;
        this.return_type = return_type;
        this.item_image = item_image;
        this.status = status;
        this.timeout_status = timeout_status;
        this.request_time = request_time;
        this.create_at = create_at;
        this.maintenance_name = maintenance_name;
        this.desc_type = desc_type;
        this.done_time = done_time;
    }

    public String getReturn_type() {
        return return_type;
    }

    public void setReturn_type(String return_type) {
        this.return_type = return_type;
    }

    public String getAvailable_quantity() {
        return available_quantity;
    }

    public void setAvailable_quantity(String available_quantity) {
        this.available_quantity = available_quantity;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getDeal_receiver_id() {
        return deal_receiver_id;
    }

    public void setDeal_receiver_id(String deal_receiver_id) {
        this.deal_receiver_id = deal_receiver_id;
    }

    public String getSender_garage_id() {
        return sender_garage_id;
    }

    public void setSender_garage_id(String sender_garage_id) {
        this.sender_garage_id = sender_garage_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCar_info() {
        return car_info;
    }

    public void setCar_info(String car_info) {
        this.car_info = car_info;
    }

    public String getPrice_request() {
        return price_request;
    }

    public void setPrice_request(String price_request) {
        this.price_request = price_request;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_quantity() {
        return item_quantity;
    }

    public void setItem_quantity(String item_quantity) {
        this.item_quantity = item_quantity;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getItem_image() {
        return item_image;
    }

    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeout_status() {
        return timeout_status;
    }

    public void setTimeout_status(String timeout_status) {
        this.timeout_status = timeout_status;
    }

    public String getRequest_time() {
        return request_time;
    }

    public void setRequest_time(String request_time) {
        this.request_time = request_time;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getMaintenance_name() {
        return maintenance_name;
    }

    public void setMaintenance_name(String maintenance_name) {
        this.maintenance_name = maintenance_name;
    }

    public String getDesc_type() {
        return desc_type;
    }

    public void setDesc_type(String desc_type) {
        this.desc_type = desc_type;
    }

    public String getDone_time() {
        return done_time;
    }

    public void setDone_time(String done_time) {
        this.done_time = done_time;
    }
}
