package com.kcs.automapgarage.Model;

public class Garage_Facility_Model {

    private String garage_facility_id;
    private String garage_id;
    private String facility_id;
    private String facility_name;

    public Garage_Facility_Model(String garage_facility_id, String garage_id, String facility_id, String facility_name) {
        this.garage_facility_id = garage_facility_id;
        this.garage_id = garage_id;
        this.facility_id = facility_id;
        this.facility_name = facility_name;
    }

    public String getGarage_facility_id() {
        return garage_facility_id;
    }

    public void setGarage_facility_id(String garage_facility_id) {
        this.garage_facility_id = garage_facility_id;
    }

    public String getGarage_id() {
        return garage_id;
    }

    public void setGarage_id(String garage_id) {
        this.garage_id = garage_id;
    }

    public String getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(String facility_id) {
        this.facility_id = facility_id;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }
}
