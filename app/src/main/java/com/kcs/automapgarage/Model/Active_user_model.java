package com.kcs.automapgarage.Model;

public class Active_user_model {

    private String status, Message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Active_user_model(String status, String message) {
        this.status = status;
        Message = message;
    }

    public Active_user_model() {
    }
}
