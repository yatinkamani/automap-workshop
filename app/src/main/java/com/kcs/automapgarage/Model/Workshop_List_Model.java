package com.kcs.automapgarage.Model;

public class Workshop_List_Model {

    private String garage_id, garage_name, owner_name,
            banner_image, owner_image, contact_no,
            email, address,
            service_name, facebook_link,
            city_name, area_name;

    public Workshop_List_Model(String garage_id, String garage_name, String owner_name, String banner_image,
                               String owner_image, String contact_no, String email, String address,
                               String service_name, String facebook_link, String city_name, String area_name) {
        this.garage_id = garage_id;
        this.garage_name = garage_name;
        this.owner_name = owner_name;
        this.banner_image = banner_image;
        this.owner_image = owner_image;
        this.contact_no = contact_no;
        this.email = email;
        this.address = address;
        this.service_name = service_name;
        this.facebook_link = facebook_link;
        this.city_name = city_name;
        this.area_name = area_name;
    }

    public String getGarage_id() {
        return garage_id;
    }

    public void setGarage_id(String garage_id) {
        this.garage_id = garage_id;
    }

    public String getGarage_name() {
        return garage_name;
    }

    public void setGarage_name(String garage_name) {
        this.garage_name = garage_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getOwner_image() {
        return owner_image;
    }

    public void setOwner_image(String owner_image) {
        this.owner_image = owner_image;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getFacebook_link() {
        return facebook_link;
    }

    public void setFacebook_link(String facebook_link) {
        this.facebook_link = facebook_link;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }
}
