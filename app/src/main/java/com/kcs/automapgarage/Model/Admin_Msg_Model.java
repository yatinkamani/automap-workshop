package com.kcs.automapgarage.Model;

public class Admin_Msg_Model {

    private String id, msg, img, video, created_at;
    private boolean isSelected;

    public Admin_Msg_Model(String id, String msg, String img, String video, String created_at, boolean isSelected) {
        this.id = id;
        this.msg = msg;
        this.img = img;
        this.video = video;
        this.created_at = created_at;
        this.isSelected = isSelected;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCreated_at() {
        return created_at;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "Admin_Msg_Model{" +
                "id='" + id + '\'' +
                ", msg='" + msg + '\'' +
                ", img='" + img + '\'' +
                ", video='" + video + '\'' +
                ", IsSelected='" + isSelected + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}
