package com.kcs.automapgarage.Model;

public class Fuel_Model {

    private String fuel_id, fuel_type, isChecked;

    public String getFuel_id() {
        return fuel_id;
    }

    public void setFuel_id(String fuel_id) {
        this.fuel_id = fuel_id;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public String toString() {
        return "Fuel_Model{" +
                "fuel_type='" + fuel_type + '\'' +
                '}';
    }
}
