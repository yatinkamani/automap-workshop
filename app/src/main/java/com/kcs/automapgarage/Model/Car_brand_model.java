package com.kcs.automapgarage.Model;

public class Car_brand_model {

    private String brand_id;
    private String brand_nm;
    private String manufacture_country_id;
    private String isChecked;
    private String Created_at;
    private String updated_at;

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_nm() {
        return brand_nm;
    }

    public void setBrand_nm(String brand_nm) {
        this.brand_nm = brand_nm;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getManufacture_country_id() {
        return manufacture_country_id;
    }

    public void setManufacture_country_id(String manufacture_country_id) {
        this.manufacture_country_id = manufacture_country_id;
    }

    public Car_brand_model(String brand_id, String brand_nm, String manufacture_country_id, String isChecked, String created_at, String updated_at) {
        this.brand_id = brand_id;
        this.brand_nm = brand_nm;
        this.manufacture_country_id = manufacture_country_id;
        this.isChecked = isChecked;
        Created_at = created_at;
        this.updated_at = updated_at;
    }

    public Car_brand_model() {
    }
}
