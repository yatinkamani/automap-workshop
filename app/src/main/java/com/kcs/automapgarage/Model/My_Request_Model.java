package com.kcs.automapgarage.Model;

public class My_Request_Model {

    private String no, request_id, item_name, item_quantity,
            item_image, car_info, item_type,
            new_offer_count, status, total_offer,
            time_status, noti_count, request_time, create_at,maintenance_name, desc_type;

    public My_Request_Model(String no, String request_id, String item_name, String item_quantity,
                            String item_image, String car_info, String item_type, String new_offer_count,
                            String status, String total_offer, String time_status, String noti_count) {
        this.no = no;
        this.request_id = request_id;
        this.item_name = item_name;
        this.item_quantity = item_quantity;
        this.item_image = item_image;
        this.car_info = car_info;
        this.item_type = item_type;
        this.item_type = item_type;
        this.item_type = item_type;
        this.new_offer_count = new_offer_count;
        this.status = status;
        this.total_offer = total_offer;
        this.time_status = time_status;
        this.noti_count = noti_count;
    }

    public My_Request_Model(String no, String request_id, String item_name, String item_quantity,
                            String item_image, String car_info, String item_type, String new_offer_count,
                            String status, String total_offer, String time_status, String noti_count,
                            String request_time, String create_at, String maintenance_name, String desc_type) {
        this.no = no;
        this.request_id = request_id;
        this.item_name = item_name;
        this.item_quantity = item_quantity;
        this.item_image = item_image;
        this.car_info = car_info;
        this.item_type = item_type;
        this.new_offer_count = new_offer_count;
        this.status = status;
        this.total_offer = total_offer;
        this.time_status = time_status;
        this.noti_count = noti_count;
        this.request_time = request_time;
        this.create_at = create_at;
        this.maintenance_name = maintenance_name;
        this.desc_type = desc_type;
    }

    public String getNoti_count() {
        return noti_count;
    }

    public void setNoti_count(String noti_count) {
        this.noti_count = noti_count;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_quantity() {
        return item_quantity;
    }

    public void setItem_quantity(String item_quantity) {
        this.item_quantity = item_quantity;
    }

    public String getCar_info() {
        return car_info;
    }

    public void setCar_info(String car_info) {
        this.car_info = car_info;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getNew_offer_count() {
        return new_offer_count;
    }

    public void setNew_offer_count(String new_offer_count) {
        this.new_offer_count = new_offer_count;
    }

    public String getItem_image() {
        return item_image;
    }

    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal_offer() {
        return total_offer;
    }

    public void setTotal_offer(String total_offer) {
        this.total_offer = total_offer;
    }

    public String getTime_status() {
        return time_status;
    }

    public void setTime_status(String time_status) {
        this.time_status = time_status;
    }

    public String getRequest_time() {
        return request_time;
    }

    public void setRequest_time(String request_time) {
        this.request_time = request_time;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getMaintenance_name() {
        return maintenance_name;
    }

    public void setMaintenance_name(String maintenance_name) {
        this.maintenance_name = maintenance_name;
    }

    public String getDesc_type() {
        return desc_type;
    }

    public void setDesc_type(String desc_type) {
        this.desc_type = desc_type;
    }
}
