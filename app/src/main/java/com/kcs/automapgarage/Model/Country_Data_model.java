package com.kcs.automapgarage.Model;

public class Country_Data_model {

    public Country_Data_model() {
    }

    public String getCon_id() {
        return con_id;
    }

    public void setCon_id(String con_id) {
        this.con_id = con_id;
    }

    public String getCon_code() {
        return con_code;
    }

    public void setCon_code(String con_code) {
        this.con_code = con_code;
    }

    public String getCon_name() {
        return con_name;
    }

    public void setCon_name(String con_name) {
        this.con_name = con_name;
    }

    public String getCon_currency() {
        return con_currency;
    }

    public void setCon_currency(String con_currency) {
        this.con_currency = con_currency;
    }

    public String getCon_language() {
        return con_language;
    }

    public void setCon_language(String con_language) {
        this.con_language = con_language;
    }

    public String getCon_ph_code() {
        return con_ph_code;
    }

    public void setCon_ph_code(String con_ph_code) {
        this.con_ph_code = con_ph_code;
    }

    String con_id;
    String con_code;
    String con_name;
    String con_currency;
    String con_language;
    String con_ph_code;
    String isChecked;

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public Country_Data_model(String con_id, String con_code, String con_name, String con_currency, String con_language, String con_ph_code, String isChecked) {
        this.con_id = con_id;
        this.con_code = con_code;
        this.con_name = con_name;
        this.con_currency = con_currency;
        this.con_language = con_language;
        this.con_ph_code = con_ph_code;
        this.isChecked = isChecked;
    }
}
