package com.kcs.automapgarage.Model;

public class Garage_Services {

    private String facility_id;
    private String facility_name;
    private String status;
    private String created_at;
    private String updated_at;

    public Garage_Services(String facility_id, String facility_name, String status, String created_at, String updated_at) {
        this.facility_id = facility_id;
        this.facility_name = facility_name;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(String facility_id) {
        this.facility_id = facility_id;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
