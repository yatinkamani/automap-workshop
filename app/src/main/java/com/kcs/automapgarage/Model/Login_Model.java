package com.kcs.automapgarage.Model;

public class Login_Model {

    public String getStatus_up() {
        return status_up;
    }

    public void setStatus_up(String status_up) {
        this.status_up = status_up;
    }

    private String status_up, message;
    private String result, user_id, name, email, contact_no, country_id, state, city_id, address, lati, longi, pin, user_image, status, service_id, reg_date, update_date;

    public Login_Model(String message, String result, String user_id, String name, String email, String contact_no,
                       String country_id, String state, String city_id, String address, String lat, String log,
                       String pin, String user_image, String status) {
        this.message = message;
        this.result = result;
        this.user_id = user_id;
        this.name = name;
        this.email = email;
        this.contact_no = contact_no;
        this.country_id = country_id;
        this.state = state;
        this.city_id = city_id;
        this.address = address;
        this.lati = lat;
        this.longi = log;
        this.pin = pin;
        this.user_image = user_image;
        this.status = status;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    public Login_Model() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

}
