package com.kcs.automapgarage.Model;

public class Garage_Brand_Model {

    private String garage_brand_model_id;
    private String garage_id;
    private String model_id;
    private String brand_id;
    private String created_at;
    private String updated_at;

    public String getGarage_brand_model_id() {
        return garage_brand_model_id;
    }

    public void setGarage_brand_model_id(String garage_brand_model_id) {
        this.garage_brand_model_id = garage_brand_model_id;
    }

    public String getGarage_id() {
        return garage_id;
    }

    public void setGarage_id(String garage_id) {
        this.garage_id = garage_id;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Garage_Brand_Model(String garage_brand_model_id, String garage_id, String model_id, String brand_id, String created_at, String updated_at) {
        this.garage_brand_model_id = garage_brand_model_id;
        this.garage_id = garage_id;
        this.model_id = model_id;
        this.brand_id = brand_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
