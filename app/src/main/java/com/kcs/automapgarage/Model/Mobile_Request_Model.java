package com.kcs.automapgarage.Model;

public class Mobile_Request_Model {

    private String request_id, request_detail_id, user_id, status, name, email, mobile_no, user_image, location_address,
            amount, oil_brand, oil_sae, request_date, request_time, brand_name, model_name, model_year, variant_name,
            fuel_type, leather_seats, change_filter, lat, lng, request_type, create_at, update_at, timeout_status;

    public Mobile_Request_Model(String request_id, String request_detail_id, String user_id, String status, String name,
                                String email, String mobile_no, String user_image, String location_address, String amount,
                                String oil_brand, String oil_sae, String request_date, String request_time, String brand_name,
                                String model_name, String model_year, String variant_name, String fuel_type,
                                String leather_seats, String change_filter, String lat, String lng, String request_type,
                                String timeout_status,String create_at, String update_at) {
        this.request_id = request_id;
        this.request_detail_id = request_detail_id;
        this.user_id = user_id;
        this.status = status;
        this.name = name;
        this.email = email;
        this.mobile_no = mobile_no;
        this.user_image = user_image;
        this.location_address = location_address;
        this.amount = amount;
        this.oil_brand = oil_brand;
        this.oil_sae = oil_sae;
        this.request_date = request_date;
        this.request_time = request_time;
        this.brand_name = brand_name;
        this.model_name = model_name;
        this.model_year = model_year;
        this.variant_name = variant_name;
        this.fuel_type = fuel_type;
        this.leather_seats = leather_seats;
        this.change_filter = change_filter;
        this.lat = lat;
        this.lng = lng;
        this.request_type = request_type;
        this.timeout_status = timeout_status;
        this.create_at = create_at;
        this.update_at = update_at;
    }

    public String getTimeout_status() {
        return timeout_status;
    }

    public void setTimeout_status(String timeout_status) {
        this.timeout_status = timeout_status;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getRequest_detail_id() {
        return request_detail_id;
    }

    public void setRequest_detail_id(String request_detail_id) {
        this.request_detail_id = request_detail_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOil_brand() {
        return oil_brand;
    }

    public void setOil_brand(String oil_brand) {
        this.oil_brand = oil_brand;
    }

    public String getOil_sae() {
        return oil_sae;
    }

    public void setOil_sae(String oil_sae) {
        this.oil_sae = oil_sae;
    }

    public String getLeather_seats() {
        return leather_seats;
    }

    public void setLeather_seats(String leather_seats) {
        this.leather_seats = leather_seats;
    }

    public String getChange_filter() {
        return change_filter;
    }

    public void setChange_filter(String change_filter) {
        this.change_filter = change_filter;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getRequest_time() {
        return request_time;
    }

    public void setRequest_time(String request_time) {
        this.request_time = request_time;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getModel_year() {
        return model_year;
    }

    public void setModel_year(String model_year) {
        this.model_year = model_year;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

}