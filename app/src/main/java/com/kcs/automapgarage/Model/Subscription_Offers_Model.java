package com.kcs.automapgarage.Model;

public class Subscription_Offers_Model {

    private String id, name, service_provide_number, dealer_number, price, validity, created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService_provide_number() {
        return service_provide_number;
    }

    public void setService_provide_number(String service_provide_number) {
        this.service_provide_number = service_provide_number;
    }

    public String getDealer_number() {
        return dealer_number;
    }

    public void setDealer_number(String dealer_number) {
        this.dealer_number = dealer_number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

}
