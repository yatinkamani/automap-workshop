package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Dealer_Request_Model;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Make_Request_Response extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    TextView txt_title;
    RecyclerView recyclerView;
    List<Dealer_Request_Model> dealer_request_models;
    AdapterMakeRequestResponse adapterMakeRequest;

    SharedPreferences preferences;
    LinearLayout linError;
    TextView txtError;
    Button btnRetry;

    String garage_id = "", dealer_request_id = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__make__request__response);

        img_back = findViewById(R.id.image_back);
        txt_title = findViewById(R.id.txt_title);
        recyclerView = findViewById(R.id.rcl_view);
        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);

        img_back.setOnClickListener(this);
        btnRetry.setOnClickListener(this);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");

        getMakRequestResponse();

        dealer_request_id = getIntent().getStringExtra(getString(R.string.pref_request_id));
        InitializeBroadcast();
    }

    // get make request offer and clear badge count
    @SuppressLint("RestrictedApi")
    public void getMakRequestResponse() {

        ProgressDialog dialog = new ProgressDialog(Act_Make_Request_Response.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_dealer_response_request", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Log.e("Tag Response", response);
                            String response1 = Html.fromHtml(response).toString();
                            String response = response1.substring(response1.indexOf("{"), response1.lastIndexOf("}") + 1);

                            if (dialog.isShowing() && !isFinishing())
                                dialog.dismiss();

                            try {
                                JSONObject object = new JSONObject(response);

                                if (object.getString("status").equals("true")) {

                                    JSONArray array = object.getJSONArray("result");
                                    dealer_request_models = new ArrayList<>();
                                    List<Dealer_Request_Model> d_r_m = new ArrayList<>();
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject data = array.getJSONObject(i);

                                        String deal_receiver_id = data.getString("deal_receiver_id");
                                        String request_id = data.getString("dealer_request_id");
                                        String sender_garage_id = data.getString("receiver_garage_id");
                                        String owner_name = check(data,"owner_name");
                                        String garage_name = check(data,"garage_name").equals("null") ? owner_name : check(data,"garage_name");
                                        String banner_image = data.getString("garage_banner");
                                        String city = check(data,"city_name");
                                        String area = check(data,"area");
                                        String contact = data.getString("contact_no");
                                        String item_name = check(data,"item_name");
                                        String car_info = check(data,"car_info");
                                        String return_type = check(data,"specific_type");
                                        String available_quantity = check(data, "available_quantity");
                                        String maintenance_name = check(data,"maintenance_name");
                                        String desc_type = check(data,"description_type");
                                        String done_time = check(data,"set_time");
                                        String item_quantity = check(data,"item_quantity");
                                        String item_type = check(data,"part_type");
                                        String price = check(data,"price");
                                        String price_request = check(data,"price_request");
                                        String item_image = data.getString("item_image");
                                        String status = data.getString("status");

                                        d_r_m.add(new Dealer_Request_Model(request_id, deal_receiver_id, sender_garage_id, garage_name,
                                                city, area, contact, banner_image, car_info, price_request, price, item_name, item_quantity, available_quantity, item_type,
                                                return_type,item_image, status,"","","",maintenance_name ,desc_type, done_time));
                                    }

                                    List<Dealer_Request_Model> d0 = new ArrayList<>();
                                    List<Dealer_Request_Model> d1 = new ArrayList<>();
                                    List<Dealer_Request_Model> d2 = new ArrayList<>();
                                    List<Dealer_Request_Model> d3 = new ArrayList<>();
                                    List<Dealer_Request_Model> d4 = new ArrayList<>();

                                    for (int d = 0; d < d_r_m.size(); d++) {

                                        if (d_r_m.get(d).getStatus().equals("0")) {
                                            d0.add(d_r_m.get(d));
                                        } else if (d_r_m.get(d).getStatus().equals("1")) {
                                            d1.add(d_r_m.get(d));
                                        } else if (d_r_m.get(d).getStatus().equals("2")) {
                                            d2.add(d_r_m.get(d));
                                        } else if (d_r_m.get(d).getStatus().equals("3")) {
                                            d3.add(d_r_m.get(d));
                                        } else if (d_r_m.get(d).getStatus().equals("4")) {
                                            d4.add(d_r_m.get(d));
                                        }
                                    }

                                    dealer_request_models.addAll(d3);
                                    dealer_request_models.addAll(d1);
                                    dealer_request_models.addAll(d4);
                                    dealer_request_models.addAll(d0);
                                    dealer_request_models.addAll(d2);

                                    if (dealer_request_models != null && dealer_request_models.size() > 0) {

                                        adapterMakeRequest = new AdapterMakeRequestResponse(getApplicationContext(), dealer_request_models);
                                        recyclerView.setAdapter(adapterMakeRequest);
                                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                        recyclerView.setVisibility(View.VISIBLE);
                                        linError.setVisibility(View.GONE);
                                        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putInt(getString(R.string.pref_badge_count_make_request), 0);
                                        editor.apply();
                                        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
                                        if (tot == 0) {
                                            ShortcutBadger.removeCount(getApplicationContext());
                                        } else {
                                            ShortcutBadger.applyCount(getApplicationContext(), tot);
                                        }

                                    } else {
                                        recyclerView.setVisibility(View.GONE);
                                        linError.setVisibility(View.VISIBLE);
                                        btnRetry.setVisibility(View.GONE);
                                        txtError.setText(getString(R.string.no_offer_available));
                                    }

                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    linError.setVisibility(View.VISIBLE);
                                    btnRetry.setVisibility(View.GONE);
                                    txtError.setText(getString(R.string.no_offer_found));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                recyclerView.setVisibility(View.GONE);
                                linError.setVisibility(View.VISIBLE);
                                btnRetry.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.something_wrong_please));
                            }

                        }


                    });

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    recyclerView.setVisibility(View.GONE);
                    linError.setVisibility(View.VISIBLE);
                    btnRetry.setVisibility(View.VISIBLE);
//                    txtError.setText(getString(R.string.something_wrong_please));
                    txtError.setText(AppUtils.serverError(getApplicationContext(), error));
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("sender_garage_id", garage_id);
                    map.put("dealer_request_id", dealer_request_id);
                    map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag params", map.toString());
                    return map;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(request);

        } else {
            recyclerView.setVisibility(View.GONE);
            linError.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));

        }
    }

    // make request offer get and display and add action
    private class AdapterMakeRequestResponse extends RecyclerView.Adapter<AdapterMakeRequestResponse.MyViewHolder> {

        Context context;
        List<Dealer_Request_Model> request_models;

        private AdapterMakeRequestResponse(Context context, List<Dealer_Request_Model> request_models) {
            this.context = context;
            this.request_models = request_models;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_make_request_response, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {

            Dealer_Request_Model model = request_models.get(i);
            holder.lin_data.setVisibility(View.VISIBLE);
            Glide.with(context).asBitmap().load(model.getBanner_image()).placeholder(R.drawable.users).thumbnail(0.01f).into(new BitmapImageViewTarget(holder.img) {
                @Override
                protected void setResource(Bitmap resource) {
                    holder.img.setImageBitmap(resource);
                }
            });

            holder.txt_name.setText(Html.fromHtml(model.getName()));
            holder.txt_address.setText(Html.fromHtml(model.getCity()+", "+model.getArea()));
//            holder.txt_contact.setText(Html.fromHtml("<b>" + getString(R.string.contact_no) + " : </b>" + model.getContact_no()));
            if (!model.getPrice().equals("")) {
                holder.txt_price.setVisibility(View.VISIBLE);
                if (model.getDone_time().equals("")){
                    holder.txt_price.setText(Html.fromHtml("<b>" + getString(R.string.price_per_unit) + " : <font color='blue'>" + model.getPrice() + " JD </font></b>"));
                }else {
                    holder.txt_price.setText(Html.fromHtml("<b>" + getString(R.string.the_price_include_installation) + " : <font color='blue'>" + model.getPrice() + " JD </font></b>"));
                }
            } else {
                holder.txt_price.setVisibility(View.GONE);
            }

            if (model.getDone_time().equals("")) {
                holder.txt_available_quantity.setVisibility(View.VISIBLE);
                if (model.getAvailable_quantity().equalsIgnoreCase("-1")){
                    holder.txt_available_quantity.setText(Html.fromHtml("<b>" + getString(R.string.available_quantity) + " : <font color='blue'>" + getString(R.string.same_qunatity_required)+"</font></b>"));
                }else {
                    holder.txt_available_quantity.setText(Html.fromHtml("<b>" + getString(R.string.available_quantity) + " : <font color='red'>" + model.getAvailable_quantity()+"</font></b>"));
                }
            } else if (!model.getDone_time().equals("")){
                holder.txt_available_quantity.setVisibility(View.VISIBLE);
                if (model.getDone_time().equalsIgnoreCase("-1")){
                    holder.txt_available_quantity.setText(Html.fromHtml("<b>" + getString(R.string.time_to_done_the_work) + " : <font color='blue'>" + getString(R.string.not_set_time)+"</font></b>"));
                }else {
                    holder.txt_available_quantity.setText(Html.fromHtml("<b>" + getString(R.string.time_to_done_the_work) + " : <font color='red'>" + model.getDone_time()+"</font></b>"));
                }
            } else{
                holder.txt_available_quantity.setVisibility(View.GONE);
            }

            if (!model.getReturn_type().equals("")) {
                holder.txt_specific_type.setVisibility(View.VISIBLE);
                if (model.getDone_time().equals("")){

                    if (model.getReturn_type().equals("1")){
                        holder.txt_specific_type.setTextColor(getResources().getColor(R.color.black));
                        holder.txt_specific_type.setText(Html.fromHtml("<b>" + getString(R.string.note) + " : <font color='blue'>" + getString(R.string.same_type) + "</font></b>"));
                    }else {
//                    holder.txt_specific_type.setTextColor(getResources().getColor(R.color.quantum_googleRedA700));
                        holder.txt_specific_type.setText(Html.fromHtml("<b>" + getString(R.string.note) + " : </b> <font color='red'>" + model.getReturn_type() +"</font>"));
                    }
                } else {
                    if (model.getReturn_type().equals("1")){
                        holder.txt_specific_type.setTextColor(getResources().getColor(R.color.black));
                        holder.txt_specific_type.setText(Html.fromHtml("<b>" + getString(R.string.description) + " : <font color='blue'>" + getString(R.string.same_as_required) + "</font></b>"));
                    }else {
//                    holder.txt_specific_type.setTextColor(getResources().getColor(R.color.quantum_googleRedA700));
                        holder.txt_specific_type.setText(Html.fromHtml("<b>" + getString(R.string.description) + " : </b> <font color='red'>" + model.getReturn_type() +"</font>"));
                    }
                }
            } else {
                holder.txt_specific_type.setVisibility(View.GONE);
            }

            holder.card_call.setVisibility(View.GONE);
            holder.card_confirm.setVisibility(View.GONE);
            holder.card_remove.setVisibility(View.GONE);
            holder.btn_call.setVisibility(View.GONE);
            if (model.getStatus().equals("0")) {
//                holder.txt_status.setText(Html.fromHtml("<b>Status</b> : Pending"));
            } else if (model.getStatus().equals("1")) {
//                holder.txt_status.setText(Html.fromHtml("<b>Status</b> : Accepted"));
                holder.card_call.setVisibility(View.VISIBLE);
                holder.card_confirm.setVisibility(View.VISIBLE);
                holder.card_remove.setVisibility(View.VISIBLE);
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.white));
            } else if (model.getStatus().equals("2")) {
                holder.card_call.setVisibility(View.GONE);
                holder.card_confirm.setVisibility(View.GONE);
                holder.card_remove.setVisibility(View.GONE);
                holder.txt_specific_type.setVisibility(View.GONE);
                holder.txt_price.setVisibility(View.GONE);
                holder.lin_data.setVisibility(View.GONE);
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.quantum_grey500));
//                holder.txt_status.setText(Html.fromHtml("<b>Status</b> : Not Accepted"));
            } else if (model.getStatus().equals("3")) {
//                holder.txt_status.setText(Html.fromHtml("<b>Status</b> : Confirmed"));
                holder.card_call.setVisibility(View.GONE);
                holder.card_confirm.setVisibility(View.GONE);
                holder.card_remove.setVisibility(View.GONE);
                holder.txt_specific_type.setVisibility(View.VISIBLE);
                holder.txt_price.setVisibility(View.VISIBLE);
                holder.txt_available_quantity.setVisibility(View.VISIBLE);
                holder.lin_data.setVisibility(View.VISIBLE);
                holder.btn_call.setVisibility(View.VISIBLE);
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.quantum_googgreen200));
            } else if (model.getStatus().equals("4")) {
                holder.card_call.setVisibility(View.GONE);
                holder.card_confirm.setVisibility(View.GONE);
                holder.card_remove.setVisibility(View.GONE);
                holder.txt_specific_type.setVisibility(View.GONE);
                holder.txt_price.setVisibility(View.GONE);
                holder.lin_data.setVisibility(View.GONE);
                holder.txt_available_quantity.setVisibility(View.GONE);
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.quantum_grey500));
//                holder.txt_status.setText(Html.fromHtml("<b>Status</b> : You have Chosen Other Dealer"));
            }

            holder.card_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.getContact()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            holder.btn_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.getContact()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            // confirm offers dialog
            holder.card_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SweetAlertDialog dialog = new SweetAlertDialog(Act_Make_Request_Response.this, SweetAlertDialog.WARNING_TYPE);
                    dialog.setContentText(getString(R.string.are_you_sure_to_confirm_dialog));
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setConfirmText(getString(R.string.yess));
                    dialog.setCancelText(getString(R.string.nos));
                    dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });
                    dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            ConfirmRequest(model.getDeal_receiver_id(), model.getSender_garage_id(), "3");
                            sweetAlertDialog.dismiss();
                        }
                    });
                    dialog.show();

                }
            });

            holder.card_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ConfirmRequest(model.getDeal_receiver_id(), model.getSender_garage_id(), "2");
                }
            });
        }

        @Override
        public int getItemCount() {
            return request_models.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            RoundedImage img;
            TextView txt_name, txt_address,txt_specific_type, txt_available_quantity, txt_price;
            CardView card_call, card_confirm, card_remove, card_main;
            LinearLayout lin_data;
            Button btn_call;

            private MyViewHolder(@NonNull View itemView) {
                super(itemView);

                img = itemView.findViewById(R.id.img);
                txt_name = itemView.findViewById(R.id.txt_name);
                txt_address = itemView.findViewById(R.id.txt_address);
                txt_address = itemView.findViewById(R.id.txt_address);
                txt_price = itemView.findViewById(R.id.txt_price);
                txt_specific_type = itemView.findViewById(R.id.txt_specific_type);
                txt_available_quantity = itemView.findViewById(R.id.txt_available_quantity);
                card_call = itemView.findViewById(R.id.card_call);
                card_confirm = itemView.findViewById(R.id.card_confirm);
                card_remove = itemView.findViewById(R.id.card_remove);
                card_main = itemView.findViewById(R.id.card_main);
                lin_data = itemView.findViewById(R.id.lin_data);
                btn_call = itemView.findViewById(R.id.btn_call);
                btn_call.setText(getString(R.string.call));
                TextView txt_confirm = itemView.findViewById(R.id.txt_confirm);
                txt_confirm.setText(getString(R.string.book_now));
                TextView txt_call = itemView.findViewById(R.id.txt_call);
                txt_call.setText(getString(R.string.call));
                TextView txt_remove = itemView.findViewById(R.id.txt_remove);
                txt_remove.setText(getString(R.string.delete));

            }
        }
    }

    // confirm particular offer and refress api
    public void ConfirmRequest(String deal_receiver_id, String receiver_garage_id, String status) {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            API api = new API(Act_Make_Request_Response.this, new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                    Log.e("Tag Response", response);
                    response = Html.fromHtml(response).toString();
                    response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("status").equals("1")) {
                            getMakRequestResponse();
                            Toast.makeText(getApplicationContext(), object.getString("message").toString(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.try_again_after_some_time), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                        Log.e("Tag Error", e.toString());
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                }
            });
            Map<String, String> map = new HashMap<>();
            map.put("deal_receiver_id", deal_receiver_id);
            map.put("dealer_request_id", dealer_request_id);
            map.put("receiver_garage_id", receiver_garage_id);
            map.put("sender_garage_id", garage_id);
            map.put("status", status);
            map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
            api.execute(100,Constant.URL+"confirm_dealer_request", map,true);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Make_Request_Response.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // new offer come refress api
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
    //  int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
    //  int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
    //  int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
    //  int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (make != 0 && !Act_Make_Request_Response.this.isFinishing()) {
                getMakRequestResponse();
            }
        }
    };

    @Override
    public void onClick(View view) {
        if (view == img_back) {
            onBackPressed();
        } else if (view == btnRetry) {
            getMakRequestResponse();
        }
    }

}
