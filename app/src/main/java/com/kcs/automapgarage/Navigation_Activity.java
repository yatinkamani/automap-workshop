package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automapgarage.BackGroundNotification.LocationUpdateService;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.MenuModel;
import com.kcs.automapgarage.adapter.ExpandableListAdapter;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.kcs.automapgarage.Act_Login.LOGIN_PREF;

public class Navigation_Activity extends AppCompatActivity implements View.OnClickListener {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    LinearLayout linError;
    TextView txtError;
    Button btnRetry;
    RoundedImage popup_menu_view;
    FirebaseAnalytics mFireBaseAnalytics;
    SharedPreferences prefs;
    String user_email, user_name, user_image;
    View header;
    TextView txt_user_email, txt_user_name, user_request, mobile_request, dealer_request, make_request, admin_msg;
    CircleImageView imageView;

    String garage_id = "", service_type = "", service_id = "", provider_type = "", sales_id = "";

    FrameLayout frame_mobile, frame_layout_make_request, frame_layout_dealer_request, frame_layout_admin_msg;
    TextView txt_badge_count, txt_mobile_request_badge_count, txt_dealer_request_badge, txt_make_request_badge, txt_admin_msg_badge;
    BroadcastReceiver receiver;

    Location location;
    Menu menu;

    LabeledSwitch switch_on_off;

    LinearLayout lin_main;

    // first time app open and device standard language arabic to app start with app default arabic other wise app default english

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
            overrideConfiguration.setLayoutDirection(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        String language = prefs.getString(getString(R.string.pref_language), "ar").trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        /*if (Locale.getDefault().getLanguage().equals("en"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);*/

        setContentView(R.layout.activity_navigation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        btnRetry = findViewById(R.id.btnRetry);
        btnRetry.setVisibility(View.GONE);
        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        txt_badge_count = findViewById(R.id.txt_badge_count);
        txt_mobile_request_badge_count = findViewById(R.id.txt_mobile_request_badge_count);
        txt_dealer_request_badge = findViewById(R.id.txt_dealer_request_badge);
        txt_make_request_badge = findViewById(R.id.txt_make_request_badge);
        txt_admin_msg_badge = findViewById(R.id.txt_admin_msg_badge);
        mobile_request = findViewById(R.id.mobile_request);
        make_request = findViewById(R.id.make_request);
        dealer_request = findViewById(R.id.dealer_request);
        admin_msg = findViewById(R.id.admin_msg);
        frame_mobile = findViewById(R.id.frame_mobile);
        popup_menu_view = findViewById(R.id.popup_menu_view);
        frame_layout_make_request = findViewById(R.id.frame_layout_make_request);
        frame_layout_dealer_request = findViewById(R.id.frame_layout_dealer_request);
        frame_layout_admin_msg = findViewById(R.id.frame_layout_admin_msg);
        switch_on_off = findViewById(R.id.switch_on_off);

        user_request = findViewById(R.id.user_request);
        lin_main = findViewById(R.id.lin_main);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(0,getStatusBarHeight(),0,0);

        user_email = prefs.getString(getString(R.string.pref_email), "");
        user_name = prefs.getString(getString(R.string.pref_garage_name), "");
        user_image = prefs.getString(getString(R.string.pref_workshop_image), "");
        garage_id = prefs.getString(getString(R.string.pref_garage_id), "");
        service_type = prefs.getString(getString(R.string.pref_special_service_name), "");
        service_id = prefs.getString(getString(R.string.pref_service_id), "");
        sales_id = prefs.getString(getString(R.string.pref_sales_id), "");

        //   provider_role = prefs.getString(getString(R.string.pref_provider_role), "");
        provider_type = prefs.getString(getString(R.string.pref_provider_type), "");
        Log.e("Pref", "user email" + user_email + " service Name " + service_type);

        mFireBaseAnalytics = FirebaseAnalytics.getInstance(this);

//        AppCenter.start(getApplication(), "b49c7edd-3066-4d82-a047-ee91326ba441", Analytics.class, Crashes.class);

        /*try {
            startService(new Intent(getApplicationContext(), LocationUpdateService.class));
        }catch (Exception e){
            e.printStackTrace();
        }*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @SuppressLint("ResourceType")
            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                Log.e("Tag Open", "Drawer");
                toolbar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("Tag Open", "Start");
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
                toolbar.getMenu().close();
                toolbar.getMenu().findItem(R.id.action_workshop).setVisible(false);
//                toolbar.getMenu().findItem(R.id.action_language).setVisible(false);
                toolbar.getMenu().findItem(R.id.action_profile).setVisible(false);
                toolbar.getMenu().findItem(R.id.action_password).setVisible(false);

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                toolbar.setOnClickListener(null);
                Log.e("Tag Open", "Closed");
                if (service_id.equals("4") || service_id.equals("18")){
                    toolbar.getMenu().findItem(R.id.action_workshop).setVisible(true);
                }
                toolbar.getMenu().findItem(R.id.action_language).setVisible(false);
//                if (service_id.equals("61")){
                    toolbar.getMenu().findItem(R.id.action_profile).setVisible(true);
//                }
                toolbar.getMenu().findItem(R.id.action_password).setVisible(true);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        UpdateUI(user_email, user_image, user_name);
        prepareMenuData();
        populateExpandableList();
        UpdateLocation();
        setNotificationSound();

        mobile_request.setOnClickListener(this);
        make_request.setOnClickListener(this);
        dealer_request.setOnClickListener(this);
        admin_msg.setOnClickListener(this);
        popup_menu_view.setOnClickListener(this);

        if (service_id.equals("61")){
            dealer_request.setText(getText(R.string.make_request));
        }

        if (Locale.getDefault().getLanguage().equals("ar"))
            popup_menu_view.animate().rotation(180);
        else
            popup_menu_view.animate().rotation(0);

        user_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_Activity.this, Act_Customer_Request.class);
                startActivity(intent);
            }
        });
//        }

        // notification badge count for different request request
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        onBadgeCount(preferences.getInt(getString(R.string.pref_badge_count), 0), preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0),
                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0), preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0),
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0));
        Log.e("Tag Badge count", "" + preferences.getInt(getString(R.string.pref_badge_count), 0));
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
        //        movableImage();
        //        SnackBar.make(findViewById(android.R.id.content), "account de-activated", Snackbar.LENGTH_LONG).setBackgroundTint(Color.RED).show();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    // notification switch on/off
    public void setNotificationSound() {

        SharedPreferences.Editor editor = prefs.edit();

        if (prefs.getBoolean(getString(R.string.pref_sound), true)){
            switch_on_off.setOn(true);
        }else {
            switch_on_off.setOn(false);
        }

        switch_on_off.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {

                if (isOn){
                    editor.putBoolean(getString(R.string.pref_sound), true).apply();
                }else {
                    editor.putBoolean(getString(R.string.pref_sound), false).apply();
                }
            }
        });
    }

    // drawer menu data set
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void prepareMenuData() {

        Log.e("Tag Service", service_type);
        MenuModel menuModel;
        // 4=partshop or 18=workshop show speciality
        if (service_id.equals("4") || service_id.equals("18")) {
            menuModel = new MenuModel(android.R.drawable.ic_menu_agenda, getString(R.string.workshop_speciality), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
            headerList.add(menuModel);
        }

        menuModel = new MenuModel(android.R.drawable.sym_contact_card, getString(R.string.subscription_offers), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_menu_share, getString(R.string.share), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        /*menuModel = new MenuModel(R.drawable.ic_notifications_none_black_24dp, getString(R.string.user_request), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        //  menuModel = new MenuModel(R.drawable.ic_notifications_none_black_24dp, getString(R.string.mobile_request), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        //  headerList.add(menuModel);

        /*if (service_id.equals("4") || service_id.equals("18")) {
            menuModel = new MenuModel(R.drawable.ic_car_model, getString(R.string.edit_workshop), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
            headerList.add(menuModel);
        }*/

        /*menuModel = new MenuModel(R.drawable.language, getString(R.string.change_language), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /* menuModel = new MenuModel(R.drawable.ic_car_service, getString(R.string.change_dealer_service), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*menuModel = new MenuModel(R.drawable.users, getString(R.string.my_profile), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        menuModel = new MenuModel(R.drawable.contact_us, getString(R.string.contact_us), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_dialog_info, getString(R.string.about_us), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        // 61=car owner
        if (service_id.equals("61")) {
            menuModel = new MenuModel(R.drawable.domain, getString(R.string.amr_group), true, true, 0); //Menu of Java Tutorials
            headerList.add(menuModel);
        } else {
            menuModel = new MenuModel(R.drawable.domain, getString(R.string.facebook_group_link), true, true, 0); //Menu of Java Tutorials
            headerList.add(menuModel);
        }

        menuModel = new MenuModel(R.drawable.icon_logout, getString(R.string.Sign_Out), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    // drawer mwnu click event
    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (headerList.get(groupPosition).menuName == getString(R.string.Sign_Out)) {
                        deleteFirebase();
                        SharedPreferences settings = getApplicationContext().getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
                        String LocalName = settings.getString(getString(R.string.pref_language), "ar");
                        settings.edit().clear().apply();
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(getString(R.string.pref_language), LocalName).apply();
                        SharedPreferences notification = getApplicationContext().getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                        notification.edit().clear().apply();
                        Intent intent = new Intent(Navigation_Activity.this, Act_Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Navigation_Activity.this.finish();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.subscription_offers)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Subscription_Offers.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.about_us)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_About_Us.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.contact_us)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Contact_Us.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.workshop_speciality)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Workshop_Speciality.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.edit_workshop)) {
//                        Toast.makeText(getApplicationContext(), "Please Contact Us", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Car_Model.class);
                        intent.putExtra("EDIT", true);
                        startActivity(intent);
                    } /*else if (headerList.get(groupPosition).menuName == getString(R.string.edit_workshop_service)) {
                        Toast.makeText(getApplicationContext(), "Please Contact Us ", Toast.LENGTH_LONG).show();
                        *//*Intent intent = new Intent(Navigation_Activity.this, Act_Garage_Car_Service.class);
                        intent.putExtra("EDIT",true);
                        startActivity(intent);*/
                    else if (headerList.get(groupPosition).menuName == getString(R.string.my_profile)) {
                        Constant.i = 1;
                        Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
                        startActivity(intent);
                    } /*else if (headerList.get(groupPosition).menuName == getString(R.string.change_dealer_service)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Select_Dealer.class);
                        intent.putExtra(getString(R.string.pref_dealer_service), "1");
                        startActivity(intent);
                    } */ else if (headerList.get(groupPosition).menuName == getString(R.string.share)) {
                        shareApp();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.facebook_group_link)) {
                        OpenWebsite();
                    }else if (headerList.get(groupPosition).menuName == getString(R.string.amr_group)) {
                        OpenAMRGroupLink();
                    }
                    if (!headerList.get(groupPosition).hasChildren) {
                        onBackPressed();
                    }
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
//                    if (model.url.length() > 0) {
//                        WebView webView = findViewById(R.id.webView);
//                        webView.loadUrl(model.url);
//                        onBackPressed();
//                    }
                }
                return false;
            }
        });
    }

    // logout time remove firebase token
    public static void deleteFirebase() {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try
                {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    FirebaseInstanceId.getInstance().getInstanceId();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                // Call your Activity where you want to land after log out
            }
        }.execute();
    }

   // select language dialog
    public void SelectLanguageDialog() {

        String[] language = {getString(R.string.lang_english), getString(R.string.lang_arabic)};
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String selected_lang = prefs.getString(getString(R.string.pref_language), "ar");
        int checked = -1;
        if (selected_lang.toString().equals("en")) {
            checked = 0;
        } else if (selected_lang.toString().equals("ar")) {
            checked = 1;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(Navigation_Activity.this);
        builder.setTitle("Select Language");
        builder.setSingleChoiceItems(language, checked, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (language[i].equals(getString(R.string.lang_english))) {
                    editor.putString(getString(R.string.pref_language), "en").apply();
                    changeLangApi("en");
                } else {
                    changeLangApi("ar");
                    editor.putString(getString(R.string.pref_language), "ar").apply();
                }
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setLangRecreate(String LocalName) {
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(getString(R.string.pref_language), LocalName).apply();
        startActivity(new Intent(getApplicationContext(), Navigation_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }

    // change language api to update server
    private void changeLangApi(@NotNull String localeName) {
        API api = new API(Navigation_Activity.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                if (isSuccess){
                    Log.e("Tag response", response);
                    setLangRecreate(localeName);
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
            }
        });

        Map<String, String> map = new HashMap<>();
        String language = "";
        if (localeName.equals("ar")){
            language = "Arabic";
        }else {
            language = "English";
        }
        map.put("language",language);
        map.put("garage_id", garage_id);
        api.execute(100, Constant.URL+"changeLanguage", map, true);
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "بكبسة واحدة !! القطعة والسعر والمكان بكون عندك مع تطبيق أصحاب ورش السيارات : https://play.google.com/store/apps/details?id=" + Navigation_Activity.this.getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void OpenWebsite() {
//        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://automap.repair/")));
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/759625367965386/?ref=share")));
    }

    private void OpenAMRGroupLink() {
//        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://automap.repair/")));
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/1750431415165198")));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    // menu item  not use
    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.e("tag item", ""+item.getItemId());
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile) {
            Constant.i = 1;
            Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_workshop) {
            Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Car_Model.class);
            intent.putExtra("EDIT", true);
            startActivity(intent);
        } else if (id == R.id.action_language) {
            SelectLanguageDialog();
        } else if (id == R.id.action_password) {
            Intent intent = new Intent(Navigation_Activity.this, Act_change_password.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    // menu item  not use
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_, menu);
        if (service_id.equals("4") || service_id.equals("18")){
          MenuItem menuItem =  menu.findItem(R.id.action_workshop);
          menuItem.setVisible(true);
        }
        if (service_id.equals("61")){
            MenuItem menuItem =  menu.findItem(R.id.action_profile);
            menuItem.setTitle(getString(R.string.profile_personaly));
            menuItem.setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    // notification count for different request
    public void onBadgeCount(int count, int mobile_count, int make_count, int dealer_count, int msg_count) {

        Log.e("Tag count", "" + count);
        final int[] int_count = {0, 1, 2, 3, 4};
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    int_count[0] = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
                    Log.e("Tag count Intent", "" + int_count[0]);
                    if (txt_badge_count != null) {
                        Log.e("Tag count Text", "" + txt_badge_count.getText());
                        if (int_count[0] == 0) {
                            txt_badge_count.setVisibility(View.GONE);
                        } else {
                            txt_badge_count.setText("" + int_count[0]);
                            txt_badge_count.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }

                    int_count[1] = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
                    Log.e("Tag count Intent", "" + int_count[1]);
                    if (txt_mobile_request_badge_count != null) {
                        Log.e("Tag count Text", "" + txt_mobile_request_badge_count.getText());
                        if (int_count[1] == 0) {
                            txt_mobile_request_badge_count.setVisibility(View.GONE);
                        } else {
                            txt_mobile_request_badge_count.setText("" + int_count[1]);
                            txt_mobile_request_badge_count.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }

                    int_count[2] = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
                    Log.e("Tag count Intent", "" + int_count[2]);
                    if (txt_dealer_request_badge != null) {
                        Log.e("Tag count Text", "" + txt_dealer_request_badge.getText());
                        if (int_count[2] == 0) {
                            txt_dealer_request_badge.setVisibility(View.GONE);
                        } else {
                            txt_dealer_request_badge.setText("" + int_count[2]);
                            txt_dealer_request_badge.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }

                    int_count[3] = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
                    Log.e("Tag count Intent", "" + int_count[3]);
                    if (txt_make_request_badge != null) {
                        Log.e("Tag count Text", "" + txt_make_request_badge.getText());
                        if (int_count[3] == 0) {
                            txt_make_request_badge.setVisibility(View.GONE);
                        } else {
                            txt_make_request_badge.setText("" + int_count[3]);
                            txt_make_request_badge.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }

                    int_count[4] = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);
                    Log.e("Tag count Intent", "" + int_count[4]);
                    if (txt_admin_msg_badge != null) {
                        Log.e("Tag count Text", "" + txt_admin_msg_badge.getText());
                        if (int_count[4] == 0) {
                            txt_admin_msg_badge.setVisibility(View.GONE);
                        } else {
                            txt_admin_msg_badge.setText("" + int_count[4]);
                            txt_admin_msg_badge.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }
                }
            }
        };

        if (txt_badge_count != null) {
            if (count == 0) {
                txt_badge_count.setVisibility(View.GONE);
            } else {
                txt_badge_count.setText("" + count);
                txt_badge_count.setVisibility(View.VISIBLE);
            }
        }

        if (txt_mobile_request_badge_count != null) {
            if (mobile_count == 0) {
                txt_mobile_request_badge_count.setVisibility(View.GONE);
            } else {
                txt_mobile_request_badge_count.setText("" + mobile_count);
                txt_mobile_request_badge_count.setVisibility(View.VISIBLE);
            }
        }

        if (txt_dealer_request_badge != null) {
            if (dealer_count == 0) {
                txt_dealer_request_badge.setVisibility(View.GONE);
            } else {
                txt_dealer_request_badge.setText("" + dealer_count);
                txt_dealer_request_badge.setVisibility(View.VISIBLE);
            }
        }

        if (txt_make_request_badge != null) {
            if (make_count == 0) {
                txt_make_request_badge.setVisibility(View.GONE);
            } else {
                txt_make_request_badge.setText("" + make_count);
                txt_make_request_badge.setVisibility(View.VISIBLE);
            }
        }

        if (txt_admin_msg_badge != null) {
            if (msg_count == 0) {
                txt_admin_msg_badge.setVisibility(View.GONE);
            } else {
                txt_admin_msg_badge.setText("" + msg_count);
                txt_admin_msg_badge.setVisibility(View.VISIBLE);
            }
        }
    }

    // onremoew to update name image
    @Override
    protected void onResume() {

        super.onResume();
        SharedPreferences prefs = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_email = prefs.getString(getString(R.string.pref_email), "");
        user_name = prefs.getString(getString(R.string.pref_garage_name), "");
        user_image = prefs.getString(getString(R.string.pref_workshop_image), "");
        provider_type = prefs.getString(getString(R.string.pref_provider_type), "");
        UpdateUI(user_email, user_image, user_name);

        /*SharedPreferences pre = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        if (txt_user_name != null) {
            txt_user_name.setText(pre.getString(getString(R.string.name), ""));
        }
        if (imageView != null) {
            Glide.with(getApplicationContext()).load(pre.getString(getString(R.string.pref_user_image), "")).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)).apply(new RequestOptions().placeholder(R.drawable.users)).into(imageView);
        }*/

        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        onBadgeCount(preferences.getInt(getString(R.string.pref_badge_count), 0), preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0),
                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0), preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0),
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0));
    }

    @Override
    public void onClick(View view) {
        if (view == mobile_request) {
            Intent intent = new Intent(Navigation_Activity.this, Act_Mobile_Request.class);
            startActivity(intent);
        } else if (view == user_request) {
            Intent intent = new Intent(Navigation_Activity.this, Act_Customer_Request.class);
            startActivity(intent);
        } else if (view == dealer_request) {
            if (service_id.equals("61")){
                Intent intent = new Intent(getApplicationContext(), Act_Add_Request.class);
                startActivity(intent);
            }else {
                Intent intent = new Intent(getApplicationContext(), Act_Dealer_Request.class);
                startActivity(intent);
             }
        } else if (view == make_request) {
            Intent intent = new Intent(getApplicationContext(), Act_Make_Request.class);
            startActivity(intent);
        } else if (view == admin_msg) {
            Intent intent = new Intent(getApplicationContext(), Act_Admin_Message.class);
            startActivity(intent);
        } else if (view == popup_menu_view) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void UpdateUI(String user_email, String user_image, String user_name) {

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);

        txt_user_email = (TextView) header.findViewById(R.id.txt_user_email);
        txt_user_email.setText(user_email);

        txt_user_name = (TextView) header.findViewById(R.id.txt_user_name);
        txt_user_name.setText(user_name);

        imageView = (CircleImageView) header.findViewById(R.id.imageView);

        Log.e("ppp", "onCreate: " + user_image);
        Glide.with(getApplicationContext()).asBitmap().load(user_image).thumbnail(0.01f)
                .apply(new RequestOptions().placeholder(R.drawable.workshop_icon))
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        imageView.setImageBitmap(resource);
                    }
                });
        Log.e("ppp", "Provider Type: " + provider_type);
        if (provider_type.equals("34")) {
            frame_mobile.setVisibility(View.VISIBLE);
//            frame_mobile.setVisibility(View.GONE);
        } else {
            frame_mobile.setVisibility(View.GONE);
        }
    }

    public void freeRegistrationDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Navigation_Activity.this);

        View view = View.inflate(Navigation_Activity.this, R.layout.free_registration_dialog, null);
        Button btn_contact_us = view.findViewById(R.id.btn_contact_us);
        TextView txt_message = view.findViewById(R.id.txt_message);
        alertDialog.setView(view);

        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        txt_message.setText("For free registration");
        btn_contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(Navigation_Activity.this, Act_Contact_Us.class);
                startActivity(intent);
            }
        });

    }

    Handler handler = null;
    Runnable runnable = null;

    RequestQueue requestQueue;
    Location lastLocation = null;

    // update location when live location change request
    private void UpdateLocation() {

        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                runnable = this;
                location = LocationUpdateService.locations;
                if (location != null && (location != lastLocation)) {
                    lastLocation = location;
                    if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "garage_location_update",
                                new Response.Listener<String>() {
                                    @SuppressLint("SetTextI18n")
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("Tag response", response);
                                        response = Html.fromHtml(response).toString();
                                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                                        try {
                                            JSONObject mainJsonObject = new JSONObject(response);
                                            if (mainJsonObject.getString("status").equals("true")) {

                                                Log.e("Tag response", mainJsonObject.toString());
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        requestQueue = null;
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("size ==> Error", error.toString());
                                        requestQueue = null;
                                    }
                                }) {

                            @Override
                            public Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("garage_id", garage_id);
                                params.put("live_latitude", String.valueOf(location.getLatitude()));
                                params.put("live_longitude", String.valueOf(location.getLongitude()));
                                Log.e("Tag", params.toString());
                                return params;
                            }
                        };
                        try {
                            if (requestQueue == null) {
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                requestQueue = Volley.newRequestQueue(Navigation_Activity.this);
                                requestQueue.add(stringRequest);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
//                         handler.postDelayed(this,5000);
//                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                        Log.e("Tag Location", "no internet");
                    }
                } else {
                    Log.e("Tag Location", "no location");
                }
                handler.postDelayed(this, 5000);
            }
        }, 3000);
    }

    @Override
    protected void onStop() {
        if (handler != null && runnable != null){
            handler.removeCallbacks(runnable);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (handler != null && runnable != null){
            handler.removeCallbacks(runnable);
        }
        super.onDestroy();
    }
}
