package com.kcs.automapgarage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.Visualizer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Country_Data_model;
import com.kcs.automapgarage.Model.Login_Model;
import com.kcs.automapgarage.newPackage.Helper;
import com.kcs.automapgarage.smsRetrive.smsBroadcastReceiver;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;
import com.mukesh.OtpView;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Registration extends AppCompatActivity {

    EditText ed_name, ed_password, ed_mobile, ed_confirm_PASSWORD;
    Spinner spinner_country, Sp_select_service;
    TextView btn_reg;
    TextView textView, tv_ph_code;
    ImageView login_back;

    String c_id;
    String con_cate;
    String c_currency;
    private ArrayList<Country_Data_model> goodModelArrayList;
    private final ArrayList<String> names = new ArrayList<String>();
    ProgressDialog progress;
    String st_nm, st_pass, st_mobile, st_confirm_pass;
    boolean isVerified = false;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    FirebaseAuth auth;
    PhoneAuthProvider.ForceResendingToken token;
    String verificationId;
    AlertDialog dialog;
    SharedPreferences prefs;

    private String DEVICE_TOKEN;
    PhoneAuthCredential authCredential;
    boolean isInstant = false;
    String CountryCode = "";

    BroadcastReceiver receiver;
    private int SMS_CONSENT_REQUEST = 1002;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__registration);

        ed_name = findViewById(R.id.Ed_name);
//        ed_email = findViewById(R.id.Ed_Email);
        ed_password = findViewById(R.id.Ed_PASSWORD);
        ed_mobile = findViewById(R.id.Ed_CONTACT);
        ed_confirm_PASSWORD = findViewById(R.id.Ed_confirm_PASSWORD);
        spinner_country = findViewById(R.id.Sp_select_contry);
        textView = findViewById(R.id.tv_teg);
        login_back = findViewById(R.id.login_back);
        tv_ph_code = findViewById(R.id.tv_ph_code);
        Sp_select_service = findViewById(R.id.Sp_select_service);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        auth = FirebaseAuth.getInstance();

        // firebase initialization and token
        InitializeToken();
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().setFcmAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    return;
                }
                DEVICE_TOKEN = task.getResult().getToken();
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
                //  "fihfQBbZf7M:APA91bEDndPUJIwb-naFmnlkF0EhTd8iFH3cehat2Dwo67j0UZ7NyarRT-RFcoH2ifnA5EDjAtImN6rPvmULC9dHq_kzmDMG8L0SSSK4g3vuTxKGRlluEKpMNZGt0lVtMIK7xJYYzqgY"
            }
        });

        Log.e("tag Country Data", getDeviceCountryCode(this));
        CountryCode = (getDeviceCountryCode(this));
//        Sp_select_service.setVisibility(View.GONE);
        retrieveJSON();
//        retrieveJsonApi();
        btn_reg = findViewById(R.id.Btn_registration);
        login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        smsRetrieveClient();
        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                st_nm = ed_name.getText().toString();
//                st_email = ed_email.getText().toString();
                st_pass = ed_password.getText().toString();
                st_confirm_pass = ed_confirm_PASSWORD.getText().toString();
                st_mobile = ed_mobile.getText().toString();
                st_mobile = st_mobile.length() == 9 ?  "0"+st_mobile : st_mobile;
                ed_mobile.setText(st_mobile);

                if (TextUtils.isEmpty(st_nm)) {
                    ed_name.setError(getString(R.string.name));
                } /*else if (TextUtils.isEmpty(st_email)) {
                    ed_email.setError("Enter email");
                } else if (!isValidEmail(st_email)) {
                    ed_email.setError("Email is not valid");
                } */ else if (TextUtils.isEmpty(st_pass)) {
                    ed_password.setError(getString(R.string.enter_password));
                } else if (st_pass.length() < 6) {
                    ed_password.setError(getString(R.string.please_enter_6digit_password));
                } else if (TextUtils.isEmpty(st_confirm_pass)) {
                    ed_confirm_PASSWORD.setError(getString(R.string.confirm_password));
                } else if (!st_pass.equals(st_confirm_pass)) {
                    ed_confirm_PASSWORD.setError(getString(R.string.password_does_not_match));
                } else if (TextUtils.isEmpty(st_mobile)) {
                    ed_mobile.setError(getString(R.string.enter_contatct));
                } else if (!isValidNumber()) {
                    return;
                } else if (st_mobile.length() < 6 || st_mobile.length() > 13) {
                    ed_mobile.setError(getString(R.string.contact_is_not_valid));
                } else if (!Helper.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), R.string.internet_not_connect, Toast.LENGTH_LONG).show();
                } else {
//                    if (checkAndRequestPermissions() || isCheck){
                        checkNumberEmail();
//                    }
                }
            }
        });

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                st_mobile = ed_mobile.getText().toString();
                goodModelArrayList.get(i).getCon_currency();
                String p_code = goodModelArrayList.get(i).getCon_ph_code();
                c_id = goodModelArrayList.get(i).getCon_id();
                c_currency = goodModelArrayList.get(i).getCon_currency();
                tv_ph_code.setText("+" + p_code);
                con_cate = st_mobile;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Act_Registration.this, Act_Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }


    // get country list and set sppiner
    private void retrieveJSON() {
        if (AppUtils.isNetworkAvailable(this)) {

            API api = new API(getApplicationContext(), new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Log.e("Country Response", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.optString("status").equals("true")) {
                            goodModelArrayList = new ArrayList<>();
                            JSONObject object = obj.getJSONObject("result");
                            JSONArray dataArray = object.getJSONArray("country");

                            int selected = 0;
                            for (int i = 0; i < dataArray.length(); i++) {

                                Country_Data_model playerModel = new Country_Data_model();
                                JSONObject data_obj = dataArray.getJSONObject(i);
                                playerModel.setCon_id(data_obj.getString("country_id"));
                                playerModel.setCon_code(data_obj.getString("country_code"));
                                playerModel.setCon_name(check(data_obj,"country_name"));
                                playerModel.setCon_currency(data_obj.getString("currency"));
                                playerModel.setCon_language(data_obj.getString("language"));
                                playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                                goodModelArrayList.add(playerModel);
                                if (CountryCode.toLowerCase().equals(playerModel.getCon_code().toLowerCase())){
                                    selected = i;
                                }
                            }
                            for (int i = 0; i < goodModelArrayList.size(); i++) {
                                names.add(goodModelArrayList.get(i).getCon_name().toString());
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Registration.this, android.R.layout.simple_spinner_dropdown_item, names);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinner_country.setAdapter(spinnerArrayAdapter);
                            spinner_country.setSelection(selected);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });

            Map<String, String> map = new HashMap<>();
            map.put(language, prefs.getString(getString(R.string.pref_language), "ar"));

            api.execute(200,Constant.URL + "get_country",map,false);

            /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Country Response", response);
                            try {
                                JSONObject obj = new JSONObject(response);
                                if (obj.optString("status").equals("true")) {
                                    goodModelArrayList = new ArrayList<>();
                                    JSONObject object = obj.getJSONObject("result");
                                    JSONArray dataArray = object.getJSONArray("country");

                                    for (int i = 0; i < dataArray.length(); i++) {

                                        Country_Data_model playerModel = new Country_Data_model();
                                        JSONObject data_obj = dataArray.getJSONObject(i);
                                        playerModel.setCon_id(data_obj.getString("country_id"));
                                        playerModel.setCon_code(data_obj.getString("country_code"));
                                        playerModel.setCon_name(data_obj.getString("country_name"));
                                        playerModel.setCon_currency(data_obj.getString("currency"));
                                        playerModel.setCon_language(data_obj.getString("language"));
                                        playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                                        goodModelArrayList.add(playerModel);
                                    }
                                    for (int i = 0; i < goodModelArrayList.size(); i++) {
                                        names.add(goodModelArrayList.get(i).getCon_name().toString());
                                    }
                                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Registration.this, android.R.layout.simple_spinner_dropdown_item, names);
                                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                    spinner_country.setAdapter(spinnerArrayAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //  displaying the error in toast if occurs
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            // request queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            requestQueue.add(stringRequest);*/
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }


    // check number is regiter or not
    private void checkNumberEmail() {

        PhoneCallBack();
        API api = new API(Act_Registration.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("Tag Response", response);

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    progress = new ProgressDialog(Act_Registration.this);
                    progress.setMessage(getString(R.string.loading));
                    progress.setCanceledOnTouchOutside(false);
                    progress.setCancelable(false);
                    progress.show();
                    if (object.getString("status").equals("true")) {
                        PhoneAuthOptions authOptions = PhoneAuthOptions.newBuilder()
                                .setPhoneNumber(tv_ph_code.getText().toString() + st_mobile)
                                .setTimeout(2L, TimeUnit.MINUTES)
                                .setCallbacks(callbacks)
                                .setActivity(Act_Registration.this).build();
                        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(tv_ph_code.getText().toString() + st_mobile
                                , 2
                                , TimeUnit.MINUTES
                                , Act_Registration.this
                                , callbacks);*/
                        PhoneAuthProvider.verifyPhoneNumber(authOptions);
                    } else {
                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("contact_no", st_mobile);
        map.put(language, prefs.getString(getString(R.string.pref_language), "ar"));
//        map.put("email", "");

        api.execute(100, Constant.URL + "checkGarareEmailAndContact", map, true);
    }

    // firbase authantication callback listener
    private void PhoneCallBack() {

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
//                Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                isInstant = true;
                authCredential = phoneAuthCredential;
                if (progress != null && progress.isShowing())
                    progress.dismiss();

                // open otp dialog
                OtpDialog();

                String code = phoneAuthCredential.getSmsCode();

                System.out.println("Tag String Code "+ phoneAuthCredential.getSmsCode());
                System.out.println("Tag String provide "+ phoneAuthCredential.getProvider());
                System.out.println("Tag String method "+ phoneAuthCredential.getSignInMethod());
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                if (true) {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.verification_failed), Toast.LENGTH_LONG).show();
                }
                if (progress != null && progress.isShowing())
                    progress.dismiss();

                if (e instanceof FirebaseTooManyRequestsException){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
                e.printStackTrace();
                Log.e("Tag String", e.getMessage());
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                token = forceResendingToken;
                verificationId = s;
                isVerified = false;
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                // opne otp dialog
                OtpDialog();
            }
        };
    }

    // register new account after otp verify
    private void RegisterApi() {

        progress = new ProgressDialog(Act_Registration.this);
        progress.setMessage(getString(R.string.wait_a_moment_varification_link_not_send));
        progress.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "garage_register", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("register_response", " " + response);
                progress.dismiss();
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {
//                        Toast.makeText(getApplicationContext(), obj.optString("message"), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Act_Registration.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
//                        regModel.setMessage(obj.getString("message"));

                        /*JSONObject dataArray = obj.getJSONObject("result");

                        regModel.setName(dataArray.getString("owner_name"));
                        regModel.setEmail(dataArray.getString("email"));
                        regModel.setCountry_id(dataArray.getString("country_id"));
                        regModel.setContact_no(dataArray.getString("contact_no"));
                        regModel.setUser_id(dataArray.getString("user_id"));
                        Reg_Model.add(regModel);*/

                        if (dialog != null) {
                            dialog.dismiss();
//                            Intent intent = new Intent(getApplicationContext(), Act_Login.class);
//                            startActivity(intent);
//                            finish();
                        }
                        loginCall();

                        /*AlertDialog.Builder builder = new AlertDialog.Builder(Act_Registration.this);
                        builder.setMessage(obj.getString("message") + "   Go to the Login Screen");
                        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), Act_Login.class);
                                startActivity(intent);
                                finish();
                            }
                        });*/

                        /*builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });*/

                        /* AlertDialog dialog = builder.create();
                        dialog.show();*/

                    } else if (obj.optString("status").equals("false")) {
                        Toast.makeText(Act_Registration.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Act_Registration.this, getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                Toast.makeText(Act_Registration.this, AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("owner_name", ed_name.getText().toString());
//                map.put("email", ed_email.getText().toString());
                map.put("password", ed_confirm_PASSWORD.getText().toString());
                map.put("country_id", c_id);
                map.put("contact_no", ed_mobile.getText().toString());
                map.put(language, prefs.getString(getString(R.string.pref_language), "ar"));
                Log.e("Params", map.toString());
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private boolean isValidNumber() {
        if (tv_ph_code.getText().toString().equals("+91") && ed_mobile.getText().toString().length() < 10) {
            ed_mobile.setError(getString(R.string.contact_is_not_valid));
            return false;
        } else if (tv_ph_code.getText().equals("+962") && ed_mobile.getText().toString().length() < 9) {
            ed_mobile.setError(getString(R.string.contact_is_not_valid));
            return false;
        } else {
            return true;
        }
    }

    // otp diralog show
    OtpView otpView;
    Button btnVerify;
    public void OtpDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Registration.this, R.style.AppCompatAlertDialogStyle);
        ViewGroup viewGroup = findViewById(android.R.id.content);

        View view = LayoutInflater.from(Act_Registration.this).inflate(R.layout.opt_verification_dialog, viewGroup, false);
        builder.setView(view);
        builder.setCancelable(false);

        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        TextView txt_message = view.findViewById(R.id.txt_message);
        otpView = view.findViewById(R.id.otpView);
        TextView txt_timer = view.findViewById(R.id.txt_timer);
        AVLoadingIndicatorView av_loading = view.findViewById(R.id.av_loading);
        LinearLayout linBtn = view.findViewById(R.id.linBtn);
        TextView btn_contact_us = view.findViewById(R.id.btn_contact_us);
//        btn_contact_us.setText(getString(R.string.contact_us_to_problem_with_verification_code));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            btn_contact_us.setText(Html.fromHtml(getString(R.string.contact_us_to_problem_with_verification_code), Html.FROM_HTML_MODE_LEGACY));
        }else {
            btn_contact_us.setText(Html.fromHtml(getString(R.string.contact_us_to_problem_with_verification_code)));
        }
        otpView.setCursorVisible(false);
        otpView.setOtpCompletionListener(otp -> {
            AppUtils.hideKeyboard(Act_Registration.this);
        });

        // firbase instance verify automatically
        if (isInstant && authCredential != null){
            auth.signInWithCredential(authCredential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            isInstant = false;

                            RegisterApi();
//                                        Toast.makeText(Act_Registration.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(Act_Registration.this, getString(R.string.verification_failed_invalid_credentials), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Act_Registration.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                            }
                        }
                        linBtn.setVisibility(View.VISIBLE);
                        av_loading.setVisibility(View.GONE);
                    }
                });
        }
        // complete instance verify automatically

        // view opt enter manually
        startCountDown(txt_timer);
        otpView.setItemBackgroundColor(Color.BLACK);
        btnVerify = view.findViewById(R.id.btn_verify);
        Button btnCancel = view.findViewById(R.id.btn_cancel);

        txt_message.setText(getString(R.string.please_type_verification_code)+" \n " + tv_ph_code.getText().toString() + " " + ed_mobile.getText().toString());

        btn_contact_us.setOnClickListener(view1 -> startActivity(new Intent(getApplicationContext(), Act_Contact_Us.class)));
        btnCancel.setOnClickListener(view1 -> {
            dialog.dismiss();
        });
        btnVerify.setOnClickListener(view1 -> {

            // task verify but regiter api error and retry to verify only api
            if (isVerified) {
                RegisterApi();
            }
            // first time button verify click
            else {
                linBtn.setVisibility(View.GONE);
                av_loading.setVisibility(View.VISIBLE);
                if (otpView.getText().toString().length() < 6) {
                    Toast.makeText(Act_Registration.this, getString(R.string.enter_otp), Toast.LENGTH_SHORT).show();
                    linBtn.setVisibility(View.VISIBLE);
                    av_loading.setVisibility(View.GONE);
                } else {

                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
                    auth.signInWithCredential(credential)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    isVerified = true;
                                    RegisterApi();
//                                        Toast.makeText(Act_Registration.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                                } else {
                                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                        Toast.makeText(Act_Registration.this, getString(R.string.verification_failed_invalid_credentials), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(Act_Registration.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                linBtn.setVisibility(View.VISIBLE);
                                av_loading.setVisibility(View.GONE);
                            }
                        });
                }
            }
        });

        if (!isFinishing() && dialog != null && !dialog.isShowing())
            dialog.show();
    }

    // timer counter dialog show
    public void startCountDown(TextView textView) {

        //60_000=60 sec or 1 min and another is interval of count down is 1 sec

        new CountDownTimer(120_000, 1000) {
            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                textView.setText(getString(R.string.enter_code_within)+" "+ (remainedSecs / 60) + ":" + (remainedSecs % 60));
                textView.setTextColor(Color.BLACK);
                textView.setBackground(null);
            }
            public void onFinish() {
                textView.setText(Html.fromHtml("<font color='#002AC1'><u>"+getString(R.string.resend_otp)+"</u></font>"));
//                textView.setBackground(getResources().getDrawable(R.drawable.btm_line_shape));
//                textView.setTextColor(Color.parseColor("#002AC1"));

                // retry send otp
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        PhoneAuthOptions authOptions = PhoneAuthOptions.newBuilder()
                                .setPhoneNumber(tv_ph_code.getText().toString() + st_mobile)
                                .setTimeout(2L, TimeUnit.MINUTES)
                                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), getString(R.string.verification_failed), Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                })
                                .setActivity(Act_Registration.this)
                                .setForceResendingToken(token).build();

                        PhoneAuthProvider.verifyPhoneNumber(authOptions);
                        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(tv_ph_code.getText().toString() + st_mobile
                                , 2
                                , TimeUnit.MINUTES
                                , Act_Registration.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), getString(R.string.verification_failed), Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                },
                                token);*/
                    }
                });
            }
        }.start();
    }

    public boolean isPhoneNumberValid(String phoneNumber, String countryCode) {
        //NOTE: This should probably be a member variable.
        /*PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));

        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(countryCode + phoneNumber, isoCode);
            String internationalFormat = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            Toast.makeText(this, "Phone Number is Valid " + internationalFormat, Toast.LENGTH_LONG).show();
            Log.e("Tag String ", internationalFormat);
            Log.e("Tag isoCode ", "" + isoCode);
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }*/

        return false;
    }

    // after register successfully login automatically call
    public void loginCall() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "garage_login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("response", " " + response);

                Login_Model login_model = new Login_Model();
                try {
                    JSONObject obj = new JSONObject(response);
//                    loginModel = new ArrayList<>();
                    boolean isModelSelect = false;
                    String provider_type = "", part_type = "", area_id = "", sales_id = "",
                            manufacture_country_id = "", dealer_service_id = "", user_type = "", language = "ar";
                    if (obj.optString("status").equals("true")) {

                        login_model.setStatus_up(obj.getString("status"));
                        login_model.setMessage(obj.getString("message"));

                        JSONArray dataArray = obj.getJSONArray("result");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            login_model.setUser_id(data_obj.getString("garage_id"));
                            login_model.setName(check(data_obj,"garage_name").equals("null") ? check(data_obj,"owner_name") : check(data_obj,"garage_name"));
                            login_model.setEmail(data_obj.getString("email"));
                            login_model.setContact_no(data_obj.getString("contact_no"));
                            login_model.setCountry_id(data_obj.getString("country_id"));
//                              login_model.setState(data_obj.getString("state"));
                            login_model.setCity_id(data_obj.optString("city_id", ""));
                            login_model.setAddress(check(data_obj,"address"));
                            login_model.setLati(data_obj.getString("latitude"));
                            login_model.setLongi(data_obj.getString("longitude"));
                            login_model.setPin(data_obj.getString("service_id"));
                            login_model.setStatus(data_obj.getString("status"));
                            login_model.setUpdate_date(data_obj.getString("updated_at"));
                            area_id = data_obj.has("area_id") ? data_obj.getString("area_id") : "";
                            sales_id = data_obj.has("salesman_id") ? data_obj.getString("salesman_id") : "";
                            user_type = data_obj.has("user_type") ? data_obj.getString("user_type") : "0";
//                                language = data_obj.has("language") ? data_obj.getString("language") : "ar";
                            login_model.setService_id(data_obj.getString("service_id"));
                            if (data_obj.has("provider_type_id")) {
                                provider_type = data_obj.getString("provider_type_id");
                            }
                            if (data_obj.has("part_type_id")) {
                                part_type = data_obj.getString("part_type_id");
                            }
                            if (data_obj.has("manufacture_country_id")) {
                                manufacture_country_id = data_obj.getString("manufacture_country_id");
                            }

                            dealer_service_id = data_obj.getString("dealer_service_id");

//                            loginModel.add(login_model);
                        }

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(getString(R.string.pref_garage_id), login_model.getUser_id());
                        editor.putString(getString(R.string.pref_garage_name), login_model.getName());
                        editor.putString(getString(R.string.pref_garage_pwd), ed_confirm_PASSWORD.getText().toString());
                        editor.putString(getString(R.string.pref_email), login_model.getEmail());
                        editor.putString(getString(R.string.pref_contact_no), login_model.getContact_no());
                        editor.putString(getString(R.string.pref_country_id), login_model.getCountry_id());
                        editor.putString(getString(R.string.pref_provider_type), provider_type);
                        editor.putString(getString(R.string.pref_area_id), area_id);
                        editor.putString(getString(R.string.pref_sales_id), sales_id);
                        editor.putString(getString(R.string.pref_service_id), login_model.getPin());
                        editor.putString(getString(R.string.pref_part_type_choose), part_type);
                        editor.putString(getString(R.string.pref_part_type), part_type);
                        editor.putString(getString(R.string.pref_manufacturing_country_id), manufacture_country_id);
                        editor.putString(getString(R.string.pref_dealer_service), dealer_service_id);
                        editor.putString(getString(R.string.pref_user_type), user_type);
//                      editor.putString(getString(R.string.pref_language), language);
                        editor.apply();

//                      if (login_model.getService_id().equals("0")) {

                        Intent i = new Intent(Act_Registration.this, Act_Garage_Car_Service.class);
                        i.putExtra("SPLASH", true);
                        startActivity(i);
                        finish();
//                        }

                        Toast.makeText(Act_Registration.this, login_model.getMessage() , Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(Act_Login.this, getString(R.string.login_successfully), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Act_Registration.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("email", ed_mobile.getText().toString());
                map.put("contact_no", ed_mobile.getText().toString());
                map.put("password", ed_confirm_PASSWORD.getText().toString());
                map.put("device_token", DEVICE_TOKEN);
                String language = "";
                if (prefs.getString(getString(R.string.pref_language),"ar").equals("ar")){
                    language = "Arabic";
                } else {
                    language = "English";
                }
                map.put("language",language);
                Log.e("params", " " + map);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(Act_Registration.this);
        request.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    public void InitializeToken() {
        new AsyncTask<Void,Void,Void>() {
            @SuppressLint("StaticFieldLeak")
            @Override
            protected Void doInBackground(Void... params) {
                DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
                // Used to get firebase token until its null so it will save you from null pointer exeption
                while(DEVICE_TOKEN == null) {
                    DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
                }
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
            }
        }.execute();
    }

    // get country code in device
    @NotNull
    private static String getDeviceCountryCode(@NotNull Context context) {
        String countryCode;

        // Try to get country code from TelephonyManager service
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if(tm != null) {
            // Query first getSimCountryIso()
            countryCode = tm.getSimCountryIso();
            if (countryCode != null && countryCode.length() == 2)
                return countryCode;

            if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                // Special case for CDMA Devices
                String network = tm.getNetworkCountryIso();
                if (network != null && network.length() == 2){
                    countryCode = tm.getNetworkCountryIso();
                }
            } else {
                // For 3G devices (with SIM) query getNetworkCountryIso()
                countryCode = tm.getNetworkCountryIso();
            }

            if (countryCode != null && countryCode.length() == 2)
                return countryCode;
        }

        // If network country not available (tablets maybe), get country code from Locale class
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            countryCode = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            countryCode = context.getResources().getConfiguration().locale.getCountry();
        }

        if (countryCode != null && countryCode.length() == 2)
            return  countryCode;

        // General fallback to "us"
        return countryCode;
    }

    public void smsRetrieveClient() {
        /*SmsRetrieverClient client = SmsRetriever.getClient(this);

        Task<Void> task = client.startSmsUserConsent(null);
        smsBroadcastReceiver.initSMSListener(null);
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsVerificationReceiver, intentFilter);
        SmsRetriever.getClient(this).startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("Tag Client Success", "SuccessFull");
                *//*smsBroadcastReceiver.initSMSListener(new SMSListener() {
                    @Override
                    public void onSuccess(String message) {
                        System.out.println("Tag Message  "+ message);
                    }

                    @Override
                    public void onError(String message) {
                        Log.e("Tag Message", message);
                    }
                });*//*
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                Log.e("Tag Client Failed", e.getMessage());
            }
        });*/

//        checkAndRequestPermissions();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    final String sender = intent.getStringExtra("Sender");
                    otpView.setText(AppUtils.getOTPForMessage(message));
                    btnVerify.performClick();
                    Toast.makeText(getBaseContext(), AppUtils.getOTPForMessage(message), Toast.LENGTH_LONG).show();
                    Log.e("OTP MESSAGE", message);

                }
            }
        };

    }

    private final BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                    Bundle extras = intent.getExtras();
                    Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                    switch (smsRetrieverStatus.getStatusCode()) {
                        case CommonStatusCodes.SUCCESS:
                            // Get consent intent
                            Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                            try {
                                // Start activity to show consent dialog to user, activity must be started in
                                // 5 minutes, otherwise you'll receive another TIMEOUT intent
                                startActivityForResult(consentIntent, SMS_CONSENT_REQUEST);
                            } catch (ActivityNotFoundException e) {
                                // Handle the exception ...
                            }
                            break;
                        case CommonStatusCodes.TIMEOUT:
                            // Time out occurred, handle the error.
                            break;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private boolean checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            int receiveSMS = ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS);
            int readSMS = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS);
            List<String> listPermissionsNeeded;
            listPermissionsNeeded = new ArrayList<>();
            if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.RECEIVE_SMS);
            }

            if (readSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_SMS);
            }


            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
                return false;
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_SMS)){
                return true;
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)){
                return true;
            }


            return true;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Tag String Message", "Code get It "+resultCode);
        if (requestCode == SMS_CONSENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get SMS message content
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                // Extract one-time code from the message and complete verification
                // `sms` contains the entire text of the SMS message, so you will need
                // to parse the string.
                if (message != null) {
                    // get the message here
                    Log.e("Tag Otp", message);
                    if (message != null) {
                        otpView.setText(parseOneTimeCode(message));
                        Log.e("Tag Otp", parseOneTimeCode(message));
                    }
                }
                // send one time code to the server
            } else {
                // Consent canceled, handle the error ...
            }
        }
    }

    private String parseOneTimeCode(String otp) {
        return otp.substring(0, 6);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        isCheck = true;
        Map<String, Integer> perms = new HashMap<>();

        perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);

        // Fill with actual results from user
        if (grantResults.length > 0) {
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for both permissions
            if (perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
            ) {
                Log.d("TAG", "camera & location services permission granted");
                checkNumberEmail();
            } else {
                /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getApplicationContext(), "you not use auto read otp in this app", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "you not use auto read otp in this app", Toast.LENGTH_LONG).show();
                }*/
                checkNumberEmail();
            }
        }
    }

    public boolean isCheck = false;
}

