package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Dealer_Request_Model;
import com.kcs.automapgarage.extra.EndlessRecyclerViewScrollListener;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Dealer_Request extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    TextView txt_title;
    RecyclerView recyclerView;
    List<Dealer_Request_Model> dealer_request_modelList;
    SharedPreferences preferences;
    String garage_id;

    AdapterRequest adapterRequest;
    LinearLayout linError;
    TextView txtError;
    Button btnRetry;
    SwipeRefreshLayout swipe_refresh;
    CardView cardNewRequest;

    private int load = 0;

    ProgressBar progress;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); // your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__dealer__request);

        img_back = findViewById(R.id.image_back);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        cardNewRequest = (CardView) findViewById(R.id.cardNewRequest);
        txt_title = findViewById(R.id.txt_title);
        recyclerView = findViewById(R.id.rcl_view);
        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);
        progress = findViewById(R.id.progress);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");

        img_back.setOnClickListener(this);
        btnRetry.setOnClickListener(this);
        cardNewRequest.setOnClickListener(this);
        /*floatingTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDealerRequest();
                floatingTextButton.setVisibility(View.GONE);
            }
        });*/

        GetDealerRequest();
        InitializeBroadcast();
        ClearNotification();

        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetDealerRequest();
            }
        });
    }

    public void ClearNotification() {
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.pref_badge_count_dealer_request), 0);
        editor.apply();
        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
        if (tot == 0) {
            ShortcutBadger.removeCount(getApplicationContext());
        } else {
            ShortcutBadger.applyCount(getApplicationContext(), tot);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == img_back) {
            onBackPressed();
        } else if (view == btnRetry) {
            GetDealerRequest();
        } else if (view == cardNewRequest) {
            GetDealerRequest();
            cardNewRequest.setVisibility(View.GONE);
        }
    }

    LinearLayoutManager layoutManager;
    public void GetDealerRequest() {

        load = 0;
        ProgressDialog dialog = new ProgressDialog(Act_Dealer_Request.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading));
        if (dialog != null && !dialog.isShowing() && !isFinishing() /*&& !swipe_refresh.isRefreshing()*/) {
            if (swipe_refresh != null && swipe_refresh.isRefreshing()) {
                swipe_refresh.setRefreshing(false);
            }
            dialog.show();
        }
        ClearNotification();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_dealer_request", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("Tag Response", response);
                            String response1 = Html.fromHtml(response).toString();
                            String response = response1.substring(response1.indexOf("{"), response1.lastIndexOf("}") + 1);
                            System.out.println("Tag Response ==="+ response);

                            if (dialog.isShowing() && !isFinishing())
                                dialog.dismiss();

                            try {

                                JSONObject object = new JSONObject(response);

                                if (object.getString("status").equals("true")) {

                                    JSONArray array = object.getJSONArray("result");
                                    dealer_request_modelList = new ArrayList<>();

                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject data = array.getJSONObject(i);

                                        String deal_receiver_id = data.getString("deal_receiver_id");
                                        String request_id = data.getString("dealer_request_id");
                                        String sender_garage_id = data.getString("sender_garage_id");
                                        String owner_name = check(data, "owner_name");
                                        String garage_name = check(data, "garage_name").equals("") ? owner_name : check(data, "garage_name");
                                        String banner_image = data.getString("garage_banner");
                                        String contact = data.getString("contact_no");
                                        String item_name = check(data, "item_name");
                                        String car_info = check(data, "car_info");
                                        String return_type = check(data, "specific_type");
                                        String available_quantity = check(data, "available_quantity");
                                        String item_quantity = check(data, "item_quantity");
                                        String item_type = check(data, "part_type");
                                        String price = check(data, "price");
                                        String price_request = check(data, "price_request");
                                        String item_image = data.getString("item_image");
                                        String status = data.getString("status");
                                        String request_time = check(data, "request_time");
                                        String create_at = data.getString("created_at");
                                        String timeOut_status = data.has("timeout_status") ? data.getString("timeout_status") : "0";
                                        String maintenance_name = check(data, "maintenance_name");
                                        String desc_type = check(data, "description_type");

                                        String city = check(data, "city_name");
                                        String area = check(data, "area");
                                        if (city.equals("null") || city.equals("")) {
                                            city = check(data, "request_city");
                                            area = check(data, "request_area");
                                        }

//                                if (!status.equals("2")) {
                                        dealer_request_modelList.add(new Dealer_Request_Model(request_id, deal_receiver_id, sender_garage_id, garage_name,
                                                city, area, contact, banner_image, car_info, price_request, price, item_name, item_quantity, available_quantity, item_type,
                                                return_type, item_image, status, timeOut_status, request_time, create_at, maintenance_name, desc_type));
//                                }
                                    }

//                                    Collections.reverse(dealer_request_modelList);

                                    if (dealer_request_modelList != null && dealer_request_modelList.size() > 0) {

                                        adapterRequest = new AdapterRequest(getApplicationContext(), dealer_request_modelList);
                                        recyclerView.setAdapter(adapterRequest);
                                        layoutManager = new LinearLayoutManager(getApplicationContext());
                                        recyclerView.setLayoutManager(layoutManager);
                                        RecycleScroll();
//                                        recyclerView.setItemAnimator(new DefaultItemAnimator());
//                                        recyclerView.setItemViewCacheSize(200);
//                                        recyclerView.setDrawingCacheEnabled(true);
//                                        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//                                        recyclerView.setHasFixedSize(true);
                                        recyclerView.setVisibility(View.VISIBLE);
                                        if (adapterRequest.getItemCount() >= 10){

                                            recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
                                                @Override
                                                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                                                    loadMore(totalItemsCount);
                                                }
                                            });
                                        }
                                        linError.setVisibility(View.GONE);

                                    } else {
                                        recyclerView.setVisibility(View.GONE);
                                        linError.setVisibility(View.VISIBLE);
                                        btnRetry.setVisibility(View.GONE);
                                        txtError.setText(getString(R.string.no_data_available));
                                    }

                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    linError.setVisibility(View.VISIBLE);
                                    btnRetry.setVisibility(View.GONE);
//                            txtError.setText(getString(R.string.no_data_available));
                                    if (object.getString("message").contains("تم تجميد حسابك لبعض الوقت.") ||
                                            object.getString("message").contains("Your account has been frozen for sometime.")) {
                                        txtError.setText(object.getString("message"));
                                    } else {
                                        txtError.setText(getString(R.string.no_data_found));
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                recyclerView.setVisibility(View.GONE);
                                linError.setVisibility(View.VISIBLE);
                                btnRetry.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.something_wrong_please));
                            }
                        }
                    });



                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    recyclerView.setVisibility(View.GONE);
                    linError.setVisibility(View.VISIBLE);
                    btnRetry.setVisibility(View.VISIBLE);
                    txtError.setText(AppUtils.serverError(getApplicationContext(), error));
//                    txtError.setText(getString(R.string.something_wrong_please));
                    Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_LONG).show();
                    if (swipe_refresh != null && swipe_refresh.isRefreshing()) {
                        swipe_refresh.setRefreshing(false);
                    } else {
                    }
                    if (dialog.isShowing() && !isFinishing())
                        dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("receiver_garage_id", garage_id);
                    map.put("start", "1");
                    map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(request);

        } else {
            recyclerView.setVisibility(View.GONE);
            linError.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));
            dialog.dismiss();
        }
    }

    public void loadMore(int totalItem){
        progress.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_dealer_request", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Tag Response", response);
                        String response1 = Html.fromHtml(response).toString();
                        String response = response1.substring(response1.indexOf("{"), response1.lastIndexOf("}") + 1);
                        System.out.println("Tag Response ==="+ response);

                        try {

                            JSONObject object = new JSONObject(response);

                            List<Dealer_Request_Model> dealer_request = new ArrayList<>();
                            if (object.getString("status").equals("true")) {

                                JSONArray array = object.getJSONArray("result");
//                                dealer_request_modelList = new ArrayList<>();

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject data = array.getJSONObject(i);

                                    String deal_receiver_id = data.getString("deal_receiver_id");
                                    String request_id = data.getString("dealer_request_id");
                                    String sender_garage_id = data.getString("sender_garage_id");
                                    String owner_name = check(data, "owner_name");
                                    String garage_name = check(data, "garage_name").equals("") ? owner_name : check(data, "garage_name");
                                    String banner_image = data.getString("garage_banner");
                                    String contact = data.getString("contact_no");
                                    String item_name = check(data, "item_name");
                                    String car_info = check(data, "car_info");
                                    String return_type = check(data, "specific_type");
                                    String available_quantity = check(data, "available_quantity");
                                    String item_quantity = check(data, "item_quantity");
                                    String item_type = check(data, "part_type");
                                    String price = check(data, "price");
                                    String price_request = check(data, "price_request");
                                    String item_image = data.getString("item_image");
                                    String status = data.getString("status");
                                    String request_time = check(data, "request_time");
                                    String create_at = data.getString("created_at");
                                    String timeOut_status = data.has("timeout_status") ? data.getString("timeout_status") : "0";
                                    String maintenance_name = check(data, "maintenance_name");
                                    String desc_type = check(data, "description_type");

                                    String city = check(data, "city_name");
                                    String area = check(data, "area");
                                    if (city.equals("null") || city.equals("")) {
                                        city = check(data, "request_city");
                                        area = check(data, "request_area");
                                    }

//                                if (!status.equals("2")) {
                                    dealer_request.add(new Dealer_Request_Model(request_id, deal_receiver_id, sender_garage_id, garage_name,
                                            city, area, contact, banner_image, car_info, price_request, price, item_name, item_quantity, available_quantity, item_type,
                                            return_type, item_image, status, timeOut_status, request_time, create_at, maintenance_name, desc_type));
//                                }
                                }

                                dealer_request_modelList.addAll(dealer_request);
//                                Collections.reverse(dealer_request_modelList);

                                if (dealer_request_modelList != null && dealer_request_modelList.size() > 0) {

                                    recyclerView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            adapterRequest.notifyItemInserted(dealer_request_modelList.size()-1);
                                        }
                                    });
//                                    adapterRequest = new AdapterRequest(getApplicationContext(), dealer_request_modelList);
//                                    recyclerView.setAdapter(adapterRequest);
//                                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                                        recyclerView.setItemViewCacheSize(200);
//                                        recyclerView.setDrawingCacheEnabled(true);
//                                        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//                                    recyclerView.setHasFixedSize(true);
//                                    recyclerView.setVisibility(View.VISIBLE);
//                                    linError.setVisibility(View.GONE);

                                } else {
//                                    recyclerView.setVisibility(View.GONE);
//                                    linError.setVisibility(View.VISIBLE);
//                                    btnRetry.setVisibility(View.GONE);
//                                    txtError.setText(getString(R.string.no_data_available));
                                }

                            } else {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                        progress.setVisibility(View.GONE);
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag Error", error.toString());
                progress.setVisibility(View.GONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("receiver_garage_id", garage_id);
                map.put("start", ""+totalItem);
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag Params", map.toString());
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void RecycleScroll() {
        if (recyclerView != null) {
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                }

                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    swipe_refresh.setEnabled(layoutManager.findFirstCompletelyVisibleItemPosition() == 0);
                }
            });
        }
    }

    public class AdapterRequest extends RecyclerView.Adapter<AdapterRequest.MyViewHolder> {

        Context context;
        List<Dealer_Request_Model> dealer_request_models = new ArrayList<>();
        int lastPosition = -1;

        private AdapterRequest(Context context, List<Dealer_Request_Model> dealer_request_models) {
            this.context = context;
            this.dealer_request_models = dealer_request_models;
        }

        private void UpdateList(List<Dealer_Request_Model> data) {
            dealer_request_models = new ArrayList<>();
            dealer_request_models.addAll(data);
            notifyDataSetChanged();
        }

        private void DeleteItem(int position) {
            if (dealer_request_models.size() >= position){

                dealer_request_models.remove(position);
                notifyItemRemoved(position);
            }
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_dealer_request, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

            Dealer_Request_Model model = dealer_request_modelList.get(i);

            String image = "";
            if (model.getBanner_image().split("http:").length > 2) {
                image = "http:" + model.getBanner_image().split("http:")[1];
            } else {
                image = model.getBanner_image();
            }
            for (int p = 0; p < model.getBanner_image().split("http:").length; p++) {
                Log.e("tah image", model.getBanner_image().split("http:")[p]);
            }

            Log.e("tah size", "" + model.getBanner_image().split("http:").length);
            Glide.with(context).asBitmap().load(image).thumbnail(0.01f)
                    .placeholder(R.drawable.workshop_icon).into(new BitmapImageViewTarget(viewHolder.img_provider) {
                @Override
                protected void setResource(Bitmap resource) {
                    viewHolder.img_provider.setImageBitmap(resource);
                }
            });

            if (!model.getItem_image().equals("")) {
                Glide.with(context).asBitmap().load(model.getItem_image()).thumbnail(0.01f).placeholder(R.drawable.workshop_icon)
                        .into(new BitmapImageViewTarget(viewHolder.img_item) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                viewHolder.img_item.setImageBitmap(resource);
                            }
                        });
                viewHolder.img_item.setVisibility(View.VISIBLE);
            } else {
                viewHolder.img_item.setVisibility(View.GONE);
            }

            if (model.getAvailable_quantity().equals("") || model.getAvailable_quantity().equals("0")) {
                viewHolder.txt_available_quantity.setVisibility(View.GONE);
            } else if (model.getAvailable_quantity().equalsIgnoreCase("-1")) {
                viewHolder.txt_available_quantity.setVisibility(View.VISIBLE);
                viewHolder.txt_available_quantity.setText(Html.fromHtml("<b> " + getString(R.string.available_quantity) + " : <font color='blue'>" + getString(R.string.quantity_is_available) + "</font></b>"));
            } else {
                viewHolder.txt_available_quantity.setVisibility(View.VISIBLE);
                viewHolder.txt_available_quantity.setText(Html.fromHtml("<b> " + getString(R.string.available_quantity) + " : <font color='red'>" + model.getAvailable_quantity() + "</font></b>"));
            }

            if (model.getReturn_type().equals("")) {
                viewHolder.txt_myItem_type.setVisibility(View.GONE);
            } else {
                viewHolder.txt_myItem_type.setVisibility(View.VISIBLE);
                if (model.getReturn_type().equals("1")) {
                    viewHolder.txt_myItem_type.setText(Html.fromHtml("<b> " + getString(R.string.my_item_type) + " : <font color='blue'>" + getString(R.string.same_type) + "</font></b>"));
                } else {
                    viewHolder.txt_myItem_type.setText(Html.fromHtml("<b> " + getString(R.string.my_item_type) + " : <font color='red'>" + model.getReturn_type() + "</font></b>"));
                }
            }

            if (!model.getPrice().equals("") && !model.getPrice().equals("null")) {
                viewHolder.txt_price.setText(Html.fromHtml("<b> " + getString(R.string.offered_price) + " : <font color='blue'>" + model.getPrice() + " JD </font></b>"));
                viewHolder.txt_price.setVisibility(View.VISIBLE);
            } else {
                viewHolder.txt_price.setVisibility(View.GONE);
            }

            viewHolder.txt_name.setText(Html.fromHtml(model.getName()));
            viewHolder.txt_address.setText(Html.fromHtml(model.getCity() + " - " + model.getArea()));
            if (!model.getItem_name().equals("")) {
                viewHolder.txt_item.setText(Html.fromHtml("<b>" + getString(R.string.require_part_type) + "  : <font color='blue'>" + model.getItem_name() + "</font></b>"));
            } else if (!model.getMaintenance_name().equals("")) {
                viewHolder.txt_item.setText(Html.fromHtml("<b>" + getString(R.string.maintenance_required) + " : <font color='blue'>" + model.getMaintenance_name() + "</font></b>"));
            }

            if (!model.getItem_quantity().equals("") && model.getMaintenance_name().equals("")) {
                viewHolder.txt_item_quantity.setVisibility(View.VISIBLE);
                viewHolder.txt_item_quantity.setText(Html.fromHtml("<b>" + getString(R.string.quantity) + " : <font color='blue'>" + model.getItem_quantity() + "</font></b>"));
            } else {
                viewHolder.txt_item_quantity.setVisibility(View.GONE);
            }

            viewHolder.txt_car_info.setText(Html.fromHtml(model.getCar_info()));
            if (!model.getItem_type().equals("")) {
                viewHolder.txt_Item_type.setText(Html.fromHtml("<b> " + getString(R.string.item_type) + " : <font color='blue'>" + model.getItem_type() + "</font></b>"));
            } else if (!model.getDesc_type().equals("")) {
                viewHolder.txt_Item_type.setText(Html.fromHtml("<b> " + getString(R.string.decs_type) + " : <font color='blue'>" + model.getDesc_type() + "</font></b>"));
            }

            viewHolder.rel_timeOut.setVisibility(View.GONE);
            viewHolder.txt_report.setVisibility(View.GONE);
            viewHolder.lin_timmer.setVisibility(View.GONE);
            viewHolder.llBottom.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.quantum_bluegrey100)));
            if (model.getStatus().equals("0")) {
                viewHolder.card_ignore.setVisibility(View.VISIBLE);
                viewHolder.card_accept.setVisibility(View.VISIBLE);
                viewHolder.card_call.setVisibility(View.VISIBLE);
                viewHolder.card_ignore.setEnabled(true);
                viewHolder.card_accept.setEnabled(true);
                viewHolder.txt_timer_text.setText(getString(R.string.time_left_to_participate));
                if (!model.getTimeout_status().equals("0")) {
                    viewHolder.rel_timeOut.setVisibility(View.VISIBLE);
                    viewHolder.card_ignore.setVisibility(View.VISIBLE);
                    viewHolder.card_accept.setVisibility(View.GONE);
                    viewHolder.card_call.setVisibility(View.GONE);
                    viewHolder.txt_timmer.setText("00:00:00");
                }
            } else if (model.getStatus().equals("1")) {
                viewHolder.card_ignore.setVisibility(View.GONE);
                viewHolder.card_accept.setVisibility(View.GONE);
//                viewHolder.card_accept.setEnabled(false);
//                viewHolder.txt_accept.setText(getString(R.string.request_accepted));
                viewHolder.ed_price.setVisibility(View.GONE);
                viewHolder.ed_quantity_available.setVisibility(View.GONE);
                viewHolder.ed_set_time.setVisibility(View.GONE);
                viewHolder.ed_return_type.setVisibility(View.GONE);
                viewHolder.rg_group.setVisibility(View.GONE);
                viewHolder.qg_group.setVisibility(View.GONE);
                viewHolder.tg_group.setVisibility(View.GONE);
                viewHolder.card_call.setVisibility(View.VISIBLE);
                if (model.getTimeout_status().equals("1")) {
                    viewHolder.rel_timeOut.setVisibility(View.VISIBLE);
                    viewHolder.card_ignore.setVisibility(View.VISIBLE);
                    viewHolder.card_call.setVisibility(View.GONE);
                    viewHolder.txt_report.setVisibility(View.VISIBLE);
                    viewHolder.txt_report.setText(getString(R.string.the_customer_did_not_chosen_any_offers));
                    viewHolder.llBottom.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yellow_opacity1)));
                    viewHolder.txt_report.setBackgroundColor(getResources().getColor(R.color.quantum_bluegrey100));
                    viewHolder.lin_timmer.setVisibility(View.GONE);
                }
                viewHolder.txt_timer_text.setText(getString(R.string.time_left_to_result));
            } else if (model.getStatus().equals("2")) {
                viewHolder.card_ignore.setVisibility(View.GONE);
                viewHolder.card_accept.setVisibility(View.GONE);
                viewHolder.card_call.setVisibility(View.GONE);
//                viewHolder.card_ignore.setEnabled(false);
//                viewHolder.txt_ignore.setText(getString(R.string.request_removed));
                viewHolder.txt_report.setVisibility(View.VISIBLE);
                viewHolder.txt_report.setTextColor(Color.BLACK);
                viewHolder.txt_report.setText(getString(R.string.select_other_owner_request_good_luck_for_nest_time));
                viewHolder.txt_report.setBackgroundColor(getResources().getColor(R.color.red));
            } else if (model.getStatus().equals("3")) {
                viewHolder.card_ignore.setVisibility(View.GONE);
                viewHolder.card_accept.setVisibility(View.GONE);
//                viewHolder.card_accept.setEnabled(false);
//                viewHolder.txt_accept.setText(getString(R.string.confirm_request));
                viewHolder.card_call.setVisibility(View.VISIBLE);
                viewHolder.txt_report.setVisibility(View.VISIBLE);
                viewHolder.txt_report.setTextColor(Color.WHITE);
                viewHolder.txt_report.setText(getString(R.string.Congratulations_you_have_been_selected_and_the_piece_is_reserved_for_the_requester));
                viewHolder.txt_report.setBackgroundColor(getResources().getColor(R.color.green));
            } else if (model.getStatus().equals("4")) {
                viewHolder.card_ignore.setVisibility(View.GONE);
                viewHolder.card_accept.setVisibility(View.GONE);
                viewHolder.card_call.setVisibility(View.GONE);
//                viewHolder.card_ignore.setEnabled(false);
//                viewHolder.txt_ignore.setText(getString(R.string.select_other_owner_request_good_luck_for_nest_time));
                viewHolder.txt_report.setVisibility(View.VISIBLE);
                viewHolder.txt_report.setTextColor(Color.BLACK);
                viewHolder.txt_report.setText(getString(R.string.select_other_owner_request_good_luck_for_nest_time));
                viewHolder.txt_report.setBackgroundColor(getResources().getColor(R.color.red));
            }

            viewHolder.card_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.getContact()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            viewHolder.rg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioGroup.getCheckedRadioButtonId() == viewHolder.rd_st.getId()) {
                        viewHolder.ed_return_type.setVisibility(View.GONE);
                        viewHolder.ed_return_type.setText("1");
                    } else {
                        viewHolder.ed_return_type.setVisibility(View.VISIBLE);
                        viewHolder.ed_return_type.setText("");
                    }
                }
            });

            viewHolder.tg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioGroup.getCheckedRadioButtonId() == viewHolder.rd_ntime.getId()) {
                        viewHolder.ed_set_time.setVisibility(View.GONE);
                        viewHolder.ed_set_time.setText("-1");
                    } else {
                        viewHolder.ed_set_time.setVisibility(View.VISIBLE);
                        viewHolder.ed_set_time.setText("");
                    }
                }
            });

            viewHolder.qg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioGroup.getCheckedRadioButtonId() == viewHolder.rq_qia.getId()) {
                        viewHolder.ed_quantity_available.setVisibility(View.GONE);
                        viewHolder.ed_quantity_available.setText("-1");
                    } else {
                        viewHolder.ed_quantity_available.setVisibility(View.VISIBLE);
                        viewHolder.ed_quantity_available.setText("");
                    }
                }
            });

            viewHolder.card_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    if (model.getPrice_request().equals("1")) {
                    if (model.getMaintenance_name().equals("") && model.getDesc_type().equals("")) {

                        viewHolder.ed_return_type.setHint(getString(R.string.enter_price_or_type));
                        viewHolder.ed_price.setHint(getString(R.string.enter_price_per_unit));
                        if (viewHolder.ed_price.getVisibility() == View.GONE) {
                            load = 1;
                            viewHolder.ed_price.setVisibility(View.VISIBLE);
//                         viewHolder.ed_price.requestFocus();
                            viewHolder.rg_group.setVisibility(View.VISIBLE);
                            viewHolder.qg_group.setVisibility(View.VISIBLE);
                            viewHolder.rd_st.setChecked(true);
                            viewHolder.rq_qia.setChecked(true);
                            viewHolder.txt_accept.setText(getString(R.string.send_offer));
                        } else if (model.getPrice_request().equals("1") && viewHolder.ed_price.getText().toString().equals("")) {
                            viewHolder.ed_price.requestFocus();
                            viewHolder.ed_price.setError(getString(R.string.enter_price_jd));
                        } /*else if (viewHolder.ed_quantity_available.getText().toString().equals("")) {
//                         viewHolder.ed_quantity_available.requestFocus();
                        viewHolder.ed_quantity_available.setError(getString(R.string.enter_available_quntuty));
                    }*/ else if (viewHolder.rd_dt.isChecked() && viewHolder.ed_return_type.getText().toString().equals("")) {
//                         viewHolder.ed_price.requestFocus();
                            viewHolder.ed_return_type.setError(getString(R.string.enter_different_type));
                        } else if (viewHolder.rq_qna.isChecked() && viewHolder.ed_quantity_available.getText().toString().equals("")) {
//                         viewHolder.ed_quantity_available.requestFocus();
                            viewHolder.ed_quantity_available.setError(getString(R.string.enter_available_quntuty));
                        } else {
                            ResponseDealerRequest(model.getDeal_receiver_id(),
                                    model.getRequest_id(),
                                    model.getSender_garage_id(),
                                    viewHolder.ed_price.getText().toString(),
                                    viewHolder.ed_quantity_available.getText().toString(),
                                    viewHolder.ed_return_type.getText().toString(),
                                    "",
                                    "1",
                                    i);
                        }
                    } else {
                        viewHolder.ed_return_type.setHint(getString(R.string.enter_price_or_type));
                        viewHolder.ed_price.setHint(getString(R.string.enter_price_include_instalation));
                        if (viewHolder.ed_price.getVisibility() == View.GONE) {
                            viewHolder.ed_price.setVisibility(View.VISIBLE);
                            viewHolder.rg_group.setVisibility(View.VISIBLE);
                            viewHolder.tg_group.setVisibility(View.VISIBLE);
                            viewHolder.rd_stime.setChecked(true);
                            viewHolder.rd_st.setChecked(true);
                            viewHolder.txt_accept.setText(getString(R.string.send_offer));
                        } else if (model.getPrice_request().equals("1") && viewHolder.ed_price.getText().toString().equals("")) {
                            viewHolder.ed_price.requestFocus();
                            viewHolder.ed_price.setError(getString(R.string.enter_price_jd));
                        } /*else if (viewHolder.ed_quantity_available.getText().toString().equals("")) {
//                         viewHolder.ed_quantity_available.requestFocus();
                        viewHolder.ed_quantity_available.setError(getString(R.string.enter_available_quntuty));
                    }*/ else if (viewHolder.rd_dt.isChecked() && viewHolder.ed_return_type.getText().toString().equals("")) {
//                         viewHolder.ed_price.requestFocus();
                            viewHolder.ed_return_type.setError(getString(R.string.enter_different_type));
                        } else if (viewHolder.rd_st.isChecked() && viewHolder.ed_set_time.getText().toString().equals("")) {
//                         viewHolder.ed_quantity_available.requestFocus();
                            viewHolder.ed_set_time.setError(getString(R.string.enter_time_to_done_work));
                        } else {
                            ResponseDealerRequest(model.getDeal_receiver_id(),
                                    model.getRequest_id(),
                                    model.getSender_garage_id(),
                                    viewHolder.ed_price.getText().toString(),
                                    "",
                                    viewHolder.ed_return_type.getText().toString(),
                                    viewHolder.ed_set_time.getText().toString(),
                                    "1",
                                    i);
                        }
                    }

                    /*} else
                        ResponseDealerRequest(model.getDeal_receiver_id(), model.getRequest_id(), model.getSender_garage_id(), "", "", "", "1", i);
                    }*/
                }
            });

            viewHolder.card_ignore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(Act_Dealer_Request.this, SweetAlertDialog.WARNING_TYPE);
                    pDialog.setTitleText(getString(R.string.are_you_sure));
                    pDialog.setContentText(getString(R.string.wont_to_able_to_delete_this_request));
                    pDialog.setCancelable(true);
                    pDialog.setConfirmText(getString(R.string.yess));
                    pDialog.setCancelText(getString(R.string.nos));
                    pDialog.setConfirmClickListener(sweetAlertDialog -> {
                        pDialog.dismissWithAnimation();
                        ResponseDealerRequest(model.getDeal_receiver_id(), model.getRequest_id(), model.getSender_garage_id(), "", "", "", "", "-1", i);
                    });
                    pDialog.show();
                }
            });

            int min;
            if (model.getRequest_time() != null && !model.getRequest_time().equals("0")) {
                min = Integer.parseInt(model.getRequest_time());
            } else {
                min = (24 * 60);
            }

            if ((model.getStatus().equals("0") || model.getStatus().equals("1")) && model.getTimeout_status().equals("0") &&
                    Long.parseLong(AppUtils.getUnitBetweenDates(AppUtils.getStringToDate(AppUtils.getUTCTime()), AppUtils.AddMinutes(AppUtils.getStringToDate(model.getCreate_at()), min), TimeUnit.MILLISECONDS)) > 0) {
                viewHolder.lin_timmer.setVisibility(View.VISIBLE);
                long l = Long.parseLong(AppUtils.getUnitBetweenDates(
                        AppUtils.getStringToDate(AppUtils.getUTCTime()),
                        AppUtils.AddMinutes(AppUtils.getStringToDate(model.getCreate_at()), min),
                        TimeUnit.MILLISECONDS));
                if (viewHolder.countDownTimer != null) {
                    viewHolder.countDownTimer.cancel();
                }

                viewHolder.countDownTimer = new CountDownTimer(l,
                        500) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        long seconds = millisUntilFinished / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;
                        String time = days + " " + "days" + " :" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
                        String h = (hours) <= 9 ? "0" + (hours) : "" + (hours);
                        String m = (minutes % 60) <= 9 ? "0" + (minutes % 60) : "" + (minutes % 60);
                        String s = (seconds % 60) <= 9 ? "0" + (seconds % 60) : "" + (seconds % 60);
                        /*if ( days > 0){
                            h = ((hours) + (days * 24)) <= 9 ? "0"+((hours % 24) + (days * 24)) : ""+((hours % 24) + (days * 24));
                            time = h + ":" + m + ":" + s;
                        }else {*/
                        time = h + ":" + m + ":" + s;
//                        }

                        viewHolder.txt_timmer.setText(time);
                    }

                    @Override
                    public void onFinish() {
                        viewHolder.lin_timmer.setVisibility(View.GONE);
                        viewHolder.rel_timeOut.setVisibility(View.VISIBLE);
                        viewHolder.card_ignore.setVisibility(View.VISIBLE);
                        viewHolder.card_accept.setVisibility(View.GONE);
                        viewHolder.card_call.setVisibility(View.GONE);
                    }
                };
                viewHolder.countDownTimer.start();
            } else {
                if (model.getStatus().equals("0")) {
                    viewHolder.lin_timmer.setVisibility(View.VISIBLE);
                    viewHolder.rel_timeOut.setVisibility(View.VISIBLE);
                    viewHolder.card_ignore.setVisibility(View.VISIBLE);
                    viewHolder.card_accept.setVisibility(View.GONE);
                    viewHolder.card_call.setVisibility(View.GONE);
                    viewHolder.txt_timmer.setText("00:00:00");
                } else {
                    viewHolder.lin_timmer.setVisibility(View.GONE);
                }
            }

            setAnimation(viewHolder.itemView, i);
        }

        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        @Override
        public int getItemCount() {
            return dealer_request_modelList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            RoundedImage img_provider, img_item;
            ImageView img_call;
            TextView txt_report, txt_name, txt_address, txt_item, txt_accept, txt_ignore, txt_available_quantity, txt_myItem_type, txt_car_info, txt_item_quantity, txt_price, txt_Item_type;
            EditText ed_price, ed_quantity_available, ed_return_type, ed_set_time;
            CardView card_call, card_accept, card_ignore;
            RadioGroup rg_group, qg_group, tg_group;
            RadioButton rd_st, rd_dt, rq_qia, rq_qna, rd_stime, rd_ntime;
            RelativeLayout rel_timeOut;
            LinearLayout lin_timmer, llBottom;
            TextView txt_timmer, txt_timer_text, txt_overText;

            CountDownTimer countDownTimer;

            private MyViewHolder(@NonNull View itemView) {
                super(itemView);

                img_provider = itemView.findViewById(R.id.img_provider);
                img_call = itemView.findViewById(R.id.img_call);
                txt_accept = itemView.findViewById(R.id.txt_accept);
                txt_accept.setText(getString(R.string.Exist));
                txt_ignore = itemView.findViewById(R.id.txt_ignore);
                txt_ignore.setText(getString(R.string.delete));
                txt_item = itemView.findViewById(R.id.txt_item);
                txt_available_quantity = itemView.findViewById(R.id.txt_available_quantity);
                txt_myItem_type = itemView.findViewById(R.id.txt_myItem_type);
                txt_car_info = itemView.findViewById(R.id.txt_car_info);
                txt_price = itemView.findViewById(R.id.txt_price);
                txt_Item_type = itemView.findViewById(R.id.txt_Item_type);
                txt_report = itemView.findViewById(R.id.txt_report);

                ed_price = itemView.findViewById(R.id.ed_price);
                ed_price.setHint(getString(R.string.enter_price_per_unit));
                ed_quantity_available = itemView.findViewById(R.id.ed_quantity_available);
                ed_quantity_available.setHint(getString(R.string.enter_quantity_you_have_available));
                ed_return_type = itemView.findViewById(R.id.ed_return_type);
                ed_return_type.setHint(getString(R.string.enter_price_or_type));

                txt_item_quantity = itemView.findViewById(R.id.txt_item_quantity);
                img_item = itemView.findViewById(R.id.img_item);
                txt_address = itemView.findViewById(R.id.txt_address);
                txt_name = itemView.findViewById(R.id.txt_name);
                card_accept = itemView.findViewById(R.id.card_accept);
                card_call = itemView.findViewById(R.id.card_call);
                card_ignore = itemView.findViewById(R.id.card_ignore);

                rg_group = itemView.findViewById(R.id.rg_group);
                rd_st = itemView.findViewById(R.id.rd_st);
                rd_st.setText(getString(R.string.same_type));
                rd_dt = itemView.findViewById(R.id.rd_dt);
                rd_dt.setText(getString(R.string.add_note));

                tg_group = itemView.findViewById(R.id.tg_group);
                rd_stime = itemView.findViewById(R.id.rd_sTime);
                rd_stime.setText(getString(R.string.set_time));
                rd_ntime = itemView.findViewById(R.id.rd_nTime);
                rd_ntime.setText(getString(R.string.not_set_time));
                ed_set_time = itemView.findViewById(R.id.ed_set_time);
                ed_set_time.setHint(getString(R.string.enter_time_to_done_work));

                qg_group = itemView.findViewById(R.id.qg_group);
                rq_qia = itemView.findViewById(R.id.rq_qia);
                rq_qia.setText(getString(R.string.quantity_is_available));
                rq_qna = itemView.findViewById(R.id.rq_qna);
                rq_qna.setText(getString(R.string.quantity_not_available));

                lin_timmer = itemView.findViewById(R.id.lin_timmer);
                txt_timmer = itemView.findViewById(R.id.txt_timmer);
                txt_timer_text = itemView.findViewById(R.id.txt_timer_text);

                txt_overText = itemView.findViewById(R.id.txt_overText);
                txt_overText.setText(getString(R.string.accept_request_ntime_is_over));

                rel_timeOut = itemView.findViewById(R.id.rel_timeOut);
                llBottom = itemView.findViewById(R.id.llBottom);
            }
        }
    }

    public void ResponseDealerRequest(String deal_receiver_id, String dealer_request_id, String sender_garage_id,
                                      String price, @NotNull String available_quantity, String return_type, String set_time,
                                      final String status, int position) {

        API api = new API(Act_Dealer_Request.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                Log.e("Tag Response", response);
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);

                try {
                    JSONObject object = new JSONObject(response);

                    load = 0;
                    if (object.getString("status").equals("1")) {

                        if (dealer_request_modelList != null && dealer_request_modelList.size() >= position) {
                            dealer_request_modelList.get(position).setStatus("" + status);
                            dealer_request_modelList.get(position).setPrice("" + price);
                            dealer_request_modelList.get(position).setAvailable_quantity("" + available_quantity);
                            dealer_request_modelList.get(position).setDone_time("" + set_time);
                            dealer_request_modelList.get(position).setReturn_type("" + return_type);
                            adapterRequest.UpdateList(dealer_request_modelList);
                        }
                    } else if (object.getString("status").equals("-1")) {
                        if (status.equals("-1")) {
//                            SweetAlertDialog pDialog = new SweetAlertDialog(Act_Dealer_Request.this, SweetAlertDialog.SUCCESS_TYPE);
//                            pDialog.setTitleText(getString(R.string.delete_successfully));
//                            pDialog.setConfirmText(getString(R.string.ok));
//                            pDialog.setCanceledOnTouchOutside(false);
//                            pDialog.setConfirmClickListener(sweetAlertDialog -> {
                            adapterRequest.DeleteItem(position);
                            if (adapterRequest.getItemCount() < 1) {
                                linError.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.no_data_found));
                                btnRetry.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            }
//                                pDialog.dismissWithAnimation();
//                            });
//                            pDialog.show();
                        }
                        Toast.makeText(getApplicationContext(), getString(R.string.delete_successfully), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Tag Error", error.toString());
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put("dealer_request_id", dealer_request_id);
        map.put("receiver_garage_id", garage_id);
        map.put("deal_receiver_id", deal_receiver_id);
        map.put("status", status);
        map.put("price", price);
        if (!available_quantity.equals("")) {
            map.put("available_quantity", available_quantity);
        } else {
            map.put("set_time", set_time);
        }
        map.put("specific_type", return_type);
        map.put("sender_garage_id", sender_garage_id);
        map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));

        api.execute(101, Constant.URL + "response_dealer_request", map, true);

    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Dealer_Request.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
//            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
//            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
//            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);
            if (dealer != 0 && !(Act_Dealer_Request.this.isFinishing())) {
                if (load == 0) {
                    GetDealerRequest();
                } else {
                    cardNewRequest.setVisibility(View.VISIBLE);
                }
            }
        }
    };
}

