package com.kcs.automapgarage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.newPackage.Helper;
import com.kcs.automapgarage.utils.AppUtils;
import com.kcs.automapgarage.view.DirectionsJSONParser;
import com.kcs.automapgarage.view.LatLngInterpolator;
import com.kcs.automapgarage.view.MarkerAnimation;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class Act_Map extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    private static final String TAG = "Act_Map";
    private GoogleMap mMap;
    SupportMapFragment mapFrag;
    private String mDistance;
    double latitude, longitude;
    private Location my_location;
    String notificationLatitude = "", notificationLongitude = "", user_name = "",
           user_id = "", garage_name = "", contact_no = "", isUpdate = "",
           mobile_request_id = "", garage_id = "";

    private GoogleApiClient googleApiClient;
    LocationRequest mLocationRequest;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    SharedPreferences preferences, singlePre;
    SharedPreferences.Editor editor, singleEditor;
    public String SinglePreference = "sp";
    Location mLastLocation;
    Marker srcMarker = null, destMarker = null;
    ImageView back;
    TextView txt_start, txt_call;
    boolean start = false;
    private Polyline polyline;
    private List<LatLng> polylineArray = new ArrayList<>();

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onResume() {
        int permissionLocation = ContextCompat.checkSelfPermission(Act_Map.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            my_location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            getMyLocation();

            // betweenPathDraw(String.valueOf(my_location.getLatitude()), String.valueOf(my_location.getLongitude()), notificationLatitude, notificationLongitude);
            // Log.e(TAG, "onResult: " + my_location.getLatitude());

        }
        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        editor = preferences.edit();

        notificationLatitude = getIntent().getStringExtra("latitude");
        notificationLongitude = getIntent().getStringExtra("longitude");
        user_name = getIntent().getStringExtra("user_name");
        user_id = getIntent().getStringExtra("user_id");
        contact_no = getIntent().getStringExtra(getString(R.string.contact_no));
        isUpdate = getIntent().getStringExtra("isUpdate");
        mobile_request_id = getIntent().getStringExtra("mobile_request_id");
        garage_name = preferences.getString(getString(R.string.pref_garage_name), "");
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");
        txt_start = findViewById(R.id.txt_start);
        txt_call = findViewById(R.id.txt_call);

        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        txt_start.setOnClickListener(this);
        txt_call.setOnClickListener(this);

        changeValue();
        initializeMap();

        buildGoogleApiClient();
        if (mobile_request_id == null) {
            checkPermissions();
        } else {
            txt_start.setVisibility(View.VISIBLE);
        }

        if (contact_no != null) {
            txt_call.setVisibility(View.VISIBLE);
        } else {
            txt_call.setVisibility(View.GONE);
        }

        singlePre = getSharedPreferences(SinglePreference, MODE_PRIVATE);
        singleEditor = singlePre.edit();
        Log.e("Tag Request", "" + singlePre.getBoolean(mobile_request_id, false));
        if (!singlePre.getBoolean(mobile_request_id, false)) {
            singleEditor.putBoolean(mobile_request_id, false);
        }
    }

    private void initializeMap() {
        if (mMap == null) {
            mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
            // check if map is created successfully or not
            /* if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }*/
        }
        //         CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        //         googleMap.animateCamera(cameraUpdate);
        //         locationManager.removeUpdates(this);
    }

    // draw path and change change
    public void betweenPathDraw(String userFromLat, String userFromLong, String userToLat, String userToLong) {
        if (run) {
            String url = getDirectionsUrl(userFromLat, userFromLong, userToLat, userToLong);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
            run = false;
            handler.postDelayed(runnable, 10000);
            UpdateLocation("");
            MakeCameraFocus(userFromLat, userFromLong, userToLat, userToLong);
        }

        if (mobile_request_id == null) {
            MakeCameraFocus(userFromLat, userFromLong, userToLat, userToLong);
        }

        LatLng srcLatLng = null;
        /*if (polylineArray != null && polylineArray.size() > 0 && mobile_request_id != null) {
            srcLatLng = polylineArray.get(0);
        } else {*/
            srcLatLng = new LatLng(Double.parseDouble(userFromLat), Double.parseDouble(userFromLong));
//        }
        if (srcMarker == null) {
            srcMarker = mMap.addMarker(new MarkerOptions().position(srcLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            srcMarker.setTitle("Your Location");
        } else {
//            MarkerAnimation.animateMarkerToGB(srcMarker, srcLatLng, new LatLngInterpolator.Spherical());
            srcMarker.setPosition(srcLatLng);
        }

        LatLng desLatLng = null;
        /*if (polylineArray != null && polylineArray.size() > 0 && mobile_request_id != null) {
            desLatLng = polylineArray.get(polylineArray.size() - 1);
        } else {*/
            desLatLng = new LatLng(Double.parseDouble(userToLat), Double.parseDouble(userToLong));
//        }
        if (destMarker == null) {
            destMarker = mMap.addMarker(new MarkerOptions().position(desLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            destMarker.setTitle(user_name);
        } else {
//            MarkerAnimation.animateMarkerToGB(destMarker, desLatLng, new LatLngInterpolator.Spherical());
            destMarker.setPosition(desLatLng);
        }
    }

    Handler handler = new Handler();
    Runnable runnable = null;
    boolean run = false;

    // call ever 5 second call
    public void changeValue() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runnable = this;
                run = true;
//                handler.postDelayed(runnable,10000);
            }
        }, 5000);
    }

    // cammera focus on draw line
    private void MakeCameraFocus(String userFromLat, String userFromLong, String userToLat, String userToLong) {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        final LatLng srcLatLng = new LatLng(Double.parseDouble(userFromLat), Double.parseDouble(userFromLong));
        final LatLng destLatLng = new LatLng(Double.parseDouble(userToLat), Double.parseDouble(userToLong));
        builder.include(srcLatLng);
        builder.include(destLatLng);
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        if (mobile_request_id == null) {
//            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            int pinShowTime = getResources().getInteger(R.integer.map_pin_time);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(srcLatLng, 15);
            mMap.animateCamera(cameraUpdate, pinShowTime, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    mDistance = getDistance(srcLatLng, destLatLng);
                    //  mTotalDistance.setText("Distance" + mDistance + " Meters");
                }

                @Override
                public void onCancel() {
                }
            });
        } else {
            if (start == false) {
//                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(srcLatLng, 15);
                int pinShowTime = getResources().getInteger(R.integer.map_pin_time);
                mMap.animateCamera(cameraUpdate, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        mDistance = getDistance(srcLatLng, destLatLng);
                        //  mTotalDistance.setText("Distance" + mDistance + " Meters");
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            } else {
                int pinShowTime = getResources().getInteger(R.integer.map_pin_time);
                LatLng dest = destLatLng, src = srcLatLng;
                if (polylineArray != null && polylineArray.size() > 1) {
                    src = polylineArray.get(0);
                    dest = polylineArray.get(1);
                }
                /*CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(srcLatLng).zoom(18).tilt(90).bearing(getBearing(src, dest)).build();*/
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(srcLatLng).zoom(15)./*bearing(getBearing(src, dest)).*/build();

                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                mMap.animateCamera(cameraUpdate, pinShowTime, new GoogleMap.CancelableCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onFinish() {
                        mDistance = getDistance(srcLatLng, destLatLng);
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            }
        }
    }

    private float getBearing(@NotNull LatLng begin, @NotNull LatLng end) {
        double dLon = (end.longitude - begin.longitude);
        double x = Math.sin(Math.toRadians(dLon)) * Math.cos(Math.toRadians(end.latitude));
        double y = Math.cos(Math.toRadians(begin.latitude)) * Math.sin(Math.toRadians(end.latitude))
                - Math.sin(Math.toRadians(begin.latitude)) * Math.cos(Math.toRadians(end.latitude)) * Math.cos(Math.toRadians(dLon));
        double bearing = Math.toDegrees((Math.atan2(x, y)));
        return (float) bearing;
    }

    // get two point distance
    @SuppressLint("DefaultLocale")
    public static String getDistance(@NotNull LatLng lat_lngA, @NotNull LatLng lat_lngB) {
        Location locationA = new Location("point A");
        locationA.setLatitude(lat_lngA.latitude);
        locationA.setLongitude(lat_lngA.longitude);
        Location locationB = new Location("point B");
        locationB.setLatitude(lat_lngB.latitude);
        locationB.setLongitude(lat_lngB.longitude);

        float distance = locationA.distanceTo(locationB) / 1000; // To convert Meter in Kilometer
        return String.format("%.2f", distance);
    }

    @NotNull
    private String getDirectionsUrl(String fromLat, String fromLong, String toLet, String toLong) {

        // Origin of route
        String str_origin = "origin=" + fromLat + "," + fromLong;

        // Destination of route
        String str_dest = "destination=" + toLet + "," + toLong;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = Helper.DIRECTION_API + output + "?" + parameters + "&key=" + Helper.API_KEY;
//        String url = Helper.DIRECTION_API + output + "?" + parameters + "&key=AIzaSyDMa4KIH9DEWgih6sQiPeXK3NNb6L4eoYQ";
//        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=AIzaSyAsGI0P0ORyhuRvUnMo4ecIRSnpvb48roc";
        Log.e(TAG, "getDirectionsUrl=> " + url);

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {

        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setPadding(0, back.getHeight() + 40, 0, 0);
        LatLng india = new LatLng(latitude, longitude);
        if (!india.equals(null) && mobile_request_id == null)
            betweenPathDraw(String.valueOf(latitude), String.valueOf(longitude), notificationLatitude, notificationLongitude);

    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.e(TAG, "onLocationChanged: " + latitude);
            Log.e(TAG, "onLocationChanged: " + longitude);
            betweenPathDraw(String.valueOf(latitude), String.valueOf(longitude), notificationLatitude, notificationLongitude);

        } else {
            // Log.d("Location is null");
        }
    }

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(Act_Map.this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[0]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                Log.e(TAG, "checkPermissions: ");
            }
        } else {
            getMyLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(Act_Map.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            // getCurrentLocation();
            // getMyLocation();.
        } else {
            checkPermissions();
        }
    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {

                int permissionLocation = ContextCompat.checkSelfPermission(Act_Map.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    my_location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(2000);
                    locationRequest.setFastestInterval(2000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(@NonNull LocationSettingsResult result) {
                            if (result == null){
                                getMyLocation();
                                return;
                            }
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat.checkSelfPermission(Act_Map.this, Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        my_location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                                        if (my_location != null) {
                                            betweenPathDraw(String.valueOf(my_location.getLatitude()), String.valueOf(my_location.getLongitude()), notificationLatitude, notificationLongitude);
                                            Log.e(TAG, "onResult: " + my_location.getLatitude());
                                        }
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(Act_Map.this, REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        // All required changes were successfully made
                        Toast.makeText(this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    // update location api call when change location
    private void UpdateLocation(final String user) {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "live_location_update",
                    new Response.Listener<String>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag response", response);
                            response = Html.fromHtml(response).toString();
                            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    if (!user.equals("")) {
                                        singleEditor.putBoolean(mobile_request_id, true).apply();
                                    }
                                    Log.e("Tag response", mainJsonObject.toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("size ==> Error", error.toString());
                        }
                    }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
//                    params.put("mobile_request_id", mobile_request_id);
                    params.put("garage_id", garage_id);
                    params.put("user_id", user);
                    params.put("garage_latitude", String.valueOf(latitude));
                    params.put("garage_longitude", String.valueOf(longitude));
                    params.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            onBackPressed();
        } else if (view == txt_start) {
            start = true;
            run = true;
            checkPermissions();
        } else if (view == txt_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_no));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable);
        }
        final LocationListener listener = this;
        if (start == true) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(Act_Map.this);
            dialog.setMessage("If you sure want to stop trip, stop update location");

            dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                    if (googleApiClient != null)
                        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, listener);
                    dialogInterface.dismiss();
                }
            });
            dialog.setNegativeButton(getString(R.string.places_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog dialog1 = dialog.create();
            dialog1.show();
        } else {
            super.onBackPressed();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);

        }
    }

    PolylineOptions lineOptions = null;

    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            Log.e(TAG, "jsonData[0]=>" + jsonData[0]);

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(jsonData[0]);
                JSONArray array = jsonObject.getJSONArray("routes");
                JSONObject route = array.getJSONObject(0);
                JSONArray legs = route.getJSONArray("legs");
                JSONObject steps = legs.getJSONObject(0);
                JSONObject distance = steps.getJSONObject("distance");
                String distances = distance.getString("text");

                Log.e("Tag Second 1", distances);
                if (!preferences.getBoolean(mobile_request_id, false)) {
                    if (distances.toLowerCase().contains("km")) {
                        Log.e("Tag Second 2", distances);
                        distances = distances.replaceAll("[^\\.0123456789]", "");
                        Log.e("Tag Second 3", distances);
                        if (Float.parseFloat(distances) < Float.parseFloat("2")) {
                            Log.e("Tag Second 4", distances);
                            UpdateLocation(user_id);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            polylineArray = new ArrayList<>();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
//                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(Objects.requireNonNull(point.get("lat")));
                    double lng = Double.parseDouble(Objects.requireNonNull(point.get("lng")));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                    polylineArray.add(new LatLng(lat, lng));
                }


                if (lineOptions == null) {
                    lineOptions = new PolylineOptions();
                    lineOptions.addAll(points);
                    lineOptions.width(12);
                    lineOptions.color(Color.parseColor("#FF2E8FED"));
                    lineOptions.geodesic(true);
                } else {
                    if (polyline != null) {
                        polyline.setPoints(points);
                    }
                    if (mobile_request_id != null == start) {
                        /*CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(latitude, longitude)).zoom(18).tilt(90).bearing(getBearing(polyline.getPoints().get(0), polyline.getPoints().get(1))).build();*/
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(latitude, longitude)).zoom(15).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        mMap.moveCamera(cameraUpdate);
                        srcMarker.setPosition(new LatLng(latitude, longitude));
//                        MarkerAnimation.animateMarkerToGB(srcMarker, polyline.getPoints().get(0), new LatLngInterpolator.Spherical());
                    }
                }
                /*if (result.size() == i+1){
                    if (mobile_request_id != null){
                        betweenPathDraw(String.valueOf(latitude),String.valueOf(longitude),notificationLatitude,notificationLongitude);
                    }
                }*/
            }


            Log.e(TAG, "lineOptions: " + lineOptions);
            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (polyline == null) {
                    polyline = mMap.addPolyline(lineOptions);
                }
            } else {
                //  Toast.makeText(getApplicationContext(), "Direction not Found..!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}