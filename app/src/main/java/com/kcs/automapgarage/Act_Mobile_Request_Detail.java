package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Request_Data_Model;
import com.kcs.automapgarage.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Mobile_Request_Detail extends AppCompatActivity implements View.OnClickListener {

    ImageView back;
    LinearLayout lin_main, line_bottom, lin_accept_reject, lin_call;
    CardView card_user_info, card_user_service, card_call, card_map, card_complain;
    TextView txt_name, txt_email, txt_cnt, txt_address, txt_brand_name,
            txt_model, txt_model_year, txt_fuel, txt_date, txt_time, txt_message;

    LinearLayout lin_service, lin_oil_brand, lin_oil_sae, lin_leather_seat, lin_filter, lin_amount;
    TextView txt_service, txt_oil_brand, txt_oil_sae, txt_leather_seat, txt_filter, txt_amount;

    Button btn_reject, btn_accept;

    boolean isFirst = false;

    String mobile_request_id = "", user_id = "", model_year = "";
    String lat = "", lng = "", name = "", garage_name = "", garage_id = "";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__mobile__request__detail);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_name = preferences.getString(getString(R.string.pref_garage_name), "");
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");


        back = findViewById(R.id.back);
        line_bottom = findViewById(R.id.line_bottom);
        lin_main = findViewById(R.id.lin_main);
        card_user_info = findViewById(R.id.card_user_info);
        card_user_service = findViewById(R.id.card_user_service);
        txt_name = findViewById(R.id.txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_cnt = findViewById(R.id.txt_cnt);
        txt_address = findViewById(R.id.txt_address);
        txt_brand_name = findViewById(R.id.txt_brand_name);
        txt_model = findViewById(R.id.txt_model);
        txt_model_year = findViewById(R.id.txt_model_year);
        txt_fuel = findViewById(R.id.txt_fuel);
        txt_time = findViewById(R.id.txt_time);
        txt_date = findViewById(R.id.txt_date);
        txt_message = findViewById(R.id.txt_message);

        lin_oil_brand = findViewById(R.id.lin_oil_brand);
        txt_oil_brand = findViewById(R.id.txt_oil_brand);

        lin_oil_sae = findViewById(R.id.lin_oil_sae);
        txt_oil_sae = findViewById(R.id.txt_oil_sae);

        lin_leather_seat = findViewById(R.id.lin_leather_seat);
        txt_leather_seat = findViewById(R.id.txt_leather_seat);

        lin_filter = findViewById(R.id.lin_filter);
        txt_filter = findViewById(R.id.txt_filter);

        lin_amount = findViewById(R.id.lin_amount);
        txt_amount = findViewById(R.id.txt_amount);

        lin_service = findViewById(R.id.lin_services);
        txt_service = findViewById(R.id.txt_services);

        btn_reject = findViewById(R.id.btn_reject);
        btn_accept = findViewById(R.id.btn_accept);

        lin_call = findViewById(R.id.lin_call);
        lin_accept_reject = findViewById(R.id.lin_accept_reject);

        card_call = findViewById(R.id.card_call);
        card_map = findViewById(R.id.card_map);
        card_complain = findViewById(R.id.card_complain);

        back.setOnClickListener(this);
        btn_accept.setOnClickListener(this);
        btn_reject.setOnClickListener(this);
        card_call.setOnClickListener(this);
        card_map.setOnClickListener(this);
        card_complain.setOnClickListener(this);

        getMobileRequestDetail();
        getIntentData();
        InitializeBroadcast();
        setButtonText();

    }

    public void setButtonText() {
        TextView txt_call_now = findViewById(R.id.txt_call_now);
        txt_call_now.setText(getString(R.string.call_now));
        TextView txt_lest_go = findViewById(R.id.txt_lest_go);
        txt_lest_go.setText(getString(R.string.let_s_go));
        TextView txt_complain = findViewById(R.id.txt_complain);
        txt_complain.setText(getString(R.string.complain));
    }

    public void getIntentData() {
        mobile_request_id = getIntent().getStringExtra(getString(R.string.pref_request_id));
        user_id = getIntent().getStringExtra(getString(R.string.pref_user_id));
        model_year = getIntent().getStringExtra("model_year");
    }

    // accept dialog for mobile request
    public void AcceptDialog(final String User_Id, final String mobile_request_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        TextView txt_title = view.findViewById(R.id.txt_dialog_title);
        txt_title.setText(getString(R.string.send_service_time));
        LinearLayout linearLayout = view.findViewById(R.id.lin_edt_price_time);
        final EditText price = view.findViewById(R.id.edt_price);
        final EditText hour = view.findViewById(R.id.edt_hour);
        TextView textView = view.findViewById(R.id.txt_arrival_time);
        textView.setText(getString(R.string.arraival_time));
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_cancel.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (price.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_price), Toast.LENGTH_LONG).show();
                } else if (hour.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.arraival_time), Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();
                    Log.e("TAG", User_Id);
                    Log.e("TAG", mobile_request_id);
                    Log.e("TAG", price.getText().toString());
                    Log.e("TAG", hour.getText().toString());
                    RejectAcceptRequest(User_Id, "1", mobile_request_id, price.getText().toString(), hour.getText().toString());
                }
            }
        });
    }

    // reject dialog for mobile request
    public void RejectAcceptRequest(final String _user_id, final String _status,
                                    final String mobile_request_id, final String price,
                                    final String time) {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        Log.e("TAG", _user_id);
        Log.e("TAG", _status);
        Log.e("TAG", price);
        Log.e("TAG", time);
        Log.e("TAG", mobile_request_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "response_mobile_request",
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_garage", " " + response);
                        try {
                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.optString("status").equals("1")) {
                                if (_status.equals("1")) {
                                    lin_accept_reject.setVisibility(View.GONE);

                                    Toast.makeText(getApplicationContext(), mainJsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                    getMobileRequestDetail();
                                } else if (_status.equals("2")) {
                                    Toast.makeText(getApplicationContext(), mainJsonObject.optString("status"), Toast.LENGTH_SHORT).show();
                                    onBackPressed();
                                }
                            } else {
                                showMessage(mainJsonObject.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  displaying the error in toast if occurs
                        Log.e("TAG", error.toString());
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            //  This indicates that the request has either time out or there is no connection
                            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            //  Error indicating that there was an Authentication Failure while performing the request
                        } else if (error instanceof ServerError) {
                            //  Indicates that the server responded with a error response
                        } else if (error instanceof NetworkError) {
                            //  Indicates that there was network error while performing the request
                        } else if (error instanceof ParseError) {
                            //  Indicates that the server response could not be parsed
                        }
                        dialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", _user_id);
                params.put("garage_id", garage_id);
                params.put("status", _status);
                params.put("mobile_request_id", mobile_request_id);
                params.put("price", price);
                params.put("arrival_time", time);
                params.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("params", " : " + params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showMessage(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Mobile_Request_Detail.this);
        builder.setMessage(msg);

        builder.setPositiveButton(getText(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                onBackPressed();
            }
        });

        builder.setNeutralButton(getString(R.string.contact_us), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(getApplicationContext(), Act_Contact_Us.class));
            }
        });

        AlertDialog dialog = builder.create();
        setFinishOnTouchOutside(false);
        dialog.show();
    }

    // get mobile request detail and status wise show detail
    private void getMobileRequestDetail() {
        final ProgressDialog dialog = new ProgressDialog(Act_Mobile_Request_Detail.this);
        dialog.setMessage(getString(R.string.loading));
        if (!isFirst) {
            dialog.show();
            isFirst = true;
        }
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "mobile_request_detail",
                    new Response.Listener<String>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(String response) {
                            Log.e("response Request Detail", response);
                            dialog.dismiss();
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                JSONArray jsonArray = mainJsonObject.getJSONArray("result");
                                if (jsonArray.length() > 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String request_id = jsonObject.getString("mobile_request_id");
//                                        String user_id = jsonObject.getString("user_id");
                                        name = check(jsonObject,"name");
                                        String status = jsonObject.getString("status");
                                        String email = jsonObject.getString("email");
                                        String contact_no = jsonObject.getString("contact_no");
                                        String model_name = check(jsonObject,"model_name");
                                        if (jsonObject.has("model_year")) {
                                            model_year = jsonObject.getString("model_year");
                                        }
                                        String brand_name = check(jsonObject,"brand_name");
//                                        String variant_name = jsonObject.getString("variant_name");
                                        String fuel_type = check(jsonObject,"fule_type");
                                        String request_date = check(jsonObject,"request_date");
                                        String request_time = check(jsonObject,"request_time");
                                        String date = jsonObject.getString("created_at");
                                        String update_at = jsonObject.getString("created_at");
                                        String request_type = check(jsonObject,"request_type");
                                        String oil_brand = check(jsonObject,"oil_brand");
                                        String oil_sae = check(jsonObject,"oil_sae");
                                        lat = jsonObject.getString("user_latitude");
                                        lng = jsonObject.getString("user_longitude");
                                        String amount = jsonObject.getString("amount");
                                        String location_address = check(jsonObject,"location_address");
                                        String leather_seats = check(jsonObject,"leather_seat");
                                        String change_filter = check(jsonObject,"change_filter");

                                        if (Integer.parseInt(status) >= 3) {
                                            card_user_info.setVisibility(View.VISIBLE);
                                            txt_name.setText(name);
                                            txt_address.setText(location_address);
                                            txt_date.setText(AppUtils.DateFormat(request_date));
                                            txt_time.setText(AppUtils.TimeFormat(request_time));
                                            txt_cnt.setText(contact_no);
                                            txt_email.setText(email);
                                        } else {
                                            card_user_info.setVisibility(View.GONE);
                                        }

                                        txt_brand_name.setText(brand_name);
                                        txt_model.setText(model_name);
                                        txt_model_year.setText(model_year);
                                        txt_fuel.setText(fuel_type);

                                        if (checkEmpty(oil_brand, oil_sae, leather_seats, change_filter, amount, request_type)) {
                                            card_user_service.setVisibility(View.VISIBLE);

                                            if (oil_brand.trim().length() > 0 && !oil_brand.equalsIgnoreCase("")) {
                                                lin_oil_brand.setVisibility(View.VISIBLE);
                                                txt_oil_brand.setText(Html.fromHtml(oil_brand));
                                            } else {
                                                lin_oil_brand.setVisibility(View.GONE);
                                            }

                                            if (oil_sae.trim().length() > 0 && !oil_sae.equalsIgnoreCase("")) {
                                                lin_oil_sae.setVisibility(View.VISIBLE);
                                                txt_oil_sae.setText(Html.fromHtml(oil_sae));
                                            } else {
                                                lin_oil_sae.setVisibility(View.GONE);
                                            }

                                            if (amount.trim().length() > 0 && !amount.equalsIgnoreCase("")) {
                                                lin_amount.setVisibility(View.VISIBLE);
                                                txt_amount.setText(Html.fromHtml(amount));
                                            } else {
                                                lin_amount.setVisibility(View.GONE);
                                            }

                                            if (leather_seats.trim().length() > 0 && !leather_seats.equalsIgnoreCase("") && !leather_seats.equals("null")) {
                                                lin_leather_seat.setVisibility(View.VISIBLE);
                                                txt_leather_seat.setText(Html.fromHtml(leather_seats));
                                            } else {
                                                lin_leather_seat.setVisibility(View.GONE);
                                            }

                                            if (change_filter.trim().length() > 0 && !change_filter.equalsIgnoreCase("") && !change_filter.equals("null") && !change_filter.toLowerCase().equals("false")) {
                                                lin_filter.setVisibility(View.VISIBLE);
                                                txt_filter.setText(Html.fromHtml(change_filter));
                                            } else {
                                                lin_filter.setVisibility(View.GONE);
                                            }

                                            if (request_type.trim().length() > 0 && !request_type.equalsIgnoreCase("")) {
                                                lin_service.setVisibility(View.VISIBLE);
                                                txt_service.setText(Html.fromHtml(request_type));
                                            } else {
                                                lin_service.setVisibility(View.GONE);
                                            }

                                        } else {
                                            card_user_service.setVisibility(View.GONE);
                                        }

                                        // status 0 means new request and accept reject visible
                                        if (status.equalsIgnoreCase("0")) {
                                            lin_accept_reject.setVisibility(View.VISIBLE);
                                        } else {
                                            lin_accept_reject.setVisibility(View.GONE);
                                        }

                                        // status 1 means  accept and bottom visible message
                                        if (status.equalsIgnoreCase("1")) {
                                            txt_message.setVisibility(View.VISIBLE);
                                            txt_message.setText(R.string.your_request_is_pending_for_customer_confirmation);
                                        } else if (status.equalsIgnoreCase("2")) {
                                            txt_message.setVisibility(View.VISIBLE);
                                            txt_message.setText(R.string.you_are_not_interrest_for_this_request);
                                        } else {
                                            txt_message.setVisibility(View.GONE);
                                        }

                                        // status 3 means reject and call visible complain visible

                                        if (status.equalsIgnoreCase("3")) {
                                            lin_call.setVisibility(View.VISIBLE);
                                            card_complain.setVisibility(View.VISIBLE);
                                        } else {
                                            lin_call.setVisibility(View.GONE);
                                        }

                                        // status 7 means user deClain and map inVisible and complain visible
                                        if (status.equals("7")) {
                                            card_complain.setVisibility(View.VISIBLE);
                                            card_map.setVisibility(View.GONE);
                                        } else {
                                            card_complain.setVisibility(View.GONE);
                                        }

                                        if (Integer.parseInt(status) > 3) {
                                            lin_call.setVisibility(View.VISIBLE);
                                            card_call.setVisibility(View.VISIBLE);
                                            card_map.setVisibility(View.GONE);
                                        }

                                        // status 5 means complete request and show ratting
                                        if (status.equals("5")) {
                                            RattingDialog(name);
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("size ==> Error", error.toString());
                            dialog.dismiss();
                        }
                    }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("mobile_request_id", mobile_request_id);
                    params.put("garage_id", garage_id);
                    params.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Mobile_Request_Detail.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // new request come to refress api
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (mobile != 0 && !Act_Mobile_Request_Detail.this.isFinishing()) {
                getMobileRequestDetail();
            }
        }
    };

    public boolean checkEmpty(@NotNull String oil_brand, String oil_sae, String leather_seat, String filter, String amount, String request_type) {

        if ((!oil_brand.equalsIgnoreCase("") || oil_brand.trim().length() > 0) ||
                (oil_sae.equalsIgnoreCase("") || oil_sae.trim().length() > 0) ||
                (leather_seat.equalsIgnoreCase("") || leather_seat.trim().length() > 0) ||
                (filter.equalsIgnoreCase("") || filter.trim().length() > 0) ||
                (amount.equalsIgnoreCase("") || amount.trim().length() > 0) ||
                (request_type.equalsIgnoreCase("") || request_type.trim().length() > 0)) {
            return true;
        } else {
            return false;
        }
    }

    float ratings = -1;

    // ratting dilaog
    private void RattingDialog(String user_name) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        final RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        final Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText("Please Rate " + user_name);
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        ratingBar.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.GONE);

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar1, float rating, boolean fromUser) {
                ratings = rating;
                if (rating == 1) {
                    Toast.makeText(getApplicationContext(), "Bad", Toast.LENGTH_LONG).show();
                } else if (rating == 2) {
                    Toast.makeText(getApplicationContext(), "Bad Good", Toast.LENGTH_LONG).show();
                } else if (rating == 3) {
                    Toast.makeText(getApplicationContext(), "Good", Toast.LENGTH_LONG).show();
                } else if (rating == 4) {
                    Toast.makeText(getApplicationContext(), "Excellent Good", Toast.LENGTH_LONG).show();
                } else if (rating == 5) {
                    Toast.makeText(getApplicationContext(), "Excellent", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (ratings != 0) {
                    ratting("6", "" + ratings);
                } else
                    Toast.makeText(getApplicationContext(), "please select rate start", Toast.LENGTH_LONG).show();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "will be notified after 24 hours", Toast.LENGTH_LONG).show();
              /*Intent intent = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
                intent.putExtra("time",30000);
                startService(intent);*/
            }
        });
    }

    // rattinf api
    private void ratting(final String _status, final String ratting) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "garage_ratting",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    getMobileRequestDetail();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile_request_id", mobile_request_id);
                    params.put("status", _status);
                    params.put("garage_id", garage_id);
                    params.put("user_id", user_id);
                    params.put("ratting", ratting);
                    params.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            onBackPressed();
        } else if (view == btn_accept) {
            AcceptDialog(user_id, mobile_request_id);
        } else if (view == btn_reject) {
            RejectAcceptRequest(user_id, "2", mobile_request_id, "0", "0");
        } else if (view == card_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + txt_cnt.getText().toString()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (view == card_map) {
            Intent intent = new Intent(getApplicationContext(), Act_Map.class);
            intent.putExtra("latitude", lat);
            intent.putExtra("longitude", lng);
            intent.putExtra("user_name", name);
            intent.putExtra("user_id", user_id);
            intent.putExtra("mobile_request_id", mobile_request_id);
            intent.putExtra(getString(R.string.contact_no), txt_cnt.getText().toString());
            intent.putExtra("isUpdate", "1");
            intent.putExtra(getString(R.string.pref_garage_name), garage_name);
            startActivity(intent);
        } else if (view == card_complain) {
            Toast.makeText(getApplicationContext(), getString(R.string.complain), Toast.LENGTH_LONG).show();
            ComplainDialog();
        }
    }

    // complain dialog
    public void ComplainDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(Act_Mobile_Request_Detail.this);

        View view = LayoutInflater.from(Act_Mobile_Request_Detail.this).inflate(R.layout.dialog_complain, null, false);
        EditText ed_complain = view.findViewById(R.id.ed_complain);
        Button button = view.findViewById(R.id.btn_complain);
        dialog.setView(view);
        AlertDialog dialog1 = dialog.create();
        dialog1.show();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_complain.getText().toString().equals("")) {
                    ed_complain.requestFocus();
                    ed_complain.setError(getString(R.string.enter_detail));
                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "garage_complain", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag Response", response);

                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("true")) {
                                    dialog1.dismiss();
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag error", error.toString());
                             Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
//                             Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("garage_id", garage_id);
                            map.put("user_id", user_id);
                            map.put("mobile_request_id", mobile_request_id);
                            map.put("status", "8");
                            map.put("complain", ed_complain.getText().toString());
                            map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                            Log.e("Tag Paramas", map.toString());
                            return map;
                        }
                    };

                    request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue queue = Volley.newRequestQueue(Act_Mobile_Request_Detail.this);
                    queue.add(request);
                }
            }
        });
    }
}
