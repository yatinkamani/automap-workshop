package com.kcs.automapgarage.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

public class PermissionUtils {

    @SuppressLint("WrongConstant")
    public static boolean hasPermission(Activity activity, String permission){
        if (useRunTimePermission()){
            return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }else {
            return false;
        }
    }

    public static void requestPermission(Activity activity, String[] permissions, int requestCode){
        if (useRunTimePermission()){
            activity.requestPermissions(permissions, requestCode);
        }
    }

    public static boolean useRunTimePermission(){
        return Build.VERSION.SDK_INT > 22;
    }
}
