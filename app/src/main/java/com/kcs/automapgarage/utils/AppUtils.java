package com.kcs.automapgarage.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.kcs.automapgarage.Act_Login;
import com.kcs.automapgarage.Model.Garage_Facility_Model;
import com.kcs.automapgarage.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils {

    public static int PERMISSION_ID = 44;
    public static String language = "language";

    public static List<Garage_Facility_Model> garage_facility_models = new ArrayList<>();

    public static boolean isNetworkAvailable(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null
                    && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null
                        && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;

    }

    public static String getOTPForMessage(String Message){

        Pattern p = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = p.matcher(Message);
        if (matcher.find()){
            return matcher.group(0);
        } else {
          return "";
        }
    }

    public static String serverError(Context context, VolleyError volleyError){
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = context.getString(R.string.check_network_connection);
        } else if (volleyError instanceof ServerError) {
            message = context.getString(R.string.try_again_after_some_time);
        } else if (volleyError instanceof AuthFailureError) {
            message = context.getString(R.string.unauthoride_access);
        } else if (volleyError instanceof ParseError) {
            message = context.getString(R.string.try_again_after_some_time);
        } else if (volleyError instanceof NoConnectionError) {
            message = context.getString(R.string.check_network_connection);
        } else if (volleyError instanceof TimeoutError) {
            message = context.getString(R.string.connection_timeour);
        }else {
            message = context.getString(R.string.something_wrong_please);
        }
        return message;
    }

    @NotNull
    public static String check(JSONObject object, String string) {

        SharedPreferences sharedPreferences = MyApplication.getInstance().getSharedPreferences(Act_Login.LOGIN_PREF,Context.MODE_PRIVATE);
        String s_lang = sharedPreferences.getString(MyApplication.getInstance().getString(R.string.pref_language), "ar");
        String lan = "_ar";
        String data_ar = "";
        String data_en = "";
        try {
            if (s_lang.equals("ar")){

                if (object.has(string+lan)){
                    if (object.getString(string+lan) != null && !object.getString(string+lan).equals("null")){
                        data_ar = object.getString(string+lan);
                        if (data_ar.equals("") && object.has(string)){
                            if (object.getString(string) != null && !object.getString(string).equals("null")){
                                data_ar = object.getString(string);
                            }else {
                                data_ar = "";
                            }
                        }
                    }else {
                        data_ar = "";
                    }
                }else if (object.has(string)){
                    if (object.getString(string) != null && !object.getString(string).equals("null")){
                        data_en = object.getString(string);
                        /*if (data_en.equals("") && object.has(string+lan)){
                            if (object.getString(string+lan) != null && !object.getString(string+lan).equals("null")){
                                data_ar = object.getString(string+lan);
                            }else {
                                data_ar = "";
                            }
                        }*/
                    }else {
                        data_en = "";
                    }
                }

            }else {
                if (object.has(string)){
                    if (object.getString(string) != null && !object.getString(string).equals("null")){
                        data_en = object.getString(string);
                    }else {
                        data_en = "";
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if (s_lang.equals("ar")){

            if (!data_ar.equals("")){
                return data_ar;
            }else if (!data_en.equals("")){
                return data_en;
            }else {
                return "";
            }
        }else {
            if (!data_en.equals("")){
                return data_en;
            }else if (!data_ar.equals("")){
                return data_ar;
            }else {
                return "";
            }
        }
    }

    public static String checkLng(JSONObject object, String s) {

        SharedPreferences sharedPreferences = MyApplication.getInstance().getSharedPreferences(Act_Login.LOGIN_PREF,Context.MODE_PRIVATE);
        String s_lang = sharedPreferences.getString(MyApplication.getInstance().getString(R.string.pref_language), "en");
        String lan = "_ar";

        try {
            if (s_lang.equals("ar")){

                if (object.has(s+lan) &&
                        object.getString(s+lan) != null &&
                        !object.getString(s+lan).equals("null") &&
                        !object.getString(s+lan).equals("")){

                    return object.getString(s+lan);

                }else if (object.has(s) &&
                        object.getString(s) != null &&
                        !object.getString(s).equals("null") &&
                        !object.getString(s).equals("")){

                    return object.getString(s);
                }else {
                    return "";
                }

            }else {
                if (object.has(s) &&
                        object.getString(s) != null &&
                        !object.getString(s).equals("null") &&
                        !object.getString(s).equals("")){

                    return object.getString(s);

                }else if (object.has(s+lan) &&
                        object.getString(s+lan) != null &&
                        !object.getString(s+lan).equals("null") &&
                        !object.getString(s+lan).equals("")){

                    return object.getString(s+lan);

                } else {
                    return "";
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            return  "";
        }
    }

    public static boolean isLocationEnabled(@NotNull Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    public static boolean checkGPSPermissions(Context context) {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public static void requestGPSPermissions(Activity activity) {
        ActivityCompat.requestPermissions(
                activity,
                new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    public static boolean checkCameraPermissions(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public static void requestCameraPermissions(Activity activity) {
        ActivityCompat.requestPermissions(
                activity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_ID
        );
    }

    public static boolean checkStoragePermissions(Context context) {
        if ( ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
             ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public static void requestStoragePermissions(Activity activity) {
        ActivityCompat.requestPermissions(
                activity,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_ID
        );
    }

    public static String DateFormat(String yyyy_MM_dd) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(yyyy_MM_dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newFormat.format(date);

    }

    public static String TimeFormat(String time) {

        int hour = Integer.parseInt(time.split(":")[0]);
        int minute = Integer.parseInt(time.split(":")[1]);
        int minutes = minute;
        String timeSet = "";
        if (hour > 12) {
            hour -= 12;
            timeSet = "PM";
        } else if (hour == 0) {
            hour += 12;
            timeSet = "AM";
        } else if (hour == 12) {
            timeSet = "PM";
        } else {
            timeSet = "AM";
        }

        String min = "";
        if (minutes < 10)
            min = "0" + minutes;
        else
            min = String.valueOf(minutes);
        Log.e("TAG AM PM", timeSet);
        Log.e("TAG Hour ", "" + hour);
        Log.e("TAG Minute", "" + min);

        String aTime = new StringBuilder().append(hour).append(':').append(min).append(" ").append(timeSet).toString();
        Log.e("TAG ATime", "" + aTime);
        return aTime;
    }

    public static String ConvertLocalTime(String time){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        TimeZone utcZone = TimeZone.getTimeZone("UTC");
        simpleDateFormat.setTimeZone(utcZone);
        Date date = new Date();
        try {
            date = simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd, MMM yyyy hh:mm a", Locale.ENGLISH);
        dateFormat.setTimeZone (TimeZone.getDefault());
        String formattedDate = dateFormat.format (date);
        return formattedDate;
    }

    public static String getddmmmyyyy(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd, MMM yyyy hh:mm a", Locale.ENGLISH);
        SimpleDateFormat Format = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);
        String date_formate = "";
        try {
          date_formate = Format.format(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date_formate;
    }

    public static String getDaysDifference(String start, String end){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date startDate, endDate;
        String numberOfDays = "0";
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    public static Date getStringToDate(String start){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = simpleDateFormat.parse(start);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getUTCTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        long timeInMili = calendar.getTimeInMillis();
        String stringDate = simpleDateFormat.format(new Date(timeInMili));
        return stringDate;
    }

    public static Date AddMinutes(Date date, int min){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, (min));
        String d = simpleDateFormat.format((calendar.getTimeInMillis()));
        try {
            return simpleDateFormat.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getUTCTime(String minute){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.add(Calendar.MINUTE,  Integer.parseInt(""+minute));
        long timeInMili = calendar .getTimeInMillis();
        String stringDate = simpleDateFormat.format(new Date(timeInMili));
        return stringDate;
    }


    public static String getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return String.valueOf(unit.convert(timeDiff, TimeUnit.MILLISECONDS));
    }

    public static void hideKeyboard(@Nullable Activity activity) {
        try {
            if(activity!=null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                //Find the currently focused view, so we can grab the correct window token from it.
                View view = activity.getCurrentFocus();
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null)
                    view = new View(activity);
                if(imm != null)
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
