package com.kcs.automapgarage;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Area_Data_Model;
import com.kcs.automapgarage.Model.City_Data_Model;
import com.kcs.automapgarage.Model.Country_Data_model;
import com.kcs.automapgarage.Model.Edit_User_Model;
import com.kcs.automapgarage.Model.Sales_Person_Model;
import com.kcs.automapgarage.Model.VolleyMultipartRequest;
import com.kcs.automapgarage.newPackage.Helper;
import com.kcs.automapgarage.utils.AppUtils;
import com.kcs.automapgarage.utils.PermissionUtils;
import com.kcs.automapgarage.view.WorkaroundMapFragment;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.kcs.automapgarage.Act_Login.LOGIN_PREF;
import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.checkCameraPermissions;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Edit_Profile_Detail extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    EditText e_nm, e_con_no, ed_owner_nm, ed_description, ed_E_facebook_link, ed_E_salesName;
    TextView txt_E_address, txt_E_nm, txt_E_facebook_link, txt_current_location, txt_banner, txt_dec, txt_area, txt_city;
    //    EditText e_email, ed_password ;
    CardView c_edit;

    TextView txtUpdateProfileTitle;

    //    Button upload;
    private Bitmap bitmap, bitmap2;
    private GoogleApiClient mGoogleApiClient;
    private LocationManager mLocationManager;
    ImageView banner_image;
//    ImageView imageView;

    String garage_id, name, country_id = "", city_id = "", area_id = "", sales_id = "", address = "",
            con_no = "", owner_nm = "", description = "", service_id = "", facebook_link = "";

    //    String email_id;
    ImageView ed_pro_back;

    Edit_User_Model ed_model = new Edit_User_Model();
    private ArrayList<Edit_User_Model> editModel;
    LinearLayout lin_banner;

    SharedPreferences prefs;
    TextView txt_upload_banner, txt_salesName;
    //    TextView txt_upload_photo;
    Spinner spinner_country, Sp_select_city, Sp_select_area;
    private ArrayList<Country_Data_model> country_data_models;
    private ArrayList<City_Data_Model> city_data_models;
    private ArrayList<Area_Data_Model> area_data_models;
    private ArrayList<Sales_Person_Model> sales_models;
    private ArrayList<String> names = new ArrayList<String>();

    ScrollView scrollView;
    private GoogleMap mMap;
    LatLng latLng;
    String place_name;
    private LocationRequest mLocationRequest;
    private Location mLocation;

    File image_file, banner_file;

    double d_latitude = 0.0;
    double d_longitude = 0.0;
    //    AutocompleteSupportFragment autocompleteFragment;
    WorkaroundMapFragment mapFragment;

    int PICK_IMAGE_CAMERA = 100, PICK_IMAGE_GALLERY = 101;
    int PICK_IMAGE_CAMERA1 = 200, PICK_IMAGE_GALLERY1 = 201;
    int LOCATION_PICKER = 199;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    public void setLanguage() {
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        String language = prefs.getString(getString(R.string.pref_language), "ar").trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        /*if (Locale.getDefault().getLanguage().equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLanguage();
        setContentView(R.layout.activity_act__edit__profile__detail);
        prefs = getSharedPreferences(LOGIN_PREF, MODE_PRIVATE);
        city_id = prefs.getString(getString(R.string.pref_city_id), "");
        area_id = prefs.getString(getString(R.string.pref_area_id), "");
        owner_nm = prefs.getString(getString(R.string.pref_owner_name), "");
        BindView();
        getIntentData();

        MapUtility.apiKey = getString(R.string.api_key);


        SpinnerModel();
        retrieveJSON2();


        // car user to hide other workshop field
        if (prefs.getString(getString(R.string.pref_service_id), "").toString().equals("61")) {
            hideUserField();
        } else {
            txt_area.setText(getText(R.string.area));
            AutoCompleteMapInitialize();
            mGoogleApiClient = new GoogleApiClient.Builder(Act_Edit_Profile_Detail.this).addApi(Places.GEO_DATA_API).build();
        }
    }

    private void BindView() {

        ed_pro_back = (ImageView) findViewById(R.id.ed_pro_back);
//        imageView = findViewById(R.id.image);
        banner_image = findViewById(R.id.banner_image);
//        upload = findViewById(R.id.btn_upload);
        e_nm = findViewById(R.id.ed_E_nm);
        e_nm.requestFocus();
//        e_email = findViewById(R.id.ed_E_email);
        ed_E_salesName = findViewById(R.id.ed_E_salesName);
        txt_salesName = findViewById(R.id.txt_salesName);
        txt_current_location = findViewById(R.id.txt_current_location);
        e_con_no = findViewById(R.id.ed_E_contect);
        ed_owner_nm = findViewById(R.id.ed_owner_nm);
        ed_description = findViewById(R.id.ed_description);
//        ed_password = findViewById(R.id.ed_password);
        ed_E_facebook_link = findViewById(R.id.ed_E_website);
        c_edit = findViewById(R.id.edit_E_card);
//        txt_upload_photo = findViewById(R.id.txt_upload_photo);
        txt_upload_banner = findViewById(R.id.txt_upload_banner);
        txt_banner = findViewById(R.id.txt_banner);
        lin_banner = findViewById(R.id.lin_banner);
        txt_dec = findViewById(R.id.txt_dec);

        txt_E_address = findViewById(R.id.txt_E_address);
        txt_E_nm = findViewById(R.id.txt_E_nm);
        txt_E_facebook_link = findViewById(R.id.txt_E_facebook);

        txt_area = findViewById(R.id.txt_area);
        txt_city = findViewById(R.id.txt_city);

        spinner_country = (Spinner) findViewById(R.id.Sp_select_country);
        Sp_select_area = (Spinner) findViewById(R.id.Sp_select_area);
        Sp_select_city = (Spinner) findViewById(R.id.Sp_select_city);

        txtUpdateProfileTitle = (TextView) findViewById(R.id.txtUpdateProfileTitle);
        mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser));
        scrollView = findViewById(R.id.scrollView);

        name = e_nm.getText().toString();

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        name = prefs.getString(getString(R.string.pref_garage_name), "");
//        email_id = prefs.getString(getString(R.string.pref_email), "");

        con_no = prefs.getString(getString(R.string.pref_contact_no), "");
        country_id = prefs.getString(getString(R.string.pref_country_id), "");
        area_id = prefs.getString(getString(R.string.pref_area_id), "");
        sales_id = prefs.getString(getString(R.string.pref_sales_id), "");
        service_id = prefs.getString(getString(R.string.pref_service_id), "");

        if (prefs.getString(getString(R.string.pref_user_type), "0").equals("1")) {
            txt_salesName.setVisibility(View.GONE);
            ed_E_salesName.setVisibility(View.GONE);
        }

        // edit time i == 1
        if (Constant.i == 1) {
            garage_id = prefs.getString(getString(R.string.pref_garage_id), "");
//            txt_salesName.setVisibility(View.VISIBLE);
//            ed_E_salesName.setVisibility(View.VISIBLE);
            txt_salesName.setVisibility(View.GONE);
            ed_E_salesName.setVisibility(View.GONE);
        } else {
            garage_id = getIntent().getStringExtra(getString(R.string.pref_garage_id));
        }

        ed_pro_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectImage(1);
//            }
//        });

        // banner image picker
        banner_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(Act_Edit_Profile_Detail.this)
                        .compress(2048)
                        .crop(16f, 9f)	//Crop image with 16:9 aspect ratio
                        .start();
//                selectImage(2);
            }
        });

        c_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (service_id.equals("61")) {
                    uploadUserBitmap();
                } else {
                    uploadBitmap(bitmap, bitmap2);
                }
            }
        });

        // open curretn location screen and selected location
        txt_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LocationPickerActivity.class);
                if (address != null && !address.equals("")) {
                    LatLng latLng = getLocationFromAddress(getApplicationContext(), address);
                    if (latLng != null) {
                        intent.putExtra(MapUtility.LATITUDE, latLng.latitude);
                        intent.putExtra(MapUtility.LONGITUDE, latLng.longitude);
                        intent.putExtra(MapUtility.ADDRESS, address);
                    }
                }
                startActivityForResult(intent, LOCATION_PICKER);
            }
        });

        GarageStatus(garage_id);

        hideShowView();
    }

    // hide when register as car owner in android
    public void hideShowView(){

        if (service_id.equalsIgnoreCase("61")){
            txtUpdateProfileTitle.setText(getString(R.string.name_editing));
            txt_E_nm.setText(getString(R.string.enter_the_personal_name));
            txt_banner.setVisibility(View.GONE);
            lin_banner.setVisibility(View.GONE);
            txt_city.setVisibility(View.GONE);
            Sp_select_city.setVisibility(View.GONE);
            txt_area.setVisibility(View.GONE);
            Sp_select_area.setVisibility(View.GONE);
            txt_dec.setVisibility(View.GONE);
            ed_description.setVisibility(View.GONE);
            txt_E_facebook_link.setVisibility(View.GONE);
            ed_E_facebook_link.setVisibility(View.GONE);
            txt_salesName.setVisibility(View.GONE);
            ed_E_salesName.setVisibility(View.GONE);
            txt_current_location.setVisibility(View.GONE);
            mapFragment.getView().setVisibility(View.GONE);
            txt_current_location.setVisibility(View.GONE);

        }
    }

    private void getIntentData() {
        if (getIntent().getBooleanExtra("SPLASH", false)) {
            ed_pro_back.setVisibility(View.INVISIBLE);
        }
    }

    // map view
    private void AutoCompleteMapInitialize() {
        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            com.google.android.libraries.places.api.Places.initialize(getApplicationContext(), getString(R.string.api_key));
        }
      /*  autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        assert autocompleteFragment != null;
        autocompleteFragment.getView().setBackgroundColor(Color.WHITE);

        final List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS);

        autocompleteFragment.setPlaceFields(fields);*/

        mapFragment.getMapAsync(Act_Edit_Profile_Detail.this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                scrollView.requestDisallowInterceptTouchEvent(true);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(Act_Edit_Profile_Detail.this)
                .addConnectionCallbacks(Act_Edit_Profile_Detail.this)
                .addOnConnectionFailedListener(Act_Edit_Profile_Detail.this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /*autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                latLng = place.getLatLng();
                d_latitude = latLng.latitude;
                d_longitude = latLng.longitude;
                scrollView.pageScroll(View.FOCUS_DOWN);
//                address = place.getAddress();
                Log.e("address", " " + address);
                Log.e("latLng2", " " + d_latitude + "------" + d_longitude);
                place_name = place.getName();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(getString(R.string.pref_address), place_name);
                editor.apply();
                mapFragment.getMapAsync(Act_Edit_Profile_Detail.this);
            }

            @Override
            public void onError(@NonNull Status status) {
                // TODO: Handle the error.
            }
        });*/
    }

    // set all sppinner selectable
    public void SpinnerModel() {

        // country spinner select and show city
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                country_id = country_data_models.get(i).getCon_id();
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString(getString(R.string.pref_spinner_country), spinner_country.getSelectedItem().toString());
                prefEditor.apply();

                String city = prefs.getString(getString(R.string.pref_spinner_city), " ");
                ArrayList<City_Data_Model> list = new ArrayList<>();
                if (city_data_models != null && city_data_models.size() > 0) {
                    for (int j = 0; j < city_data_models.size(); j++) {
                        if (city_data_models.get(j).getCon_id().equals(country_id)) {
                            list.add(city_data_models.get(j));
                        }
                    }
                }

                ArrayAdapter<City_Data_Model> adapter = new ArrayAdapter<City_Data_Model>(Act_Edit_Profile_Detail.this, R.layout.my_spiner_item, list) {

                    public View getView(int position, View convertView, ViewGroup parent) {

                        View v = super.getView(position, convertView, parent);

                        ((TextView) v).setGravity(Gravity.CENTER);

                        return v;

                    }

                    @Override
                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View v = super.getDropDownView(position, convertView, parent);

                        ((TextView) v).setGravity(Gravity.CENTER);

                        return v;

                    }
                };

                adapter.setDropDownViewResource(R.layout.my_spiner_item);
                Sp_select_city.setAdapter(adapter);
                Sp_select_city.setSelection(0);
                for (int p = 0; p < list.size(); p++) {
                    if (city_id.equals(list.get(p).getCity_id())) {
                        Sp_select_city.setSelection(p);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        // city spinner select and show area if available
        Sp_select_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                City_Data_Model city_data_model = (City_Data_Model) adapterView.getItemAtPosition(i);
                city_id = city_data_model.getCity_id();
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString(getString(R.string.pref_spinner_city), Sp_select_city.getSelectedItem().toString());
                prefEditor.apply();

                ArrayList<Area_Data_Model> models = new ArrayList<>();
                if (area_data_models != null && area_data_models.size() > 0) {
                    for (int a = 0; a < area_data_models.size(); a++) {
                        if (city_id.equals(area_data_models.get(a).getCity_id())) {
                            models.add(area_data_models.get(a));
                        }
                    }
                }

                ArrayAdapter<Area_Data_Model> adapter = new ArrayAdapter<Area_Data_Model>(Act_Edit_Profile_Detail.this, R.layout.my_spiner_item, models) {

                    public View getView(int position, View convertView, ViewGroup parent) {

                        View v = super.getView(position, convertView, parent);

                        ((TextView) v).setGravity(Gravity.CENTER);

                        return v;

                    }

                    @Override
                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View v = super.getDropDownView(position, convertView, parent);

                        ((TextView) v).setGravity(Gravity.CENTER);

                        return v;

                    }
                };
                adapter.setDropDownViewResource(R.layout.my_spiner_item);
                Sp_select_area.setAdapter(adapter);

                for (int p = 0; p < models.size(); p++) {
                    if (area_id.equals(models.get(p).getArea_id())) {
                        Sp_select_area.setSelection(p);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // area spinner
        Sp_select_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Area_Data_Model area_data_model = (Area_Data_Model) adapterView.getItemAtPosition(i);
                area_id = area_data_model.getArea_id();
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString(getString(R.string.pref_area_id), area_data_models.get(i).getArea_id());
                prefEditor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }

        return hasImage;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // image select from gallery not use
        if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {

            Uri imageUri = data.getData();
            Log.e("Tag Image Data", "" + requestCode + " " + resultCode + " ");
            try {
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                imageView.setImageBitmap(bitmap);
//                txt_upload_photo.setVisibility(View.GONE);
                image_file = new File(picturePath);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Tag Error", e.toString());
            }

            Log.e("Tag data", "" + bitmap);
            Log.e("Tag data", "" + data.getData());
        }
        // image select from cammera not use
        else if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && data != null) {
            File destination;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HH mm ss", Locale.ENGLISH).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image_file = new File(destination.getAbsolutePath());
//                imageView.setImageBitmap(bitmap);
//                txt_upload_photo.setVisibility(View.GONE);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // image select from gallery fro user image not use
        else if (requestCode == PICK_IMAGE_GALLERY1 && resultCode == RESULT_OK && data != null) {
            //  getting the image Uri
            Uri imageUri = data.getData();
            try {
                //  getting bitmap object from uri
                bitmap2 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                //  displaying selected image to ImageView
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                if (bitmap2 != null) {
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    banner_image.setImageBitmap(bitmap2);
                }
                txt_upload_banner.setVisibility(View.GONE);
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                try {
                    if (picturePath != null) {
                        banner_file = new File(picturePath);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  calling the method uploadBitmap to upload image
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // image select from cammera fro user image not use
        else if (requestCode == PICK_IMAGE_CAMERA1 && resultCode == RESULT_OK && data != null) {
            File destination;
            try {
                Uri selectedImage = data.getData();
                bitmap2 = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                banner_file = new File(destination.getAbsolutePath());
                banner_image.setImageBitmap(bitmap2);
                txt_upload_banner.setVisibility(View.GONE);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // select location and indicate map focus this particular location
        else if (requestCode == LOCATION_PICKER && resultCode == RESULT_OK && data != null) {
            if (data.getStringExtra(MapUtility.ADDRESS) != null) {
                String addresse = data.getStringExtra(MapUtility.ADDRESS);
                double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);

                address = addresse;
                txt_current_location.setText(addresse);
                txt_current_location.setSelected(true);
                txt_current_location.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                txt_current_location.setMarqueeRepeatLimit(-1);
                latLng = new LatLng(selectedLatitude, selectedLongitude);
                d_latitude = latLng.latitude;
                d_longitude = latLng.longitude;
                scrollView.pageScroll(View.FOCUS_DOWN);
//                address = place.getAddress();
                Log.e("address", " " + address);
                Log.e("latLng2", " " + d_latitude + "------" + d_longitude);
                place_name = addresse;
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(getString(R.string.pref_address), place_name);
                editor.apply();
                mapFragment.getMapAsync(Act_Edit_Profile_Detail.this);
                //  lat = "" + selectedLatitude;
                //  log = "" + selectedLongitude;
                //  txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                Log.e("Tag Address", address);
                Log.e("TagLat Log", " " + selectedLatitude + "  " + selectedLongitude);
            }
        }
        // image pick for imaage picker banner
        else if (resultCode == RESULT_OK ){

                bitmap2 = BitmapFactory.decodeFile(ImagePicker.Companion.getFilePath(data));

            if (bitmap2 != null) {
                banner_image.setImageBitmap(bitmap2);
            }
            txt_upload_banner.setVisibility(View.GONE);
            banner_file = ImagePicker.Companion.getFile(data);
        }else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    ProgressDialog progress;

    // upload user workshop detail with banner image
    private void uploadBitmap(final Bitmap bitmap, final Bitmap bitmap2) {

        //  getting the tag from the editText
        //  country_id = e_country.getText().toString();
        //  address = userAddress.getText().toString();
        name = e_nm.getText().toString();
//        email_id = e_email.getText().toString();
        con_no = e_con_no.getText().toString();
        owner_nm = ed_owner_nm.getText().toString();
        facebook_link = ed_E_facebook_link.getText().toString();
        description = ed_description.getText().toString();
//        password = ed_password.getText().toString();
        if (ed_E_salesName.getText().toString().equals("")) {
            sales_id = "";
        }

        if (e_nm.getVisibility() == View.VISIBLE && TextUtils.isEmpty(name)) {
            e_nm.setError(getString(R.string.name));
        } /*else if (TextUtils.isEmpty(owner_nm)) {
            ed_owner_nm.setError(getString(R.string.name));
        }*/ /*else if (TextUtils.isEmpty(email_id)) {
            e_email.setError("enter email");
        } */ else if (TextUtils.isEmpty(city_id)) {
            Toast.makeText(getApplicationContext(), R.string.select_city, Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(address)) {
            Toast.makeText(getApplicationContext(), getString(R.string.select_current_location), Toast.LENGTH_LONG).show();
        } else if (d_latitude == 0.0 && d_longitude == 0.0) {
            Toast.makeText(getApplicationContext(), getString(R.string.address), Toast.LENGTH_LONG).show();
        } /* else if (TextUtils.isEmpty(description)) {
            ed_description.setError("Enter description");
            ed_description.requestFocus();
        } */ /*else if (TextUtils.isEmpty(password)) {
            ed_password.setError(getString(R.string.enter_password));
            ed_password.requestFocus();
        }*/ /*else if (TextUtils.isEmpty(con_no)) {
            e_con_no.setError(getString(R.string.enter_contatct));
            e_con_no.requestFocus();
        }*/ else if (ed_E_salesName.getVisibility() == View.VISIBLE && !ed_E_salesName.getText().toString().equals("") && !checkSalesMan(ed_E_salesName.getText().toString())) {
            ed_E_salesName.setError(getString(R.string.enter_valid_sales_name));
            ed_E_salesName.requestFocus();
        } /*else if (!hasImage(imageView)) {
            Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.upload_photo), Toast.LENGTH_SHORT).show();
        }*/ /*else if (!hasImage(banner_image)) {
            Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.upload_photo), Toast.LENGTH_SHORT).show();
        }*/ else if (!Helper.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(Act_Edit_Profile_Detail.this, R.string.internet_not_connect, Toast.LENGTH_SHORT).show();
        } else {

            progress = new ProgressDialog(Act_Edit_Profile_Detail.this);
            progress.setMessage(getString(R.string.please_wait_updating_profile));
            progress.setCancelable(false);
            progress.show();

            // our custom volley request
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "garage_profile_update",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            progress.dismiss();
                            Log.e("Tag Before Response", new String(response.data));
                            String res = Html.fromHtml(new String(response.data)).toString();
                            try {
                                res = res.substring(res.indexOf("{"), res.lastIndexOf("}") + 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.e("response", " " + res);
                            try {
                                JSONObject obj = new JSONObject(res);

                                ed_model.setMessage(obj.getString("message"));
                                Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();

                                if (obj.optString("status").equals("true")) {

                                    editModel = new ArrayList<>();

                                    JSONObject data_obj = obj.getJSONObject("result");

                                    ed_model.setName(check(data_obj, "garage_name"));
                                    ed_model.setEmail(check(data_obj, "email"));
                                    ed_model.setCountry_id(data_obj.getString("country_id"));
                                    ed_model.setContact_no(data_obj.getString("contact_no"));
                                    ed_model.setCity_id(data_obj.getString("city_id"));
                                    ed_model.setAddress(check(data_obj, "address"));
                                    ed_model.setLatitude(data_obj.getString("latitude"));
                                    ed_model.setLongitude(data_obj.getString("longitude"));
                                    ed_model.setStatus_d(obj.getString("status"));

                                    editModel.add(ed_model);

                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString(getString(R.string.pref_garage_name), ed_model.getName());
                                    editor.putString(getString(R.string.pref_user_image), "" + image_file);
                                    if (banner_file != null) {
                                        editor.putString(getString(R.string.pref_workshop_image), "" + banner_file);
                                    }
                                    editor.putString(getString(R.string.pref_sales_id), sales_id);
                                    editor.apply();

                                    Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.my_profile), Toast.LENGTH_SHORT).show();
                                    Log.e("ppp", "onResponse: " + data_obj);

                                    Intent intent = new Intent(Act_Edit_Profile_Detail.this, Navigation_Activity.class);
                                    intent.putExtra("SPLASH", true);
                                    startActivity(intent);
                                    finish();

                                }
                            } catch (JSONException e) {
                                Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progress.dismiss();
                            Log.e("volley_error", " " + error.getMessage());

                            Toast.makeText(Act_Edit_Profile_Detail.this, AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_SHORT).show();
//                            Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();

                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 */

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("garage_id", garage_id);
//                    params.put("email", email_id);
//                    params.put("password", password);
                    params.put("facebook_link", facebook_link);
                    params.put("description", description);
                    params.put("garage_name", name);
                    params.put("owner_name", owner_nm);
                    params.put("country_id", country_id);
                    params.put("contact_no", con_no);
                    params.put("city_id", city_id);
                    params.put("area_id", area_id);
                    params.put("address", address);
                    params.put("latitude", String.valueOf(d_latitude));
                    params.put("longitude", String.valueOf(d_longitude));
                    params.put("salesman_id", sales_id);
                    params.put(language, prefs.getString(getString(R.string.pref_language), "ar"));
                    Log.e("params", " " + params);

                    return params;
                }

                /*
                 * Here we are passing image by renaming it with a unique name
                 * */

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError {

                    Map<String, DataPart> params = new HashMap<>();
                    if (bitmap != null && bitmap2 != null) {
                        long image_name = System.currentTimeMillis();
//                        params.put("owner_profile_img", new DataPart(image_name + ".png", getFileDataFromDrawable(bitmap)));
                        params.put("garage_banner", new DataPart(image_name + ".png", getFileDataFromDrawable(bitmap2)));
                        return params;
                    } else if (bitmap != null) {
                        long image_name = System.currentTimeMillis();
                        params.put("owner_profile_img", new DataPart(image_name + ".png", getFileDataFromDrawable(bitmap)));
                        return params;
                    } else if (bitmap2 != null) {
                        long image_name = System.currentTimeMillis();
                        params.put("garage_banner", new DataPart(image_name + ".png", getFileDataFromDrawable(bitmap2)));
                        return params;
                    }
                    Log.e("params", " " + params);
                    return params;
                }
            };

            // adding the request to volley
            Volley.newRequestQueue(this).add(volleyMultipartRequest);
            volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        }
    }

    // upload car owner user detail
    private void uploadUserBitmap() {

        owner_nm = e_nm.getText().toString();
        /*if (TextUtils.isEmpty(city_id)) {
            Toast.makeText(getApplicationContext(), getString(R.string.select_city), Toast.LENGTH_LONG).show();
        } else */if (owner_nm.equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.name), Toast.LENGTH_LONG).show();
        }else if (!Helper.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(Act_Edit_Profile_Detail.this, R.string.internet_not_connect, Toast.LENGTH_SHORT).show();
        } else {

            progress = new ProgressDialog(Act_Edit_Profile_Detail.this);
            progress.setMessage(getString(R.string.please_wait_updating_profile));
            progress.setCancelable(false);
            progress.show();

            // our custom volley request
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "garage_profile_update",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            progress.dismiss();
                            Log.e("Tag Before Response", new String(response.data));
                            String res = Html.fromHtml(new String(response.data)).toString();
                            try {
                                res = res.substring(res.indexOf("{"), res.lastIndexOf("}") + 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.e("response", " " + res);
                            try {
                                JSONObject obj = new JSONObject(res);

                                ed_model.setMessage(obj.getString("message"));
                                Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();

                                if (obj.optString("status").equals("true")) {

                                    editModel = new ArrayList<>();

                                    JSONObject data_obj = obj.getJSONObject("result");

                                    ed_model.setName(check(data_obj, "garage_name").equals("") ? check(data_obj, "owner_name") : check(data_obj, "garage_name"));
                                    ed_model.setEmail(data_obj.getString("email"));
                                    ed_model.setCountry_id(data_obj.getString("country_id"));
                                    ed_model.setContact_no(data_obj.getString("contact_no"));
                                    ed_model.setCity_id(data_obj.getString("city_id"));
                                    ed_model.setAddress(check(data_obj, "address"));
                                    ed_model.setLatitude(data_obj.getString("latitude"));
                                    ed_model.setLongitude(data_obj.getString("longitude"));
                                    ed_model.setStatus_d(obj.getString("status"));
                                    String garage_banner = data_obj.getString("garage_banner");

                                    editModel.add(ed_model);

                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString(getString(R.string.pref_garage_name), ed_model.getName());
                                    editor.putString(getString(R.string.pref_user_image), "" + image_file);
                                    if (banner_file != null) {
                                        editor.putString(getString(R.string.pref_workshop_image), "" + banner_file);
                                    } else {
                                        editor.putString(getString(R.string.pref_workshop_image), garage_banner);
                                    }
                                    editor.putString(getString(R.string.pref_sales_id), sales_id);
                                    editor.apply();

                                    Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.my_profile), Toast.LENGTH_SHORT).show();
                                    Log.e("ppp", "onResponse: " + data_obj);

                                    Intent intent = new Intent(Act_Edit_Profile_Detail.this, Navigation_Activity.class);
                                    intent.putExtra("SPLASH", true);
                                    startActivity(intent);
                                    finish();

                                }
                            } catch (JSONException e) {
                                Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progress.dismiss();
                            Log.e("volley_error", " " + error.getMessage());

                            Toast.makeText(Act_Edit_Profile_Detail.this, AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_SHORT).show();
//                            Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();

                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 */

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("garage_id", garage_id);
                    params.put("country_id", country_id);
                    params.put("owner_name", owner_nm);
                    params.put("garage_name", owner_nm);
                    params.put("city_id", "");
                    params.put("area_id", "");
                    params.put("service_id", service_id);
                    params.put(language, prefs.getString(getString(R.string.pref_language), "ar"));
                    Log.e("params", " " + params);

                    return params;
                }

                /*
                 * Here we are passing image by renaming it with a unique name
                 * */

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError {
                    return super.getByteData();
                }
            };

            // adding the request to volley
            Volley.newRequestQueue(this).add(volleyMultipartRequest);
            volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        }
    }

    // check and get detail of login user detail and set defaulr value
    private void GarageStatus(final String Garage_id) {
        progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.loading));
        progress.show();
        RequestQueue queue = Volley.newRequestQueue(Act_Edit_Profile_Detail.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "check_garage_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("OnResponse", "onResponse: " + response);
                String garage_name = "";
                String email = "";
                String contact_no = "";
                String owner_name = "";
                String description = "";
                String facebook_link = "";
                String city = "";
                String address_ = "";
                String latitude = "";
                String longitude = "";
                String garage_banner = "";
                String owner_profile_img = "";
                String currency = "";
                String service_provider_role = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("true")) {
                        JSONArray jsonArray = object.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);
                            garage_name = check(object1, "garage_name");
                            email = check(object1, "email");
                            contact_no = object1.getString("contact_no");
                            owner_name = check(object1, "owner_name");
                            description = check(object1, "description");
                            facebook_link = check(object1, "facebook_link");
                            city_id = object1.optString("city_id", "").equals("null") ? city_id : object1.getString("city_id");
                            country_id = object1.getString("country_id");
                            area_id = object1.has("area_id") ? object1.getString("area_id") : "";
                            sales_id = object1.has("salesman_id") ? object1.getString("salesman_id") : "";
                            address_ = check(object1, "address");
                            latitude = object1.getString("latitude");
                            longitude = object1.getString("longitude");
                            garage_banner = object1.getString("garage_banner");
                            owner_profile_img = object1.getString("owner_profile_img");

                            JSONObject country_data = object1.optJSONObject("country_data");

                            Log.e("ppp_ppp", "latLng: " + garage_name + "- -" + owner_name + "- -" + facebook_link + "- -" + city + " " + owner_profile_img);
                            Log.e("ppp_ppp", "onResponse: " + response);

                        }

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(getString(R.string.pref_currency), currency);
                        editor.putString(getString(R.string.pref_user_image), owner_profile_img);
                        editor.putString(getString(R.string.name), garage_name);
                        editor.putString(getString(R.string.pref_provider_role), service_provider_role);
                        editor.apply();

                        if (!garage_name.equals("null") && Constant.i == 1) {
                            e_nm.setText(garage_name);
                            e_nm.setSelection(garage_name.length());
                        }
                        if (!email.equals("null"))
//                            e_email.setText(email);
                            if (!contact_no.equals("null"))
                                e_con_no.setText(contact_no);
                        if (!owner_name.equals("null")) {
                            owner_nm = owner_name;
                            ed_owner_nm.setText(owner_name);
                        }

                        if (!description.equals("null"))
                            ed_description.setText(description);
                        if (!facebook_link.equals("null"))
                            ed_E_facebook_link.setText(facebook_link);
                        if (city_id.equals("null")) {
                            city_id = "";
                        }

                        if (address_ != null) {
                            if (!address_.equals("null")) {
//                                autocompleteFragment.setText(address_);
                                address = address_;
                            }
                        }

                        if (!banner_image.toString().equals("null")) {
                            Glide.with(getApplicationContext()).load(garage_banner).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    Log.e("Tag Error", e.toString());
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    banner_image.setImageDrawable(resource);
                                    return false;
                                }
                            }).into(banner_image);
                            txt_upload_banner.setVisibility(View.GONE);
                        } else {
//                            banner_image.setImageDrawable(null);
                            txt_upload_banner.setVisibility(View.VISIBLE);
                        }

                        if (service_provider_role.equalsIgnoreCase("Tow Truck")) {

                            txt_E_nm.setVisibility(View.GONE);
                            e_nm.setVisibility(View.GONE);
                            txt_E_address.setVisibility(View.GONE);
                            txt_E_facebook_link.setVisibility(View.GONE);
                            ed_E_facebook_link.setVisibility(View.GONE);
//                            autocompleteFragment.getView().setVisibility(View.GONE);
                            mapFragment.getView().setVisibility(View.GONE);

                        } else {

                            if (!service_id.equals("61")) {

                                if (latitude != null && longitude != null && !latitude.equals("null") && !longitude.equals("null")) {
                                    latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                }
                                if (!address.equals("")) {
                                    txt_current_location.setText(address);
                                }

                                d_longitude = latLng.longitude;
                                d_latitude = latLng.latitude;
                                Log.e("ppp_ppp", "LatLng: " + place_name + " -- " + address + " -- " + latLng + " -- " + d_latitude + "  " + d_longitude);
                                mapFragment.getMapAsync(Act_Edit_Profile_Detail.this);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progress.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ppp_ppp", "onErrorResponse: " + error);
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("garage_id", Garage_id);
                map.put(language, prefs.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag params", map.toString());
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("My Current Location", "No Address Returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My Current Location", "Cannot Get Address!");
        }
        return strAdd;
    }

    public byte[] getFileDataFromDrawable(@NotNull Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return p1;
    }

    // get country city area data and set spinner
    private void retrieveJSON2() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Tag Country City", response);
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.optString("status").equals("true")) {

                                country_data_models = new ArrayList<>();
                                JSONObject object = obj.getJSONObject("result");

                                JSONArray dataArray = object.getJSONArray("country");
                                for (int i = 0; i < dataArray.length(); i++) {

                                    Country_Data_model country_data_model = new Country_Data_model();
                                    JSONObject jsonObject = dataArray.getJSONObject(i);
                                    country_data_model.setCon_id(jsonObject.getString("country_id"));
                                    country_data_model.setCon_code(jsonObject.getString("country_code"));
                                    country_data_model.setCon_name(check(jsonObject, "country_name"));
                                    country_data_model.setCon_currency(jsonObject.getString("currency"));
                                    country_data_model.setCon_language(jsonObject.getString("language"));
                                    country_data_model.setCon_ph_code(jsonObject.getString("phone_code"));
                                    country_data_models.add(country_data_model);

                                }

                                city_data_models = new ArrayList<>();
                                JSONArray array = object.optJSONArray("city");
                                if (array == null)
                                    array = new JSONArray();
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    City_Data_Model city_data_model = new City_Data_Model();

                                    city_data_model.setCity_id(object1.getString("city_id"));
                                    city_data_model.setCity_name(check(object1, "city_name"));
                                    city_data_model.setCon_id(object1.getString("country_id"));
                                    city_data_models.add(city_data_model);
                                }

                                area_data_models = new ArrayList<>();
                                if (object.has("area")) {
                                    JSONArray jsonArray = object.getJSONArray("area");
                                    if (jsonArray == null)
                                        jsonArray = new JSONArray();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        Area_Data_Model model = new Area_Data_Model();
                                        model.setArea_id(jsonObject.getString("area_id"));
                                        model.setCity_id(jsonObject.getString("city_id"));
                                        model.setCountry_id(jsonObject.getString("country_id"));
                                        model.setArea_name(check(jsonObject, "area"));
                                        area_data_models.add(model);

                                    }
                                }

                                for (int i = 0; i < country_data_models.size(); i++) {
                                    names.add(country_data_models.get(i).getCon_name());
                                }
                                String country = prefs.getString(getString(R.string.pref_spinner_country), " ");

                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Edit_Profile_Detail.this, R.layout.my_spiner_item, names) {

                                    public View getView(int position, View convertView, ViewGroup parent) {

                                        View v = super.getView(position, convertView, parent);

                                        ((TextView) v).setGravity(Gravity.CENTER);

                                        return v;

                                    }

                                    @Override
                                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView, parent);

                                        ((TextView) v).setGravity(Gravity.CENTER);

                                        return v;

                                    }
                                };
                                ;
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.my_spiner_item); // The drop down view
                                spinner_country.setAdapter(spinnerArrayAdapter);

                                for (int i = 0; i < country_data_models.size(); i++) {
                                    if (country.equals(spinner_country.getItemAtPosition(i).toString())) {
                                        spinner_country.setSelection(i);
                                        break;
                                    }
                                    if (country_id.equals(country_data_models.get(i).getCon_id())) {
                                        spinner_country.setSelection(i);
                                    }
                                }

                                String city = prefs.getString(getString(R.string.pref_spinner_city), " ");
                                ArrayList<City_Data_Model> list = new ArrayList<>();
                                if (city_data_models != null && city_data_models.size() > 0) {
                                    for (int j = 0; j < city_data_models.size(); j++) {
                                        if (city_data_models.get(j).getCon_id().equals(country_id)) {
                                            list.add(city_data_models.get(j));
                                        }
                                    }
                                }

                                ArrayAdapter<City_Data_Model> spinnerArrayAdapterCity = new ArrayAdapter<City_Data_Model>(Act_Edit_Profile_Detail.this, R.layout.my_spiner_item, list) {

                                    public View getView(int position, View convertView, ViewGroup parent) {

                                        View v = super.getView(position, convertView, parent);

                                        ((TextView) v).setGravity(Gravity.CENTER);

                                        return v;

                                    }

                                    @Override
                                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView, parent);

                                        ((TextView) v).setGravity(Gravity.CENTER);

                                        return v;

                                    }
                                };
                                ;
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.my_spiner_item); // The drop down view
                                Sp_select_city.setAdapter(spinnerArrayAdapterCity);
                                Sp_select_city.setSelection(0);
                                for (int p = 0; p < list.size(); p++) {
                                    if (city_id.equals(list.get(p).getCity_id())) {
                                        Sp_select_city.setSelection(p);
                                    }
                                }

                                sales_models = new ArrayList<>();
                                if (object.has("salesman")) {
                                    JSONArray sales_team = object.getJSONArray("salesman");
                                    for (int i = 0; i < sales_team.length(); i++) {
                                        JSONObject object1 = sales_team.getJSONObject(i);
                                        Sales_Person_Model model = new Sales_Person_Model();
                                        model.setId(object1.getString("salesman_id"));
                                        model.setPerson_name(check(object1, "name"));
                                        model.setPerson_code(object1.getString("code"));
                                        model.setPerson_email(object1.getString("email"));
                                        model.setPerson_mobile(object1.getString("contact_no"));
                                        sales_models.add(model);
                                    }
                                }

                                if (sales_models != null && sales_models.size() > 0) {
                                    for (int i = 0; i < sales_models.size(); i++) {
                                        if (sales_id.equals(sales_models.get(i).getId())) {
                                            ed_E_salesName.setText(sales_models.get(i).getPerson_name());
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(language, prefs.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        // request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    // check salesman code validation
    private boolean checkSalesMan(String name) {

        boolean isName = false;
        if (sales_models != null) {

            for (int i = 0; i < sales_models.size(); i++) {
                Log.e("tag sale man", sales_models.get(i).getPerson_name() + "   " + name.toLowerCase());
                if (name.toLowerCase().equals(sales_models.get(i).getPerson_name().toLowerCase())) {
                    isName = true;
                    sales_id = sales_models.get(i).getId();
                    break;
                }
            }
        }
        return isName;
    }

    Marker marker;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        Log.e("Tag Map", "onMapReady: " + latLng + "----" + place_name);
//        rel_main.fullScroll(ScrollView.FOCUS_DOWN);
        /*if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            //   here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //   int[] grantResults)
            //   to handle the case where the user grants the permission. See the documentation
            //   for ActivityCompat#requestPermissions for more details.
            return;
        }
        ((WorkaroundMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapUser)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                    @Override
                    public void onTouch()
                    {
                        rel_main.requestDisallowInterceptTouchEvent(true);
                    }
                });*/
        if (marker != null) {
            marker.remove();
        }

        if (latLng != null) {
            Log.e("Tag Map", "" + latLng);
            marker = mMap.addMarker(new MarkerOptions().position(latLng).title(place_name));
            //mMap.addMarker(new MarkerOptions().position(latLng).title(place_name));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.moveCamera(location);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            //    here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            //  to handle the case where the user grants the permission. See the documentation
            //  for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            // mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));

        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                location.getLatitude() + "," +
                location.getLongitude();
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        // it was pre written
        final WorkaroundMapFragment mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser));
        mapFragment.getMapAsync(Act_Edit_Profile_Detail.this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                //  rel_main.requestDisallowInterceptTouchEvent(true);
            }
        });
    }

    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 20000; /* 20 sec */

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //  TODO: Consider calling
            //    ActivityCompat#requestPermissions
            //  here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            //  to handle the case where the user grants the permission. See the documentation
            //  for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        Log.d("request", "--->>>>");
    }

    private void selectImage(int def) {

        if (def == 1) {

            try {
//                PackageManager pm = getPackageManager();
//                int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                if (PermissionUtils.hasPermission(this, Manifest.permission.CAMERA)) {
                    final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.chose_from_gallery), getString(R.string.cancle)};
                    AlertDialog.Builder builder = new AlertDialog.Builder(Act_Edit_Profile_Detail.this);
                    builder.setTitle(getString(R.string.select_option));
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals(getString(R.string.take_photo))) {
                                dialog.dismiss();
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);
                            } else if (options[item].equals(getString(R.string.chose_from_gallery))) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                            } else if (options[item].equals(getString(R.string.cancle))) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                    PermissionUtils.requestPermission(this, new String[]{Manifest.permission.CAMERA}, 0);
                }

            } catch (Exception e) {
                Toast.makeText(this, getString(R.string.service_permisions_are_required_for_this_app), Toast.LENGTH_SHORT).show();
                PermissionUtils.requestPermission(this, new String[]{Manifest.permission.CAMERA}, 0);
                e.printStackTrace();
            }
        } else {
            try {
//                PackageManager pm = getPackageManager();
//                int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                if (PermissionUtils.hasPermission(this, Manifest.permission.CAMERA)) {
                    final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(Act_Edit_Profile_Detail.this);
                    builder.setTitle("Select Option");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {
                                dialog.dismiss();
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA1);
                            } else if (options[item].equals("Choose From Gallery")) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY1);
                            } else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                    PermissionUtils.requestPermission(this, new String[]{Manifest.permission.CAMERA}, 0);
                }
            } catch (Exception e) {
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                PermissionUtils.requestPermission(this, new String[]{Manifest.permission.CAMERA}, 0);
                e.printStackTrace();
            }
        }
    }


    // hide car owner as regireter
    public void hideUserField() {
        txt_area.setText(getText(R.string.closest_area));
        txt_banner.setVisibility(View.GONE);
        lin_banner.setVisibility(View.GONE);
        txt_dec.setVisibility(View.GONE);
        ed_description.setVisibility(View.GONE);
        txt_E_facebook_link.setVisibility(View.GONE);
        ed_E_facebook_link.setVisibility(View.GONE);
        mapFragment.getView().setVisibility(View.GONE);
//        mapFragment.setUserVisibleHint(false);
        txt_current_location.setVisibility(View.GONE);
        txt_E_address.setVisibility(View.GONE);
    }
}

