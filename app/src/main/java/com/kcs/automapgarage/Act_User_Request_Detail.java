package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Garage_Facility;
import com.kcs.automapgarage.Model.RejectReason;
import com.kcs.automapgarage.Model.Request_Data_Model;
import com.kcs.automapgarage.adapter.Adapter_Customer_Request;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_User_Request_Detail extends AppCompatActivity implements View.OnClickListener {

    TextView txt_title, txt_name, txt_email, txt_cnt, txt_address, txt_map,
            txt_brand_name, txt_model, txt_model_year, txt_fuel, txt_date, txt_time;
    LinearLayout lin_dynamic_txt, lin_accept_reject, lin_appointment_date,
            lin_appointment_time, lin_agree, lin_work, line_bottom, lin_call;
    Button btn_accept, btn_reject;
    Button btn_done, btn_no, btn_yes;
//    List<RejectReason> rejectReasons;
    ImageView back;
    CardView card_user_info, card_user_service;

    LinearLayout lin_address, lin_cnt, lin_email;

    LinearLayout lin_problem, lin_service, lin_oil_brand, lin_oil_sae, lin_tire_brand, lin_tire_size_no, lin_battery_brand;
    TextView txt_problem, txt_service, txt_oil_brand, txt_oil_sae, txt_tire_brand, txt_tire_size_no, txt_battery_brand;

    HorizontalScrollView image_scroll;
    LinearLayout lin_add_img;
    SharedPreferences preferences;

    String language = "ar";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__user__request__detail);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        language = preferences.getString(getString(R.string.pref_language),"ar");

//        getRejectionReasonList();
        txt_title = findViewById(R.id.txt_title);
        txt_name = findViewById(R.id.txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_cnt = findViewById(R.id.txt_cnt);
        txt_address = findViewById(R.id.txt_address);
        txt_brand_name = findViewById(R.id.txt_brand_name);
        txt_model = findViewById(R.id.txt_model);
        txt_model_year = findViewById(R.id.txt_model_year);
        txt_fuel = findViewById(R.id.txt_fuel);
        lin_dynamic_txt = findViewById(R.id.lin_dynamic_txt);
        lin_accept_reject = findViewById(R.id.lin_accept_reject);
        lin_appointment_time = findViewById(R.id.lin_appointment_time);
        txt_time = findViewById(R.id.txt_time);
        btn_accept = findViewById(R.id.btn_accept);
        btn_accept.setText(getString(R.string.i_would_like_to_participate));
        btn_reject = findViewById(R.id.btn_reject);
        btn_reject.setText(getString(R.string.ignor));
        btn_done = findViewById(R.id.btn_done);
        btn_done.setText(getString(R.string.done));
        txt_map = findViewById(R.id.txt_map);
        lin_appointment_date = findViewById(R.id.lin_appointment_date);
        txt_date = findViewById(R.id.txt_date);
//        reason_dynamic = findViewById(R.id.reason_dynamic);
        lin_agree = findViewById(R.id.lin_agree);
        line_bottom = findViewById(R.id.line_bottom);
        btn_no = findViewById(R.id.btn_no);
        btn_no.setText(getString(R.string.nos));
        btn_yes = findViewById(R.id.btn_yes);
        btn_yes.setText(getString(R.string.yess));
        lin_work = findViewById(R.id.lin_work);
        back = findViewById(R.id.back);
        lin_email = findViewById(R.id.lin_email);
        lin_address = findViewById(R.id.lin_address);
        lin_cnt = findViewById(R.id.lin_cnt);
        lin_call = findViewById(R.id.lin_call);
        card_user_info = findViewById(R.id.card_user_info);
        card_user_service = findViewById(R.id.card_user_service);

        TextView txt_call_now = findViewById(R.id.txt_call_now);
        txt_call_now.setText(getString(R.string.call_now));

        lin_problem = findViewById(R.id.lin_problem);
        txt_problem = findViewById(R.id.txt_problem);

        lin_oil_brand = findViewById(R.id.lin_oil_brand);
        txt_oil_brand = findViewById(R.id.txt_oil_brand);

        lin_oil_sae = findViewById(R.id.lin_oil_sae);
        txt_oil_sae = findViewById(R.id.txt_oil_sae);

        lin_battery_brand = findViewById(R.id.lin_battery_brand);
        txt_battery_brand = findViewById(R.id.txt_battery_brand);

        lin_tire_brand = findViewById(R.id.lin_tire_brand);
        txt_tire_brand = findViewById(R.id.txt_tire_brand);

        lin_tire_size_no = findViewById(R.id.lin_tire_size_no);
        txt_tire_size_no = findViewById(R.id.txt_tire_size_no);

        lin_service = findViewById(R.id.lin_services);
        txt_service = findViewById(R.id.txt_services);

        image_scroll = findViewById(R.id.image_scroll);
        lin_add_img = findViewById(R.id.lin_add_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        lin_call.setOnClickListener(this);

        @SuppressLint("SimpleDateFormat") final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm aa", Locale.ENGLISH);
        //    final Date[] dateObj = new Date[1];
        //    Log.e("TAG","data"+Constant.notification_model.getStatus());

        if (Constant.request_data == null) {
            onBackPressed();
        }

        txt_title.setText(Constant.request_data.getName());
        txt_name.setText(Constant.request_data.getName());
        txt_email.setText(Constant.request_data.getEmail());
        txt_cnt.setText(Constant.request_data.getContact_no());
        txt_address.setText(Constant.request_data.getAddress());
        txt_model.setText((Constant.request_data.getModel_name()));
        txt_fuel.setText((Constant.request_data.getFuel_name()));
        txt_model_year.setText((Constant.request_data.getModel_year()));
        txt_brand_name.setText((Constant.request_data.getBrand()));

        Log.e("TAG TIME", Constant.request_data.getAppointments_time());

        setConstantView();

        btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn_reject.getText().equals(getString(R.string.make_complain))) {
//                    Toast.makeText(getApplicationContext(), "Complain for this Appointment", Toast.LENGTH_LONG).show();
                    ComplainDialog(Constant.request_data);
                } else {
                    RejectAcceptRequest(Constant.request_data.getUser_id(), "2", Constant.request_data.getGarage_appointment_id(), "", "");

                }
            }
        });

        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                if (btn_accept.getText().equals(getString(R.string.i_would_like_to_participate)))
                //                else if (btn_accept.setVisibility();)
                AcceptDialog(Constant.request_data.getUser_id(), Constant.request_data.getGarage_appointment_id());
            }
        });

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // status = 8 karo

                if (Constant.request_data.getStatus().equals("6")) {
                    CancelOrderApi("8", null);
                    lin_agree.setVisibility(View.GONE);
                    lin_work.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_wiat_customer_response), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // status = 9 karo
                if (Constant.request_data.getStatus().equals("6")) {
//                    NotAgreeDialog();
                    CancelOrderApi("9",null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_wiat_customer_response), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constant.request_data.getStatus().equals("11")) {
                    CancelOrderApi("10", null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_wait_customer_check_service), Toast.LENGTH_LONG).show();
                }
            }
        });

        txt_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Act_Map.class);
                i.putExtra("latitude", Constant.request_data.getLatitude());
                i.putExtra("longitude", Constant.request_data.getLongitude());
                i.putExtra("user_name", Constant.request_data.getName());
                i.putExtra(getString(R.string.contact_no), Constant.request_data.getContact_no());
                startActivity(i);
            }
        });

        InitializeBroadcast();

    }

    @SuppressLint("SetTextI18n")
    public void setConstantView() {

        if (Constant.request_data.getProblem().equals("")) {
            lin_problem.setVisibility(View.GONE);
        } else {
            lin_problem.setVisibility(View.VISIBLE);
            txt_problem.setText(Constant.getFormattedDateWithTime(Constant.request_data.getProblem()));
        }

        if (Constant.request_data.getAppointment_date().equals("")) {
            lin_appointment_date.setVisibility(View.GONE);
        } else {
            lin_appointment_date.setVisibility(View.VISIBLE);
            txt_date.setText(AppUtils.DateFormat(Constant.request_data.getAppointment_date()));
        }

        if (Constant.request_data.getAppointments_time().equals("")) {
            lin_appointment_time.setVisibility(View.GONE);
        } else {
            lin_appointment_time.setVisibility(View.VISIBLE);
            txt_time.setText("" + AppUtils.TimeFormat(Constant.request_data.getAppointments_time()));
        }

        if (Constant.request_data.getOil_sae().equals("")) {
            lin_oil_sae.setVisibility(View.GONE);
        } else {
            lin_oil_sae.setVisibility(View.VISIBLE);
            txt_oil_sae.setText(Constant.request_data.getOil_sae());
        }

        if (Constant.request_data.getOil_brand().equals("")) {
            lin_oil_brand.setVisibility(View.GONE);
        } else {
            lin_oil_brand.setVisibility(View.VISIBLE);
            txt_oil_brand.setText(Constant.request_data.getOil_brand());
        }

        if (Constant.request_data.getTire_size_no().equals("")) {
            lin_tire_size_no.setVisibility(View.GONE);
        } else {
            lin_tire_size_no.setVisibility(View.VISIBLE);
            txt_tire_size_no.setText(Constant.request_data.getTire_size_no());
        }

        if (Constant.request_data.getTire_brand().equals("")) {
            lin_tire_brand.setVisibility(View.GONE);
        } else {
            lin_tire_brand.setVisibility(View.VISIBLE);
            txt_tire_brand.setText(Constant.request_data.getTire_brand());
        }

        if (Constant.request_data.getBattery_brand().equals("")) {
            lin_battery_brand.setVisibility(View.GONE);
        } else {
            lin_battery_brand.setVisibility(View.VISIBLE);
            txt_battery_brand.setText(Constant.request_data.getBattery_brand());
        }

        if (Constant.request_data.getAppointment_type().size() > 0) {
            lin_service.setVisibility(View.VISIBLE);
            txt_service.setText(Constant.request_data.getAppointment_type().toString().replaceAll("\\[|\\]", ""));
        } else {
            lin_service.setVisibility(View.GONE);
        }

        if (Constant.request_data.getImage().size() > 0) {

            lin_add_img.removeAllViews();

            image_scroll.setVisibility(View.VISIBLE);
            for (int i = 0; i < Constant.request_data.getImage().size(); i++) {

                View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.appointment_image, null);
                final ImageView img = view.findViewById(R.id.appoint_img);

                Glide.with(this)
                        .asBitmap()
                        .load(Constant.request_data.getImage().get(i))
                        .thumbnail(0.01f)
                        .into(new BitmapImageViewTarget(img) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                img.setImageBitmap(resource);
                            }
                        });
                lin_add_img.addView(view);
            }
        } else {
            image_scroll.setVisibility(View.GONE);
        }

        lin_dynamic_txt.removeAllViews();
        for (int i = 0; i < Constant.request_data.getGarageFacilityList().size(); i++) {

            TextView textView = new TextView(this);
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText("( " + (i + 1) + " )  " + Constant.request_data.getGarageFacilityList().get(i).getFacility_name());
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTextSize(14);
//            textView.setPadding(5,10,5,5);

            lin_dynamic_txt.addView(textView);

            Log.e("TAG", "onCreate: " + Constant.request_data.getGarageFacilityList().get(i).getFacility_name());
        }
        // status 0 new appointment request
        Log.e("TAG", "data " + Constant.request_data.getStatus());
        if (Constant.request_data.getStatus().equals("0")) {
            line_bottom.setVisibility(View.VISIBLE);
            txt_map.setVisibility(View.GONE);
            lin_cnt.setVisibility(View.GONE);
            lin_email.setVisibility(View.GONE);
            lin_address.setVisibility(View.GONE);
            lin_call.setVisibility(View.GONE);
            card_user_info.setVisibility(View.GONE);
        }
        // status 1 participate workshop appointment request
        else if (Constant.request_data.getStatus().equals("1")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            btn_accept.setText(R.string.you_are_participated_with_this_workshop);
            lin_call.setVisibility(View.GONE);
            txt_map.setVisibility(View.GONE);
            card_user_info.setVisibility(View.GONE);
        }
        // status 2 ignore workshop appointment request
        else if (Constant.request_data.getState().equals("2")) {
            line_bottom.setVisibility(View.GONE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.GONE);
            txt_map.setVisibility(View.GONE);
            lin_call.setVisibility(View.GONE);
            card_user_info.setVisibility(View.GONE);
        }
        // status 3 customer confirmed  appointment request
        else if (Constant.request_data.getStatus().equals("3")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            btn_accept.setText(R.string.customer_conformed_your_Offer_congratulation_you_have_chosen);
            txt_map.setVisibility(View.VISIBLE);
            lin_call.setVisibility(View.VISIBLE);
            card_user_info.setVisibility(View.VISIBLE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            Date appointmentDate = null;
            Date currentDate = null;

            try {
                appointmentDate = format.parse(Constant.request_data.getAppointment_date() + " " + Constant.request_data.getAppointments_time());
                currentDate = format.parse(format.format(new Date()));
                Log.e("TAG TIME", "" + format.format(currentDate));
                Log.e("TAG TIME", "" + format.format(appointmentDate));

            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("TAG TIME", e.toString());
            }

            // time over to show complain
            Log.e("TAG TIME", "" + Constant.request_data.getAppointment_date() + " " + Constant.request_data.getAppointments_time());
            if (currentDate.before(appointmentDate)) {
                long diff = appointmentDate.getTime() - currentDate.getTime();
                Log.e("days", "" + diff);
            } else {
                long diff = currentDate.getTime() - appointmentDate.getTime();
                Log.e("days", "" + diff);
                if (diff > 0) {
                    long second = diff / 1000;
                    long minute = second / 60;
                    long hour = minute / 60;
                    long days = (hour / 24) + 1;
                    Log.e("hour", "" + hour);
                    if (hour > 24) {
                        btn_reject.setText(getString(R.string.make_complain));
                        btn_reject.setVisibility(View.VISIBLE);
                        btn_accept.setVisibility(View.GONE);
//                        txt_map.setVisibility(View.GONE);
                    }
                }
            }
        }
        // status 4 customer cancel appointment request
        else if (Constant.request_data.getStatus().equals("4")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.VISIBLE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            txt_map.setVisibility(View.GONE);
            btn_reject.setText(getString(R.string.make_complain));
            btn_accept.setText(R.string.customer_cancel_your_order);
        }
        // status 5 customer cancel appointment request
        else if (Constant.request_data.getStatus().equals("5")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            txt_map.setVisibility(View.GONE);
            btn_accept.setText(R.string.customer_cancel_your_order);
        }
        // status 6 customer agree with workshop appointment request
        else if (Constant.request_data.getStatus().equals("6")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            lin_work.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            lin_agree.setVisibility(View.VISIBLE);
            txt_map.setVisibility(View.GONE);
            btn_accept.setText(R.string.user_agree_with_your_workshop);
            lin_call.setVisibility(View.VISIBLE);
        }
        // status 7 customer not agree with workshop appointment request
        else if (Constant.request_data.getStatus().equals("7")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            btn_accept.setText(R.string.user_not_agree_with_your_workshop);
            txt_map.setVisibility(View.GONE);
        }
        // status 8 customer and workshop agree appointment request
        else if (Constant.request_data.getStatus().equals("8")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            btn_accept.setText(R.string.user_and_you_are_agree_this_service);
            lin_call.setVisibility(View.VISIBLE);
            txt_map.setVisibility(View.VISIBLE);
        }
        // status 9 customer agree but workshop not agree appointment request
        else if (Constant.request_data.getStatus().equals("9")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            btn_accept.setText(R.string.you_are_not_agree_this_service);
            lin_call.setVisibility(View.VISIBLE);
            txt_map.setVisibility(View.GONE);
        }
        // status 10 customer done work appointment request
        else if (Constant.request_data.getStatus().equals("10")) {
            line_bottom.setVisibility(View.GONE);
            RattingDialog(Constant.request_data);
        }
        // status 11 customer rate for worshop appointment request
        else if (Constant.request_data.getStatus().equals("11")) {
            line_bottom.setVisibility(View.GONE);
            RattingDialog(Constant.request_data);
        }
        // status 12 work done and rate appointment request
        else if (Constant.request_data.getStatus().equals("12")) {
            line_bottom.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            btn_accept.setVisibility(View.VISIBLE);
            btn_accept.setEnabled(false);
            btn_accept.setText(R.string.your_service_is_complete_thank_you);
            txt_map.setVisibility(View.GONE);
            lin_call.setVisibility(View.GONE);
        } else {
//            line_bottom.setVisibility(View.GONE);
            lin_call.setVisibility(View.GONE);
        }

        if (Constant.request_data.getLatitude().equals("") || Constant.request_data.getLatitude().equals("null") ||
                Constant.request_data.getLongitude().equals("") || Constant.request_data.getLongitude().equals("null")) {
            txt_map.setVisibility(View.GONE);
        }
    }

    // get customer request data with status of request and clear badge count
    private void getCustomerRequest() {
        final ProgressDialog dialog = new ProgressDialog(Act_User_Request_Detail.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "garage_appointment_list",
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                        Log.e("Appointment List", " " + response);
                        Log.e("Appointment Url", Constant.URL + "garage_appointment_list");
                        dialog.dismiss();

                        try {
                            Log.e("notification_list", " " + response);
                            JSONObject mainJsonObject = new JSONObject(response);
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length() > 0) {

                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);
                                    if (Constant.request_data.getGarage_appointment_detail_id().equals(jsonObject.getString("garage_appointment_detail_id"))) {

                                        String garage_appointment_id = jsonObject.getString("garage_appointment_id");
                                        String garage_appointment_detail_id = jsonObject.getString("garage_appointment_detail_id");
                                        String user_id = jsonObject.getString("user_id");
                                        String garage_ids = jsonObject.getString("garage_id");
                                        String name = check(jsonObject,"name");
                                        String status = jsonObject.getString("status");
                                        String email = jsonObject.getString("email");
                                        String address = check(jsonObject,"address");
                                        String pinCode = jsonObject.getString("pincode");
                                        String garage_user_image = jsonObject.getString("user_image");
                                        String contact_no = jsonObject.getString("contact_no");
                                        String latitude = jsonObject.getString("latitude");
                                        String longitude = jsonObject.getString("longitude");
                                        String date = jsonObject.getString("created_at");
                                        String model_name = check(jsonObject,"model_name");
                                        String rims_brand = check(jsonObject,"rims_brand");
                                        String model_year = jsonObject.getString("model_year");
                                        String rims_size = check(jsonObject,"rims_size");
                                        String brand_name = check(jsonObject,"brand_name");
                                        String fuel_type = check(jsonObject,"fule_type");
                                        String car_problem = check(jsonObject,"car_problem");
                                        String appointment_date = jsonObject.getString("appointment_date");
                                        String appointment_time = jsonObject.getString("appointment_time");

                                        ArrayList<String> appointment_type = new ArrayList<>();
                                        if (jsonObject.has("appointment_type")) {

                                            try {
                                                JSONArray jsontype = jsonObject.getJSONArray("appointment_type");
                                                for (int p = 0; p < jsontype.length(); p++) {
                                                    JSONObject object = jsontype.getJSONObject(p);
                                                    if (!object.getString("garage_appointment_type").trim().equals(""))
                                                        appointment_type.add(check(jsonObject,"garage_appointment_type"));
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        String tire_size_no = check(jsonObject,"tire_size_number");
                                        String tire_brand = check(jsonObject,"tire_brand");
                                        String oil_brand = check(jsonObject,"oil_brand");
                                        String oil_sae = check(jsonObject,"oil_sae");
                                        String battery_brand = check(jsonObject,"battery_brand");

                                        if (appointment_date.equals("null") || appointment_time.equals("")) {
                                            appointment_date = "00-00-0000";
                                            appointment_time = "00:00";
                                        }

                                        List<Garage_Facility> garage_facilities = new ArrayList<>();
                                        if (jsonObject.has("garage_facility")) {
                                            JSONArray jsonArray = jsonObject.getJSONArray("garage_facility");
                                            for (int j = 0; j < jsonArray.length(); j++) {
                                                JSONObject object = jsonArray.getJSONObject(j);
                                                String facility_id = object.getString("facility_id");
                                                String facility_name = check(object,"facility_name");
                                                String statu = object.getString("status");
                                                String created_at = object.getString("created_at");
                                                garage_facilities.add(new Garage_Facility(facility_id, facility_name, statu, created_at, null));
                                            }
                                        }

                                        ArrayList<String> appointment_image = new ArrayList<>();
                                        if (jsonObject.has("appointmentImage")) {
                                            JSONArray image = jsonObject.getJSONArray("appointmentImage");
                                            for (int j = 0; j < image.length(); j++) {
                                                JSONObject object = image.getJSONObject(j);
                                                appointment_image.add(object.getString("appointment_image"));
                                            }
                                        }

                                        Request_Data_Model request_data_model = new Request_Data_Model(garage_appointment_id, garage_appointment_detail_id, user_id,
                                                garage_ids, name, email, contact_no, "", "", address, pinCode, garage_user_image, latitude,
                                                longitude, status, date, model_name, brand_name, model_year, "", fuel_type, car_problem,
                                                appointment_date, appointment_time, garage_facilities, appointment_image, oil_brand, tire_brand,
                                                battery_brand, tire_size_no, oil_sae, rims_brand, rims_size, appointment_type, "");

                                        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putInt(getString(R.string.pref_badge_count), 0);
                                        editor.apply();
                                        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0) +
                                                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0));
                                        if (tot == 0) {
                                            ShortcutBadger.removeCount(getApplicationContext());
                                        } else {
                                            ShortcutBadger.applyCount(getApplicationContext(), tot);
                                        }

                                        Constant.request_data = request_data_model;
                                        setConstantView();
                                        break;
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("size == > Error", e.toString());
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Log.e("size ==> Error", error.toString());
//                          Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
//                            error_dialog();
                    }
                }) {

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("garage_id", Constant.request_data.getGarage_id());
                params.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_User_Request_Detail.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // new message to refress
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
//            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
//            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
//            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
//            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (appointment != 0 && !Act_User_Request_Detail.this.isFinishing()) {
                getCustomerRequest();
            }
        }
    };

    private void getRejectionReasonList() {
        final ProgressDialog dialog = new ProgressDialog(getApplicationContext());
        dialog.setMessage(getString(R.string.loading));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_rejection_reason",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("notification_list", " " + response);
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                        dialog.dismiss();
//                        rejectReasons = new ArrayList<>();
                        JSONObject mainJsonObject = null;
                        try {
                            mainJsonObject = new JSONObject(response);
                            JSONArray result = mainJsonObject.getJSONArray("result");

                            for (int i = 0; i < result.length(); i++) {
                                JSONObject object = result.getJSONObject(i);
                                String id = object.getString("rejection_reson_id");
                                String status = object.getString("status");
                                String reason = object.getString("reason");
//                                rejectReasons.add(new RejectReason(id, status, reason, "0"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(stringRequest);

    }

    ArrayList<String> strings = new ArrayList<>();

    public void NotAgreeDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        TextView txt_title = view.findViewById(R.id.txt_dialog_title);
        txt_title.setText(getString(R.string.reson_for_rejection));
        CheckBox chk_prise = view.findViewById(R.id.chk_price);
        CheckBox chk_time = view.findViewById(R.id.chk_time);
        CheckBox chk_trust = view.findViewById(R.id.chk_trust);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        LinearLayout linearLayout = view.findViewById(R.id.lin_dynamic_checkbox);
        btn_cancel.setVisibility(View.GONE);
        chk_prise.setVisibility(View.GONE);
        chk_time.setVisibility(View.GONE);
        chk_trust.setVisibility(View.GONE);
//        chk_trust.setText("Car requirements are not available by my Garage");

        strings = new ArrayList<>();

        linearLayout.removeAllViews();
        /*for (int i = 0; i < rejectReasons.size(); i++) {
            if (rejectReasons.get(i).getStatus().equals("0") || rejectReasons.get(i).getStatus().equals("3")) {

                CheckBox checkBox = new CheckBox(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                checkBox.setText(rejectReasons.get(i).getReason());
                checkBox.setTextColor(Color.BLACK);
                checkBox.setLayoutParams(params);
                final int finalI = i;
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            rejectReasons.get(finalI).setIs_checked("1");
                            strings.add(rejectReasons.get(finalI).getId());
                        } else {
                            rejectReasons.get(finalI).setIs_checked("0");
                            strings.remove(rejectReasons.get(finalI).getId());
                        }
                    }
                });
                linearLayout.addView(checkBox);
            }
        }*/

        /*chk_prise.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    strings.add(buttonView.getText().toString());
                }else {
                    strings.remove(buttonView.getText().toString());
                }
            }
        });
        chk_time.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    strings.add(buttonView.getText().toString());
                }else {
                    strings.remove(buttonView.getText().toString());
                }
            }
        });

        chk_trust.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    strings.add(buttonView.getText().toString());
                }else {
                    strings.remove(buttonView.getText().toString());
                }
            }
        });*/

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strings.size() > 0) {
                    Toast.makeText(getApplicationContext(), "" + strings, Toast.LENGTH_LONG).show();
                    Log.e("Tag", "" + strings);
                    CancelOrderApi("9", dialog);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Select Reason", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // accept dialog
    public void AcceptDialog(final String User_Id, final String Garage_Appointment_Id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        TextView txt_title = view.findViewById(R.id.txt_dialog_title);
        txt_title.setText(getString(R.string.send_service_detail));
        LinearLayout linearLayout = view.findViewById(R.id.lin_edt_price_time);
        final EditText price = view.findViewById(R.id.edt_price);
        final EditText hour = view.findViewById(R.id.edt_hour);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_cancel.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (price.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_price), Toast.LENGTH_LONG).show();
                } /*else if (hour.getText().equals("")){
                    Toast.makeText(getApplicationContext(),"please enter the time",Toast.LENGTH_LONG).show();
                }*/ else {
                    dialog.dismiss();
                    Log.e("TAG", User_Id);
                    Log.e("TAG", Garage_Appointment_Id);
                    Log.e("TAG", price.getText().toString());
                    Log.e("TAG", hour.getText().toString());
                    RejectAcceptRequest(User_Id, "1", Garage_Appointment_Id, price.getText().toString(), hour.getText().toString());
                }
            }
        });

    }

    // request accept and reject api call
    public void RejectAcceptRequest(final String _user_id, final String _status, final String garage_appointment_id,
                                    final String price, final String time) {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        Log.e("TAG user id", _user_id);
        Log.e("TAG status", _status);
        Log.e("TAG price", price);
        Log.e("TAG time", time);
        Log.e("TAG id", garage_appointment_id);
        Log.e("TAG ", Constant.request_data.getGarage_id());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "response_garage_appointment",
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_garage", " " + response);
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                        try {
                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.optString("status").equals("1")) {
                                Constant.request_data.setStatus(_status);
                                if (_status.equals("1")) {
                                    lin_accept_reject.setVisibility(View.VISIBLE);
                                    btn_reject.setVisibility(View.GONE);
                                    btn_accept.setEnabled(false);
                                    btn_accept.setText(getString(R.string.you_are_participated_with_this_cutomer_servicec));
                                    Toast.makeText(getApplicationContext(), mainJsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                } else if (_status.equals("2")) {
                                    Toast.makeText(getApplicationContext(), mainJsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                    onBackPressed();
                                }
                            } else {
                                /* lin_accept_reject.setVisibility(View.GONE);
                                btn_accept.setVisibility(View.GONE);
                                Constant.request_data.setStatus("0");
                                Toast.makeText(getApplicationContext(), "You are not Interested for this Customer Services", Toast.LENGTH_SHORT).show();*/
                                showMessage(mainJsonObject.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  displaying the error in toast if occurs
                        Log.e("TAG", error.toString());
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            //  This indicates that the request has either time out or there is no connection
                            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            //  Error indicating that there was an Authentication Failure while performing the request
                        } else if (error instanceof ServerError) {
                            //  Indicates that the server responded with a error response
                        } else if (error instanceof NetworkError) {
                            //  Indicates that there was network error while performing the request
                        } else if (error instanceof ParseError) {
                            //  Indicates that the server response could not be parsed
                        }
                        dialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", _user_id);
                params.put("garage_id", Constant.request_data.getGarage_id());
                params.put("status", _status);
                params.put("garage_appointment_id", garage_appointment_id);
                params.put("price", price);
                params.put("time", time);
                params.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("params", " : " + params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showMessage(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_User_Request_Detail.this);
        builder.setMessage(msg);
        builder.setPositiveButton(getText(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setNeutralButton(getString(R.string.contact_us), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(getApplicationContext(), Act_Contact_Us.class));
            }
        });

        AlertDialog dialog = builder.create();
        setFinishOnTouchOutside(false);
        dialog.show();
    }

    // cancle request api
    public void CancelOrderApi(final String status, final AlertDialog alertDialog) {
        final ProgressDialog dialogs = new ProgressDialog(this);
        dialogs.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "response_user_order",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Cancel Order response", response);
                        dialogs.dismiss();
                        if (response != null) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    if (status.equals("5")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.customer_cancel_your_order), Toast.LENGTH_LONG).show();
                                        finish();
                                    } else if (status.equals("8")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.you_agree_with_user), Toast.LENGTH_LONG).show();
                                    } else if (status.equals("9")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.you_are_not_agree_this_service), Toast.LENGTH_LONG).show();
                                        finish();
                                    } else if (status.equals("10")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.is_work_done), Toast.LENGTH_LONG).show();
                                        RattingDialog(Constant.request_data);
                                    } else if (status.equals("12")) {
                                        Toast.makeText(getApplicationContext(), "Thank you for Rate this Customer service", Toast.LENGTH_LONG).show();
                                        finish();
                                    }
                                    Constant.request_data.setStatus(status);
                                    if (alertDialog != null) {
                                        alertDialog.dismiss();

                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                dialogs.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("status", status);
                map.put("garage_appointment_id", Constant.request_data.getGarage_appointment_id());
                map.put("garage_appointment_detail_id", Constant.request_data.getGarage_appointment_detail_id());
                map.put("user_id", Constant.request_data.getUser_id());
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    // rattinf user api
    private void ratting(final String _status, final String ratting, final AlertDialog alertDialog) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "garage_ratting",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    alertDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), getString(R.string.your_service_is_complete_thank_you), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.try_again_after_some_time), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("garage_appointment_id", Constant.request_data.getGarage_appointment_id());
                    params.put("status", _status);
                    params.put("garage_id", Constant.request_data.getGarage_id());
                    params.put("user_id", Constant.request_data.getUser_id());
                    params.put("ratting", ratting);
                    params.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    float ratings = -1;

    // ratting dialog
    private void RattingDialog(final Request_Data_Model request_data) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        final RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        final Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText("Please Rate " + request_data.getName());
        btn_ok.setText(getString(R.string.yess));
        btn_ok.setVisibility(View.VISIBLE);
        btn_cancel.setText(getString(R.string.nos));
        ratingBar.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.GONE);

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar1, float rating, boolean fromUser) {
                ratings = rating;
                if (rating == 1) {
                    Toast.makeText(getApplicationContext(), "Bad", Toast.LENGTH_LONG).show();
                } else if (rating == 2) {
                    Toast.makeText(getApplicationContext(), "Bad Good", Toast.LENGTH_LONG).show();
                } else if (rating == 3) {
                    Toast.makeText(getApplicationContext(), "Good", Toast.LENGTH_LONG).show();
                } else if (rating == 4) {
                    Toast.makeText(getApplicationContext(), "Excellent Good", Toast.LENGTH_LONG).show();
                } else if (rating == 5) {
                    Toast.makeText(getApplicationContext(), "Excellent", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (ratings != 0) {
//                    CancelOrderApi("12", dialog);
                    ratting("12", "" + ratings, dialog);
                } else
                    Toast.makeText(getApplicationContext(), "please select rate start", Toast.LENGTH_LONG).show();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "will be notified after 24 hours", Toast.LENGTH_LONG).show();
                /*  Intent intent = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
                    intent.putExtra("time",30000);
                    startService(intent);  */
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == lin_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + txt_cnt.getText().toString()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    // complain dialog
    public void ComplainDialog(Request_Data_Model model) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(Act_User_Request_Detail.this);

        View view = LayoutInflater.from(Act_User_Request_Detail.this).inflate(R.layout.dialog_complain, null, false);
        dialog.setView(view);
        AlertDialog dialog1 = dialog.create();
        dialog1.show();

        EditText ed_complain = view.findViewById(R.id.ed_complain);
        Button button = view.findViewById(R.id.btn_complain);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_complain.getText().toString().equals("")) {
                    ed_complain.requestFocus();
                    ed_complain.setError(getString(R.string.enter_detail));
                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "garage_complain", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag Response", response);

                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("true")) {
                                    dialog1.dismiss();
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag error", error.toString());
//                            Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("garage_id", model.getGarage_id());
                            map.put("user_id", model.getUser_id());
                            map.put("garage_appointment_id", model.getGarage_appointment_id());
                            map.put("complain", ed_complain.getText().toString());
                            map.put("status", "14");
                            map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                            Log.e("Tag Params", map.toString());
                            return map;
                        }
                    };

                    request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue queue = Volley.newRequestQueue(Act_User_Request_Detail.this);
                    queue.add(request);
                }
            }
        });

    }

}
