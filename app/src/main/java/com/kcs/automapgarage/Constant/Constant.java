package com.kcs.automapgarage.Constant;

import com.kcs.automapgarage.Model.Request_Data_Model;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Constant {

   public static  int i;
   public static  int i2;
   public static String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
   public static String OUT_JSON = "/json";
   public static String API_KEY = "AIzaSyBBIiuqET41PjtO4SvCiA6T4BYJzS3OSy0";

   public static String TYPE_AUTOCOMPLETE = "/autocomplete";

   public static final String LOGIN_PREF = "Login_preference" ;
//      public static final String URL = "http://mobwebdemo.xyz/automap/api/" ;
//      public static final String URL = "http://arccus.in/webmobdemo/automap/api/" ;
      public static final String BASE_URL = "http://kaprat.com/dev/automap/";
//      public static final String BASE_URL = "http://ec2-3-129-16-59.us-east-2.compute.amazonaws.com/admin/";
//   public static final String BASE_URL = "http://amrgroup.online/admin/";
//   public static final String URL = BASE_URL+"api/";
   public static final String URL = BASE_URL+"api_v1_0_0/" ;
   public static final String SERVICE_IMAGE_URL = BASE_URL+"assets/uploads/";

   public static Request_Data_Model request_data = null;

   public static String getFormattedDateWithTime(String simple_date) {
      try {
         SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd k:mm:ss", Locale.ENGLISH);
         Date date = fmt.parse(simple_date);

         SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM,yyyy", Locale.ENGLISH);
         return fmtOut.format(date);
      } catch (Exception e) {
         e.printStackTrace();
         return simple_date;
      }
   }

   public static @NotNull List<String> getStringToArray(String Value){
      List<String> strings = new ArrayList<>();

      if (Value != null){
         String s[] = Value.split(",");
         strings.addAll(Arrays.asList(s));
      }
      return strings;
   }

}
