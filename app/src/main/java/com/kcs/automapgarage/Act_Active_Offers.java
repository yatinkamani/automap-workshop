package com.kcs.automapgarage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Sales_Person_Model;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

public class Act_Active_Offers extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    TextView txt_message, txt_title;
    EditText ed_sales_code, ed_salesman_code, ed_salesman_name;
    LinearLayout lin2, lin1, lin_link;
    Button btn_ok2, btn_ok1;

    SharedPreferences preferences;
    String sales_id = "", sales_code = "", offer_id = "", garage_id = "";
    List<Sales_Person_Model> sales_models;

    public static String resume = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__active__offers);

        resume = "";
        bindView();
        retrieveJSON2();
    }

    public void bindView() {

        txt_message = findViewById(R.id.txt_message);
        txt_title = findViewById(R.id.txt_title);
        ed_sales_code = findViewById(R.id.ed_sales_code);
        ed_salesman_name = findViewById(R.id.ed_salesman_name);
        ed_salesman_code = findViewById(R.id.ed_salesman_code);
        lin2 = findViewById(R.id.lin2);
        lin1 = findViewById(R.id.lin1);
        lin_link = findViewById(R.id.lin_link);
        btn_ok1 = findViewById(R.id.btn_ok1);
        btn_ok2 = findViewById(R.id.btn_ok2);
        img_back = findViewById(R.id.img_back);

        img_back.setOnClickListener(this);
        btn_ok1.setOnClickListener(this);
        btn_ok2.setOnClickListener(this);
        txt_message.setOnClickListener(this);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        sales_id = preferences.getString(getString(R.string.pref_sales_id), "");
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");

        if (getIntent() != null) {
            offer_id = getIntent().getStringExtra("offer_id");
            txt_title.setText(getIntent().getStringExtra("offer_name"));
        }

        // if selasmane active offer display
        if (sales_id != null && (sales_id.equals("") || sales_id.equals("0"))) {
            lin2.setVisibility(View.VISIBLE);
            lin1.setVisibility(View.GONE);
            txt_message.setText(getString(R.string.contact_us_for_free_registration));
        } else {
            lin1.setVisibility(View.VISIBLE);
            lin2.setVisibility(View.GONE);
            txt_message.setText(getString(R.string.contact_us_to_activate_the_subscription));
        }
    }

    @Override
    public void onClick(View view) {

        if (view == img_back) {
            onBackPressed();
        } else if (view == txt_message) {
            Intent intent = new Intent(Act_Active_Offers.this, Act_Contact_Us.class);
            startActivity(intent);
        } else if (view == btn_ok1) {

            if (ed_sales_code.getText().toString().equals("")) {
                ed_sales_code.requestFocus();
                ed_sales_code.setError(getString(R.string.enter_sales_code));
            } else if (!ed_sales_code.getText().toString().toLowerCase().equals(sales_code.toLowerCase())) {
                ed_sales_code.requestFocus();
                ed_sales_code.setError(getString(R.string.enter_valid_source_code));
            } else {
                AddSubscription("0");
            }

        } else if (view == btn_ok2) {
            if (ed_salesman_name.getText().toString().equals("")) {
                ed_salesman_name.requestFocus();
                ed_salesman_name.setError(getString(R.string.enter_salesman_name));
            } else if (!checkSalesMan(ed_salesman_name.getText().toString())) {
                ed_salesman_name.requestFocus();
                ed_salesman_name.setError(getString(R.string.enter_valid_salesman_name));
            } else if (ed_salesman_code.getText().toString().equals("")) {
                ed_salesman_code.requestFocus();
                ed_salesman_code.setError(getString(R.string.enter_sales_code));
            } else if (!ed_salesman_code.getText().toString().toLowerCase().equals(sales_code.toLowerCase())) {
                ed_salesman_code.requestFocus();
                ed_salesman_code.setError(getString(R.string.enter_valid_source_code));
            } else {
                AddSubscription("1");
            }
        }
    }

    // get sales list
    private void retrieveJSON2() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Tag Country City", response);
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);

                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.optString("status").equals("true")) {

                                sales_models = new ArrayList<>();
                                Sales_Person_Model models = new Sales_Person_Model();
                                JSONObject object = obj.getJSONObject("result");
                                if (object.has("salesman")) {
                                    JSONArray sales_team = object.getJSONArray("salesman");
                                    for (int i = 0; i < sales_team.length(); i++) {
                                        JSONObject object1 = sales_team.getJSONObject(i);
                                        Sales_Person_Model model = new Sales_Person_Model();
                                        model.setId(object1.getString("salesman_id"));
                                        model.setPerson_name(check(object1,"name"));
                                        model.setPerson_code(object1.getString("code"));
                                        model.setPerson_email(object1.getString("email"));
                                        model.setPerson_mobile(object1.getString("contact_no"));
                                        sales_models.add(model);
                                    }
                                }

                                if (sales_models != null && sales_models.size() > 0) {

                                    for (int i = 0; i < sales_models.size(); i++) {
                                        if (sales_id.equals(sales_models.get(i).getId())) {
                                            sales_code = sales_models.get(i).getPerson_code();
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(),error), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        // request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    // check salesman validation
    private boolean checkSalesMan(String name) {

        boolean isName = false;
        if (sales_models != null) {

            for (int i = 0; i < sales_models.size(); i++) {
                Log.e("tag sale man", sales_models.get(i).getPerson_name() + "   " + name.toLowerCase());
                if (name.toLowerCase().equals(sales_models.get(i).getPerson_name().toLowerCase())) {
                    isName = true;
                    sales_id = sales_models.get(i).getId();
                    sales_code = sales_models.get(i).getPerson_code();
                    break;
                }
            }
        }
        return isName;
    }

    // add subscription offer and back the screen
    private void AddSubscription(String status) {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            API api = new API(Act_Active_Offers.this, new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Log.e("Tag Error", response);

                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("status").equals("1")) {
//                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(), getString(R.string.add_subscription_offers), Toast.LENGTH_LONG).show();
                            resume = "1";
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.no_data_available), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Log.e("Tag Error", error.toString());
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                }
            });

            Map<String, String> map = new HashMap<>();
            map.put("garage_id", garage_id);
            map.put("offer_id", offer_id);
//            if (status.equals("0")) {
//                map.put("sales_code", sales_code);
//            } else {
            map.put("salesman_id", sales_id);
            map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
//            }
            Log.e("Tag Params", map.toString());
            api.execute(100, Constant.URL + "add_subscription", map, true);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }
}
