package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Dealer_Request_Model;
import com.kcs.automapgarage.Model.My_Request_Model;
import com.kcs.automapgarage.extra.EndlessRecyclerViewScrollListener;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;

public class Act_Make_Request extends AppCompatActivity implements View.OnClickListener {

    Button btn_send;
    ImageView img_back;
    TextView txt_title;
    RecyclerView recyclerView;
    List<My_Request_Model> my_request_models;
    AdapterMakeRequest adapterMakeRequest;
    SharedPreferences preferences;
    LinearLayout linError;
    TextView txtError;
    Button btnRetry;
    SwipeRefreshLayout swipe_refresh;
    ProgressBar progress;

    String garage_id = "", service_id = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__make__request);

        btn_send = findViewById(R.id.btn_send);
        img_back = findViewById(R.id.image_back);
        txt_title = findViewById(R.id.txt_title);
        recyclerView = findViewById(R.id.rcl_view);
        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        progress = findViewById(R.id.progress);

        img_back.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        btnRetry.setOnClickListener(this);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");
        service_id = preferences.getString(getString(R.string.pref_service_id), "");

//        getMakeRequest();

        // car owner time hide button
        if (service_id.equals("61")){
            btn_send.setVisibility(View.GONE);
        }

        swipe_refresh.setColorSchemeResources(R.color.quantum_amberA700, R.color.quantum_cyanA700,
                R.color.quantum_deeporangeA700, R.color.quantum_lightgreenA700);

        swipe_refresh.setOnRefreshListener(() -> {
            swipe_refresh.setRefreshing(true);
            getMakeRequest();
        });

        InitializeBroadcast();
        /*my_request_models = new ArrayList<>();
        my_request_models.add(new My_Request_Model("1", "1", "test 1", "5", "", "Audi A1 service 2020", "New", "2", "0"));
        my_request_models.add(new My_Request_Model("2", "2", "test 2", "4", "", "BMW A2 service 2020", "Used", "10", "1"));
        my_request_models.add(new My_Request_Model("3", "3", "test 3", "3", "", "Audi A3 service 2020", "Commercial", "0", "0"));
        my_request_models.add(new My_Request_Model("4", "4", "test 4", "2", "", "Audi A4 service 2020", "New", "0", "1"));
        my_request_models.add(new My_Request_Model("4", "6", "test 5", "1", "", "Audi A5 service 2020", "New", "3", "0"));

        adapterMakeRequest = new AdapterMakeRequest(getApplicationContext(), my_request_models);
        recyclerView.setAdapter(adapterMakeRequest);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));*/
        ClearNotification();
    }

    // clear preference message badge count
    public void ClearNotification() {
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.pref_badge_count_make_request), 0);
        editor.apply();
        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
        if (tot == 0) {
            ShortcutBadger.removeCount(getApplicationContext());
        } else {
            ShortcutBadger.applyCount(getApplicationContext(), tot);
        }
    }

    LinearLayoutManager layoutManager;

    public void RecycleScroll() {
        if (recyclerView != null) {
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                }

                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    swipe_refresh.setEnabled(layoutManager.findFirstCompletelyVisibleItemPosition() == 0);
                }
            });
        }
    }

    // car owner as res.. 61
    public void makeButtonVisibility(){
        if (service_id.equals("61")){
            btn_send.setVisibility(View.GONE);
        }else {
            btn_send.setVisibility(View.VISIBLE);
        }
    }

    // get all make request api call and display request and eqach request badge count
    @SuppressLint("RestrictedApi")
    public void getMakeRequest() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            ProgressDialog dialog = new ProgressDialog(Act_Make_Request.this);
            dialog.setMessage(getString(R.string.loading));
            if (swipe_refresh != null && !swipe_refresh.isRefreshing()) {
                if (dialog != null && !dialog.isShowing())
                    dialog.show();
            }

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_make_request", new Response.Listener<String>() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onResponse(String response) {
                    Log.e("Tag Response", response);
                    try {
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        JSONObject object = new JSONObject(response);
                        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        if (object.getString("status").equals("true")) {

                            JSONArray array = object.getJSONArray("result");
                            my_request_models = new ArrayList<>();
                            List<My_Request_Model> my_request_models0 = new ArrayList<>();
                            List<My_Request_Model> my_request_models1 = new ArrayList<>();
                            List<My_Request_Model> my_request_models2 = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject data = array.getJSONObject(i);

                                String request_id = data.getString("dealer_request_id");
                                String item_name = check(data,"item_name");
                                String item_quantity = data.getString("item_quantity");
                                String car_info = check(data,"car_info");
                                String item_image = data.getString("item_image");
                                String item_type = check(data,"part_type");
                                String new_offer_count = data.getString("new_offer");
                                String status = data.getString("request_status");
                                String created_at = data.getString("created_at");
                                String request_time = data.getString("request_time");
                                String time_status = data.has("timeout_status") ? data.getString("timeout_status") : "0";
                                String total_offer = data.has("total_offer") ? data.getString("total_offer") : "0";
                                String maintenance_name = check(data,"maintenance_name");
                                String desc_type = check(data,"description_type");
                                int total = 0;
                                try {
                                    total = Integer.parseInt(data.getString("total"));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }


                                int n_count = preferences.getInt("dealer_request_" + request_id, 0);
                                int new_offer = Integer.parseInt(TextUtils.isDigitsOnly(new_offer_count) ? new_offer_count : "" + 0);

                                String noti_count = "0";
                                if (new_offer > n_count) {
                                    noti_count = String.valueOf(new_offer - n_count);
                                }

//                              editor.putInt("dealer_request_"+request_id, new_offer).apply();

                                /*if (status.equals("0")) {
                                    my_request_models0.add(new My_Request_Model("" + (i + 1), request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
                                } else if (status.equals("3")) {
                                    my_request_models1.add(new My_Request_Model("" + (i + 1), request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
                                } else {*/
                                    my_request_models2.add(new My_Request_Model("" +(total - i)/* (i + 1)*/, request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
//                                }
                            }
                            my_request_models.addAll(my_request_models2);
//                            my_request_models.addAll(my_request_models1);
//                            my_request_models.addAll(my_request_models0);
//                            Collections.reverse(my_request_models);

                            if (my_request_models != null && my_request_models.size() > 0) {

                                adapterMakeRequest = new AdapterMakeRequest(getApplicationContext(), my_request_models);
                                recyclerView.setAdapter(adapterMakeRequest);
                                layoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(layoutManager);
                                RecycleScroll();
                                recyclerView.setVisibility(View.VISIBLE);
                                if (adapterMakeRequest.getItemCount() >= 10){
                                    recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
                                        @Override
                                        public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                                            loadMore(page, totalItemsCount);
                                        }
                                    });
                                }
                                makeButtonVisibility();
                                linError.setVisibility(View.GONE);
                                /*SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt(getString(R.string.pref_badge_count_make_request), 0);
                                editor.apply();
                                int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
                                if (tot == 0) {
                                    ShortcutBadger.removeCount(getApplicationContext());
                                } else {
                                    ShortcutBadger.applyCount(getApplicationContext(), tot);
                                }*/

                            } else {
                                /*if (service_id.equals("61")){
                                    startActivity(new Intent(getApplicationContext(), Act_Add_Request.class));
                                    finish();
                                }*/
                                recyclerView.setVisibility(View.GONE);
                                makeButtonVisibility();
                                linError.setVisibility(View.VISIBLE);
                                btnRetry.setVisibility(View.GONE);
                                txtError.setText(getString(R.string.no_data_available));
                            }

                        } else {
                            /*if (service_id.equals("61")){
                                startActivity(new Intent(getApplicationContext(), Act_Add_Request.class));
                                finish();
                            }*/
                            recyclerView.setVisibility(View.GONE);
                            makeButtonVisibility();
                            linError.setVisibility(View.VISIBLE);
                            btnRetry.setVisibility(View.GONE);
//                            txtError.setText(getString(R.string.no_data_found));
                            if (object.getString("message").contains("تم تجميد حسابك لبعض الوقت.") ||
                                    object.getString("message").contains("Your account has been frozen for sometime.")){
                                txtError.setText(object.getString("message"));
                            } else {
                                txtError.setText(getString(R.string.no_data_found));
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        recyclerView.setVisibility(View.GONE);
                        makeButtonVisibility();
                        linError.setVisibility(View.VISIBLE);
                        btnRetry.setVisibility(View.VISIBLE);
                        txtError.setText(getString(R.string.something_wrong_please));
                    }
                    dialog.dismiss();
                    swipe_refresh.setRefreshing(false);
                }
            }, new Response.ErrorListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    recyclerView.setVisibility(View.GONE);
                    makeButtonVisibility();
                    linError.setVisibility(View.VISIBLE);
                    btnRetry.setVisibility(View.VISIBLE);
//                    txtError.setText(getString(R.string.something_wrong_please));
                    txtError.setText(AppUtils.serverError(getApplicationContext(),error));
                    dialog.dismiss();
                    swipe_refresh.setRefreshing(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("sender_garage_id", garage_id);
                    map.put("start", "1");

                    map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag parameter", map.toString());
                    return map;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(request);

        } else {
            swipe_refresh.setRefreshing(false);
            recyclerView.setVisibility(View.GONE);
            btn_send.setVisibility(View.GONE);
            linError.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));

        }
    }

    public void loadMore(int page, int totalItem){

        progress.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_make_request", new Response.Listener<String>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(String response) {
                Log.e("Tag Response", response);
                try {
                    response = Html.fromHtml(response).toString();
                    response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                }catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    if (object.getString("status").equals("true")) {

                        JSONArray array = object.getJSONArray("result");
                        List<My_Request_Model> my_request_models0 = new ArrayList<>();
                        List<My_Request_Model> my_request_models1 = new ArrayList<>();
                        List<My_Request_Model> my_request_models2 = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject data = array.getJSONObject(i);

                            String request_id = data.getString("dealer_request_id");
                            String item_name = check(data,"item_name");
                            String item_quantity = data.getString("item_quantity");
                            String car_info = check(data,"car_info");
                            String item_image = data.getString("item_image");
                            String item_type = check(data,"part_type");
                            String new_offer_count = data.getString("new_offer");
                            String status = data.getString("request_status");
                            String created_at = data.getString("created_at");
                            String request_time = data.getString("request_time");
                            String time_status = data.has("timeout_status") ? data.getString("timeout_status") : "0";
                            String total_offer = data.has("total_offer") ? data.getString("total_offer") : "0";
                            String maintenance_name = check(data,"maintenance_name");
                            String desc_type = check(data,"description_type");
                            int total = 0;
                            try {
                                total = Integer.parseInt(data.getString("total"));
                            }catch (Exception e){
                                e.printStackTrace();
                            }


                            int n_count = preferences.getInt("dealer_request_" + request_id, 0);
                            int new_offer = Integer.parseInt(TextUtils.isDigitsOnly(new_offer_count) ? new_offer_count : "" + 0);

                            String noti_count = "0";
                            if (new_offer > n_count) {
                                noti_count = String.valueOf(new_offer - n_count);
                            }

                                editor.putInt("dealer_request_"+request_id, new_offer).apply();

                            my_request_models2.add(new My_Request_Model("" + (total - (totalItem + i))/*((totalItem + i) + 1)*/, request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
                            /*if (status.equals("0")) {
                                my_request_models0.add(new My_Request_Model("" + (i + 1), request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
                            } else if (status.equals("3")) {
                                my_request_models1.add(new My_Request_Model("" + (i + 1), request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
                            } else {
                                my_request_models2.add(new My_Request_Model("" + (i + 1), request_id, item_name, item_quantity, item_image, car_info, item_type, new_offer_count, status, total_offer, time_status, ""+noti_count, request_time, created_at, maintenance_name, desc_type));
                            }*/
                        }
                        my_request_models.addAll(my_request_models2);
                        /*my_request_models.addAll(my_request_models1);
                        my_request_models.addAll(my_request_models0);*/

//                        Collections.reverse(my_request_models);

                        if (my_request_models != null && my_request_models.size() > 0) {

//                            adapterMakeRequest = new AdapterMakeRequest(getApplicationContext(), my_request_models);
//                            recyclerView.setAdapter(adapterMakeRequest);
//                            layoutManager = new LinearLayoutManager(getApplicationContext());
//                            recyclerView.setLayoutManager(layoutManager);
//                            RecycleScroll();
//                            recyclerView.setVisibility(View.VISIBLE);
                            recyclerView.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapterMakeRequest.notifyItemInserted((my_request_models.size()-1));
                                }
                            });

//                            makeButtonVisibility();
//                            linError.setVisibility(View.GONE);
                                /*SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt(getString(R.string.pref_badge_count_make_request), 0);
                                editor.apply();
                                int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_make_request), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_admin_msg), 0) +
                                        preferences.getInt(getString(R.string.pref_badge_count_dealer_request), 0));
                                if (tot == 0) {
                                    ShortcutBadger.removeCount(getApplicationContext());
                                } else {
                                    ShortcutBadger.applyCount(getApplicationContext(), tot);
                                }*/

                        } else {
                                /*if (service_id.equals("61")){
                                    startActivity(new Intent(getApplicationContext(), Act_Add_Request.class));
                                    finish();
                                }*/
                           /* recyclerView.setVisibility(View.GONE);
                            makeButtonVisibility();
                            linError.setVisibility(View.VISIBLE);
                            btnRetry.setVisibility(View.GONE);
                            txtError.setText(getString(R.string.no_data_available));*/
                        }

                    } else {
                            /*if (service_id.equals("61")){
                                startActivity(new Intent(getApplicationContext(), Act_Add_Request.class));
                                finish();
                            }*/
                        /*recyclerView.setVisibility(View.GONE);
                        makeButtonVisibility();
                        linError.setVisibility(View.VISIBLE);
                        btnRetry.setVisibility(View.GONE);*/
//                            txtError.setText(getString(R.string.no_data_found));
                       /* if (object.getString("message").contains("تم تجميد حسابك لبعض الوقت.") ||
                                object.getString("message").contains("Your account has been frozen for sometime.")){
                            txtError.setText(object.getString("message"));
                        } else {
                            txtError.setText(getString(R.string.no_data_found));
                        }*/

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
//                    recyclerView.setVisibility(View.GONE);
//                    makeButtonVisibility();
//                    linError.setVisibility(View.VISIBLE);
//                    btnRetry.setVisibility(View.VISIBLE);
//                    txtError.setText(getString(R.string.something_wrong_please));
                }
                progress.setVisibility(View.GONE);
//                dialog.dismiss();
//                swipe_refresh.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag Error", error.toString());
                progress.setVisibility(View.GONE);
//                recyclerView.setVisibility(View.GONE);
//                makeButtonVisibility();
//                linError.setVisibility(View.VISIBLE);
//                btnRetry.setVisibility(View.VISIBLE);
//                    txtError.setText(getString(R.string.something_wrong_please));
//                txtError.setText(AppUtils.serverError(getApplicationContext(),error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("sender_garage_id", garage_id);
                map.put("start", ""+totalItem);

                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag parameter", map.toString());
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    // adpte of request listing
    private class AdapterMakeRequest extends RecyclerView.Adapter<AdapterMakeRequest.MyViewHolder> {

        Context context;
        public List<My_Request_Model> request_models;
        private int lastPosition = -1;

        private AdapterMakeRequest(Context context, List<My_Request_Model> request_models) {
            this.context = context;
            this.request_models = request_models;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_make_request, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {

            My_Request_Model model = request_models.get(i);
            holder.txt_number.setText(getString(R.string.request_number) + " " + model.getNo());

            if (model.getItem_image().equals("")) {
                holder.img_item.setVisibility(View.GONE);
            } else {
                holder.img_item.setVisibility(View.VISIBLE);

                Glide.with(context).asBitmap().load(model.getItem_image()).placeholder(R.drawable.users)
                        .thumbnail(0.01f).into(new BitmapImageViewTarget(holder.img_item) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.img_item.setImageBitmap(resource);
                    }
                });
            }

            if (!model.getItem_type().equals("")){
                holder.txt_item.setText(Html.fromHtml(getString(R.string.item) + " : <b><font color='blue'>" + model.getItem_name() + "</font></b>"));
            } else if (!model.getMaintenance_name().equals("")){
                holder.txt_item.setText(Html.fromHtml("<b>"+getString(R.string.maintenance_required) + " : </b><b><font color='blue'>" + model.getMaintenance_name() + "</font></b>"));
            }

            if (model.getMaintenance_name().equals("")){
                if (!model.getItem_quantity().equals("")){
                    holder.txt_item_quantity.setVisibility(View.VISIBLE);
                    holder.txt_item_quantity.setText(Html.fromHtml(getString(R.string.item_quantity) + " : <b><font color='blue'>" + model.getItem_quantity() + "</font></b>"));
                }else {
                    holder.txt_item_quantity.setVisibility(View.GONE);
                }
            } else {
                holder.txt_item_quantity.setVisibility(View.GONE);
            }

            if (model.getCar_info().equals("")) {
                holder.txt_car_info.setVisibility(View.GONE);
            } else {
                holder.txt_car_info.setVisibility(View.VISIBLE);
                holder.txt_car_info.setText(Html.fromHtml(model.getCar_info()));
            }

            if (model.getNoti_count() != null && !model.getNoti_count().equals("0")) {
                holder.txt_offer_count.setVisibility(View.VISIBLE);
                holder.txt_offer_count.setText(Html.fromHtml(model.getNoti_count()));
            } else {
                holder.txt_offer_count.setVisibility(View.GONE);
            }

            if (!model.getItem_type().equals("")) {
                holder.txt_item_type.setVisibility(View.VISIBLE);
                holder.txt_item_type.setText(Html.fromHtml("<b>" + getString(R.string.item_type) + " : <font color='blue'>" + model.getItem_type() + "</font></b>"));
            } else if (!model.getDesc_type().equals("")){
                holder.txt_item_type.setVisibility(View.VISIBLE);
                holder.txt_item_type.setText(Html.fromHtml("<b>" + getString(R.string.description) + " : <font color='blue'>" + model.getDesc_type() + "</font></b>"));
            }else {
                holder.txt_item_type.setVisibility(View.GONE);
            }

            holder.btn_arrow_offers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("dealer_request_"+model.getRequest_id(),
                            TextUtils.isDigitsOnly(model.getNew_offer_count()) ? Integer.parseInt(model.getNew_offer_count()) : 0).apply();
                    Intent intent = new Intent(getApplicationContext(), Act_Make_Request_Response.class);
                    intent.putExtra(getString(R.string.pref_request_id), model.getRequest_id());
                    startActivity(intent);
                }
            });

            if (model.getTime_status().equals("0")) {
                holder.rel_timeOut.setVisibility(View.GONE);
            } else {
                holder.rel_timeOut.setVisibility(View.VISIBLE);
            }

            if (model.getTotal_offer().equals("0")) {
                holder.btn_arrow_offers.setText(getString(R.string.no_offer_yet));
            } else {
                holder.btn_arrow_offers.setText(model.getTotal_offer() + "  " + getString(R.string.offers));
            }

            if (model.getStatus().equals("3")) {
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.quantum_vanillagreen100));
            } else if (model.getStatus().equals("0")) {
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.white));
            } else {
                holder.card_main.setCardBackgroundColor(getResources().getColor(R.color.quantum_bluegrey100));
            }

            int min;
            if (model.getRequest_time() != null && !model.getRequest_time().equals("0")){
                min = Integer.parseInt(model.getRequest_time());
            }else {
                min = (24 * 60);
            }

            if (model.getStatus().equals("0") && model.getTime_status().equals("0") && Long.parseLong(AppUtils.getUnitBetweenDates(AppUtils.getStringToDate(AppUtils.getUTCTime()),AppUtils.AddMinutes(AppUtils.getStringToDate(model.getCreate_at()),min), TimeUnit.MILLISECONDS)) > 0){
                long l = Long.parseLong(AppUtils.getUnitBetweenDates(AppUtils.getStringToDate(AppUtils.getUTCTime()), AppUtils.AddMinutes(AppUtils.getStringToDate(model.getCreate_at()),min), TimeUnit.MILLISECONDS));
                holder.lin_timmer.setVisibility(View.VISIBLE);
                if (holder.countDownTimer != null){
                    holder.countDownTimer.cancel();
                }
                holder.countDownTimer = new CountDownTimer(l, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        long seconds = millisUntilFinished / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;
                        String time = days+" "+"days" +" :" +hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
                        String h = (hours) <= 9 ? "0"+(hours) : ""+(hours);
                        String m = (minutes % 60 ) <= 9 ? "0"+(minutes % 60 ) : ""+(minutes % 60 );
                        String s = (seconds % 60 ) <= 9 ? "0"+(seconds % 60 ) : ""+(seconds % 60 );
                        /*if ( days > 0){
                            h = ((hours % 24) + (days * 24)) <= 9 ? "0"+((hours % 24) + (days * 24)) : ""+((hours % 24) + (days * 24));
                            time = h + ":" + m + ":" + s;
                        }else {*/
                            time = h + ":" + m + ":" + s;
//                        }

                        holder.txt_timmer.setText(time);
                    }

                    @Override
                    public void onFinish() {
                        holder.txt_timmer.setText("00:00:00");
                    }
                };
                holder.countDownTimer.start();
            } else {
                holder.lin_timmer.setVisibility(View.GONE);
                holder.txt_timmer.setText("00:00:00");
            }

            setAnimation(holder.itemView, i);
        }

        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        @Override
        public int getItemCount() {
            return request_models.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            RoundedImage img_item;
            TextView txt_item, txt_item_quantity, txt_number, txt_car_info, txt_item_type, txt_offer_count;
            CardView card_main;
            MaterialButton btn_arrow_offers;
            RelativeLayout rel_timeOut;
            LinearLayout lin_timmer;
            TextView txt_timmer, txt_time_text;
            CountDownTimer countDownTimer;

            private MyViewHolder(@NonNull View itemView) {
                super(itemView);

                txt_item = itemView.findViewById(R.id.txt_item);
                txt_item_quantity = itemView.findViewById(R.id.txt_item_quantity);
                txt_car_info = itemView.findViewById(R.id.txt_car_info);
                txt_number = itemView.findViewById(R.id.txt_number);
                txt_item_type = itemView.findViewById(R.id.txt_item_type);
                img_item = itemView.findViewById(R.id.img_item);
                card_main = itemView.findViewById(R.id.card_main);
                txt_offer_count = itemView.findViewById(R.id.txt_offer_count);
                btn_arrow_offers = itemView.findViewById(R.id.btn_arrow_offers);
                rel_timeOut = itemView.findViewById(R.id.rel_timeOut);
                lin_timmer = itemView.findViewById(R.id.lin_timmer);
                txt_timmer = itemView.findViewById(R.id.txt_timmer);
                txt_time_text = itemView.findViewById(R.id.txt_time_text);
                txt_time_text.setText(getString(R.string.time_left_yo_receive_offers));
                TextView txt_request_time_over = itemView.findViewById(R.id.txt_request_time_over);
                txt_request_time_over.setText(getString(R.string.accept_request_ntime_is_over));

            }
        }
    }

    @Override
    protected void onResume() {
        getMakeRequest();
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        if (view == btn_send) {
            Intent intent = new Intent(getApplicationContext(), Act_Add_Request.class);
            startActivity(intent);
        } else if (view == img_back) {
            onBackPressed();
        } else if (view == btnRetry) {
            getMakeRequest();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                img_back.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Tag Error", e.toString());
            }
        } else {
            Log.e("Tag Null", "" + requestCode);
        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Make_Request.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            /*int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);*/
            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
//            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (make != 0 && !(Act_Make_Request.this.isFinishing())) {
                getMakeRequest();
            }
        }
    };
}