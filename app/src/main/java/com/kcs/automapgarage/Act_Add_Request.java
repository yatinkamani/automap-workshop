package com.kcs.automapgarage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Area_Data_Model;
import com.kcs.automapgarage.Model.CarPart;
import com.kcs.automapgarage.Model.Car_Model_model;
import com.kcs.automapgarage.Model.Car_brand_model;
import com.kcs.automapgarage.Model.City_Data_Model;
import com.kcs.automapgarage.Model.Dealer_Data_Model;
import com.kcs.automapgarage.Model.Dealer_Service_Model;
import com.kcs.automapgarage.Model.Manufacture_Country_Model;
import com.kcs.automapgarage.Model.VolleyMultipartRequest;
import com.kcs.automapgarage.Model.Workshop_List_Model;
import com.kcs.automapgarage.extra.EndlessRecyclerViewScrollListener;
import com.kcs.automapgarage.utils.API;
import com.kcs.automapgarage.utils.APIResponse;
import com.kcs.automapgarage.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

// laithhmdan
// 123456789
// 112345

public class Act_Add_Request extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    LinearLayout lin_services, lin_main, linError, lin_country;
    Spinner spinner_select_service, spinner_select_country;
    //    MultiSpinner multiSelectSpinner;
    RecyclerView recycler_view;
    //    EditText ed_item_quantity;
    AutoCompleteTextView ed_part_type, ed_car_spec, ed_item_name, ed_item_quantity, ed_maintenance_name, ed_desc_type;
    RelativeLayout relPartItem, relGarageItem;
    Switch switch_price;
    ImageView item_image;
    TextView txt_item_image;
    RoundedImage img_delete;
    RelativeLayout rel_main;
    TextView txtError;
    Button btnRetry;
    CardView card_send;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    List<Workshop_List_Model> workshop_list_models = new ArrayList<>();
    List<Dealer_Service_Model> dealer_service_models = new ArrayList<>();
    List<Manufacture_Country_Model> manufacture_country_models = new ArrayList<>();
    AdapterWorkshopList adapterWorkshopList;

    List<Dealer_Data_Model> dataModels = new ArrayList<>();

    List<Car_brand_model> brandModels = new ArrayList<>();
    List<Car_brand_model> brandModels_ar = new ArrayList<>();
    List<CarPart> carParts = new ArrayList<>();
    List<CarPart> carParts_ar = new ArrayList<>();
    List<Car_Model_model> modelModels = new ArrayList<>();
    List<Car_Model_model> modelModels_ar = new ArrayList<>();

    String service_id = "", garage_id = "", sales_id = "", select_service_id = "", last_garage_id = "", Mc_id = "", dealer_request_id = "", isPrice = "1";

    String selected_garage_id = "";
    Bitmap bitmap_item_image = null;

    Spinner spinner_select_city, spinner_select_area, spinner_select_year;
    private ArrayList<City_Data_Model> city_data_models;
    private ArrayList<Area_Data_Model> area_data_models;

    String selected_city = "", selected_area = "", country_id = "";
    String car_info = "", car_info_ar = "";
    String item_name = "", item_name_ar = "";
    String garage_json = "";
    String back_msg = "";
    String selected_year = "2015";

    FirebaseAnalytics analytics;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__add__request);

        bindView();

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        editor = preferences.edit();
        service_id = preferences.getString(getString(R.string.pref_service_id), "");
        country_id = preferences.getString(getString(R.string.pref_country_id), "");
        garage_id = preferences.getString(getString(R.string.pref_garage_id), "");
        sales_id = preferences.getString(getString(R.string.pref_sales_id), "");
        Log.e("Tag service_id", service_id);

        analytics = FirebaseAnalytics.getInstance(this);

        getAllService();
        CountryDataApi();
        getCityArea();
        CarPartApi();
        AutoCompletePartType();
        AutoCompleteMainName();
        AutoCompleteDescType();

        switch_price.setChecked(true);
        switch_price.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                isPrice = "1";
            } else {
                isPrice = "0";
            }
        });
    }

    // register as workshop
    public void setUserDataGarage() {
        relGarageItem.setVisibility(View.VISIBLE);
        relPartItem.setVisibility(View.GONE);
    }

    // register as partshop
    public void setUserDataPart() {

        relGarageItem.setVisibility(View.GONE);
        relPartItem.setVisibility(View.VISIBLE);

    }

    private void bindView() {
        img_back = findViewById(R.id.img_back);
        lin_services = findViewById(R.id.lin_services);
        lin_main = findViewById(R.id.lin_main);
        lin_country = findViewById(R.id.lin_country);
        rel_main = findViewById(R.id.rel_main);
        ed_part_type = findViewById(R.id.ed_part_type);
        ed_item_name = findViewById(R.id.ed_item_name);
        ed_item_quantity = findViewById(R.id.ed_item_quantity);
        ed_car_spec = findViewById(R.id.ed_car_spec);

        ed_maintenance_name = findViewById(R.id.ed_maintenance_name);
        ed_desc_type = findViewById(R.id.ed_desc_type);
        relPartItem = findViewById(R.id.relPartItem);
        relGarageItem = findViewById(R.id.relGarageItem);

        switch_price = findViewById(R.id.switch_price);
        txt_item_image = findViewById(R.id.txt_item_image);
        img_delete = findViewById(R.id.img_delete);
        item_image = findViewById(R.id.item_image);
        linError = findViewById(R.id.linError);
        spinner_select_service = findViewById(R.id.spinner_select_service);
        spinner_select_country = findViewById(R.id.spinner_select_country);
        spinner_select_year = findViewById(R.id.spinner_select_year);
//        multiSelectSpinner = findViewById(R.id.multiSelectSpinner);
        recycler_view = findViewById(R.id.recycler_view);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);
        card_send = findViewById(R.id.card_send);

        spinner_select_city = findViewById(R.id.spinner_select_city);
        spinner_select_area = findViewById(R.id.spinner_select_area);

        card_send.setOnClickListener(this);
        btnRetry.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_delete.setOnClickListener(this);

        setYear();
    }

    // set year selection spinner
    public void setYear() {
        ArrayList<String> strings = new ArrayList<>();

        int selected = 0;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = 1950; i <= year; i++) {
            strings.add("" + i);
        }

        Collections.reverse(strings);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(Act_Add_Request.this, R.layout.my_spiner_item, strings);
        selected = adapter.getPosition("2015");
        spinner_select_year.setAdapter(adapter);
        spinner_select_year.setSelection(selected);

        spinner_select_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_year = "" + parent.getSelectedItem();
                Log.e("Tag 0", selected_year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // autoComplete decription type
    public void AutoCompleteDescType() {
        ArrayList<String> decsType = new ArrayList<>();
        decsType.add(getString(R.string.used));
        decsType.add(getString(R.string.neww));
        decsType.add(getString(R.string.commercial));
        ed_desc_type.setAdapter(new ArrayAdapter<String>(Act_Add_Request.this, android.R.layout.simple_dropdown_item_1line, decsType));
        ed_desc_type.setThreshold(1);
    }

    // autoComplete maintaness type
    public void AutoCompleteMainName() {
        ArrayList<String> maintenance_item = new ArrayList<>();
        maintenance_item.add(getString(R.string.oil_change));
        maintenance_item.add(getString(R.string.spark_plug));
        maintenance_item.add(getString(R.string.fuel_pump));
        ed_maintenance_name.setAdapter(new ArrayAdapter<String>(Act_Add_Request.this, android.R.layout.simple_dropdown_item_1line, maintenance_item));
        ed_maintenance_name.setThreshold(1);
    }

    // autoCompletet part type
    private void AutoCompletePartType() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add(getString(R.string.new_or_used));
        strings.add(getString(R.string.original_used));
        strings.add(getString(R.string.original_new));
        strings.add(getString(R.string.new_commercial));
        strings.add(getString(R.string.used_new));
//        strings.add(getString(R.string.neww) + " " + getString(R.string.original));
//        strings.add(getString(R.string.used) + " " + getString(R.string.original));
//        strings.add(getString(R.string.commercial) + " " + getString(R.string.used));
        ed_part_type.setAdapter(new ArrayAdapter<String>(Act_Add_Request.this, android.R.layout.simple_dropdown_item_1line, strings));
        ed_part_type.setThreshold(1);
    }

    // get dealer service and all dropdown list
    public void getAllService() {

        API api = new API(Act_Add_Request.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("garage_facility", " " + response);
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("true")) {

                        JSONArray array = object.getJSONArray("result");
                        dealer_service_models = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject data = array.getJSONObject(i);
                            String dealer_service_request_id = data.getString("dealer_service_request_id");
                            String service_idd = data.getString("service_id");
                            String dealer_service_id = data.getString("dealer_service_id");
                            String dealer_service_name = check(data, "dealer_service_name");
                            String service_name = check(data, "service_name");
                            String created_at = data.getString("created_at");

                            if (service_id.equals(service_idd)) {

                                dealer_service_models.add(new Dealer_Service_Model(dealer_service_request_id, service_idd, dealer_service_id,
                                        dealer_service_name, service_name, created_at));
                            }
                        }

                        ServiceSelect();

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Tag Error", error.toString());
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("service_id", service_id);
        map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
        api.execute(100, Constant.URL + "get_dealer_service", map, true);
    }

    // inite dealer service and previous selected
    public void ServiceSelect() {

        if (dealer_service_models != null && dealer_service_models.size() > 0) {
            String service_name = "";
            ArrayList<String> serviceList_name = new ArrayList<>();
            ArrayList<String> serviceList_id = new ArrayList<>();
            /*  serviceList_name.add("All");
            serviceList_id.add("All");*/
            int selected = 0;
            Log.e("Tag Service", "" + dealer_service_models.size());
            for (int i = 0; i < dealer_service_models.size(); i++) {
                Log.e("Tag Service Id", "" + dealer_service_models.get(i).getDealer_service_id());

                serviceList_name.add(dealer_service_models.get(i).getDealer_service_name());
                serviceList_id.add(dealer_service_models.get(i).getDealer_service_id());
                if (preferences.getString(getString(R.string.pref_last_s_id), "").toString().equals(dealer_service_models.get(i).getDealer_service_id())) {
                    selected = i;
                }

            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, serviceList_name);
            spinner_select_service.setAdapter(adapter);
            spinner_select_service.setSelection(selected);

            spinner_select_service.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    lin_country.setVisibility(View.GONE);
                    Mc_id = "";

                   /* if (i == 0) {
                        JSONArray array = new JSONArray();
                        for (int s = 0; s < serviceList_id.size(); s++) {
                            if (s != 0) {

                                JSONObject object = new JSONObject();
                                try {
                                    object.put("service_id", serviceList_id.get(s));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                array.put(object);
                            }
                        }
                        getGarageList("" + array);
                        select_service_id = "" + array;
                    } else {*/
                    JSONArray array = new JSONArray();
                    JSONObject object = new JSONObject();
                    try {
                        object.put("service_id", serviceList_id.get(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    array.put(object);
                    // service select partshop and workshop to country slection visible
                    if (serviceList_id.get(i).equals("4") || serviceList_id.get(i).equals("18")) {
                        lin_country.setVisibility(View.VISIBLE);
                        CountryDataApi();
                    } else {
                        getGarageList("" + array);
                    }

                    // register as car owner to
                    if (service_id.equals("61")) {
                        if (serviceList_id.get(i).equals("18")) {
                            setUserDataGarage();
                        } else {
                            setUserDataPart();
                        }
                    }
                    select_service_id = "" + array;
                    editor.putString(getString(R.string.pref_last_s_id), serviceList_id.get(i));
                    editor.apply();
//                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    // country dropdown selection defaulr selection
    public void CountrySelect() {

        if (manufacture_country_models != null && manufacture_country_models.size() > 0) {
//            String service_name = "";
            ArrayList<String> CountryList_name = new ArrayList<>();
            ArrayList<String> CountryList_id = new ArrayList<>();
//            CountryList_name.add("All");
//            CountryList_id.add("All");
            int selected_position = 0;
            Log.e("Tag Service", "" + manufacture_country_models.size());
            for (int i = 0; i < manufacture_country_models.size(); i++) {
                Log.e("Tag Service Id", "" + manufacture_country_models.get(i).getMc_id());
                CountryList_name.add(manufacture_country_models.get(i).getMc_name());
                CountryList_id.add(manufacture_country_models.get(i).getMc_id());
                if (preferences.getString(getString(R.string.pref_last_mc_id), "").toString().equals(manufacture_country_models.get(i).getMc_id())) {
                    selected_position = i;
                    Log.e("Tag pref Mc id", manufacture_country_models.get(i).getMc_id());
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CountryList_name);
            spinner_select_country.setAdapter(adapter);

            spinner_select_country.setSelection(selected_position);
            Mc_id = CountryList_id.get(selected_position);

            spinner_select_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    /*if (i == 0) {
                        JSONArray array = new JSONArray();
                        for (int s = 0; s < CountryList_id.size(); s++) {
                            if (s != 0) {

                                JSONObject object = new JSONObject();
                                try {
                                    object.put("service_id", CountryList_id.get(s));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                array.put(object);
                            }
                        }
                        getGarageList("" + array);
                        select_service_id = "" + array;
                    } else {*/
//                        JSONArray array = new JSONArray();
//                        JSONObject object = new JSONObject();
//                        try {
//                            object.put("service_id", CountryList_id.get(i));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        array.put(object);
                    Mc_id = "" + CountryList_id.get(i);
                    getGarageList("" + select_service_id);
                    editor.putString(getString(R.string.pref_last_mc_id), Mc_id);
                    editor.apply();
//                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    // get car country, model, brand,
    private void CountryDataApi() {

        API api = new API(Act_Add_Request.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        JSONObject object_result = obj.getJSONObject("result");

                        brandModels = new ArrayList<>();
                        brandModels_ar = new ArrayList<>();
                        JSONArray c_brand = object_result.getJSONArray("car_brand");
                        for (int i = 0; i < c_brand.length(); i++) {
                            JSONObject object = c_brand.getJSONObject(i);
                            Car_brand_model model = new Car_brand_model();
                            model.setBrand_id(object.getString("brand_id"));
                            model.setBrand_nm(object.getString("brand_name"));
                            brandModels.add(model);

                            Car_brand_model model_ar = new Car_brand_model();
                            if (object.has("brand_name_ar")) {
                                model_ar.setBrand_id(object.getString("brand_id"));
                                model_ar.setBrand_nm(object.getString("brand_name_ar"));
                                brandModels_ar.add(model_ar);
                            } else {
                                brandModels_ar.add(model);
                            }
                        }
                        modelModels = new ArrayList<>();
                        modelModels_ar = new ArrayList<>();
                        JSONArray c_model = object_result.getJSONArray("car_model");
                        for (int i = 0; i < c_model.length(); i++) {
                            JSONObject object = c_model.getJSONObject(i);
                            Car_Model_model model = new Car_Model_model();
                            model.setModel_id(object.getString("model_id"));
                            model.setModel_nm(object.getString("model_name"));
                            model.setBrand_id(object.getString("brand_id"));
                            model.setModel_year(object.getString("model_year"));
                            for (Car_brand_model modell : brandModels) {
                                if (modell.getBrand_id().equals(object.getString("brand_id"))) {
                                    model.setBrand_nm(modell.getBrand_nm());
                                }
                            }
                            modelModels.add(model);

                            Car_Model_model model_ar = new Car_Model_model();
                            if (object.has("model_name_ar")) {
                                model_ar.setModel_id(object.getString("model_id"));
                                model_ar.setModel_nm(object.getString("model_name_ar"));
                                model_ar.setBrand_id(object.getString("brand_id"));
                                model_ar.setModel_year(object.getString("model_year"));
                                for (Car_brand_model modell : brandModels_ar) {
                                    if (modell.getBrand_id().equals(object.getString("brand_id"))) {
                                        model_ar.setBrand_nm(modell.getBrand_nm());
                                    }
                                }
                                modelModels_ar.add(model_ar);
                            } else {
                                modelModels_ar.add(model);

                            }
                        }
                        AutoCompleteCarDetail();

                        manufacture_country_models = new ArrayList<>();
                        if (lin_country.getVisibility() == View.VISIBLE) {
                            JSONArray m_country = object_result.getJSONArray("manufacture_country");
                            for (int i = 0; i < m_country.length(); i++) {
                                JSONObject object = m_country.getJSONObject(i);
                                Manufacture_Country_Model model = new Manufacture_Country_Model();
                                model.setMc_id(object.getString("manufacture_country_id"));
                                model.setMc_name(check(object, "country_name"));
                                model.setIsChecked("0");
                                manufacture_country_models.add(model);
                            }
                            CountrySelect();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Tag String", error.toString());
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));

        api.execute(102, Constant.URL + "car_detail", map, true);

        /*final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "car_detail", new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        JSONObject object_result = obj.getJSONObject("result");

                        brandModels = new ArrayList<>();
                        JSONArray c_brand = object_result.getJSONArray("car_brand");
                        for (int i = 0; i < c_brand.length(); i++) {
                            JSONObject object = c_brand.getJSONObject(i);
                            Car_brand_model model = new Car_brand_model();
                            model.setBrand_id(object.getString("brand_id"));
                            model.setBrand_nm(object.getString("brand_name"));
                            brandModels.add(model);
                        }
                        modelModels = new ArrayList<>();
                        JSONArray c_model = object_result.getJSONArray("car_model");
                        for (int i = 0; i < c_model.length(); i++) {
                            JSONObject object = c_model.getJSONObject(i);
                            Car_Model_model model = new Car_Model_model();
                            model.setModel_id(object.getString("model_id"));
                            model.setModel_nm(object.getString("model_name"));
                            model.setBrand_id(object.getString("brand_id"));
                            model.setModel_year(object.getString("model_year"));
                            for (Car_brand_model modell : brandModels) {
                                if (modell.getBrand_id().equals(object.getString("brand_id"))) {
                                    model.setBrand_nm(modell.getBrand_nm());
                                }
                            }
                            modelModels.add(model);
                        }
                        AutoCompleteCarDetail();

                        manufacture_country_models = new ArrayList<>();
                        if (lin_country.getVisibility() == View.VISIBLE) {
                            JSONArray m_country = object_result.getJSONArray("manufacture_country");
                            for (int i = 0; i < m_country.length(); i++) {
                                JSONObject object = m_country.getJSONObject(i);
                                Manufacture_Country_Model model = new Manufacture_Country_Model();
                                model.setMc_id(object.getString("manufacture_country_id"));
                                model.setMc_name(object.getString("country_name"));
                                model.setIsChecked("0");
                                manufacture_country_models.add(model);
                            }
                            CountrySelect();
                        }

                    }

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Tag String", error.toString());
//                error_dialog();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);*/
    }

    public void getDealer() {
//        card_save.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_service_provider_facility", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag Response", response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("true")) {

                        JSONArray array = object.optJSONArray("result");
                        dataModels = new ArrayList<>();

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            Dealer_Data_Model model = new Dealer_Data_Model();
                            model.setId(jsonObject.getString("service_id"));
                            model.setDealer_name(jsonObject.getString("service_name"));
                            dataModels.add(model);
                        }
//                        MakeRadioGroup(dataModels);
                        ServiceSelect();

                    } else {
//                        rg_dynamic_service.setVisibility(View.GONE);
//                        linError.setVisibility(View.VISIBLE);
//                        card_save.setVisibility(View.GONE);
//                        txtError.setText(getString(R.string.no_data_found));
//                        btnRetry.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    rg_dynamic_service.setVisibility(View.GONE);
//                    linError.setVisibility(View.VISIBLE);
//                    txtError.setText(getString(R.string.something_wrong_please));
//                    btnRetry.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                rg_dynamic_service.setVisibility(View.GONE);
//                linError.setVisibility(View.VISIBLE);
//                txtError.setText(getString(R.string.something_wrong_please));
//                btnRetry.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("service_provider", getString(R.string.dealer));
                Log.e("Tag params", map.toString());
                return map;

            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    // add autocomplte decription to model data
    public void AutoCompleteCarDetail() {

        ed_car_spec.setText("");
        ArrayList<String> strings = new ArrayList<>();

//        if (preferences.getString(getString(R.string.pref_language), "en").equals("en")){

        for (int b = 0; b < modelModels.size(); b++) {

            if (!strings.contains(modelModels.get(b).getBrand_nm() + " " + modelModels.get(b).getModel_nm())) {
                strings.add(modelModels.get(b).getBrand_nm() + " " + modelModels.get(b).getModel_nm());
            }
        }
        /*}else {
            for (int b = 0; b < modelModels_ar.size(); b++) {

                if (!strings.contains(modelModels_ar.get(b).getBrand_nm() + " - " + modelModels_ar.get(b).getModel_nm() + " - " + modelModels_ar.get(b).getModel_year())) {
                    strings.add(modelModels_ar.get(b).getBrand_nm() + " - " + modelModels_ar.get(b).getModel_nm() + " - " + modelModels_ar.get(b).getModel_year());
                }
            }
        }*/
        Log.e("Tag Size", "" + strings.size());
        Log.e("Tag Array", "" + Arrays.toString(strings.toArray()));

        ed_car_spec.setAdapter(new ArrayAdapter<>(Act_Add_Request.this, android.R.layout.simple_dropdown_item_1line, strings));
        ed_car_spec.setThreshold(1);

        /*ed_car_spec.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                car_info = modelModels.get(position).getBrand_nm() + " - " + modelModels.get(position).getModel_nm() + " - " + modelModels.get(position).getModel_year();
                car_info_ar = modelModels_ar.get(position).getBrand_nm() + " - " + modelModels_ar.get(position).getModel_nm() + " - " + modelModels_ar.get(position).getModel_year();
            }
        });*/
    }

    // get all car part data
    private void CarPartApi() {

        API api = new API(Act_Add_Request.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        JSONArray object_result = obj.getJSONArray("result");

                        carParts = new ArrayList<>();
                        carParts_ar = new ArrayList<>();
                        for (int i = 0; i < object_result.length(); i++) {
                            JSONObject object = object_result.getJSONObject(i);
                            CarPart model = new CarPart();
                            model.setPart_id(object.getString("car_part_id"));
                            model.setPart_name(object.getString("car_part_name"));
                            carParts.add(model);
                            if (object.has("cart_part_name_ar")) {
                                CarPart model_ar = new CarPart();
                                model_ar.setPart_id(object.getString("car_part_id"));
                                model_ar.setPart_name(object.getString("car_part_name_ar"));
                                carParts.add(model_ar);
                            } else {
                                carParts.add(model);
                            }
                        }
                    }
                    AutoCompleteItemPart();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Tag String", error.toString());
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));

        api.execute(100, Constant.URL + "getAllCarPart", map, true);
    }

    // add car part data autoCompplete data
    private void AutoCompleteItemPart() {

        ed_item_name.setText("");
        ArrayList<String> strings = new ArrayList<>();

//        if (preferences.getString(getString(R.string.pref_language), "en").equals("en")){

        for (int b = 0; b < carParts.size(); b++) {

            if (!strings.contains(carParts.get(b).getPart_name())) {
                strings.add(carParts.get(b).getPart_name());
            }
        }
        /*}else {
            for (int b = 0; b < carParts_ar.size(); b++) {

                if (!strings.contains(carParts_ar.get(b).getPart_name())) {
                    strings.add(carParts_ar.get(b).getPart_name());
                }
            }
        }*/
        Log.e("Tag Size", "" + strings.size());
        Log.e("Tag Array", "" + Arrays.toString(strings.toArray()));

        ed_item_name.setAdapter(new ArrayAdapter<>(Act_Add_Request.this, android.R.layout.simple_dropdown_item_1line, strings));
        ed_item_name.setThreshold(1);

        /*ed_item_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                item_name = carParts.get(position).getPart_name();
                item_name_ar = carParts_ar.get(position).getPart_name();
                Log.e("Tag Data", item_name);
            }
        });*/
    }

    // get workshop and listing all on based on selected country and service
    private void getGarageList(String service_id) {

        API api = new API(Act_Add_Request.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                Log.e("response workshops", response);
                try {
                    workshop_list_models = new ArrayList<>();
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("1")) {
                        JSONArray result = mainJsonObject.getJSONArray("result");
                        if (result.length() > 0) {
                            linError.setVisibility(View.GONE);
                            rel_main.setVisibility(View.VISIBLE);
                            JSONArray array = new JSONArray();
                            ArrayList<String> garage_id_list = new ArrayList<>();
                            for (int i = 0; i < result.length(); i++) {
//                                JSONArray array1 = result.getJSONArray(i);
//                                if (array1.length() > 0) {

//                                    for (int j = 0; j < array1.length(); j++) {
                                JSONObject jsonObject = result.getJSONObject(i);
                                String garage_name = check(jsonObject, "garage_name");
                                String garage_banner = jsonObject.getString("garage_banner");
                                String id = jsonObject.getString("garage_id");
                                String owner_name = check(jsonObject, "owner_name");
                                String contact = jsonObject.getString("contact_no");
                                String image = jsonObject.getString("owner_profile_img");
                                String address = check(jsonObject, "address");
                                String service_name = check(jsonObject, "service_name");
                                String email = check(jsonObject, "email");
                                String facebook_link = jsonObject.getString("facebook_link");
                                String city_name = check(jsonObject, "city_name");
                                String area_name = check(jsonObject, "area");

                                if (!garage_id.equals(id)) {
                                    if (!garage_id_list.contains(id)) {
                                        garage_id_list.add(id);
                                        workshop_list_models.add(new Workshop_List_Model(id, garage_name, owner_name, garage_banner, image, contact, email, address, service_name, facebook_link, city_name, area_name));
                                        JSONObject object = new JSONObject();
                                        object.put("receiver_garage_id", id);
                                        array.put(object);
                                    }
                                }
//                                    }
                                /*} else {
                                    Log.e("Tag Inside Array", "null");
                                }*/
                            }
                            /*if (array.length() > 0)
                                selected_garage_id = "" + array;*/

                            if (workshop_list_models != null && workshop_list_models.size() > 0) {
//                                recycler_view.removeAllViews();

                                if (spinner_select_area != null && area_data_models != null && area_data_models.size() > 0) {
                                    spinner_select_area.setSelection(0);
                                }

                                if (spinner_select_city != null && city_data_models != null && city_data_models.size() > 0) {
                                    spinner_select_city.setSelection(0);
                                }

                                GridLayoutManager layoutManager = new GridLayoutManager(Act_Add_Request.this,2);
                                recycler_view.setLayoutManager(layoutManager);
                                adapterWorkshopList = new AdapterWorkshopList(Act_Add_Request.this, workshop_list_models);
                                recycler_view.setAdapter(adapterWorkshopList);
                                recycler_view.setHasFixedSize(true);

                            } else {
                                rel_main.setVisibility(View.GONE);
                                linError.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.no_data_found));
                                btnRetry.setVisibility(View.GONE);
                            }
                        } else {
                            rel_main.setVisibility(View.GONE);
                            linError.setVisibility(View.VISIBLE);
                            txtError.setText(getString(R.string.no_data_found));
                            btnRetry.setVisibility(View.GONE);
                        }
                    } else {
                        rel_main.setVisibility(View.GONE);
                        linError.setVisibility(View.VISIBLE);
                        txtError.setText(getString(R.string.no_data_available));
                        btnRetry.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    rel_main.setVisibility(View.GONE);
                    linError.setVisibility(View.VISIBLE);
                    txtError.setText(getString(R.string.something_wrong_please));
                    btnRetry.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("response", " " + error);
                rel_main.setVisibility(View.GONE);
                linError.setVisibility(View.VISIBLE);
                txtError.setText(error);
                btnRetry.setVisibility(View.VISIBLE);
            }
        });
        Map<String, String> map = new HashMap<String, String>();
        map.put("service_json", service_id);
        map.put("manufacture_country_id", Mc_id);
//      map.put("service_json", "[{\"service_id\":\"4\"},{\"service_id\":\"10\"}]");
        map.put("garage_id", garage_id);
        map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
        Log.e("Tag Params", map.toString());
        api.execute(103, Constant.URL + "get_dealer_workshop_list", map, true);

    }

    // get city and area and set spinner
    private void getCityArea() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Tag Country City", response);
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.optString("status").equals("true")) {

//                                country_data_models = new ArrayList<>();
                                JSONObject object = obj.getJSONObject("result");

                                /*JSONArray dataArray = object.getJSONArray("country");
                                for (int i = 0; i < dataArray.length(); i++) {

                                    Country_Data_model country_data_model = new Country_Data_model();
                                    JSONObject jsonObject = dataArray.getJSONObject(i);
                                    country_data_model.setCon_id(jsonObject.getString("country_id"));
                                    country_data_model.setCon_code(jsonObject.getString("country_code"));
                                    country_data_model.setCon_name(jsonObject.getString("country_name"));
                                    country_data_model.setCon_currency(jsonObject.getString("currency"));
                                    country_data_model.setCon_language(jsonObject.getString("language"));
                                    country_data_model.setCon_ph_code(jsonObject.getString("phone_code"));
                                    country_data_models.add(country_data_model);

                                }*/

                                city_data_models = new ArrayList<>();
                                City_Data_Model city = new City_Data_Model();
                                city.setCity_id("0");
                                city.setCity_name(getString(R.string.all_city));
                                city.setCon_id("0");
                                city_data_models.add(city);
                                JSONArray array = object.optJSONArray("city");
                                if (array == null)
                                    array = new JSONArray();
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    if (object1.getString("country_id").equals(country_id)) {

                                        City_Data_Model city_data_model = new City_Data_Model();
                                        city_data_model.setCity_id(object1.getString("city_id"));
                                        city_data_model.setCity_name(check(object1, "city_name"));
                                        city_data_model.setCon_id(object1.getString("country_id"));
                                        city_data_models.add(city_data_model);
                                    }
                                }

                                area_data_models = new ArrayList<>();
                                Area_Data_Model area_data_model = new Area_Data_Model();
                                area_data_model.setArea_id("0");
                                area_data_model.setArea_name(getString(R.string.all_area));
                                area_data_model.setCity_id("0");
                                area_data_model.setCountry_id("0");
                                area_data_models.add(area_data_model);
                                if (object.has("area")) {
                                    JSONArray jsonArray = object.getJSONArray("area");
                                    if (jsonArray == null)
                                        jsonArray = new JSONArray();
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        if (jsonObject.getString("country_id").equals(country_id)) {

                                            Area_Data_Model model = new Area_Data_Model();
                                            model.setArea_id(jsonObject.getString("area_id"));
                                            model.setCity_id(jsonObject.getString("city_id"));
                                            model.setCountry_id(jsonObject.getString("country_id"));
                                            model.setArea_name(check(jsonObject, "area"));
                                            area_data_models.add(model);
                                        }
                                    }
                                }

                                ArrayAdapter<City_Data_Model> cityAdapter = new ArrayAdapter<City_Data_Model>(Act_Add_Request.this, android.R.layout.simple_spinner_dropdown_item, city_data_models);
                                cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner_select_city.setAdapter(cityAdapter);
                                spinner_select_city.setSelection(0);

                                spinner_select_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (position != 0) {

                                            ArrayList<Area_Data_Model> data_models = new ArrayList<>();
                                            data_models.add(area_data_model);
                                            for (int a = 0; a < area_data_models.size(); a++) {
                                                if (area_data_models.get(a).getCity_id().equals(city_data_models.get(position).getCity_id())) {
                                                    data_models.add(area_data_models.get(a));
                                                }
                                            }
                                            selected_city = city_data_models.get(position).getCity_name();
                                            if (data_models != null && data_models.size() > 0) {
                                                ArrayAdapter<Area_Data_Model> areaAdapter = new ArrayAdapter<Area_Data_Model>(Act_Add_Request.this, android.R.layout.simple_spinner_dropdown_item, data_models);
                                                areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                                spinner_select_area.setAdapter(areaAdapter);
                                                spinner_select_area.setSelection(0);
                                            } else {
                                                List<Workshop_List_Model> workshop_list = new ArrayList<>();
                                                for (int c = 0; c < workshop_list_models.size(); c++) {

                                                    if (selected_area.equals("")) {

                                                        if (workshop_list_models.get(c).getCity_name().equalsIgnoreCase(selected_city)
                                                        ) {
                                                            workshop_list.add(workshop_list_models.get(c));
                                                        }
                                                    } else {
                                                        if (workshop_list_models.get(c).getCity_name().equalsIgnoreCase(selected_city)
                                                                && selected_area.equals(workshop_list_models.get(c).getArea_name())) {
                                                            workshop_list.add(workshop_list_models.get(c));
                                                        }
                                                    }
                                                }
//                                            if (workshop_list_models.size() > 0){
                                                if (adapterWorkshopList != null)
                                                    adapterWorkshopList.UpdateList(workshop_list);
                                            }
//                                            }
                                        } else {

                                            ArrayAdapter<Area_Data_Model> areaAdapter = new ArrayAdapter<Area_Data_Model>(Act_Add_Request.this, android.R.layout.simple_spinner_dropdown_item, area_data_models);
                                            areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                            spinner_select_area.setAdapter(areaAdapter);
                                            spinner_select_area.setSelection(0);

                                            selected_city = "";
                                            selected_area = "";
                                            if (workshop_list_models != null && adapterWorkshopList != null)
                                                adapterWorkshopList.UpdateList(workshop_list_models);
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                /*ArrayAdapter<Area_Data_Model> areaAdapter = new ArrayAdapter<Area_Data_Model>(Act_Add_Request.this, android.R.layout.simple_spinner_dropdown_item, area_data_models);
                                areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner_select_area.setAdapter(areaAdapter);
                                spinner_select_area.setSelection(0);*/

                                spinner_select_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (!parent.getItemAtPosition(position).toString().equalsIgnoreCase(getString(R.string.all_area))) {

                                            selected_area = parent.getItemAtPosition(position).toString();
                                            List<Workshop_List_Model> workshop_list = new ArrayList<>();
                                            for (int c = 0; c < workshop_list_models.size(); c++) {

                                                if (selected_city.equals("")) {

                                                    if (workshop_list_models.get(c).getArea_name().equalsIgnoreCase(selected_area)) {
                                                        workshop_list.add(workshop_list_models.get(c));
                                                    }
                                                } else {
//                                                    Log.e("Tag Area", workshop_list_models.get(c).getArea_name()+"  "+selected_area);
//                                                    Log.e("Tag Area City", workshop_list_models.get(c).getCity_name()+"  "+selected_city);
                                                    if (workshop_list_models.get(c).getArea_name().equalsIgnoreCase(selected_area)
                                                            && selected_city.equals(workshop_list_models.get(c).getCity_name())) {
                                                        workshop_list.add(workshop_list_models.get(c));
                                                    }
                                                }
                                            }

//                                            if (workshop_list_models.size() > 0){
                                            if (adapterWorkshopList != null)
                                                adapterWorkshopList.UpdateList(workshop_list);
//                                            }
                                        } else {
                                            selected_area = "";
                                            List<Workshop_List_Model> workshop_list = new ArrayList<>();
                                            for (int c = 0; c < workshop_list_models.size(); c++) {

                                                if (selected_city.equals("")) {
                                                    workshop_list.add(workshop_list_models.get(c));
                                                } else if (selected_area.equals("")) {

                                                    if (workshop_list_models.get(c).getCity_name().equalsIgnoreCase(selected_city)) {
                                                        workshop_list.add(workshop_list_models.get(c));
                                                    }
                                                } else {
                                                    if (workshop_list_models.get(c).getCity_name().equalsIgnoreCase(selected_city)
                                                            && selected_area.equals(workshop_list_models.get(c).getArea_name())) {
                                                        workshop_list.add(workshop_list_models.get(c));
                                                    }
                                                }
                                            }
//                                            if (workshop_list_models.size() > 0){
                                            if (adapterWorkshopList != null)
                                                adapterWorkshopList.UpdateList(workshop_list);
                                            /*if (workshop_list_models != null && adapterWorkshopList != null)
                                                adapterWorkshopList.UpdateList(workshop_list_models);*/
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), AppUtils.serverError(getApplicationContext(), error), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(language, preferences.getString(getString(R.string.pref_language), "ar"));
                return map;
            }
        };

        // request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    // workshop detail dialog show
    private void DetailDialog(@NotNull Workshop_List_Model model) {

//        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Add_Request.this);
        Dialog dialog = new Dialog(Act_Add_Request.this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_workshop_detail, null, false);

        ImageView image = view.findViewById(R.id.owner_img);
        TextView txt_workshop_name = view.findViewById(R.id.txt_workshop_name);
        TextView txt_owner_name = view.findViewById(R.id.txt_owner_name);
        TextView txt_address = view.findViewById(R.id.txt_address);
        TextView txt_email = view.findViewById(R.id.txt_email);
        txt_email.setVisibility(View.GONE);
        TextView txt_contact = view.findViewById(R.id.txt_contact);
        TextView txt_facebook = view.findViewById(R.id.txt_facebook);
        txt_facebook.setVisibility(View.GONE);
        TextView txt_service = view.findViewById(R.id.txt_service);
        TextView close = view.findViewById(R.id.close);

        if (model.getOwner_image().startsWith("http://") || model.getOwner_image().startsWith("https://")) {
            Log.e("Tag image", model.getOwner_image());
            Glide.with(getApplicationContext()).asBitmap().load(model.getBanner_image()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(image) {
                @Override
                protected void setResource(Bitmap resource) {
                    image.setImageBitmap(resource);
                }
            });
        } else {
            String url = Constant.SERVICE_IMAGE_URL + "Garage_Banner_Img/";
            Glide.with(getApplicationContext()).asBitmap().load(url + model.getBanner_image()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(image) {
                @Override
                protected void setResource(Bitmap resource) {
                    image.setImageBitmap(resource);
                }
            });
            Log.e("Tag image", url + model.getBanner_image());
        }
        txt_workshop_name.setText(Html.fromHtml(model.getGarage_name()));
        txt_owner_name.setText(Html.fromHtml(getString(R.string.dealer_name) + model.getOwner_name()));
        txt_address.setText(Html.fromHtml(getString(R.string.adress) + model.getAddress()));
        txt_email.setText(Html.fromHtml(getString(R.string.emails) + "<u><font color='blue'>" + model.getEmail() + "</font></u>"));
        txt_email.setVisibility(View.GONE);
        txt_contact.setText(Html.fromHtml(getString(R.string.contatcts) + "<u><font color='blue'>" + model.getContact_no() + "</font></u>"));
        if (!model.getFacebook_link().equals("") && !model.getFacebook_link().equals("null")) {
            txt_facebook.setVisibility(View.VISIBLE);
            txt_facebook.setText(Html.fromHtml(getString(R.string.facebooks_title) + "<u><font color='blue'>" + model.getFacebook_link() + "</font></u>"));
        }
        txt_service.setText(Html.fromHtml(getString(R.string.services) + model.getService_name()));

        txt_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.getContact_no()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + model.getEmail()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        txt_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url;
                if (!model.getFacebook_link().startsWith("http://") && !model.getFacebook_link().startsWith("https://"))
                    url = "http://" + model.getFacebook_link();
                else
                    url = model.getFacebook_link();

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    // workshop listing adapte
    private class AdapterWorkshopList extends RecyclerView.Adapter<AdapterWorkshopList.MyViewHolder> {

        Activity context;
        List<Workshop_List_Model> workshop_list_models;

        AdapterWorkshopList(Activity context, List<Workshop_List_Model> workshop_list_models) {
            this.context = context;
            this.workshop_list_models = workshop_list_models;
        }

        public Workshop_List_Model getWorkshop_list_models(int position) {
            return workshop_list_models.get(position);
        }

        public void UpdateList(List<Workshop_List_Model> workshop_list_models) {
            this.workshop_list_models = new ArrayList<>();
            this.workshop_list_models = workshop_list_models;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_service_provider, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {

            Workshop_List_Model model = workshop_list_models.get(i);

            holder.bind(model);

            holder.txt_name.setText(Html.fromHtml(model.getGarage_name()));

            holder.txt_address.setText(Html.fromHtml(model.getCity_name()) + " - " + Html.fromHtml(model.getArea_name()));

            holder.card_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.getContact_no()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            holder.card_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DetailDialog(model);
                }
            });

            /*if (!views.contains(holder.card_main)) {
                views.add(holder.selected_view);
            }*/

            /*holder.card_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (View cardView : views) {
                        cardView.setVisibility(View.GONE);
                    }
                    selected_garage_id = model.getGarage_id();
                    holder.selected_view.setVisibility(View.VISIBLE);
                }
            });*/
        }

        @Override
        public int getItemCount() {
            return workshop_list_models.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            CardView card_main, card_call, card_more;
            ImageView img;
            TextView txt_name, txt_address;
            LinearLayout lin_bottom;
            View selected_view;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                card_main = itemView.findViewById(R.id.card_main);
                card_more = itemView.findViewById(R.id.card_more);
                card_call = itemView.findViewById(R.id.card_call);
                img = itemView.findViewById(R.id.img);
                txt_name = itemView.findViewById(R.id.txt_name);
                txt_address = itemView.findViewById(R.id.txt_address);
                lin_bottom = itemView.findViewById(R.id.lin_bottom);
                selected_view = itemView.findViewById(R.id.selected_view);
            }

            public void bind(Workshop_List_Model model){

                if (model.getBanner_image().startsWith("http://") && model.getBanner_image().startsWith("https://")) {

                    Log.e("Tag image", model.getBanner_image());
                    Glide.with(getApplicationContext()).asBitmap()
                            .load(model.getBanner_image())
                            .thumbnail(0.01f)
                            .placeholder(R.mipmap.ic_launcher)
                            .error(R.drawable.amr_icon)
                            .into(new BitmapImageViewTarget(img) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    img.setImageBitmap(resource);
                                }
                            });
                } else {
                    String url = Constant.SERVICE_IMAGE_URL + "Garage_Banner_Img/";
                    Glide.with(getApplicationContext()).asBitmap()
                            .load(url + model.getBanner_image())
                            .thumbnail(0.01f)
                            .placeholder(R.mipmap.ic_launcher)
                            .error(R.drawable.amr_icon)
                            .into(new BitmapImageViewTarget(img) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    img.setImageBitmap(resource);
                                }
                            });
                    Log.e("Tag image", url + model.getBanner_image());
                }

            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnRetry) {
            getGarageList(select_service_id);
        }
        // send request to display workshop list
        else if (view == card_send) {
            // get id of listed workshop id
            JSONArray array = new JSONArray();
            if (adapterWorkshopList != null) {
                for (int g = 0; g < adapterWorkshopList.getItemCount(); g++) {
                    //                if (!adapterWorkshopList.getWorkshop_list_models(g).getGarage_id().equals(garage_id)){
                    JSONObject object = new JSONObject();
                    try {
                        object.put("receiver_garage_id", adapterWorkshopList.getWorkshop_list_models(g).getGarage_id());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    array.put(object);
                    //                }
                }
            }

            if (array.length() > 0)
                selected_garage_id = "" + array;

            /*for (int i=0; i<array.length(); i++){
                JSONObject object = new JSONObject();
                String id = object.get(i);
            }*/

            // partShop as reg..
            if (relPartItem.getVisibility() == View.VISIBLE || relGarageItem.getVisibility() == View.GONE) {

                if (ed_car_spec.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_car_info), Toast.LENGTH_LONG).show();
                } else if (ed_item_name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_fill_item_name), Toast.LENGTH_LONG).show();
                } else if (ed_item_quantity.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_fill_item_qunatity), Toast.LENGTH_LONG).show();
                } else if (selected_garage_id.equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.select_any_dealer_to_send_request), Toast.LENGTH_LONG).show();
                } else if (ed_part_type.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_part_type), Toast.LENGTH_LONG).show();
                } /* else if (sales_id != null && (sales_id.equals("") || sales_id.equals("0"))) {
                Toast.makeText(getApplicationContext(), "sales person not entered", Toast.LENGTH_LONG).show();
            }*/ else {
//                Log.e("Tag garage", selected_garage_id);
                    SendRequest();
                }
            } else {

                if (ed_car_spec.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_car_info), Toast.LENGTH_LONG).show();
                } else if (ed_maintenance_name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_fill_item_name), Toast.LENGTH_LONG).show();
                } else if (ed_desc_type.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_fill_item_name), Toast.LENGTH_LONG).show();
                } else if (selected_garage_id.equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.select_any_dealer_to_send_request), Toast.LENGTH_LONG).show();
                } /*else if (ed_desc_type.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_part_type), Toast.LENGTH_LONG).show();
                } else if (sales_id != null && (sales_id.equals("") || sales_id.equals("0"))) {
                Toast.makeText(getApplicationContext(), "sales person not entered", Toast.LENGTH_LONG).show();
                }*/ else {
//                Log.e("Tag garage", selected_garage_id);
                    SendRequest();
                }

            }

        } else if (view == img_back) {
            onBackPressed();
        } else if (view == item_image) {
            ImagePicker.Companion.with(this)
                    .compress(512)
                    .maxResultSize(620, 620)
                    .crop(16f, 9f)
                    .start();
        } else if (view == img_delete) {
            bitmap_item_image = null;
            item_image.setImageBitmap(null);
            txt_item_image.setVisibility(View.VISIBLE);
            img_delete.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                item_image.setImageBitmap(bitmap);
                txt_item_image.setVisibility(View.GONE);
                img_delete.setVisibility(View.VISIBLE);
                bitmap_item_image = bitmap;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Tag Error", e.toString());
            }

        } else {
            Log.e("Tag Null", "" + requestCode);
        }
    }

    // send new request on listed workShop
    public void SendRequest() {
        garage_json = "";
        card_send.setEnabled(false);
        ProgressDialog dialog = new ProgressDialog(Act_Add_Request.this);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        // other workshop send same request
        if (!dealer_request_id.equals("") && !last_garage_id.equals("")) {
            try {
                JSONArray array = new JSONArray(last_garage_id);
                JSONArray array1 = new JSONArray(selected_garage_id);
                JSONArray jsonArray = new JSONArray();
                for (int n = 0; n < array1.length(); n++) {
                    JSONObject object = array1.getJSONObject(n);
                    boolean isSame = false;
                    for (int o = 0; o < array.length(); o++) {
                        JSONObject object1 = array.getJSONObject(o);
                        if (object.getString("receiver_garage_id").equals(object1.getString("receiver_garage_id"))) {
                            isSame = true;
                        }
                    }
                    JSONObject jsonobject = new JSONObject();
                    if (!isSame) {
                        jsonobject.put("receiver_garage_id", object.getString("receiver_garage_id"));
                        jsonArray.put(jsonobject);
                    }
                }
                if (jsonArray.length() > 0)
                    garage_json = jsonArray.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            garage_json = selected_garage_id;
        }

        // other workshop list is same to show dialog
        if (garage_json.equals("")) {
            AlReadySendDialog();
//            Toast.makeText(getApplicationContext(), "Already send request this workshops", Toast.LENGTH_LONG).show();
            dialog.dismiss();
            card_send.setEnabled(true);
            return;
        }

        VolleyMultipartRequest request = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "send_dealer_request", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                Log.e("Tag response", new String(response.data));
                card_send.setEnabled(true);
                try {
                    String res_pose = Html.fromHtml(new String(response.data)).toString();
                    res_pose = res_pose.substring(res_pose.indexOf("{"), res_pose.lastIndexOf("}") + 1);
                    Log.e("response", " " + res_pose);
                    JSONObject object = new JSONObject(new String(res_pose));

                    if (object.getString("status").equals("1")) {
                        last_garage_id = selected_garage_id;
                        JSONObject jsonObject = object.getJSONObject("result");
                        dealer_request_id = !jsonObject.getString("dealer_request_id").equals("null") ? jsonObject.getString("dealer_request_id") : "";
//                        Toast.makeText(getApplicationContext(), getString(R.string.request_send_successfully), Toast.LENGTH_LONG).show();
                        ConfirmAlertDialog(object.getString("message"));
                    } else {
                        dealer_request_id = "null";
                        AlertDialogMessage(object.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                card_send.setEnabled(true);
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag Error", error.toString());
                card_send.setEnabled(true);
                dialog.dismiss();
            }
        }) {
            @NotNull
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("sender_garage_id", garage_id);
                map.put("dealer_request_id", dealer_request_id);
                if (relPartItem.getVisibility() == View.VISIBLE) {
                    map.put("item_name", ed_item_name.getText().toString());
                    /*if (preferences.getString(getString(R.string.pref_language), "en").equals("en")){
                        map.put("item_name_ar", item_name_ar);
                    }else {
                        map.put("item_name_ar", ed_item_name.getText().toString());
                        map.put("item_name", item_name);
                    }*/
                    map.put("item_quantity", ed_item_quantity.getText().toString());
                    map.put("part_type", ed_part_type.getText().toString());
                } else {
                    map.put("maintenance_name", ed_maintenance_name.getText().toString());
                    map.put("description_type", ed_desc_type.getText().toString());
                }
//                if (service_id.equals("61")){
                map.put("request_area", spinner_select_area.getSelectedItem().toString());
                map.put("request_city", spinner_select_city.getSelectedItem().toString());
//                }
//                map.put("note", ed_car_spec.getText().toString());
                map.put("car_info", ed_car_spec.getText().toString() + " " + selected_year);
                /*if (preferences.getString(getString(R.string.pref_language), "en").equals("en")){
                    map.put("car_info_ar", car_info_ar);
                }else {
                    map.put("car_info", car_info);
                    map.put("car_info_ar", ed_car_spec.getText().toString());
                }*/
                map.put("price_request", isPrice);
                map.put("receiver_garage_id_json", garage_json);
                map.put(AppUtils.language, preferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag Params", map.toString());
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, garage_id);
                bundle.putString(FirebaseAnalytics.Param.GROUP_ID, garage_json);
                String date = new Date().toString();
                bundle.putString(FirebaseAnalytics.Param.START_DATE, date);
                analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
//                return new HashMap<>();
                return map;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> map = new HashMap<>();
                if (bitmap_item_image != null) {
                    map.put("item_image", new DataPart(System.currentTimeMillis() + ".png", getFileDataFromDrawable(bitmap_item_image)));
                }
                return map;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        request.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public byte[] getFileDataFromDrawable(@NotNull Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void AlertDialogMessage(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Add_Request.this);
        builder.setMessage(msg);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });

        if (dealer_request_id.equals("null")) {
            builder.setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
            builder.setNeutralButton(getString(R.string.contact_us), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(getApplicationContext(), Act_Contact_Us.class));
                }
            });
        } else {
            builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
        }

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        if (!isFinishing()) {
            dialog.show();
        }
    }

    private void ConfirmAlertDialog(String msg) {
        SweetAlertDialog dialog = new SweetAlertDialog(Act_Add_Request.this, SweetAlertDialog.NORMAL_TYPE);
        dialog.setTitle(getString(R.string.your_request_has_been_sent_successfully));
        dialog.setCanceledOnTouchOutside(false);
        if (!dealer_request_id.equals("")) {
            dialog.setContentText(getString(R.string.do_you_want_to_send_the_request_to_other_workshop));
            dialog.showCancelButton(true);
            dialog.setCancelText(getString(R.string.yess));
            dialog.setConfirmText(getString(R.string.nos));
            dialog.setCancelClickListener(SweetAlertDialog -> {
                back_msg = msg;
                dialog.dismissWithAnimation();
                ed_part_type.setEnabled(false);
                ed_car_spec.setEnabled(false);
                ed_item_name.setEnabled(false);
                ed_item_quantity.setEnabled(false);
                switch_price.setEnabled(false);
                item_image.setEnabled(false);
            });
        } else {
            dialog.setConfirmText(getString(R.string.ok));
        }
        dialog.setConfirmClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
            AlertDialogMessage(msg);
        });

        if (!this.isFinishing()) {
            dialog.show();
        }

    }

    private void AlReadySendDialog() {
        SweetAlertDialog dialog = new SweetAlertDialog(Act_Add_Request.this, SweetAlertDialog.NORMAL_TYPE);
        dialog.setTitle(getString(R.string.you_dont_want_to_send_request_to_other_workshop));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setCancelText(getString(R.string.nos));
        dialog.setConfirmText(getString(R.string.yess));
        dialog.setConfirmClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
        });
        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
            AlertDialogMessage(back_msg);
        });
        if (!isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        if (!ed_part_type.isEnabled()) {
            AlertDialogMessage(back_msg);
        } else {
            super.onBackPressed();
        }
    }

    private void backPressDialog() {
        SweetAlertDialog dialog = new SweetAlertDialog(Act_Add_Request.this, SweetAlertDialog.NORMAL_TYPE);
        dialog.setTitleText(getString(R.string.you_dont_want_to_send_request_to_other_workshop));
        dialog.setConfirmText(getString(R.string.yess));
        dialog.showCancelButton(true);
        dialog.setCancelText(getString(R.string.nos));
        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
        });
        dialog.setConfirmClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
            AlertDialogMessage(back_msg);
        });
        dialog.show();
    }
}
