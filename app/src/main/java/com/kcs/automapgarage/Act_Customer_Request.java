package com.kcs.automapgarage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automapgarage.Constant.Constant;
import com.kcs.automapgarage.Model.Garage_Facility;
import com.kcs.automapgarage.Model.Request_Data_Model;
import com.kcs.automapgarage.adapter.Adapter_Customer_Request;
import com.kcs.automapgarage.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automapgarage.utils.AppUtils.check;
import static com.kcs.automapgarage.utils.AppUtils.language;

// status new = 0, participate = 1, ignore = 2,

public class Act_Customer_Request extends AppCompatActivity implements View.OnClickListener {

    ImageView back;
    RecyclerView rv_customer_request;
    LinearLayout linError;
    TextView txtError;
    Button btnRetry;
    List<Request_Data_Model> requestDataModels = new ArrayList<>();
    Adapter_Customer_Request adapter_customer_request;
    SharedPreferences sharedpreferences;
    String garage_id = "";

    String garage_appointment_id = "", garage_appointment_detail_id = "";
    String user_id = "";
    String garage_ids = "";
    String name = "";
    String status = "";
    String city = "";
    String address = "";
    String pinCode = "";
    String garage_user_image = "";
    String contact_no = "";
    String state = "";
    String email = "";
    String latitude = "", longitude = "", date = "", rims_size = "", variant_name = "",
            fuel_type = "", model_name = "", model_year = "", brand_name = "", rims_brand = "";

    String car_problem = "", appointment_date = "", appointment_time = "", oil_brand = "", oil_sae = "",
            tire_brand = "", tire_size_no = "", battery_brand = "", create_at = "", timeout_status = "";

    ArrayList<String> appointment_image = new ArrayList<>();
    ArrayList<String> appointment_type = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer__request);

        back = findViewById(R.id.back);
        rv_customer_request = findViewById(R.id.rv_customer_request);
        linError = findViewById(R.id.linError);
        txtError = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);

        sharedpreferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        garage_id = sharedpreferences.getString("GARAGE_ID", "");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnRetry.setOnClickListener(this);
        getCustomerRequest();
        InitializeBroadcast();
        ClearNotification();
    }

    // clear preference message badge count
    public void ClearNotification(){
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.pref_badge_count), 0);
        editor.apply();
        int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                preferences.getInt(getString(R.string.pref_badge_count_make_request),0) +
                preferences.getInt(getString(R.string.pref_badge_count),0) +
                preferences.getInt(getString(R.string.pref_badge_count_dealer_request),0) +
                preferences.getInt(getString(R.string.pref_badge_count_admin_msg),0));
        if (tot == 0) {
            ShortcutBadger.removeCount(getApplicationContext());
        } else {
            ShortcutBadger.applyCount(getApplicationContext(), tot);
        }
    }

    // get all request of customer
    private void getCustomerRequest() {
        final ProgressDialog dialog = new ProgressDialog(Act_Customer_Request.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "garage_appointment_list",
                    new Response.Listener<String>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(String response) {
                            response = Html.fromHtml(response).toString();
                            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                            Log.e("Appointment List", " " + response);
                            Log.e("Appointment Url", Constant.URL + "garage_appointment_list");
                            dialog.dismiss();
                            requestDataModels = new ArrayList<>();
                            try {
                                Log.e("notification_list", " " + response);
                                JSONObject mainJsonObject = new JSONObject(response);
                                JSONArray result = mainJsonObject.getJSONArray("result");
                                if (result.length() > 0) {

                                    List<Request_Data_Model> s0 = new ArrayList<>();
                                    List<Request_Data_Model> s1 = new ArrayList<>();
                                    List<Request_Data_Model> s2 = new ArrayList<>();
                                    List<Request_Data_Model> s3 = new ArrayList<>();
                                    List<Request_Data_Model> s4 = new ArrayList<>();
                                    List<Request_Data_Model> s5 = new ArrayList<>();
                                    List<Request_Data_Model> s6 = new ArrayList<>();
                                    List<Request_Data_Model> s7 = new ArrayList<>();
                                    List<Request_Data_Model> s8 = new ArrayList<>();
                                    List<Request_Data_Model> s9 = new ArrayList<>();
                                    List<Request_Data_Model> s10 = new ArrayList<>();
                                    List<Request_Data_Model> s11 = new ArrayList<>();
                                    requestDataModels = new ArrayList<>();
                                    for (int i = 0; i < result.length(); i++) {

                                        JSONObject jsonObject = result.getJSONObject(i);
                                        garage_appointment_id = jsonObject.getString("garage_appointment_id");
                                        garage_appointment_detail_id = jsonObject.getString("garage_appointment_detail_id");
                                        user_id = jsonObject.getString("user_id");
                                        garage_ids = check(jsonObject,"garage_id");
                                        name = check(jsonObject,"name");
                                        status = jsonObject.getString("status");
                                        email = jsonObject.getString("email");
                                        address = check(jsonObject,"address");
                                        pinCode = jsonObject.getString("pincode");
                                        garage_user_image = jsonObject.getString("user_image");
                                        contact_no = jsonObject.getString("contact_no");
                                        latitude = jsonObject.getString("latitude");
                                        longitude = jsonObject.getString("longitude");
                                        date = jsonObject.getString("created_at");
                                        model_name = check(jsonObject,"model_name");
                                        rims_brand = check(jsonObject,"rims_brand");
                                        model_year = check(jsonObject,"model_year");
                                        rims_size = check(jsonObject,"rims_size");
                                        brand_name = check(jsonObject,"brand_name");
                                        fuel_type = check(jsonObject,"fule_type");
                                        car_problem = check(jsonObject,"car_problem");
                                        appointment_date = jsonObject.getString("appointment_date");
                                        appointment_time = jsonObject.getString("appointment_time");
                                        create_at = jsonObject.getString("created_at");
                                        appointment_type = new ArrayList<>();
                                        if (jsonObject.has("appointment_type")) {

                                            try {
                                                JSONArray jsonType = jsonObject.getJSONArray("appointment_type");
                                                for (int p = 0; p < jsonType.length(); p++) {
                                                    JSONObject object = jsonType.getJSONObject(p);
                                                    if (!object.getString("garage_appointment_type").trim().equals(""))
                                                        appointment_type.add(check(object,"garage_appointment_type"));
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        tire_brand = check(jsonObject,"tire_brand");
                                        oil_brand = check(jsonObject,"oil_brand");
                                        oil_sae = check(jsonObject,"oil_sae");
                                        battery_brand = check(jsonObject,"battery_brand");

                                        if (appointment_date.equals("null") || appointment_time.equals("")) {
                                            appointment_date = "00-00-0000";
                                            appointment_time = "00:00";
                                        }

                                        List<Garage_Facility> garage_facilities = new ArrayList<>();
                                        if (jsonObject.has("garage_facility")) {
                                            JSONArray jsonArray = jsonObject.getJSONArray("garage_facility");
                                            for (int j = 0; j < jsonArray.length(); j++) {
                                                JSONObject object = jsonArray.getJSONObject(j);
                                                String facility_id = object.getString("facility_id");
                                                String facility_name = check(object,"facility_name");
                                                String status = object.getString("status");
                                                String created_at = object.getString("created_at");
                                                garage_facilities.add(new Garage_Facility(facility_id, facility_name, status, created_at, null));
                                            }
                                        }

                                        appointment_image = new ArrayList<>();
                                        if (jsonObject.has("appointmentImage")) {
                                            JSONArray image = jsonObject.getJSONArray("appointmentImage");
                                            for (int j = 0; j < image.length(); j++) {
                                                JSONObject object = image.getJSONObject(j);
                                                appointment_image.add(object.getString("appointment_image"));
                                            }
                                        }
                                        String appointment_request_time = jsonObject.getString("appointment_request_time");
                                        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        Log.e("Tag Timezone Local", dateFormatGmt.format(new Date()));
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        Date appointmentDate = null;
                                        Date currentDate = null;
                                        try {
                                            appointmentDate = format.parse("" + create_at);
                                            currentDate = format.parse(dateFormatGmt.format(new Date()));
                                            Log.e("TAG TIME", "" + format.format(currentDate));
                                            Log.e("TAG TIME", "" + format.format(appointmentDate));

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            Log.e("TAG TIME", e.toString());
                                        }
                                        timeout_status = "0";
                                        long days = 0;
                                        Log.e("TAG TIME", "" + appointment_date + " " + appointment_time);
                                        if (currentDate.before(appointmentDate)) {
                                            long diff = appointmentDate.getTime() - currentDate.getTime();
                                            Log.e("Tag days", "" + diff);
                                        } else {
                                            long diff = currentDate.getTime() - appointmentDate.getTime();
                                            Log.e("days", "" + diff);
                                            if (diff > 0) {
                                                long second = diff / 1000;
                                                long minute = second / 60;
                                                long hour = minute / 60;
                                                days = (hour / 24) + 1;
                                                Log.e("Tag hour", "" + hour);
                                                Log.e("Tag day", "" + days);
                                                if (appointment_request_time.matches(".*minute*.") ||
                                                        appointment_request_time.contains("minute") ||
                                                        appointment_request_time.matches(".*minutes*.") ||
                                                        appointment_request_time.contains("minutes")){
//                                                          Integer.parseInt(mobile_request_time.replaceAll("\\D",""));
                                                    if (minute >= Integer.parseInt(appointment_request_time.replaceAll("\\D",""))) {
                                                        timeout_status = "1";
                                                    } else {
                                                        timeout_status = "0";
                                                    }

                                                } else {
                                                    if (hour >= Integer.parseInt(appointment_request_time.replaceAll("\\D",""))) {
                                                        timeout_status = "1";
                                                    } else {
                                                        timeout_status = "0";
                                                    }
                                                }
                                                Log.e("Tag times", appointment_request_time);
                                                Log.e("Tag times digit", ""+Integer.parseInt(appointment_request_time.replaceAll("\\D","")));
                                            }
                                        }

                                        Request_Data_Model m = new Request_Data_Model(garage_appointment_id, garage_appointment_detail_id, user_id,
                                                garage_ids, name, email, contact_no, state, city, address, pinCode, garage_user_image, latitude,
                                                longitude, status, date, model_name, brand_name, model_year, variant_name, fuel_type, car_problem,
                                                appointment_date, appointment_time, garage_facilities, appointment_image, oil_brand, tire_brand,
                                                battery_brand, tire_size_no, oil_sae, rims_brand, rims_size, appointment_type,timeout_status);

                                        Log.e("Tag Status", status);
                                        Log.e("Tag time Status", timeout_status);

                                        if (days > 1 && status.equalsIgnoreCase("0") || status.equalsIgnoreCase("2") || status.equalsIgnoreCase("4")) {
                                            Log.e("Tag Days", status);
                                        } else if (status.equals("2") || status.equals("4")) {
                                            Log.e("Tag", status);
                                        } else {
                                            requestDataModels.add(new Request_Data_Model(garage_appointment_id, garage_appointment_detail_id, user_id,
                                                    garage_ids, name, email, contact_no, state, city, address, pinCode, garage_user_image, latitude,
                                                    longitude, status, date, model_name, brand_name, model_year, variant_name, fuel_type, car_problem,
                                                    appointment_date, appointment_time, garage_facilities, appointment_image, oil_brand, tire_brand,
                                                    battery_brand, tire_size_no, oil_sae, rims_brand, rims_size, appointment_type,timeout_status));
                                        }

                                        // new and over time request not diaplay
                                        if (days > 1 && status.equals("0") || status.equals("2") || status.equals("4") || status.equals("7") || status.equals("9")) {

                                        }  else {

                                            // status wise position change each request

                                            if (status.equals("0") && timeout_status.equals("0")) {
                                                s0.add(m);
                                            } else if (status.equals("0") && timeout_status.equals("1")) {
                                                s5.add(m);
                                            } else if (status.equals("1")) {
                                                s1.add(m);
                                            } else if (status.equals("3")) {
                                                s2.add(m);
                                            } else if (status.equals("4")) {
                                                s4.add(m);
                                            } else if (status.equals("5")) {
                                                s5.add(m);
                                            } else if (status.equals("6")) {
                                                s6.add(m);
                                            } else if (status.equals("7")) {
                                                s7.add(m);
                                            } else if (status.equals("8")) {
                                                s8.add(m);
                                            } else if (status.equals("9")) {
                                                s9.add(m);
                                            } else if (status.equals("10")) {
                                                s10.add(m);
                                            } else if (status.equals("11")) {
                                                s11.add(m);
                                            } else if (status.equals("12")) {
                                                s3.add(m);
                                            } else {
                                                s3.add(m);
                                            }
                                        }
                                    }
                                    requestDataModels = new ArrayList<>();
                                    requestDataModels.addAll(s0);
                                    requestDataModels.addAll(s1);
                                    requestDataModels.addAll(s2);
                                    requestDataModels.addAll(s6);
                                    requestDataModels.addAll(s8);
                                    requestDataModels.addAll(s10);
                                    requestDataModels.addAll(s11);
                                    requestDataModels.addAll(s3);
                                    requestDataModels.addAll(s9);
                                    requestDataModels.addAll(s7);
                                    requestDataModels.addAll(s4);
                                    requestDataModels.addAll(s5);
                                    if (requestDataModels != null && requestDataModels.size() > 0) {
                                        adapter_customer_request = new Adapter_Customer_Request(Act_Customer_Request.this, requestDataModels);
                                        rv_customer_request.setLayoutManager(new GridLayoutManager(Act_Customer_Request.this, 1));
                                        rv_customer_request.setAdapter(adapter_customer_request);
                                        linError.setVisibility(View.GONE);
                                        rv_customer_request.setVisibility(View.VISIBLE);
                                        Log.e("TAG", "onResponse: " + requestDataModels.size());
                                        Log.e("TAG", "onResponse: " + adapter_customer_request.getItemCount());
                                    } else {
                                        linError.setVisibility(View.VISIBLE);
                                        txtError.setText(getString(R.string.no_data_found));
                                        btnRetry.setVisibility(View.GONE);
                                        rv_customer_request.setVisibility(View.GONE);
                                    }
                                    /*SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt(getString(R.string.pref_badge_count), 0);
                                    editor.apply();
                                    int tot = (preferences.getInt(getString(R.string.pref_badge_count_mobile_request), 0) +
                                              preferences.getInt(getString(R.string.pref_badge_count_make_request),0) +
                                              preferences.getInt(getString(R.string.pref_badge_count),0) +
                                              preferences.getInt(getString(R.string.pref_badge_count_dealer_request),0) +
                                              preferences.getInt(getString(R.string.pref_badge_count_admin_msg),0));
                                    if (tot == 0) {
                                        ShortcutBadger.removeCount(getApplicationContext());
                                    } else {
                                        ShortcutBadger.applyCount(getApplicationContext(), tot);
                                    }*/

                                } else {
                                    linError.setVisibility(View.VISIBLE);
                                    txtError.setText(getString(R.string.no_data_found));
                                    btnRetry.setVisibility(View.GONE);
                                    rv_customer_request.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("size == > Error", e.toString());
                                linError.setVisibility(View.VISIBLE);
                                txtError.setText(getString(R.string.something_wrong_please));
                                btnRetry.setVisibility(View.VISIBLE);
                                rv_customer_request.setVisibility(View.GONE);
                                dialog.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //displaying the error in toast if occurs
                            Log.e("size ==> Error", error.toString());
//                          Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            linError.setVisibility(View.VISIBLE);
                            txtError.setText(AppUtils.serverError(getApplicationContext(), error));
//                            txtError.setText(getString(R.string.something_wrong_please));
                            btnRetry.setVisibility(View.VISIBLE);
                            rv_customer_request.setVisibility(View.GONE);
                            dialog.dismiss();
//                            error_dialog();
                        }
                    }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("garage_id", garage_id);
                    params.put(language, sharedpreferences.getString(getString(R.string.pref_language), "ar"));
                    Log.e("Tag", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            linError.setVisibility(View.VISIBLE);
            txtError.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            rv_customer_request.setVisibility(View.GONE);
            dialog.dismiss();
        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Customer_Request.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // new request come refress api
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int appointment = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
            int mobile = intent.getIntExtra(getString(R.string.pref_badge_count_mobile_request), 0);
            int dealer = intent.getIntExtra(getString(R.string.pref_badge_count_dealer_request), 0);
            int make = intent.getIntExtra(getString(R.string.pref_badge_count_make_request), 0);
            int admin = intent.getIntExtra(getString(R.string.pref_badge_count_admin_msg), 0);

            if (appointment != 0 && !Act_Customer_Request.this.isFinishing()){
                getCustomerRequest();
                ClearNotification();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        getCustomerRequest();
        ClearNotification();
    }

    @Override
    public void onClick(View v) {
        if (v == btnRetry) {
            getCustomerRequest();
        }
    }
}
